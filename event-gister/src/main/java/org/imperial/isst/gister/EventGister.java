package org.imperial.isst.gister;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.BSONObject;
import org.carrot2.clustering.lingo.LingoClusteringAlgorithm;
import org.carrot2.clustering.stc.STCClusteringAlgorithm;
import org.carrot2.core.Cluster;
import org.carrot2.core.Controller;
import org.carrot2.core.ControllerFactory;
import org.carrot2.core.Document;
import org.carrot2.core.ProcessingResult;
import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.database.DBGistFields;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.objects.Entity;
import org.imperial.isst.objects.ProcessedTweet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

// Take in a symptom, date range, node and analyze the tweets

public class EventGister extends DefenderApplication {

	public static void main(String[] args) throws ParseException {

		EventGister app = new EventGister("/config.properties");
		Date start = DateHandler.sdf.parse("2014-04-20");

		Date end = DateHandler.sdf.parse("2014-04-21");

		try {
			app.doSetup();
			DBObject gist = app.produceGist(start, end, "vomit", "2", true);
			app.outputToDatabase(gist);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private DBCollection processedTweets;
	private DBCollection gists;
	private DBCollection news;
	private DBCollection symptoms;
	private final DBScanner scanner;

	private NodeAssigner nodeAssigner;

	public EventGister(String configFile) {
		super(configFile);
		scanner = new DBScanner();
		try {
			nodeAssigner = new NodeAssigner(connectToDB("node"));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addInfoForTweets(List<ProcessedTweet> tweets, DBObject gist, String symptom, String node) {
		JSONObject points = getPoints(tweets);
		// outputStringToFile(points.toString(), symptom+node+".json");
		gist.put("points", JSON.parse(points.toString()));
		// doTopicModelling(tweets);
		DBObject clusters = doClustering(tweets, symptom);
		gist.put(DBGistFields.CLUSTERS, clusters);
		DBObject tags = outputHashtags(tweets);
		gist.put(DBGistFields.HASH_TAGS, tags);
		DBObject stats = getBasicStats(tweets);
		gist.put("stats", stats);
		DBObject symptoms = getSymptoms(tweets);
		gist.put("symptoms", symptoms);
		DBObject keywords = getKeywords(tweets);
		gist.put("keywords", keywords);

		DBObject entities = outputEntities(tweets);
		gist.put("entities", entities);

	}

	private Map<Entity, HashMap<String, Integer>> createEntityMap() {
		Map<Entity, HashMap<String, Integer>> res = new HashMap<Entity, HashMap<String, Integer>>();
		for (Entity ent : Entity.values()) {
			res.put(ent, new HashMap<String, Integer>());
		}
		return res;
	}

	private DBObject doClustering(List<ProcessedTweet> tweets, String symptom) {

		/* Prepare Carrot2 documents */

		final ArrayList<Document> documents = new ArrayList<Document>();

		for (ProcessedTweet tweet : tweets) {
			Document doc = new Document(tweet.getProcessedText(), tweet.getProcessedText());
			doc.setField("tweetId", tweet.getTweetId());
			documents.add(doc);
		}

		return getClusters(documents, symptom);
	}

	private DBObject doGeoClustering(List<ProcessedTweet> tweets, String symptom, String node, Date start, Date end) {
		BasicDBList res = new BasicDBList();
		Map<Integer, List<ProcessedTweet>> geoClusters = scanner.doClustering(tweets);
		for (Integer i : geoClusters.keySet()) {
			DBObject cluster = new BasicDBObject();
			addInfoForTweets(geoClusters.get(i), cluster, symptom, node);
			res.add(cluster);

			// Now get the extra tweets from the area around the cluster and do them too...
			DBObject nearObj = new BasicDBObject();
			List<ProcessedTweet> nearbyTweets = fetchGeoTweets(start, end, (DBObject) geoClusters.get(i).get(0)
					.getOriginalTweet().get("geo"));
			addInfoForTweets(nearbyTweets, nearObj, symptom, node);
			cluster.put("nearbyTweets", nearObj);
		}

		return res;
	}

	public void doSetup() throws UnknownHostException {
		processedTweets = connectToDB("tweet");
		gists = connectToDB("gist");
		news = connectToDB("news");
		symptoms = connectToDB("symptom");
	}

	private List<ProcessedTweet> fetchGeoTweets(Date start, Date end, DBObject point) {
		List<ProcessedTweet> results = new ArrayList<ProcessedTweet>();
		DBCursor cursor = processedTweets.find(DBQueries.buildGeoQuery(start, end, point));
		while (cursor.hasNext()) {
			results.add(new ProcessedTweet(cursor.next()));
		}

		return results;
	}

	private List<ProcessedTweet> fetchHashTagTweets(Date start, Date end, BasicDBList tags, String node) {
		List<ProcessedTweet> results = new ArrayList<ProcessedTweet>();
		DBCursor cursor = processedTweets.find(DBQueries.buildTagQuery(start, end, tags, node));
		while (cursor.hasNext()) {
			results.add(new ProcessedTweet(cursor.next()));
		}

		return results;
	}

	private DBObject fetchRelevantNews(String symptom, String node, Date start, Date end, DBObject currentGist) {
		NewsLinker linker = new NewsLinker(news, symptoms, nodeAssigner);

		return linker.fetchRelevantNews(symptom, node, start, end, currentGist);
	}

	private List<ProcessedTweet> fetchRelevantTweets(Date start, Date end, String symptom, String node) {
		List<ProcessedTweet> results = new ArrayList<ProcessedTweet>();
		DBCursor cursor = processedTweets.find(DBQueries.buildSymptomQuery(start, end, symptom, node));
		while (cursor.hasNext()) {
			results.add(new ProcessedTweet(cursor.next()));
		}

		return results;
	}

	private DBObject getBasicStats(List<ProcessedTweet> tweets) {
		DBObject res = new BasicDBObject();
		logger.info("Number of tweets: " + tweets.size());
		res.put("tweets", tweets.size());
		Set<Long> distinctUsers = new HashSet<Long>();
		for (ProcessedTweet tweet : tweets) {
			distinctUsers.add(tweet.getUserId());
		}
		logger.info("Number of distinct users: " + distinctUsers.size());
		res.put("distinctUsers", distinctUsers.size());

		return res;
	}

	private BasicDBList getClusterObject(List<Cluster> clusters) {
		BasicDBList res = new BasicDBList();
		int count = clusters.size(); // Math.min(clusters.size(), 5);
		for (int i = 0; i < count; i++) {
			Cluster cl = clusters.get(i);
			DBObject clObj = new BasicDBObject();
			clObj.put("label", cl.getLabel());
			clObj.put("score", cl.getScore());
			BasicDBList tweetIds = new BasicDBList();
			for (Document doc : cl.getAllDocuments()) {
				tweetIds.add(doc.getField("tweetId"));
			}
			clObj.put("tweetIds", tweetIds);
			res.add(clObj);
		}
		return res;
	}

	private DBObject getClusters(List<Document> documents, String symptom) {

		DBObject clusters = new BasicDBObject();
		/* A controller to manage the processing pipeline. */
		final Controller controller = ControllerFactory.createSimple();

		/*
		 * Perform clustering by topic using the Lingo algorithm. Lingo can take advantage of the original query, so we
		 * provide it along with the documents.
		 */
		final ProcessingResult byTopicClusters = controller.process(documents, symptom, LingoClusteringAlgorithm.class);
		final List<Cluster> LingoClusters = byTopicClusters.getClusters();

		final ProcessingResult bySTCClusters = controller.process(documents, symptom, STCClusteringAlgorithm.class);
		final List<Cluster> STCClusters = bySTCClusters.getClusters();

		logger.info("Lingo Results");
		ConsoleFormatter.displayClusters(LingoClusters);
		for (Cluster cl : LingoClusters) {
			System.out.println(cl.getAttributes().toString());
			System.out.println((cl.getScore() + " " + cl.getAllDocuments().size() + " " + cl.getLabel()));
		}

		clusters.put(DBGistFields.LINGO, getClusterObject(LingoClusters));
		clusters.put(DBGistFields.STC, getClusterObject(STCClusters));
		logger.info("STC Results");
		ConsoleFormatter.displayClusters(STCClusters);
		for (Cluster cl : STCClusters) {

			System.out.println(cl.getAttributes().toString());
			System.out.println((cl.getScore() + " " + cl.getAllDocuments().size() + " " + cl.getLabel()));
		}
		return clusters;
	}

	private DBObject getKeywords(List<ProcessedTweet> tweets) {
		BasicDBList res = new BasicDBList();
		Map<String, Integer> counts = new HashMap<String, Integer>();
		for (ProcessedTweet tweet : tweets) {
			for (String keyword : tweet.getKeywords()) {
				incrementMap(counts, keyword);
			}
		}
		for (String key : counts.keySet()) {
			res.add(new BasicDBObject(key, counts.get(key)));
		}

		return res;
	}

	private JSONObject getPoints(List<ProcessedTweet> tweets) {
		JSONObject featureCollection = new JSONObject();
		try {
			featureCollection.put("type", "FeatureCollection");
			JSONArray featureList = new JSONArray();
			// iterate through your list
			for (ProcessedTweet tweet : tweets) {
				// {"geometry": {"type": "Point", "coordinates": [-94.149, 36.33]}
				JSONObject point = new JSONObject();
				point.put("type", "Point");
				// construct a JSONArray from a string; can also use an array or list
				JSONArray coord = new JSONArray("[" + tweet.getCoords().getLatitude() + ","
						+ tweet.getCoords().getLongitude() + "]");
				point.put("coordinates", coord);
				JSONObject feature = new JSONObject();
				feature.put("geometry", point);
				feature.put("type", "Feature");

				JSONObject props = new JSONObject();
				props.put("processedText", tweet.getProcessedText());
				props.put("created_date", tweet.getCreated_date());
				// geo-json doesn't like the tweet id for some reason
				props.put("tweetId", String.valueOf(tweet.getTweetId()));
				feature.put("properties", props);

				featureList.put(feature);
				featureCollection.put("features", featureList);
			}
		} catch (JSONException e) {
			logger.error("can't save json object: " + e.toString());
		}
		System.out.println("featureCollection=" + featureCollection.toString());
		// output the result
		return featureCollection;
	}

	private DBObject getSymptoms(List<ProcessedTweet> tweets) {
		BasicDBList res = new BasicDBList();
		Map<String, Integer> counts = new HashMap<String, Integer>();
		for (ProcessedTweet tweet : tweets) {
			for (String symptom : tweet.getSymptoms()) {
				incrementMap(counts, symptom);
			}
		}
		for (String key : counts.keySet()) {
			res.add(new BasicDBObject(key, counts.get(key)));
		}

		return res;
	}

	private void incrementMap(Map<String, Integer> map, String key) {
		if (map.containsKey(key)) {
			map.put(key, map.get(key) + 1);
		} else {
			map.put(key, 1);
		}
	}

	private DBObject outputEntities(List<ProcessedTweet> tweets) {
		Map<Entity, HashMap<String, Integer>> allEnts = createEntityMap();

		for (ProcessedTweet tweet : tweets) {
			for (Entity ent : Entity.values()) {
				HashMap<String, Integer> tweetTerms = tweet.getEntities().get(ent);
				HashMap<String, Integer> allEntTerms = allEnts.get(ent);
				for (String term : tweetTerms.keySet()) {
					if (!allEntTerms.containsKey(term)) {
						allEntTerms.put(term, tweetTerms.get(term));
					} else {
						Integer value = allEntTerms.get(term);
						allEntTerms.put(term, value + tweetTerms.get(term));
					}
				}
			}
		}
		return DBQueries.convertEntities(allEnts);
	}

	private DBObject outputHashtags(List<ProcessedTweet> tweets) {
		DBObject res = new BasicDBObject();
		Map<String, Set<Long>> tags = new HashMap<String, Set<Long>>();
		for (ProcessedTweet tweet : tweets) {
			for (String tag : tweet.getHashtags()) {
				if (!tags.containsKey(tag)) {
					Set<Long> ids = new HashSet<Long>();
					ids.add(tweet.getTweetId());
					tags.put(tag, ids);
				} else {
					Set<Long> ids = tags.get(tag);
					ids.add(tweet.getTweetId());
				}
			}
		}
		System.out.println(tags);
		BasicDBList allTags = new BasicDBList();
		allTags.addAll(tags.keySet());
		BasicDBList multiUserTags = new BasicDBList();
		for (String tag : tags.keySet()) {
			if (tags.get(tag).size() > 1) {
				multiUserTags.add(tag);
			}
		}
		res.put(DBGistFields.ALL_TAGS, allTags);
		res.put(DBGistFields.MULTI_TAGS, multiUserTags);
		return res;
	}

	public void outputToDatabase(DBObject currentGist) {
		gists.insert(currentGist);
	}

	/*
	 * private void outputTweetText(List<ProcessedTweet> tweets, String fileName) { BufferedWriter writer = null; try {
	 * //create a temporary file File output = new File(fileName);
	 * 
	 * // This will output the full path where the file will be written to...
	 * System.out.println(output.getCanonicalPath());
	 * 
	 * writer = new BufferedWriter(new FileWriter(output)); for (ProcessedTweet tweet: tweets) {
	 * writer.write(tweet.getProcessedText()); writer.newLine(); } } catch (Exception e) { e.printStackTrace(); }
	 * finally { try { // Close the writer regardless of what happens... writer.close(); } catch (Exception e) { } } }
	 */

	public DBObject produceGist(Date start, Date end, String symptom, String node, boolean fetchExtraTweets) {
		logger.info("Producing gist: " + symptom + " " + node);
		List<ProcessedTweet> tweets = fetchRelevantTweets(start, end, symptom, node);
		DBObject currentGist = new BasicDBObject();
		currentGist.put("symptom", symptom);
		currentGist.put("node", node);
		currentGist.put("nodeName", nodeAssigner.getNodeLocality(node));
		currentGist.put("start", start);
		currentGist.put("end", end);
		// outputTweetText(tweets, symptom+node+".txt");

		BasicDBList gistTweets = new BasicDBList();
		for (ProcessedTweet tweet : tweets) {
			DBObject originalTweet = tweet.getOriginalTweet();
			originalTweet.put("tweetId_str", String.valueOf(tweet.getTweetId()));
			gistTweets.add(originalTweet);
		}
		currentGist.put("tweets", gistTweets);

		addInfoForTweets(tweets, currentGist, symptom, node);

		if (fetchExtraTweets) {
			BasicDBList tags = (BasicDBList) ((DBObject) currentGist.get(DBGistFields.HASH_TAGS))
					.get(DBGistFields.MULTI_TAGS);
			List<ProcessedTweet> tagTweets = fetchHashTagTweets(start, end, tags, node);
			DBObject extraTagTweets = new BasicDBObject();
			addInfoForTweets(tagTweets, extraTagTweets, symptom, node);
			currentGist.put("extraTagTweets", extraTagTweets);

			DBObject geoClusters = doGeoClustering(tweets, symptom, node, start, end);
			currentGist.put("geoClusters", geoClusters);
		}

		// This will use some of the information we've already added to the gist
		DBObject newsArticles = fetchRelevantNews(symptom, node, start, end, currentGist);
		currentGist.put("newsArticles", newsArticles);

		currentGist.put("gist_time", new Date());

		final ArrayList<Document> documents = new ArrayList<Document>();

		for (Object article : (BasicDBList) newsArticles) {
			Document doc = new Document(((BSONObject) article).get("title").toString(), ((BSONObject) article).get(
					"article").toString());

			documents.add(doc);
		}

		DBObject clusters = getClusters(documents, symptom);
		currentGist.put("newsclusters", clusters);

		return currentGist;
	}

}