package org.imperial.isst.gister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.ml.clustering.DoublePoint;
import org.imperial.isst.objects.ProcessedTweet;

public class DBScanner {

	// Want to get > this number of tweets from distinct users to count as a cluster
	private final int USER_CUTOFF = 3;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<Integer, List<ProcessedTweet>> doClustering(List<ProcessedTweet> tweets) {
		System.out.println("GeoClustering now...");
		
		Map<Integer, List<ProcessedTweet>> resultClusters = new HashMap<Integer, List<ProcessedTweet>>();
		DBSCANClusterer dbscan = new DBSCANClusterer(.004, USER_CUTOFF);
		List<DoublePoint> tweetPoints = new ArrayList<DoublePoint>();
		for (ProcessedTweet tweet: tweets) {
			tweetPoints.add(getDoublePoint(tweet));
		}
	    List<Cluster<DoublePoint>> cluster = dbscan.cluster(tweetPoints);

	    //
	    
	    int clusterNo = 0;
	    for(Cluster<DoublePoint> c: cluster){
	    	for (ProcessedTweet tweet: tweets) {
	    		if (c.getPoints().contains(getDoublePoint(tweet))) {
	    			if (resultClusters.containsKey(clusterNo)) {
	    				resultClusters.get(clusterNo).add(tweet);
	    			} else {
	    				List<ProcessedTweet> clusterTweets = new ArrayList<ProcessedTweet>();
	    				clusterTweets.add(tweet);
	    				resultClusters.put(clusterNo, clusterTweets );
	    			}
	    		}
	    	}
	    	clusterNo++;
	        
	    }
	    
	    // Remove the clusters without enough unique users
	    for (Iterator<Map.Entry<Integer, List<ProcessedTweet>>> it = resultClusters.entrySet().iterator(); it.hasNext(); ) {
			Set<Long> distinctUsers = new HashSet<Long>();

	    	Map.Entry<Integer, List<ProcessedTweet>> entry = it.next();
			for (ProcessedTweet tweet: entry.getValue()) {
				distinctUsers.add(tweet.getUserId());
			}
	    	
	    	if(distinctUsers.size() <= USER_CUTOFF) {
	          it.remove();
	        }
	      }

	    
	    System.out.println(resultClusters);
	    
	    return resultClusters;
	}
	
	private DoublePoint getDoublePoint(ProcessedTweet tweet) {
		return new DoublePoint(new double[]{tweet.getCoords().getLatitude(),
				tweet.getCoords().getLongitude()} );
	}
	
	
}
