package org.imperial.isst.gister;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.BasicBSONObject;
import org.imperial.isst.database.DBGistFields;
import org.imperial.isst.database.DBNewsFields;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.location.Location;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.news.Matches;
import org.imperial.isst.news.Matches.FieldMatch;
import org.imperial.isst.news.Matches.LocationMatch;
import org.imperial.isst.news.Matches.TypeMatch;
import org.imperial.isst.objects.Entity;
import org.imperial.isst.utils.KeywordUtils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class NewsLinker {

	private final DBCollection newsColl;
	private final NodeAssigner nodeAssigner;
	private String nodeToFind;
	private final Map<String, List<String>> keywords;
	private final Map<String, DBObject> articlesFound = new HashMap<String, DBObject>();
	private DBObject dateQuery;
	private DBObject currentGist;
	private BasicDBList newsReturned;
	private static final int INTERVAL = 7;
	// This is in degrees
	private static final Double maxDistance = 0.5;

	public NewsLinker(DBCollection newsColl, DBCollection symptomColl, NodeAssigner nodeAssigner) {
		this.newsColl = newsColl;
		this.nodeAssigner = nodeAssigner;
		keywords = KeywordUtils.loadKeywords(symptomColl);
	}

	private void addArticleToList(DBObject article, DBObject locMatch, DBObject topicMatch, BasicDBList list) {
		String id = article.get(DBNewsFields.ID).toString();
		if (articlesFound.containsKey(id)) {
			((BasicDBList) (articlesFound.get(id).get(DBGistFields.TOPIC_MATCHES))).add(topicMatch);
		} else {

			DBObject condensedArticle = getCondensedArticle(article, locMatch, topicMatch);
			articlesFound.put(id, condensedArticle);
			list.add(condensedArticle);
		}
	}

	private void computeRelevance(BasicDBList newsReturned) {
		for (Object obj : newsReturned) {
			computeRelevance((DBObject) obj);
		}
	}

	private void computeRelevance(DBObject newsArticle) {
		double relevance = 0;
		int featuresSeen = 0;

		BasicDBList semanticMatches = (BasicDBList) newsArticle.get("semanticMatches");

		if (semanticMatches.size() > 1) {
			featuresSeen++;
		}
		for (Object obj : semanticMatches) {
			if (((BasicDBList) ((DBObject) obj).get("matches")).size() > 1) {
				featuresSeen++;
				break;
			}
		}
		for (Object obj : semanticMatches) {
			for (Object matchobj : ((BasicDBList) ((DBObject) obj).get("matches"))) {
				@SuppressWarnings("rawtypes")
				Map objMap = ((DBObject) matchobj).toMap();
				for (Object value : objMap.values()) {
					if ((Integer) value > 1) {
						featuresSeen++;
						break;
					}
				}
			}

		}

		// Use location to boost those we already want boosted somewhat
		if (featuresSeen > 0) {
			if (((BasicBSONObject) newsArticle.get(DBGistFields.LOCATION_MATCH)).get("matchType").equals(
					LocationMatch.LOCAL_MATCH.toString())) {
				featuresSeen++;
			}
		}

		relevance = 1.0 - (1.0 / (1.0 + featuresSeen));
		newsArticle.put("relevance", relevance);
	}

	private void extractNewsFromDBObject(DBObject article, List<String> keywords, Matches.TypeMatch type,
			Matches.FieldMatch fieldType) {
		String field = getFieldFromType(fieldType);
		Map<String, Integer> occs = KeywordUtils.getOccurrences(keywords, article.get(field).toString());
		DBObject match = getLocationMatch(article, nodeToFind, currentGist);
		DBObject topicMatch = getTopicMatch(type, fieldType, occs);
		if (match != null) {
			addArticleToList(article, match, topicMatch, newsReturned);
		}
	}

	private void fetchAllNews(List<String> keywords, Matches.TypeMatch type, boolean useLowerCase) {
		if (keywords.size() > 0) {
			fetchNews(keywords, type, FieldMatch.TITLE_MATCH, useLowerCase);
			fetchNews(keywords, type, FieldMatch.DESCRIPTION_MATCH, useLowerCase);
			fetchNews(keywords, type, FieldMatch.ARTICLE_MATCH, useLowerCase);
		}

	}

	private void fetchNews(List<String> keywords, Matches.TypeMatch type, Matches.FieldMatch fieldType,
			boolean useLowerCase) {
		String field = getFieldFromType(fieldType);
		DBCursor cursor = newsColl.find(DBQueries.buildNewsRegexQuery(dateQuery, field, keywords, useLowerCase));
		while (cursor.hasNext()) {
			DBObject article = cursor.next();
			extractNewsFromDBObject(article, keywords, type, fieldType);
		}
	}

	public DBObject fetchRelevantNews(String symptom, String node, Date start, Date end, DBObject gist) {
		newsReturned = new BasicDBList();
		dateQuery = DBQueries.getDateQueryObject(start, end, INTERVAL);
		nodeToFind = node;
		currentGist = gist;

		List<String> keys = keywords.get(symptom);
		fetchAllNews(keys, TypeMatch.SYMPTOM_MATCH, true);

		List<String> strippedTags = getHashTagsForSearch(gist);
		fetchAllNews(strippedTags, TypeMatch.HASHTAG_MATCH, true);

		// List<String> clusters = getClustersForSearch(gist);
		// fetchAllNews(clusters, TypeMatch.CLUSTER_MATCH, true);

		List<String> ents = getEntitiesForSearch(gist);
		fetchAllNews(ents, TypeMatch.ENTITY_MATCH, false);

		// fetchSolrNews(symptom, node, gist);

		computeRelevance(newsReturned);

		return newsReturned;
	}

	private DBObject getCondensedArticle(DBObject article, DBObject locMatch, DBObject topicMatch) {
		DBObject res = new BasicDBObject();
		res.put(DBNewsFields.ID, article.get(DBNewsFields.ID));
		res.put(DBNewsFields.TITLE, article.get(DBNewsFields.TITLE));
		res.put(DBNewsFields.ARTICLE, article.get(DBNewsFields.ARTICLE));
		res.put(DBNewsFields.LINK, article.get(DBNewsFields.LINK));
		res.put(DBNewsFields.DESCRIPTION, article.get(DBNewsFields.DESCRIPTION));
		res.put(DBNewsFields.FEED, article.get(DBNewsFields.FEED));
		res.put(DBNewsFields.PUB_DATE, article.get(DBNewsFields.PUB_DATE));
		res.put(DBGistFields.LOCATION_MATCH, locMatch);
		BasicDBList topicMatches = new BasicDBList();
		topicMatches.add(topicMatch);
		res.put(DBGistFields.TOPIC_MATCHES, topicMatches);
		return res;
	}

	private List<String> getEntitiesForSearch(DBObject gist) {
		Map<Entity, HashMap<String, Integer>> entities = DBQueries.convertEntities((DBObject) gist.get("entities"));
		List<String> res = new ArrayList<String>();
		HashMap<String, Integer> orgs = entities.get(Entity.ORGANIZATION);

		for (String key : orgs.keySet()) {
			if (orgs.get(key) > 1) {
				res.add(key);
			}
		}
		return res;
	}

	private String getFieldFromType(Matches.FieldMatch fieldType) {
		String res = "";
		switch (fieldType) {
		case TITLE_MATCH:
			res = DBNewsFields.TITLE;
			break;
		case DESCRIPTION_MATCH:
			res = DBNewsFields.DESCRIPTION;
			break;
		case ARTICLE_MATCH:
			res = DBNewsFields.ARTICLE;
			break;
		}
		return res;
	}

	private List<String> getHashTagsForSearch(DBObject gist) {
		List<String> strippedTags = new ArrayList<String>();
		BasicDBList tags = (BasicDBList) ((DBObject) gist.get(DBGistFields.HASH_TAGS)).get(DBGistFields.MULTI_TAGS);
		for (Object obj : tags) {
			strippedTags.add(obj.toString().replace("#", ""));
		}
		return strippedTags;
	}



	private DBObject getLocationMatch(DBObject article, String node, DBObject gist) {
		DBObject res = new BasicDBObject();
		List<Location> locs = getLocationsFromArticle(article);

		if (locs.isEmpty()) {
			return null;
		}

		System.out.println(locs);
		Collections.sort(locs, Collections.reverseOrder());
		System.out.println(locs);

		Location primaryLoc = getPrimaryLocation(locs);
		res.put("locationMatched", primaryLoc.getLocality());
		System.out.println("Primary location: " + primaryLoc.toString());

		String nodeCountry = nodeAssigner.getNodeCountry(node);

		if (isCountryType(primaryLoc.getLocalityType())) {
			if ((primaryLoc.getLocality().equals(nodeCountry))
					|| (primaryLoc.getCountry().equalsIgnoreCase("United Kingdom"))) {
				res.put("matchType", Matches.LocationMatch.COUNTRY_MATCH.toString());
				return res;
			} else {
				return null;
			}
		}

		Double distance = nodeAssigner.getDistanceFromNode(primaryLoc.getCoords(), node);
		System.out.println("Distance of point " + primaryLoc.getCoords().toString() + " from node: " + node + " is: "
				+ distance.toString());
		if (distance < maxDistance) {
			res.put("matchType", Matches.LocationMatch.LOCAL_MATCH.toString());
		} else {
			return null;
		}
		return res;
	}

	private List<Location> getLocationsFromArticle(DBObject article) {
		List<Location> res = new ArrayList<Location>();
		BasicDBList locs = (BasicDBList) article.get(DBNewsFields.LOCATIONS);
		for (Object loc : locs) {
			res.add(new Location((DBObject) loc));
		}
		return res;
	}

	// Take the second location if its the same confidence as the first but more specific.
	private Location getPrimaryLocation(List<Location> locs) {
		if (locs.size() == 1) {
			return locs.get(0);
		}
		Location loc1 = locs.get(0);
		Location loc2 = locs.get(1);

		if ((loc2.getConfidence() == loc1.getConfidence()) && (loc2.getCountry().equals("GB"))
				&& (isCountryType(loc1.getLocalityType()))) {
			return loc2;
		} else {
			return loc1;
		}
	}

	private DBObject getTopicMatch(Matches.TypeMatch type, Matches.FieldMatch fieldMatch, Map<String, Integer> occs) {
		DBObject res = new BasicDBObject();
		res.put("typeMatch", type.toString());
		res.put("fieldMatch", fieldMatch.toString());
		BasicDBList matches = new BasicDBList();
		for (String occ : occs.keySet()) {
			if (occs.get(occ) > 0) {
				matches.add(new BasicDBObject(occ, occs.get(occ)));
			}

		}
		res.put("matches", matches);
		return res;
	}

	private boolean isCountryType(String localityType) {
		boolean res = false;
		if (localityType.equals("COUNTRY") || (localityType.equals("ADMIN1")) || (localityType.equals("STATE"))) {
			res = true;
		}
		return res;
	}
}
