/**
 * AlarmProcessor.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.earlywarning;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.BSONObject;
import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.gister.EventGister;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class AlarmProcessor extends DefenderApplication {

	/**
	 * The required minimum count level for an alarm to be maintained for examination.
	 */
	private static final int MINIMUM_ACTIVITY_THRESHOLD = 0;

	/**
	 * One day in milliseconds.
	 */
	private static final long ONE_DAY_MILLIS = 86400 * 1000;

	/**
	 * The length of the window to check for alarm groups.
	 */
	private static final long GROUP_WINDOW = (5 * ONE_DAY_MILLIS);

	private static final int WINDOW = 5;

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			AlarmProcessor processor = new AlarmProcessor("/config.properties");
			Date end = null;
			if (args.length > 0) {
				end = DateHandler.sdf.parse(args[0]);
			} else {
				end = new Date();
			}
			processor.processAlarms(end);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The algorithm looks backwards from this date to see if there are any historical alarmsColl that may be grouped
	 * with those on the current day. The current day need not be today only the current day to check from.
	 */
	private Date currentDate;

	/**
	 * The source alarmsColl collection.
	 */
	private DBCollection alarmsColl;

	/**
	 * The filtered and grouped alarmsColl for a single symptom/node combination.
	 */
	private DBCollection processedSymptomColl;

	/**
	 * The event gisting component.
	 */
	private final EventGister gister;

	/**
	 * Initialises a new instance of the {@link AlarmProcessor} class.
	 * 
	 * @param currentDateText
	 * @throws ParseException
	 * @throws IOException
	 */
	public AlarmProcessor(String configFile) throws ParseException, IOException {
		super(configFile);
		connectToCollections();
		gister = new EventGister(configFile);
		gister.doSetup();
	}

	/**
	 * Add event <i>gists</i> to the processed alarms.
	 */
	private void addGists() {
		DBCursor processed = processedSymptomColl.find();

		for (DBObject obj : processed) {
			if ((Double) obj.get("alarm_confidence") > 0) {
				String node = obj.get("node_id").toString();
				String symptom = getSymptom(obj);
				BasicDBList dates = (BasicDBList) obj.get("dates");
				Date start = (Date) dates.get(0);
				Date end = (Date) dates.get(dates.size() - 1);
				DBObject gist = gister.produceGist(start, DateHandler.addDaysTo(end, 1), symptom, node, false);

				double weightedNum = getWeightedNumberOfArticles(((BasicDBList) gist.get("newsArticles")));

				DBObject updateDoc = new BasicDBObject();
				DBObject fields = new BasicDBObject();
				fields.put("gist", gist);
				fields.put("news_confidence", newsRank(weightedNum));
				updateDoc.put("$set", fields);

				BasicDBObject searchQuery = new BasicDBObject("id", obj.get("id"));
				processedSymptomColl.update(searchQuery, updateDoc);
			}
		}
	}

	/**
	 * Add a new processed alarm to the processed alarm collection.
	 * 
	 * @param previousProcessedAlarms
	 * @param currentSymptomAlarms
	 * @param filteredAlarm
	 * @param node
	 * @param symptom
	 * @param currentDate
	 */
	private void addNewProcessedAlarm(List<DBObject> previousProcessedAlarms,
			Map<String, Set<String>> currentSymptomAlarms, DBObject filteredAlarm, String node, String symptom,
			Date currentDate) {
		BasicDBList dates = new BasicDBList();
		dates.add(currentDate);
		BasicDBList sources = new BasicDBList();
		sources.add(filteredAlarm.get("id").toString());

		BasicDBList fullSources = new BasicDBList();
		fullSources.add(filteredAlarm);

		DBObject newProcAlarm = new BasicDBObject();
		newProcAlarm.put("node", node);
		newProcAlarm.put("node_id", filteredAlarm.get("node_id").toString());
		newProcAlarm.put("symptom", symptom);
		int nextId = findHighestId(node, symptom) + 1;
		newProcAlarm.put("id", node + ":" + symptom + ":" + nextId);
		newProcAlarm.put("number", nextId);
		newProcAlarm.put("last_date", currentDate);
		newProcAlarm.put("dates", dates);
		newProcAlarm.put("impact", getImpact(filteredAlarm));
		newProcAlarm.put("alarm_confidence", alarmRank(1));
		newProcAlarm.put("source_ids", sources);
		newProcAlarm.put("full_sources", fullSources);
		newProcAlarm.put("max_mad_from_med", getMaxDeviationFromMedian(filteredAlarm));
		if (currentSymptomAlarms.get(symptom).size() > 1) {
			newProcAlarm.put("other_node_confidence", 1);
		} else {
			newProcAlarm.put("other_node_confidence", 0);
		}

		processedSymptomColl.insert(newProcAlarm);

		previousProcessedAlarms.add(newProcAlarm);
		logger.debug("New processed alarm added: " + newProcAlarm);
	}

	/**
	 * Add the processed alarmsColl to the processed symptoms collection.
	 * 
	 * @param filteredAlarms
	 * @param previousProcessedAlarms
	 * @throws ParseException
	 */
	private void addProcessedAlarms(List<DBObject> filteredAlarms, List<DBObject> previousProcessedAlarms)
			throws ParseException {
		Map<String, Set<String>> currentSymptomAlarms = getCurrentAlarmSymptoms(filteredAlarms);

		for (DBObject filteredAlarm : filteredAlarms) {
			String node = filteredAlarm.get("node").toString();
			String symptom = getSymptom(filteredAlarm);
			Date currentDate = DateHandler.sdf.parse(filteredAlarm.get("date").toString());
			boolean foundGroup = false;
			for (DBObject processedAlarm : previousProcessedAlarms) {
				String processedNode = (processedAlarm).get("node").toString();
				String processedSymptom = getSymptom(processedAlarm);

				if (node.equalsIgnoreCase(processedNode) && symptom.equalsIgnoreCase(processedSymptom)) {
					foundGroup = updateExistingProcessedAlarm(currentSymptomAlarms, filteredAlarm, symptom,
							currentDate, processedAlarm);
				}
			}
			if (!foundGroup) {
				addNewProcessedAlarm(previousProcessedAlarms, currentSymptomAlarms, filteredAlarm, node, symptom,
						currentDate);
			}
		}

	}

	/**
	 * Between [0,1] with extra alarms giving decreasing amounts of confidence.
	 * 
	 * @param numberOfAlarms
	 * @return
	 */
	private double alarmRank(int numberOfAlarms) {
		return 1 - (1 / (double) numberOfAlarms);
	}

	/**
	 * Connect to the relevant alarmsColl collections.
	 * 
	 * @throws UnknownHostException
	 * 
	 */
	private void connectToCollections() throws UnknownHostException {
		alarmsColl = connectToDB("alarms");
		processedSymptomColl = connectToDB("processedsymptom");
		processedSymptomColl.remove(new BasicDBObject());
	}

	/**
	 * Filter alarms by minimum threshold.
	 * 
	 * @param alarms
	 * @return
	 */
	private List<DBObject> filterAlarms(List<DBObject> alarms) {
		List<DBObject> res = new ArrayList<DBObject>();
		for (DBObject alarm : alarms) {
			if ((Integer) alarm.get("count") > MINIMUM_ACTIVITY_THRESHOLD) {
				if (!alarm.get("node_id").toString().equals("0")) {
					res.add(alarm);
				}

			}
		}
		return res;
	}

	/**
	 * Find the current highest id in the db for this symptom/node pair.
	 * 
	 * @param node
	 * @param symptom
	 * @return the highest id found for this symptom/node pair. If no alarmsColl were found for this group then zero is
	 *         returned.
	 */
	private int findHighestId(String node, String symptom) {
		DBObject searchQuery = new BasicDBObject();
		int res;
		searchQuery.put("node", node);
		searchQuery.put("symptom", symptom);
		DBCursor highest = processedSymptomColl.find(searchQuery).sort(new BasicDBObject("number", -1)).limit(1);
		if (highest.hasNext()) {
			DBObject highestNumber = highest.next();
			res = (Integer) highestNumber.get("number");
		} else {
			res = 0;
		}

		return res;
	}

	private Map<String, Set<String>> getCurrentAlarmSymptoms(List<DBObject> filteredAlarms) {
		Map<String, Set<String>> currentSymptomAlarms = new HashMap<String, Set<String>>();
		for (DBObject filteredAlarm : filteredAlarms) {
			String symptom = getSymptom(filteredAlarm);
			String node = filteredAlarm.get("node_id").toString();
			if (currentSymptomAlarms.containsKey(symptom)) {
				currentSymptomAlarms.get(symptom).add(node);
			} else {
				Set<String> sympNodes = new HashSet<String>();
				sympNodes.add(node);
				currentSymptomAlarms.put(symptom, sympNodes);
			}
		}
		return currentSymptomAlarms;
	}

	/**
	 * Get the impact of this alarm.
	 * 
	 * @param filteredAlarm
	 * @return
	 */
	private DBObject getImpact(DBObject filteredAlarm) {
		// TODO: finish this..
		return null;
	}

	/**
	 * Get the impact of this alarm.
	 * 
	 * @param filteredAlarm
	 * @param processedAlarms
	 * @return
	 */
	private DBObject getImpact(DBObject filteredAlarm, DBObject processedAlarms) {

		return null;
	}

	private double getMaxDeviationFromMedian(DBObject filteredAlarm) {
		double res = 0.0;
		if (filteredAlarm.get("mad_from_med") != null) {
			res = (double) filteredAlarm.get("mad_from_med");
		}
		return res;
	}

	/**
	 * Get the source alarmsColl for the current date.
	 * 
	 * @return
	 */
	private List<DBObject> getNewAlarms() {
		DBObject matchClause = new BasicDBObject();
		matchClause.put("date", DateHandler.sdf.format(currentDate));

		DBObject match = new BasicDBObject("$match", matchClause);

		DBObject id = new BasicDBObject();
		id.put("id", "$id");
		id.put("node", "$node");
		id.put("node_id", "$node_id");
		id.put("symptom", "$symptom");
		id.put("method", "$detection_method");
		id.put("count", "$day_count");
		id.put("date", "$date");
		id.put("data_dis", "$data_dis");
		id.put("mad_from_med", "$mad_from_med");

		DBObject groupContents = new BasicDBObject();
		groupContents.put("_id", id);
		groupContents.put("date", new BasicDBObject("$first", "$date"));
		DBObject group = new BasicDBObject("$group", groupContents);

		AggregationOutput sourceAlarms = alarmsColl.aggregate(match, group);

		List<DBObject> res = new ArrayList<DBObject>();
		for (DBObject result : sourceAlarms.results()) {
			res.add((DBObject) result.get("_id"));
		}

		return res;
	}

	/**
	 * Get the processed alarms for all symptom/node pairs in the observation window.
	 * 
	 * @return
	 */
	private List<DBObject> getPreviousProcessedAlarms() {
		DBObject matchClause = new BasicDBObject();

		matchClause.put("last_date", new BasicDBObject("$gte", new Date(currentDate.getTime() - GROUP_WINDOW)));
		matchClause.put("last_date", new BasicDBObject("$lte", new Date(currentDate.getTime())));

		Calendar cal = DateHandler.getCalendar();
		cal.setTime(currentDate);
		cal.add(Calendar.DATE, WINDOW * -1);
		Date start = cal.getTime();

		DBObject dateQuery = DBQueries.getDateQueryObject(start, currentDate, 0);

		DBCursor processed = processedSymptomColl.find(new BasicDBObject("last_date", dateQuery));
		List<DBObject> res = new ArrayList<DBObject>();
		for (DBObject obj : processed) {
			res.add(obj);
		}
		return res;
	}

	/**
	 * Get the symptom associated with an alarm.
	 * 
	 * @param alarm
	 * @return
	 */
	private String getSymptom(DBObject alarm) {
		return alarm.get("symptom").toString().replace(".", " ");
	}

	/**
	 * Gets the weighted number of relevant articles.
	 * 
	 * @param articles
	 * @return
	 */
	private double getWeightedNumberOfArticles(BasicDBList articles) {
		double res = 0.0;
		for (Object obj : articles) {
			res += (Double) ((BSONObject) obj).get("relevance");
		}
		return res;
	}

	/**
	 * Note: Can have zero articles so add 1 to denominator.
	 * 
	 * @param weightedNumberOfArticles
	 * @return
	 */
	private double newsRank(double weightedNumberOfArticles) {
		return 1 - (1 / (weightedNumberOfArticles + 1));
	}

	/**
	 * Process the source alarms and new filtered alarms to the processed collection.
	 * 
	 * @throws ParseException
	 */
	private void processAlarms(Date end) throws ParseException {
		Date start = DateHandler.sdf.parse(configProperties.getProperty("startdate"));
		List<Date> dates = DateHandler.datesBetween(start, end);
		long startTime = System.currentTimeMillis();
		for (Date date : dates) {
			currentDate = date;
			List<DBObject> alarms = getNewAlarms();
			List<DBObject> filteredAlarms = filterAlarms(alarms);
			List<DBObject> previousProcessedAlarms = getPreviousProcessedAlarms();
			addProcessedAlarms(filteredAlarms, previousProcessedAlarms);

		}
		addGists();
		long duration = System.currentTimeMillis() - startTime;
		logger.info("Processing all alarms took: " + duration + " ms");
	}

	/**
	 * Update an existing processed alarm with the information form a new source alarm.
	 * 
	 * @param currentSymptomAlarms
	 * @param filteredAlarm
	 * @param symptom
	 * @param currentDate
	 * @param processedAlarm
	 * @return
	 */
	private boolean updateExistingProcessedAlarm(Map<String, Set<String>> currentSymptomAlarms, DBObject filteredAlarm,
			String symptom, Date currentDate, DBObject processedAlarm) {
		boolean foundGroup;
		BasicDBList dates = (BasicDBList) processedAlarm.get("dates");
		BasicDBList sourceIds = (BasicDBList) processedAlarm.get("source_ids");
		BasicDBList fullSourceIds = (BasicDBList) processedAlarm.get("full_sources");
		dates.add(currentDate);
		sourceIds.add(filteredAlarm.get("id").toString());
		fullSourceIds.add(filteredAlarm);
		/*
		 * Update the processed alarm with the new alarm details. 1. Set the last date to be the current date. 2. Add
		 * new date entry to grouped alarm. 3. Add the id of this alarm to the source ids array.
		 */
		DBObject updateDoc = new BasicDBObject();
		DBObject fields = new BasicDBObject();
		fields.put("last_date", currentDate);
		fields.put("dates", dates);
		fields.put("source_ids", sourceIds);
		fields.put("full_sources", fullSourceIds);
		fields.put("alarm_confidence", alarmRank(sourceIds.size()));
		fields.put("impact", getImpact(filteredAlarm, processedAlarm));
		fields.put("max_mad_from_med",
				Math.max((double) processedAlarm.get("max_mad_from_med"), getMaxDeviationFromMedian(filteredAlarm)));
		if (currentSymptomAlarms.get(symptom).size() > 1) {
			fields.put("other_node_confidence", 1);
		}
		updateDoc.put("$set", fields);

		BasicDBObject searchQuery = new BasicDBObject("id", processedAlarm.get("id"));
		processedSymptomColl.update(searchQuery, updateDoc);
		foundGroup = true;

		logger.debug("Updated processed alarm: " + processedAlarm);
		return foundGroup;
	}
}
