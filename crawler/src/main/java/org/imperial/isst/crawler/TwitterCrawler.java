package org.imperial.isst.crawler;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.utils.KeywordUtils;

import com.google.common.collect.ImmutableSet;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.twitter.hbc.core.endpoint.Location;
import com.twitter.hbc.core.endpoint.Location.Coordinate;


public class TwitterCrawler extends DefenderApplication
{
	private HoseBirdWrapper hbc;
	private DBCollection tweetCollection;
	public static boolean stopped = false;
	private static final Set<String> includedFields;
	private static final Set<String> includedUserFields;
	static {
		includedFields = ImmutableSet.of( "created_at", "id", "id_str", "text", "source", "user", "geo","coordinates", "place", "retweeted" );
		includedUserFields = ImmutableSet.of("id", "id_str", "name", "screen_name", "location", "description", "followers_count", "friends_count", "created_at", "favourites_count", "utc_offset", "time_zone", "geo_enabled", "statuses_count", "lang");
	}
	
	public TwitterCrawler (HoseBirdWrapper hbc, String configFile) {
		super(configFile);
		this.hbc = hbc;
		configureHoseBird();	
	}
	
	private void configureHoseBird() {

		String csvLocation = configProperties.getProperty("termlocation");
		List<String> termsToTrack = new ArrayList<String>();
		HashMap<String, List<String>> terms = KeywordUtils.loadKeywords(csvLocation); 
		for (List<String> list : terms.values()) {
			termsToTrack.addAll(list);
		}
		
		AuthDetails myDetails = new AuthDetails();
		myDetails.consumerKey = configProperties.getProperty("consumerkey");
		myDetails.consumerSecret = configProperties.getProperty("consumersecret");
		myDetails.secret = configProperties.getProperty("secret");
		myDetails.token = configProperties.getProperty("token");
		
		Location boundingBox = null;
	
		boolean useGeoLocation = Boolean.valueOf(configProperties.getProperty("useGeoLocation"));

		if (useGeoLocation) {
			// Dimensions of the bounding box if geo-located
			double swlatitude = Double.valueOf(configProperties.getProperty("swlatitude"));
			double swlongitude = Double.valueOf(configProperties.getProperty("swlongitude"));
			double nelatitude = Double.valueOf(configProperties.getProperty("nelatitude"));
			double nelongitude = Double.valueOf(configProperties.getProperty("nelongitude"));
			boundingBox = new Location(new Coordinate( swlongitude,swlatitude), 
					new Coordinate( nelongitude,nelatitude));
			
		}		
		
		hbc.configure(useGeoLocation, boundingBox, termsToTrack, myDetails);	
	}
	
	protected DBCollection getCollection(DB db, String collectionName) {
		String collectionSize = configProperties.getProperty("crawldatabasesize");
		DBCollection collection;
		if (!db.collectionExists(collectionName)) {
	        DBObject colloptions = BasicDBObjectBuilder.start().add("capped", true).add("size", Long.valueOf(collectionSize)).get();
			db.createCollection(collectionName, colloptions);
		}
		collection = super.getCollection(db, collectionName);
		collection.ensureIndex("id");
		return collection;
	}
	
	public void connectCollections() throws UnknownHostException {
		tweetCollection = connectToDB("crawl");		
	}
	
	public void collectTweets() {
		int msgRead = 0;
		long startTime = System.currentTimeMillis();
		logger.warn("Crawler started.");
		startScanningConsole();
		try {
			logger.info("Contacting Twitter.");
			hbc.connect();
			startTime = System.currentTimeMillis();
			while (true) {
				String msg = hbc.queue.take();
				DBObject dbObject = (DBObject) JSON.parse(msg);
				
				// ignore retweets
				if (dbObject.containsField("retweeted_status")) {
					continue;
				}
				
				// These are warnings that we're breaching firehose limits and missing tweets
				if (dbObject.containsField("limit")) {
					logger.debug("Limit message received: " + msg);
					continue;
				}
				
				try {
					filterFields(dbObject);
				} catch(Exception e) {
					logger.debug("Exception filtering tweet fields. Tweet was: " + msg);
				}

				try {
					tweetCollection.insert(dbObject);
				}
				catch (Exception e) {
					logger.debug("Exception writing to database. Tweet was: " + msg);
				}
				if (dbObject.containsField("text")) {
					logger.debug(dbObject.get("text").toString());

				}
				msgRead++;
				if (stopped) {
					logger.info("System stopped.");
					break;
				}

			}			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug("Interrupted exception! " + e.toString());
		} catch (Exception e) {
			logger.debug("Unexpected exception: " + e.toString());
			logger.debug("Attempting to print stack trace", e);
		}
		
		finally {
			logger.info("Stopping the hosebird client.");
			hbc.stop();
			logger.info("Read " + msgRead + " tweets in " + (System.currentTimeMillis()-startTime) + " ms.");
			logger.warn("Crawler stopped.");
		}

	}
	
	private void filterFields(DBObject dbObject) {
		Set<String> keys = new HashSet<String>(dbObject.keySet());
		
		for (String field : keys) {
			if (!includedFields.contains(field)) {
				dbObject.removeField(field);
			}
		}
		DBObject user = (DBObject)dbObject.get("user");
		Set<String> userkeys = new HashSet<String>(user.keySet());
		for (String field: userkeys) {
			if (!includedUserFields.contains(field)) {
				user.removeField(field);
			}
		}
	}
	
	private void startScanningConsole() {
		new Thread(
			    new Runnable() {
			      public void run() {
			        Scanner sc = new Scanner(System.in);
			        while (true) {
				        try {
					        sc.next("q");
					        stopped = true;
					        break;
				        } catch (InputMismatchException e) {
				            sc.next(); 
				        }	
			        }
			        sc.close();
			      }
			    }
			  ).start();	
	}

	public static void main(String[] args) {
		HoseBirdWrapper hbc = new HoseBirdWrapper();
		TwitterCrawler crawler;
		try {
			crawler = new TwitterCrawler(hbc, "/config.properties");
			crawler.connectCollections();

			final Thread mainThread = Thread.currentThread();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					stopped = true;
					try {
						mainThread.join();
					} catch (Exception e) {
						System.out.println("Exception!");
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			crawler.collectTweets();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
