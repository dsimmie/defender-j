package org.imperial.isst.crawler;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.Location;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

public class HoseBirdWrapper {

	private Client client;
	private StatusesFilterEndpoint endpoint;
	public BlockingQueue<String> queue;

	
	public HoseBirdWrapper () {
		queue = new LinkedBlockingQueue<String>(10000);
		endpoint = new StatusesFilterEndpoint();
	}
	
	private void setTerms(List<String> items) {
		endpoint.trackTerms(items);		
	}
	
	public void configure(boolean useGeoLocation, Location boundingBox, List<String> termsToTrack, AuthDetails details) {
		
		if (useGeoLocation) {
			endpoint.locations(Lists.newArrayList(boundingBox));
		} else {
			// add some track terms
			setTerms(termsToTrack);			
		}

		endpoint.languages(Lists.newArrayList("en"));

		Authentication auth = new OAuth1(details.consumerKey, details.consumerSecret, details.secret, details.token);

		// Create a new BasicClient. By default gzip is enabled.
		client = new ClientBuilder()
		.hosts(Constants.STREAM_HOST)
		.endpoint(endpoint)
		.authentication(auth)
		.processor(new StringDelimitedProcessor(queue))
		.build();
	}
	
	public void connect() {
		client.connect();
	}
	
	public void stop() {
		client.stop();
	}
	
	

}
