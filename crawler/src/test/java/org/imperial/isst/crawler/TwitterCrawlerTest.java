package org.imperial.isst.crawler;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.runners.MockitoJUnitRunner;

import com.twitter.hbc.core.endpoint.Location;


/**
 * Unit test for simple App.
 */
@RunWith(MockitoJUnitRunner.class)
public class TwitterCrawlerTest 

{
	
	@SuppressWarnings("unused")
	private TwitterCrawler crawler;
	private HoseBirdWrapper mockWrapper;
	@Captor
    private ArgumentCaptor<List<String>> captor;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */


    @BeforeClass
    public static void oneTimeSetUp() {
        // one-time initialization code   

    	System.out.println("@BeforeClass - oneTimeSetUp");
    }
 
    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
    	System.out.println("@AfterClass - oneTimeTearDown");
    }
 
    @Before
    public void setUp() {
        mockWrapper = mock(HoseBirdWrapper.class);
        System.out.println("@Before - setUp");
    }
 
    @After
    public void tearDown() {
        System.out.println("@After - tearDown");
    }
    
    /**
     * Rigorous Test :-)
     */
	@Test
	public void testConfigLoad()
    {

		crawler = new TwitterCrawler(mockWrapper, "/test.properties");

        ArgumentCaptor<AuthDetails> argument2 = ArgumentCaptor.forClass(AuthDetails.class);
        ArgumentCaptor<Location> argument3 = ArgumentCaptor.forClass(Location.class);
        ArgumentCaptor<Boolean> argument1 = ArgumentCaptor.forClass(Boolean.class);
        verify(mockWrapper).configure(argument1.capture(), argument3.capture(), captor.capture(), argument2.capture());
        assertEquals("cough", captor.getValue().get(0));
        assertEquals("coughing", captor.getValue().get(1));
        
        assertEquals("testingkey", argument2.getValue().consumerKey);
        assertEquals("testsecret", argument2.getValue().consumerSecret);       

        
    }

}
