package org.imperial.isst.graphpopulation;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.date.DateFormat;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.graph.GraphRelationship;
import org.imperial.isst.graph.Neo4JFactory;
import org.imperial.isst.graph.Neo4JUtils;
import org.imperial.isst.location.GeoCoords;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.utils.KeywordUtils;
import org.imperial.isst.utils.TableUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.index.lucene.ValueContext;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import com.google.common.collect.ArrayTable;
import com.google.common.collect.ImmutableSet;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * The {@link GraphPopulator} class queries the database and summarises it for
 * output to csv format as well as Neo4J
 * 
 * 
 */
public class GraphPopulator extends DefenderApplication implements Runnable {

	protected static final String BASE_OUTPUT_DIR = System
			.getProperty("user.home")
			+ "/node-data/"
			+ DateHandler.sdf.format(new Date()) + "/";

	private final String WEEKEND = "WEEKEND";
	private final String WEEKDAY = "WEEKDAY";
	private final String ALL_TRAVEL = "all";

	private DBCollection tweetCollection;
	private DBCollection symptomCollection;

	private RestGraphDatabase graphDb;
	private RestCypherQueryEngine engine;

	private Map<ImmutableSet<String>, Map<String, Integer>> edges;
	private Map<ImmutableSet<String>, Map<String, Integer>> averagedEdges;
	
	private Map<String, Map<String,Integer>> averagedCounts;
	private int numberOfWeekDays = 0;
	private int numberofWeekEndDays = 0;

	private Map<String, Integer> nodeCounts;
	private Map<String, Integer> nodeUserCounts;

	private NodeAssigner nodeAssigner;

	private HashMap<String, ArrayTable<Date, String, Double>> symptomCounts;
	private HashMap<String, ArrayTable<Date, String, Double>> symptomUserCounts;
	
	private Date startDate;
	private List<Date> nodeDateRange;

	private Set<String> symptomKeys;

	public GraphPopulator(String configFile) {
		super(configFile);

		graphDb = Neo4JFactory.createNeo4JService(configProperties
				.getProperty("graphdb"));
		System.setProperty("org.neo4j.rest.batch_transaction", "true");
		engine = new RestCypherQueryEngine(graphDb.getRestAPI());
		try {
			nodeAssigner = new NodeAssigner(connectToDB("node"));
			edges = nodeAssigner.getEdges();
			// Need to work out the average weekend and weekday strengths for
			// each edge
			averagedEdges = nodeAssigner.getEdges();
			for (ImmutableSet<String> key : averagedEdges.keySet()) {
				Map<String, Integer> props = averagedEdges.get(key);
				props.put(WEEKEND, 0);
				props.put(WEEKDAY, 0);
			}
			
			averagedCounts = new HashMap<String, Map<String,Integer>>();
			for (String node: nodeAssigner.getNodeNames()) {
				Map<String, Integer> counts = new HashMap<String, Integer>();
				counts.put(WEEKEND, 0);
				counts.put(WEEKDAY, 0);
				averagedCounts.put(node, counts);
			}
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			startDate = DateHandler.sdf.parse(configProperties
					.getProperty("start_date"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void connectCollections() throws UnknownHostException {
		tweetCollection = connectToDB("tweet");
		symptomCollection = connectToDB("symptom");
	}

	private void buildSymptomCountTable(List<Date> dateRange) {

		symptomKeys = KeywordUtils.loadKeywords(symptomCollection).keySet();
		symptomCounts = new HashMap<String, ArrayTable<Date, String, Double>>();
		symptomUserCounts = new HashMap<String, ArrayTable<Date, String, Double>>();
		for (String node : nodeAssigner.getNodeNames()) {
			ArrayTable<Date, String, Double> table = ArrayTable.create(
					dateRange, symptomKeys);
			TableUtils.zeroTable(table);
			symptomCounts.put(node, table);
			
			ArrayTable<Date, String, Double> userTable = ArrayTable.create(
					dateRange, symptomKeys);
			TableUtils.zeroTable(userTable);
			symptomUserCounts.put(node, userTable);
		}
	}
	
	@Override
	public void run() {

		// We fetch data for the whole range to output to csv
		// We only create the nodes which don't already exist
		Date yesterday = getPreviousDay(new Date());
		logger.info("Yesterday is: " + yesterday.toString());
		Date lastNodeDate = getLatestNodeDate();
		logger.info("Last node date is: " + lastNodeDate.toString());
		List<Date> wholeDateRange = getDateRangeForRun(startDate, yesterday);
		nodeDateRange = getDateRangeForRun(lastNodeDate, yesterday);
		logger.info(nodeDateRange.toString());
		try {
			connectCollections();
			buildSymptomCountTable(wholeDateRange);
			calculateAndPopulateCounts(wholeDateRange);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void calculateAndPopulateCounts(List<Date> dateRange) {

		logger.info("Starting graph population.");

		for (Date date : dateRange) {
			try {
				calculateGraph(date);
			} catch (Exception e) {
				logger.error(
						"Exception calculating graph for date: "
								+ date.toString(), e);
			}

		}

		updateReferenceGraph();

		logger.info("Reference graph created.");

		outputToCSV();

		logger.info("Graph population complete.");
	}

	public void updateReferenceGraph() {
		numberofWeekEndDays = Math.max(1, numberofWeekEndDays);
		numberOfWeekDays = Math.max(1, numberOfWeekDays);
		
		for (String node : nodeAssigner.getNodeNames()) {
			Double weekDayCount = (double) (averagedCounts.get(node).get(WEEKDAY)/ numberOfWeekDays);
			Double weekEndCount = (double) (averagedCounts.get(node).get(WEEKEND)/ numberofWeekEndDays);
					
			engine.query(
					String.format("MERGE (b:" + Neo4JUtils.REFERENCE
							+ " {nodeId:%s})", node),
					new HashMap<String, Object>());
			engine.query(String.format("MATCH (b:" + Neo4JUtils.REFERENCE
							+ " {nodeId:%s}) set b.weekDayCount = %.2f, b.weekEndCount = %.2f",  node, weekDayCount, weekEndCount),
							new HashMap<String, Object>());
		}
		for (ImmutableSet<String> key : averagedEdges.keySet()) {
			String first = key.asList().get(0);
			String second = key.asList().get(1);
			
			Double weekDayStrength = (double) (averagedEdges.get(key).get(
					WEEKDAY) / numberOfWeekDays);
			Double weekEndStrength = (double) (averagedEdges.get(key).get(
					WEEKEND) / numberofWeekEndDays);
			String cypherQuery = "MATCH (a:"
					+ Neo4JUtils.REFERENCE
					+ " {nodeId: %s}), (b:"
					+ Neo4JUtils.REFERENCE
					+ " {nodeId: %s}) MERGE (a)-[r:REF]->(b) SET r.weekDay=%.2f,r.weekEnd=%.2f";
			engine.query(String.format(cypherQuery, first, second,
					weekDayStrength, weekEndStrength),
					new HashMap<String, Object>());
		}
	}

	private void calculateGraph(Date date) {
		logger.info("Calculating for date: " + date.toString());
		DBObject query = DBQueries.buildQueryForDay(date, "created_date");
		populateSymptomCounts(query, date);
		populateEdgeWeights(query, date);
		logger.info("Edge weights populated.");

		if (nodeDateRange.contains(date)) {
			logger.info("Adding nodes to database. Date: " + date.toString());
			// run aggregation against tweet database to build up our counts
			getNodeCounts(query, date);

			HashMap<String, Node> nodeMap = new HashMap<String, Node>();

			// Build a list of nodes at the previous timestamp
			IndexManager index = graphDb.index();
			Index<Node> timestamps = index.forNodes("timestamps");
			Date previousDay = getPreviousDay(date);
			Map<String, Node> yesterdaysNodes = Neo4JUtils.getNodesForDate(
					timestamps, previousDay);
			logger.info("Iterating through nodes.");
			for (String node : nodeAssigner.getNodeNames()) {
				logger.debug("Creating node " + node);
				long startTime = System.currentTimeMillis();
				ArrayTable<Date, String, Double> nodeSymptomCounts = symptomCounts
						.get(node);

				Node newNode = createNode(node, date, nodeSymptomCounts);

				timestamps.add(newNode, "timestamp",
						new ValueContext(date.getTime()).indexNumeric());

				// Create a relationship back to the same place at the previous
				// timestamp
				if (yesterdaysNodes.containsKey(node)) {
					newNode.createRelationshipTo(yesterdaysNodes.get(node),
							GraphRelationship.RelTypes.TEMPORAL);
				}

				// Add the node to the hashmap for reference later
				nodeMap.put(node, newNode);

				logger.debug("Completed in "
						+ (System.currentTimeMillis() - startTime) + " ms.");
			}

			// Create spatial relationships between nodes
			logger.info("Nodes created. Adding relationships.");
			addSpatialRelationships(nodeMap);

		}

		logger.info("Date completed: " + query.toString());
	}

	// Must be called within the transaction context
	private void addSpatialRelationships(HashMap<String, Node> nodeMap) {
		long startTime = System.currentTimeMillis();
		try (Transaction tx = graphDb.beginTx()) {

			// Create spatial relationships between nodes
			for (ImmutableSet<String> key : edges.keySet()) {
				Node first = nodeMap.get(key.asList().get(0));
				Node second = nodeMap.get(key.asList().get(1));

				long firstId = first.getId();
				long secondId = second.getId();
				Map<String, Object> props = new HashMap<String, Object>();
				for (String label : edges.get(key).keySet()) {
					props.put(label.replace(" ", ""), edges.get(key).get(label));
				}
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("props1", props);
				engine.query(
						"start a=node(" + firstId + ") , b=node(" + secondId
								+ ") create a-[:" + GraphRelationship.RelTypes.SPATIAL.toString()
								+ " {props1} ]->b ", params);

			}
			tx.success();
		}

		logger.debug("Completed in " + (System.currentTimeMillis() - startTime)
				+ " ms.");
	}

	// Must be called within the transaction context
	private Node createNode(String node, Date date,
			ArrayTable<Date, String, Double> nodeSymptomCounts) {
		Node newNode = null;
		Map<String, Object> nodeProps = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();

		Integer count = nodeCounts.get(node);
		Integer userCount = nodeUserCounts.get(node);
		if (count == null) {
			count = 0;
			userCount = 0;
		}

		nodeProps.put("name", node);
		nodeProps.put("count", count);
		nodeProps.put("userCount", userCount);
		GeoCoords geo = nodeAssigner.getNodeCentroid(node);
		nodeProps.put("latitude", geo.getLatitude());
		nodeProps.put("longitude", geo.getLongitude());
		nodeProps.put("timestamp", date.getTime());
		nodeProps.put("date", DateHandler.sdf.format(date));
		nodeProps.put("locality", nodeAssigner.getNodeLocality(node));
		for (String key : nodeSymptomCounts.columnKeyList()) {
			nodeProps.put(KeywordUtils.removeWhiteSpace(key),
					nodeSymptomCounts.get(date, key));
		}
		params.put("nodeProps", nodeProps);
		String queryStr = "CREATE (a:" + Neo4JUtils.DATA
				+ "{nodeProps}) return a";
		QueryResult<Map<String, Object>> result = engine
				.query(queryStr, params);

		for (Map<String, Object> row : result) {
			newNode = (Node) row.get("a");
		}

		return newNode;
	}

	private Date getLatestNodeDate() {
		logger.info("Getting latest run date.");

		Date res = null;
		try {
			QueryResult<Map<String, Object>> result = engine.query(
					"MATCH (a:DATA) where a.hpaExtract is null and not ()-[:TEMPORAL]->(a) RETURN a LIMIT 1",
					new HashMap<String, Object>());


			for (Map<String, Object> row : result) {
				logger.info(row.toString());
				Node n = (Node) row.get("a");
				res = DateHandler.sdf.parse(n.getProperty("date").toString());
				logger.info(res.toString());

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (res == null) {
			res = startDate;
		}

		return res;
	}

	private void populateEdgeWeights(DBObject query, Date date) {
		AggregationOutput journeys = getJourneys(query);
		AggregationOutput symptomJourneys = getSymptomJourneys(query);
		resetEdgeWeights();
		setEdgeWeights(journeys);
		incrementAverageEdgeWeights(date);
		setEdgeWeights(symptomJourneys);
	}

	private void resetEdgeWeights() {
		// First reset the edges to 0
		for (ImmutableSet<String> key : edges.keySet()) {
			HashMap<String, Integer> edgeMap = new HashMap<String, Integer>();
			for (String symptom : symptomKeys) {
				edgeMap.put(symptom, 0);
			}
			edgeMap.put(ALL_TRAVEL, 0);
			edges.put(key, edgeMap);
		}
	}

	private void setEdgeWeights(AggregationOutput output) {

		for (DBObject res : output.results()) {
			BasicDBList locations = (BasicDBList) res.get("nodes");
			ArrayList<Integer> intlocations = new ArrayList<Integer>();
			for (int i = 0; i < locations.size(); i++) {
				intlocations.add((Integer) locations.get(i));
			}
			Collections.sort(intlocations);
			List<String> labelsForEdge = new ArrayList<String>();
			if (res.containsField("symptoms")) {
				BasicDBList symptoms = (BasicDBList) res.get("symptoms");
				for (int i = 0; i < symptoms.size(); i++) {
					if (!symptoms.get(i).equals("None")) {
						labelsForEdge.add(symptoms.get(i).toString());
					}
				}
			} else {
				labelsForEdge.add(ALL_TRAVEL);
			}
			for (int i = 0; i < intlocations.size() - 1; i++) {
				for (int j = i + 1; j < intlocations.size(); j++) {
					HashSet<String> pair = new HashSet<String>();
					pair.add(intlocations.get(i).toString());
					pair.add(intlocations.get(j).toString());
					ImmutableSet<String> key = ImmutableSet.copyOf(pair);
					if (edges.containsKey(key)) {
						for (String label : labelsForEdge) {
							int value = edges.get(key).get(label);
							edges.get(key).put(label, value + 1);
						}

					} else {
						logger.error("Edge not in dictionary! "
								+ key.toString());
					}
				}
			}
		}
	}

	private void incrementAverageEdgeWeights(Date date) {

		String edgeKey;
		if (DateHandler.isWeekend(date)) {
			edgeKey = WEEKEND;
			numberofWeekEndDays++;
		} else {
			edgeKey = WEEKDAY;
			numberOfWeekDays++;
		}

		for (ImmutableSet<String> key : edges.keySet()) {
			int weightForDay = edges.get(key).get(ALL_TRAVEL);

			int currentValue = averagedEdges.get(key).get(edgeKey);
			averagedEdges.get(key).put(edgeKey, weightForDay + currentValue);
		}
	}

	private void populateSymptomCounts(DBObject query, Date date) {
		AggregationOutput keyCounts = getKeyCounts(query);

		for (DBObject res : keyCounts.results()) {
			String keyword = ((DBObject) res.get("_id")).get("symptoms")
					.toString();
			String node = ((DBObject) res.get("_id")).get("node").toString();
			Double count = Double.valueOf(res.get("count").toString());
			if (symptomCounts.get(node).containsColumn(keyword)) {
				symptomCounts.get(node).put(date, keyword, count);
			}
			
			double userCount = ((BasicDBList)res.get("users")).size();
			if (symptomUserCounts.get(node).containsColumn(keyword)) {
				symptomUserCounts.get(node).put(date, keyword, userCount);
			}
			
		}
	}

	private AggregationOutput getKeyCounts(DBObject query) {
		// Build the count of keywords per node
		DBObject match = new BasicDBObject("$match", query);
		DBObject keyMatch = new BasicDBObject("$match", new BasicDBObject(
				"symptoms.0", new BasicDBObject("$exists", true)));
		DBObject healthMatch = new BasicDBObject("$match", new BasicDBObject(
				"health", "Health"));
		DBObject unwind = new BasicDBObject("$unwind", "$symptoms");
		DBObject keyGroups = new BasicDBObject("symptoms", "$symptoms");
		keyGroups.put("node", "$node");
		DBObject keyGroupFields = new BasicDBObject("_id", keyGroups);
		keyGroupFields.put("count", new BasicDBObject("$sum", 1));
		keyGroupFields.put("users", new BasicDBObject("$addToSet", "$userId"));
		DBObject keyGroup = new BasicDBObject("$group", keyGroupFields);

		AggregationOutput keyCounts = tweetCollection.aggregate(match,
				keyMatch, healthMatch, unwind, keyGroup);
		return keyCounts;
	}

	private void getNodeCounts(DBObject query, Date date) {
		// Build the count of tweets for each node for this date
		DBObject match = new BasicDBObject("$match", query);
		DBObject groupFields = new BasicDBObject("_id", "$node");
		groupFields.put("count", new BasicDBObject("$sum", 1));
		groupFields.put("users", new BasicDBObject("$addToSet", "$userId"));
		DBObject group = new BasicDBObject("$group", groupFields);
		AggregationOutput output = tweetCollection.aggregate(match, group);

		nodeCounts = new HashMap<String, Integer>();
		nodeUserCounts = new HashMap<String, Integer>();

		for (DBObject res : output.results()) {
			nodeCounts.put(res.get("_id").toString(),
					(Integer) res.get("count"));
			nodeUserCounts.put(res.get("_id").toString(),
					((BasicDBList) (res.get("users"))).size());
		}
		
		String countKey;;
		if (DateHandler.isWeekend(date)) {
			countKey = WEEKEND;
		} else {
			countKey = WEEKDAY;
		}

		for (String node: nodeUserCounts.keySet()) {
			int countForDay = nodeUserCounts.get(node);

			int currentValue = averagedCounts.get(node).get(countKey);
			averagedCounts.get(node).put(countKey, currentValue+countForDay);
		}
	}

	private AggregationOutput getJourneys(DBObject query) {
		// create our pipeline operations, first with the $match against the
		// date query
		DBObject match = new BasicDBObject("$match", query);

		// Now the $group operation
		DBObject groupFields = new BasicDBObject("_id", "$userId");
		groupFields.put("nodes", new BasicDBObject("$addToSet", "$node"));
		DBObject group = new BasicDBObject("$group", groupFields);

		// Clever trick that finds where we have more than one id
		DBObject secondMatch = new BasicDBObject("$match", new BasicDBObject(
				"nodes.1", new BasicDBObject("$exists", true)));
		// run aggregation
		AggregationOutput output = tweetCollection.aggregate(match, group,
				secondMatch);
		return output;
	}

	public AggregationOutput getSymptomJourneys(DBObject query) {
		// create our pipeline operations, first with the $match against the
		// date query
		DBObject match = new BasicDBObject("$match", query);
		DBObject projectFields = new BasicDBObject("userId", 1);
		projectFields.put("node", 1);
		projectFields.put("health", 1);

		List<Object> neOperands = new ArrayList<Object>();
		neOperands.add("$symptoms");
		neOperands.add(new DBObject[0]);
		DBObject ne = new BasicDBObject("$ne", neOperands);

		List<Object> eqOperands = new ArrayList<Object>();
		eqOperands.add("$health");
		eqOperands.add("Health");
		DBObject eq = new BasicDBObject("$eq", eqOperands);

		List<Object> andOperands = new ArrayList<Object>();
		andOperands.add(ne);
		andOperands.add(eq);

		DBObject and = new BasicDBObject("$and", andOperands);
		String[] dummySymptom = { "None" };

		List<Object> positiveCondOperands = new ArrayList<Object>();
		positiveCondOperands.add(and);
		positiveCondOperands.add("$symptoms");
		positiveCondOperands.add(dummySymptom);
		BasicDBObject finalPositiveCond = new BasicDBObject("$cond",
				positiveCondOperands);
		projectFields.put("symptoms", finalPositiveCond);

		DBObject project = new BasicDBObject("$project", projectFields);
		DBObject unwind = new BasicDBObject("$unwind", "$symptoms");
		// Now the $group operation
		DBObject groupFields = new BasicDBObject("_id", "$userId");
		groupFields.put("nodes", new BasicDBObject("$addToSet", "$node"));
		groupFields
				.put("symptoms", new BasicDBObject("$addToSet", "$symptoms"));
		DBObject group = new BasicDBObject("$group", groupFields);

		// Clever trick that finds where we have more than one id
		DBObject matches = new BasicDBObject("nodes.1", new BasicDBObject(
				"$exists", true));
		matches.put("symptoms.1", new BasicDBObject("$exists", true));
		DBObject secondMatch = new BasicDBObject("$match", matches);
		// run aggregation

		AggregationOutput output = tweetCollection.aggregate(match, project,
				unwind, group, secondMatch);
		return output;
	}

	private void outputToCSV() {
		for (Map.Entry<String, ArrayTable<Date, String, Double>> entry : symptomCounts
				.entrySet()) {
			String key = entry.getKey();
			String name = nodeAssigner.getNodeLocality(key);
			ArrayTable<Date, String, Double> value = entry.getValue();
			try {
				String outputDir = configProperties.getProperty("csvoutputdir")
						+ "/" + DateHandler.sdf.format(new Date()) + "/";
				TableUtils.outputToCSV(value, outputDir + "/" + key + name
						+ ".csv", DateFormat.PRETTY);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for (Map.Entry<String, ArrayTable<Date, String, Double>> entry : symptomUserCounts
				.entrySet()) {
			String key = entry.getKey();
			String name = nodeAssigner.getNodeLocality(key);
			ArrayTable<Date, String, Double> value = entry.getValue();
			try {
				String outputDir = configProperties.getProperty("csvoutputdir")
						+ "/" + DateHandler.sdf.format(new Date()) + "/";
				TableUtils.outputToCSV(value, outputDir + "/users/" + key  + name
						+ ".csv", DateFormat.PRETTY);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private List<Date> getDateRangeForRun(Date firstDate, Date yesterday) {

		Calendar cal = DateHandler.getCalendar();
		cal.setTime(firstDate);
		cal.add(Calendar.DATE, 1);
		Date firstRunNeeded = cal.getTime();

		List<Date> dateRange = DateHandler.datesBetween(firstRunNeeded,
				yesterday);
		logger.info(dateRange.toString());
		return dateRange;
	}

	private Date getPreviousDay(Date date) {
		Calendar cal = DateHandler.getCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, -1);
		Date yesterday = cal.getTime();
		return yesterday;
	}



	public static void main(String[] args) {
		GraphPopulator app = new GraphPopulator("/config.properties");
		app.run();
	}
}
