import java.net.UnknownHostException;
import java.text.ParseException;

import org.imperial.isst.database.DBQueries;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.graphpopulation.GraphPopulator;
import org.junit.Test;

import com.mongodb.AggregationOutput;
import com.mongodb.DBObject;


public class GraphPopulatorTest {

	@Test
	public void test() throws UnknownHostException, ParseException {
		DBObject query = DBQueries.buildQueryForDay(DateHandler.sdf.parse("2014-04-06"), "created_date");
		GraphPopulator app = new GraphPopulator("/config.properties");
		app.connectCollections();
		AggregationOutput out = app.getSymptomJourneys(query);
		System.out.println(out);
		int count = 0;
		for (DBObject res: out.results()) {
			count++;
			System.out.println(res.toString());
		}
		System.out.println("Count: " + count );
	}
	
//	@Test public void testCreateRefs() throws UnknownHostException {
//		GraphPopulator app = new GraphPopulator("/config.properties");
//		app.connectCollections();
//		app.updateReferenceGraph();
//	}

}
