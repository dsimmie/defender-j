/**
 * EntityFinder.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.entities;

import java.io.IOException;
import java.util.HashMap;

import org.imperial.isst.objects.Entity;

/**
 * 
 * @author Donal Simmie
 * 
 */
public interface NamedEntityRecogniser {

	/**
	 * Find named entities within a given text.
	 * 
	 * @param text
	 * @return
	 * @throws IOException
	 */
	HashMap<Entity, HashMap<String, Integer>> find(String text);
}
