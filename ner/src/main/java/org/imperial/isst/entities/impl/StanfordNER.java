/**
 * StanfordEntityFinder.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.entities.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.imperial.isst.entities.NamedEntityRecogniser;
import org.imperial.isst.objects.Entity;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;

/**
 * The {@link StanfordNER} class finds named entities within text using the Standford NER library.
 * 
 * @author Donal Simmie
 * 
 */
public class StanfordNER implements NamedEntityRecogniser {

	/**
	 * Jar internal location for the 7 class English NER model: Time, Location, Organization, Person, Money, Percent,
	 * Date
	 * 
	 * @see http://nlp.stanford.edu/software/CRF-NER.shtml#Models
	 */
	private static final String SEVEN_CLASS_MODEL = "/edu/stanford/nlp/models/ner/english.muc.7class.distsim.crf.ser.gz";


	/**
	 * The NER classifier.
	 */
	private final AbstractSequenceClassifier<CoreLabel> classifier;

	/**
	 * Initialises a new instance of the {@link StanfordNER} class.
	 */
	public StanfordNER() {
		classifier = CRFClassifier.getJarClassifier(SEVEN_CLASS_MODEL, null);
	}

	/**
	 * Find named entities within a given text.
	 * 
	 * @param text
	 * @return
	 * @see org.imperial.isst.entities.NamedEntityRecogniser#find(java.lang.String)
	 */
	public HashMap<Entity, HashMap<String, Integer>> find(String text){

	    HashMap<Entity, HashMap<String, Integer>> entities =
	            new HashMap<Entity, HashMap<String, Integer>>();

	    for (List<CoreLabel> lcl : classifier.classify(text)) {

	        Iterator<CoreLabel> iterator = lcl.iterator();

	        if (!iterator.hasNext())
	            continue;

	        CoreLabel cl = iterator.next();

	        while (iterator.hasNext()) {
	            String answer =
	                    cl.getString(CoreAnnotations.AnswerAnnotation.class);

	            if (answer.equals("O")) {
	                cl = iterator.next();
	                continue;
	            }

	            if (!entities.containsKey(Entity.valueOf(answer)))
	                entities.put(Entity.valueOf(answer), new HashMap<String, Integer>());

	            String value = cl.getString(CoreAnnotations.ValueAnnotation.class);

	            while (iterator.hasNext()) {
	                cl = iterator.next();
	                if (answer.equals(
	                        cl.getString(CoreAnnotations.AnswerAnnotation.class)))
	                    value = value + " " +
	                           cl.getString(CoreAnnotations.ValueAnnotation.class);
	                else {
	                    if (!entities.get(Entity.valueOf(answer)).containsKey(value))
	                        entities.get(Entity.valueOf(answer)).put(value, 0);

	                    entities.get(Entity.valueOf(answer)).put(value,
	                            entities.get(Entity.valueOf(answer)).get(value) + 1);

	                    break;
	                }
	            }

	            if (!iterator.hasNext())
	                break;
	        }
	    }

	    return entities;
	}
}
