/**
 * Entity.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.objects;

/**
 * 
 * @author Donal Simmie
 * 
 */
public enum Entity {
	TIME, LOCATION, ORGANIZATION, PERSON, MONEY, PERCENT, DATE
}
