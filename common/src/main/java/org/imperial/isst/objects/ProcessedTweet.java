package org.imperial.isst.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.BasicBSONObject;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.location.GeoCoords;

import com.mongodb.DBObject;

public class ProcessedTweet {

	/**
	 * The ID of the tweet.
	 */
	private final Long tweetId;

	/**
	 * The ID of the user.
	 */
	private final Long userId;
	
	/**
	 * The date of the tweet.
	 */
	private final Date created_date;
	
	private final String node;
	
	/**
	 * The locality assigned by Twitter.
	 */
	private final String locality;

	/**
	 * The geographical coordinates of the tweet.
	 */
	private final GeoCoords coords;
	
	/**
	 * The text of the tweet after text processing.
	 */
	private final String processedText;
	
	private final List<String> symptoms;
	private  List<String> keywords;
	private  List<String> hashtags;
	private  List<String> mentions;
	private  List<String> links;
	private Map<Entity, HashMap<String, Integer>> entities;
	private DBObject originalTweet;
	
	// Instantiate from a MongoDB Object
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ProcessedTweet(DBObject tweet) {
		originalTweet = tweet;
		userId = ((BasicBSONObject)tweet).getLong("userId");
		tweetId =  ((BasicBSONObject)tweet).getLong("tweetId");
		processedText = tweet.get("processedText").toString();
		created_date = (Date) tweet.get("created_date");
		node = tweet.get("node").toString();
		locality = tweet.get("locality").toString();
		symptoms = new ArrayList<String>((Collection)tweet.get("symptoms"));
		keywords = new ArrayList<String>((Collection)tweet.get("keywords"));
		hashtags = new ArrayList<String>((Collection)tweet.get("tags"));
		mentions = new ArrayList<String>((Collection)tweet.get("mentions"));
		links = new ArrayList<String>((Collection)tweet.get("links"));
		coords = DBQueries.createCoordsFromGeoJson((DBObject) tweet.get("geo"));
		entities = DBQueries.convertEntities((DBObject)tweet.get("entities"));
	}
	
	public Map<Entity, HashMap<String, Integer>> getEntities() {
		return entities;
	}

	public void setEntities(Map<Entity, HashMap<String, Integer>> entities) {
		this.entities = entities;
	}

	@Override
	public String toString() {
		return "ProcessedTweet [tweetId=" + tweetId + ", userId=" + userId
				+ ", created_date=" + created_date + ", node=" + node
				+ ", locality=" + locality + ", coords=" + coords
				+ ", processedText=" + processedText + ", symptoms=" + symptoms
				+ ", keywords=" + keywords + ", hashtags=" + hashtags
				+ ", mentions=" + mentions + ", links=" + links + ", entities="
				+ entities + ", originalTweet=" + originalTweet + "]";
	}
	
	public DBObject getOriginalTweet() {
		return originalTweet;
	}

	public String getLocality() {
		return locality;
	}

	public String getNode() {
		return node;
	}
	
	public List<String> getSymptoms() {
		return symptoms;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public List<String> getHashtags() {
		return hashtags;
	}

	public List<String> getMentions() {
		return mentions;
	}

	public List<String> getLinks() {
		return links;
	}

	public String getProcessedText() {
		return processedText;
	}

	public long getTweetId() {
		return tweetId;
	}

	public long getUserId() {
		return userId;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public GeoCoords getCoords() {
		return coords;
	}

	
}
