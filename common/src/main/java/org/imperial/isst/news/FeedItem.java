/**
 * FeedItem.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.news;

import static com.google.common.base.Preconditions.checkArgument;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;

import org.imperial.isst.news.FeedMetadata;

import com.google.common.hash.Hashing;

/**
 * A single feed entry item.
 * 
 * @author Donal Simmie
 * 
 */
public class FeedItem {

	/**
	 * The unique id of the article is a hash of the title and link. A guid field is provided in the RSS spec but they
	 * are different across different sources and we want a standard approach for generating the ID of a feed item.
	 */
	private final int id;

	/**
	 * The title of the article.
	 */
	private final String title;

	/**
	 * The URL of the original article.
	 */
	private final URL link;

	/**
	 * The description of the article. In practice mostly plain text but may have HTML formatting.
	 * <p>
	 * The RSS 2.0 spec does not mention whether it should be plain text so HTML formatting seems to be valid.
	 * 
	 * @see <a href="http://www.rssboard.org/rss-specification">RSS 2.0 Spec</a>
	 */
	private final String description;

	/**
	 * The publish date of the article.
	 */
	private final Date pubDate;

	/**
	 * The text of the article.
	 */
	private final String articleText;

	/**
	 * The data describing this feed item.
	 */
	private final FeedMetadata metadata;

	/**
	 * The feed from which this item was downloaded.
	 */
	private final Feed feed;

	/**
	 * Initialises a new instance of the {@link FeedItem} class.
	 * 
	 * @param title
	 * @param link
	 * @param description
	 * @param pubDate
	 */
	public FeedItem(String title, URL link, String description, Date pubDate, String articleText,
			FeedMetadata metadata, Feed feed) {
		checkArgument(title != null && title != "");
		checkArgument(link != null);
		checkArgument(description != null && description != "");
		checkArgument(pubDate != null);
		checkArgument(metadata != null);
		checkArgument(feed != null);

		id = Hashing.murmur3_32().hashString((title + link.toString()).toLowerCase(), Charset.defaultCharset()).asInt();
		this.title = title;
		this.link = link;
		this.description = description;
		this.pubDate = pubDate;
		this.articleText = articleText;
		this.metadata = metadata;
		this.feed = feed;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeedItem other = (FeedItem) obj;
		if (articleText == null) {
			if (other.articleText != null)
				return false;
		} else if (!articleText.equals(other.articleText))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		if (metadata == null) {
			if (other.metadata != null)
				return false;
		} else if (!metadata.equals(other.metadata))
			return false;
		if (pubDate == null) {
			if (other.pubDate != null)
				return false;
		} else if (!pubDate.equals(other.pubDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/**
	 * @return the articleText
	 */
	public String getArticleText() {
		return articleText;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the feed
	 */
	public Feed getFeed() {
		return feed;
	}

	/**
	 * @return the id
	 */
	public int getID() {
		return id;
	}

	/**
	 * @return the link
	 */
	public URL getLink() {
		return link;
	}

	/**
	 * @return the metadata
	 */
	public FeedMetadata getMetadata() {
		return metadata;
	}

	/**
	 * @return the pubDate
	 */
	public Date getPubDate() {
		return pubDate;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((articleText == null) ? 0 : articleText.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		result = prime * result + ((metadata == null) ? 0 : metadata.hashCode());
		result = prime * result + ((pubDate == null) ? 0 : pubDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FeedItem [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", link=");
		builder.append(link);
		builder.append(", description=");
		builder.append(description);
		builder.append(", pubDate=");
		builder.append(pubDate);
		builder.append(", articleText=");
		builder.append(articleText);
		builder.append(", metadata=");
		builder.append(metadata);
		builder.append("]");
		return builder.toString();
	}
}
