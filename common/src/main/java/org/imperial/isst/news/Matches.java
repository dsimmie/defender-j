package org.imperial.isst.news;

public class Matches {
	
	public enum LocationMatch {
		NO_MATCH, LOCAL_MATCH, COUNTRY_MATCH
	}
	
	public enum TypeMatch {
		SYMPTOM_MATCH, CLUSTER_MATCH, TOPIC_MATCH, HASHTAG_MATCH, ENTITY_MATCH
	}
	
	public enum FieldMatch {
		TITLE_MATCH, DESCRIPTION_MATCH, ARTICLE_MATCH
	}

}
