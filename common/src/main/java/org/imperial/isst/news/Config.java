/**
 * Config.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.news;

/**
 * Config constantsc class.
 * 
 * @author Donal Simmie
 * 
 */
public final class Config {

	/**
	 * The feeds file document comment character.
	 */
	public static final char COMMENT_CHAR = '#';

	/**
	 * The keywords (symptoms/diseases file).
	 */
	public static final String KEYWORDS_FILE = "keywords.file";

	/**
	 * The feeds details file name.
	 */
	public static final String FEEDS_FILE = "feeds.file";
	
	/**
	 * The knowledge base serialized object file name.
	 */
	public static final String KNOWLEDGE_FILE = "knowledge.file";

	/**
	 * The project repository absolute path.
	 */
	public static final String VCS_REPO = "vcs.repo.path";

	/**
	 * Config key for db host.
	 */
	public static final String HOST = "host";

	/**
	 * Coonfig key for db name.
	 */
	public static final String DB = "db";

	/**
	 * Config key for the extracted collection.
	 */
	public static final String EXTRACTED_COLL = "extracted";

	/**
	 * Config key for the feeds collection.
	 */
	public static final String FEEDS_COLL = "feeds";

	/**
	 * Config key for the parsed collection.
	 */
	public static final String PARSED_COLL = "parsed";

	/**
	 * Config key for the source collection.
	 */
	public static final String SOURCE_COLL = "source";
	
	/**
	 * Config key for the symptom collection.
	 */
	public static final String SYMPTOM_COLL = "symptom";

	/**
	 * Config key for the DB username.
	 */
	public static final String DB_USER = "newsuser";

	/**
	 * Config key for the DB user password
	 */
	public static final String DB_PASS = "newspassword";

}
