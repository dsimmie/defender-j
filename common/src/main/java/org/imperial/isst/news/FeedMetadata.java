/**
 * FeedMetadata.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.news;

import java.util.Date;

/**
 * Metadata about the feed item that has been downloaded.
 * <p>
 * This is useful for determining provenance information and being able to attribute data and replicate that data from
 * partial information.
 * 
 * @author Donal Simmie
 * 
 */
public class FeedMetadata {

	/**
	 * The class responsible for downloading news feeds.
	 */
	private final Class<?> downloader;

	/**
	 * A version string representing the version of the downloader code.
	 * <p>
	 * The actual string depends on your VCS. We are currently using Git, to expenad to others this should be an object
	 * rather than a simple string.
	 */
	private final String codeVersion;

	/**
	 * The time at which the feed was downloaded into the system.
	 */
	private final Date entryTime;

	/**
	 * Initialises a new instance of the {@link FeedMetadata} class.
	 * 
	 * @param downloader
	 * @param codeVersion
	 * @param entryTime
	 */
	public FeedMetadata(Class<?> downloader, String codeVersion, Date entryTime) {
		this.downloader = downloader;
		this.codeVersion = codeVersion;
		this.entryTime = entryTime;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeedMetadata other = (FeedMetadata) obj;
		if (codeVersion == null) {
			if (other.codeVersion != null)
				return false;
		} else if (!codeVersion.equals(other.codeVersion))
			return false;
		if (downloader == null) {
			if (other.downloader != null)
				return false;
		} else if (!downloader.equals(other.downloader))
			return false;
		if (entryTime == null) {
			if (other.entryTime != null)
				return false;
		} else if (!entryTime.equals(other.entryTime))
			return false;
		return true;
	}

	/**
	 * @return the codeVersion
	 */
	public String getCodeVersion() {
		return codeVersion;
	}

	/**
	 * @return the downloader
	 */
	public Class<?> getDownloader() {
		return downloader;
	}

	/**
	 * @return the entryTime
	 */
	public Date getEntryTime() {
		return entryTime;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeVersion == null) ? 0 : codeVersion.hashCode());
		result = prime * result + ((downloader == null) ? 0 : downloader.hashCode());
		result = prime * result + ((entryTime == null) ? 0 : entryTime.hashCode());
		return result;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FeedMetadata [downloader=");
		builder.append(downloader);
		builder.append(", codeVersion=");
		builder.append(codeVersion);
		builder.append(", entryTime=");
		builder.append(entryTime);
		builder.append("]");
		return builder.toString();
	}
}
