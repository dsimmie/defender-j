/**
 * FeedOptions.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.news;

import java.net.URL;
import java.util.Date;
import java.util.Set;

import org.imperial.isst.news.FeedItem;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class Feed {

	/**
	 * The plaintext name of the feed.
	 */
	private final String name;

	/**
	 * The URL of the feed.
	 */
	private final URL feedPath;

	/**
	 * Flag to indicate if the description contains HTML formatting. TODO: include reliable way of check for HTML
	 * elements to perform this check automatically.
	 */
	private transient final boolean descriptionContainsHTML;

	/**
	 * The feed content.
	 */
	private Set<FeedItem> items;

	/**
	 * The date/time the feed was accessed.
	 */
	private final Date accessDate;

	/**
	 * Initialises a new instance of the {@link Feed} class.
	 * 
	 * @param name
	 * @param feedPath
	 * @param descriptionContainsHTML
	 */
	public Feed(String name, URL feedPath, boolean descriptionContainsHTML) {
		this.name = name;
		this.feedPath = feedPath;
		this.descriptionContainsHTML = descriptionContainsHTML;
		accessDate = new Date();
	}

	/**
	 * @return the descriptionContainsHTML
	 */
	public boolean doesDescriptionContainsHTML() {
		return descriptionContainsHTML;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feed other = (Feed) obj;
		if (descriptionContainsHTML != other.descriptionContainsHTML)
			return false;
		if (feedPath == null) {
			if (other.feedPath != null)
				return false;
		} else if (!feedPath.equals(other.feedPath))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * @return the accessDate
	 */
	public Date getAccessDate() {
		return accessDate;
	}

	/**
	 * @return the feedPath
	 */
	public URL getFeedPath() {
		return feedPath;
	}

	/**
	 * @return the items
	 */
	public Set<FeedItem> getItems() {
		return items;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (descriptionContainsHTML ? 1231 : 1237);
		result = prime * result + ((feedPath == null) ? 0 : feedPath.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * @return the descriptionContainsHTML
	 */
	public boolean isDescriptionContainsHTML() {
		return descriptionContainsHTML;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(Set<FeedItem> items) {
		this.items = items;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Feed [name=");
		builder.append(name);
		builder.append(", feedPath=");
		builder.append(feedPath);
		builder.append(", descriptionContainsHTML=");
		builder.append(descriptionContainsHTML);
		builder.append("]");
		return builder.toString();
	}

}
