/**
 * XMLUtils.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * XML utilities class.
 * 
 * @author Donal Simmie
 * 
 */
public final class XMLUtils {

	/**
	 * The class logger.
	 */
	private static Logger logger = LoggerFactory.getLogger(XMLUtils.class);

	/**
	 * Perform a XPath query on an input XML string.
	 * 
	 * @param xmlData
	 *            the XML formatted data.
	 * @param expression
	 *            the XPath expression string.
	 * @return the string result of the XPath expression or null if nothing was found.
	 */
	public static String xPathQuery(String xmlData, String expression) {
		return (String) xPathQuery(xmlData, expression, XPathConstants.STRING);
	}

	/**
	 * Perform a XPath query on an input XML string.
	 * 
	 * @param xmlData
	 *            the XML formatted data.
	 * @param expression
	 *            the XPath expression string.
	 * @param returnType
	 *            the type of the resulting object, see {@link XPathConstants} for options.
	 * @return the result of the XPath expression or null if nothing was found.
	 */
	public static Object xPathQuery(String xmlData, String expression, QName returnType) {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Object result = null;
		try {
			builder = builderFactory.newDocumentBuilder();
			Document xmlDocument = builder.parse(new ByteArrayInputStream(xmlData.getBytes("utf-8")));
			XPath xPath = XPathFactory.newInstance().newXPath();
			result = xPath.compile(expression).evaluate(xmlDocument, returnType);
		} catch (ParserConfigurationException pe) {
			logger.error("XML parser configuration error", pe);
		} catch (SAXException se) {
			logger.error("Error parsing XML", se);
		} catch (XPathExpressionException xe) {
			logger.error("Invalid XPAth expression", xe);
		} catch (IOException ie) {
			logger.error("I/O error parsing XML", ie);
		}
		return result;
	}

	/**
	 * Exists to defeat instantiation.
	 * 
	 */
	private XMLUtils() {
		throw new AssertionError();
	}
}
