/**
 * LocalAuthorityHealthDataLoader.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.location;

import java.net.UnknownHostException;

import com.mongodb.DBObject;

/**
 * The Local Authority Boundary Data Health Data Loader.
 * 
 * @author Donal Simmie
 * 
 */
public class LocalAuthorityHealthDataLoader extends BaseHealthDataLoader {

	/**
	 * Initialises a new instance of the {@link LocalAuthorityHealthDataLoader} class.
	 * 
	 * @param features
	 * @throws UnknownHostException
	 */
	public LocalAuthorityHealthDataLoader(DBObject features, NodeAssigner assigner) throws UnknownHostException {
		super(features, assigner);
	}

	/**
	 * 
	 * @param feature
	 * @return
	 * @see org.imperial.isst.location.BaseHealthDataLoader#getFeatureId(com.mongodb.DBObject)
	 */
	@Override
	protected String getFeatureId(DBObject feature) {
		return ((DBObject) feature.get("properties")).get("CODE").toString();
	}
}
