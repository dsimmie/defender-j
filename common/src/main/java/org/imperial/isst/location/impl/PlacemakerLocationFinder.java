/**
 * PlacemakerLocationFinder.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.location.impl;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.xpath.XPathConstants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.imperial.isst.location.GeoCoords;
import org.imperial.isst.location.Location;
import org.imperial.isst.location.LocationFinder;
import org.imperial.isst.location.Source;
import org.imperial.isst.xml.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class PlacemakerLocationFinder implements LocationFinder {

	/**
	 * 
	 */
	private static final String PLACEMAKER_API = "http://wherein.yahooapis.com/v1/document";

	/**
	 * The HTTP client.
	 */
	private final HttpClient client;

	/**
	 * The class logger.
	 */
	private final Logger logger;

	/**
	 * The application identifier, for authenticating with the placemaker service.
	 */
	private final String appId;

	/**
	 * Initialises a new instance of the {@link PlacemakerLocationFinder} class.
	 * 
	 */
	public PlacemakerLocationFinder(String appId) {
		this.appId = appId;
		client = new DefaultHttpClient();
		logger = LoggerFactory.getLogger(getClass());
	}

	/**
	 * Convert the XML response into a collection of location objects.
	 * 
	 * @param xmlResponse
	 * @return the locations found in the XML text or an empty collection if none were found.
	 */
	private List<Location> convertResponse(String xmlResponse) {
		LinkedList<Location> locations = new LinkedList<Location>();
		Node localScopes = (Node) XMLUtils.xPathQuery(xmlResponse, "//localScopes", XPathConstants.NODE);

		if (localScopes != null) {
			GeoCoords coords = null;
			String locationText = null;
			String localityType = null;
			NodeList scopesList = null;
			scopesList = localScopes.getChildNodes();

			for (int i = 0; i < scopesList.getLength(); i++) {
				Node localScope = scopesList.item(i);
				NodeList localScopeChildren = localScope.getChildNodes();

				for (int j = 0; j < localScopeChildren.getLength(); j++) {
					Node localScopeChild = localScopeChildren.item(j);
					if (localScopeChild.getNodeName().equalsIgnoreCase("name")) {
						locationText = localScopeChild.getFirstChild().getNodeValue();
					} else if (localScopeChild.getNodeName().equalsIgnoreCase("type")) {
						localityType = localScopeChild.getFirstChild().getNodeValue().toUpperCase();
					} else if (localScopeChild.getNodeName().equalsIgnoreCase("centroid")) {
						double latitude = getChildNodeValue(localScopeChild, "latitude");
						double longitude = getChildNodeValue(localScopeChild, "longitude");
						coords = new GeoCoords(latitude, longitude);
					}
				}
				if (locationText != null && locationText != "") {
					String locality = getLocality(locationText);
					String country = getCountry(locationText);
					Location location = new Location(locality, localityType, country, coords, Source.PLACEMAKER, 1);
					locations.add(location);
					logger.debug("Discovered new location in text: " + location);
					locationText = null;
				}
			}
		}

		return locations;
	}

	/**
	 * Find locations in the input text.
	 * 
	 * @param text
	 * @return
	 * @throws IOException
	 * @see org.imperial.isst.location.LocationFinder#find(java.lang.String)
	 */
	@Override
	public List<Location> find(String text) throws IOException {
		return find(text, null);
	}

	/**
	 * Find locations in the input text and a possible title.
	 * 
	 * @param text
	 *            the input text with possible location entries.
	 * @param title
	 *            the title of the document (optional).
	 * @return the found locations or null if none were found.
	 * @throws UnsupportedEncodingException
	 * @see org.imperial.isst.location.LocationFinder#find(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Location> find(String text, String title) {
		try {

			HttpPost post = new HttpPost(PLACEMAKER_API);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("appId", appId));
			nameValuePairs.add(new BasicNameValuePair("documentType", "text/plain"));
			nameValuePairs.add(new BasicNameValuePair("documentContent", text));
			if (title != null)
				nameValuePairs.add(new BasicNameValuePair("documentTitle", title));

			HttpEntity urlEcondedEntity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
			post.setEntity(urlEcondedEntity);

			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			logger.debug("Placemaker service HTTP status code: " + response.getStatusLine().getStatusCode());
			String line = "";
			String xmlResponse = "";
			while ((line = rd.readLine()) != null) {
				xmlResponse += line + "\n";
			}

			List<Location> locations = convertResponse(xmlResponse);
			return locations;
		} catch (Exception e) {
			logger.info("Exception finding locations: ", e);
			return new ArrayList<Location>();
		}

	}

	/**
	 * Gets the double value of the first node found matching the provided name.
	 * 
	 * @param parent
	 * @param name
	 * @return the node value, parsed to a double.
	 */
	private double getChildNodeValue(Node parent, String name) {
		NodeList children = parent.getChildNodes();
		double value = 0.0;
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (child.getNodeName().equalsIgnoreCase(name)) {
				value = Double.parseDouble(child.getFirstChild().getNodeValue());
				break;
			}
		}
		return value;
	}

	/**
	 * Get the country string from the location text.
	 * <p>
	 * Examples include:
	 * <ul>
	 * <li>Central London, London, England, GB (Colloquial)</li>
	 * <li>Somerset, England, GB (County)</li>
	 * <li>SE5 9, London, England, GB (Zip)</li>
	 * <li>Devi, Palakkad, Kerala, IN (POI)</li>
	 * <li>England, GB (Country)</li>
	 * <li>Kenya (Country)</li>
	 * </ul>
	 * 
	 * @param locationText
	 * @return
	 */
	private String getCountry(String locationText) {
		checkArgument(locationText != null);

		String country = null;
		StringTokenizer tokenizer = new StringTokenizer(locationText, ",");
		while (tokenizer.hasMoreTokens()) {
			country = tokenizer.nextToken();
		}
		return country.substring(0, country.indexOf('(')).trim();
	}

	/**
	 * Get the locality (lowest level) string from the location text.
	 * <p>
	 * Examples include:
	 * <ul>
	 * <li>Central London, London, England, GB (Colloquial)</li>
	 * <li>Somerset, England, GB (County)</li>
	 * <li>SE5 9, London, England, GB (Zip)</li>
	 * <li>Devi, Palakkad, Kerala, IN (POI)</li>
	 * <li>England, GB (Country)</li>
	 * <li>Kenya (Country)</li>
	 * </ul>
	 * 
	 * @param locationText
	 * @return
	 */
	private String getLocality(String locationText) {
		checkArgument(locationText != null);
		/*
		 * Check if has only 1 entry and that is country in which case we do not set locality as country will already be
		 * set.
		 */
		String locality = null;
		StringTokenizer tokenizer = new StringTokenizer(locationText, ",");
		// if (!locationText.contains(COUNTRY)) {
		locality = tokenizer.nextToken();
		// }
		return locality;
	}
}
