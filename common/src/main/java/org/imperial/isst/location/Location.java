package org.imperial.isst.location;

import static com.google.common.base.Preconditions.checkArgument;

import org.imperial.isst.database.DBNewsFields;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * A location represented at the highest level by country and locality at the lowest level.
 */
public class Location implements Comparable<Location> {

	/**
	 * The country name.
	 */
	private final String country;

	/**
	 * The lowest available level of geographical description, e.g. street, town or city, etc.
	 */
	private final String locality;
	
	/**
	 * The type of locality as assigned by the source.
	 */
	private final String localityType;

	/**
	 * The geographical coordinates of this location.
	 */
	private final GeoCoords coords;

	/**
	 * The source of the Location.
	 */
	private final Source source;
	
	/**
	 * The confidence in the location value. Currently this is the number of mentions.
	 */
	private int confidence;


	public int getConfidence() {
		return confidence;
	}

	public void setConfidence(int confidence) {
		this.confidence = confidence;
	}

	/**
	 * Initialises a new instance of the {@link Location} class.
	 * 
	 * @param locality
	 *            the lowest available level of geographical description, e.g. street, town or city, etc.
	 * @param country
	 *            the country name.
	 */
	public Location(String locality, String localityType, String country, GeoCoords coords, Source source, int confidence) {
		checkArgument(country != null);
		checkArgument(coords != null);
		checkArgument(source != null);
		this.locality = locality;
		this.country = country;
		this.coords = coords;
		this.source = source;
		this.localityType = localityType;
		this.confidence = confidence;
	}
	
	public Location(DBObject locationObj) {
		this.locality = locationObj.get(DBNewsFields.LOCALITY).toString();
		this.country = locationObj.get(DBNewsFields.COUNTRY).toString();
		DBObject geo = (DBObject) locationObj.get(DBNewsFields.GEO);
		this.coords = new GeoCoords((Double)geo.get(DBNewsFields.LAT),(Double)geo.get(DBNewsFields.LONG)) ;
		this.source = Source.valueOf(locationObj.get(DBNewsFields.SOURCE).toString());
		this.localityType = locationObj.get(DBNewsFields.LOCALITY_TYPE).toString();
		this.confidence = (int) locationObj.get(DBNewsFields.CONFIDENCE);
	}
	
	public DBObject toDBObject() {
		DBObject locationDoc = new BasicDBObject();
		locationDoc.put(DBNewsFields.LOCALITY, getLocality());
		locationDoc.put(DBNewsFields.COUNTRY, getCountry());
		locationDoc.put(DBNewsFields.LOCALITY_TYPE, getLocalityType());
		locationDoc.put(DBNewsFields.CONFIDENCE, getConfidence());
		BasicDBObject coords = new BasicDBObject();
		coords.put(DBNewsFields.LAT, getCoords().getLatitude());
		coords.put(DBNewsFields.LONG, getCoords().getLongitude());
		locationDoc.put(DBNewsFields.GEO, coords);
		locationDoc.put(DBNewsFields.SOURCE, getSource().toString());
		return locationDoc;
	}

	public String getLocalityType() {
		return localityType;
	}
	
	@Override public int compareTo(Location otherLoc) { 
		// Compare the confidence
		Integer confidenceValue = Integer.valueOf(getConfidence()).compareTo(otherLoc.confidence);
		// Break ties by preferring TwoFishes
		if (confidenceValue == 0) {
			if ((getSource() == Source.PLACEMAKER) && (otherLoc.getSource() == Source.TWOFISHES)) {
				confidenceValue = -1;
			} else if ((getSource() == Source.TWOFISHES) && (otherLoc.getSource() == Source.PLACEMAKER)) {
				confidenceValue = 1;
			}
		}
		
		return confidenceValue;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (confidence != other.confidence)
			return false;
		if (coords == null) {
			if (other.coords != null)
				return false;
		} else if (!coords.equals(other.coords))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (locality == null) {
			if (other.locality != null)
				return false;
		} else if (!locality.equals(other.locality))
			return false;
		if (localityType == null) {
			if (other.localityType != null)
				return false;
		} else if (!localityType.equals(other.localityType))
			return false;
		if (source != other.source)
			return false;
		return true;
	}

	/**
	 * @return the coords
	 */
	public GeoCoords getCoords() {
		return coords;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @return the locality
	 */
	public String getLocality() {
		return locality;
	}

	/**
	 * @return the confidence
	 */
	public Source getSource() {
		return source;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + confidence;
		result = prime * result + ((coords == null) ? 0 : coords.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result
				+ ((locality == null) ? 0 : locality.hashCode());
		result = prime * result
				+ ((localityType == null) ? 0 : localityType.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Location [country=" + country + ", locality=" + locality
				+ ", localityType=" + localityType + ", coords=" + coords
				+ ", source=" + source + ", confidence=" + confidence + "]";
	}
}
