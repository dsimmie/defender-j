/**
 * NodeAssigner.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.location;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.imperial.isst.database.DBQueries;

import com.google.common.collect.ImmutableSet;
import com.mongodb.BasicDBList;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

/**
 * Assign the <i>tweets</i> to nodes in our spatial network.
 * 
 * @author Donal Simmie
 * 
 */
public class NodeAssigner {

	
	/**
	 * The identifier for the noise node.
	 */
	private static final String NOISE_NODE = "0";

	/**
	 * The polygon representation of the nodes.
	 */
	public Map<String, Polygon> nodePolygons;

	/**
	 * The centroids of the nodes.
	 */
	private Map<String, GeoCoords> nodeCentroids;
	
	private Map<String, String> nodeLocalities;
	
	private Map<String, String> nodeCountries;
	
	/**
	 * The network nodes collection.
	 */
	private DBCollection nodes;

	/**
	 * The geometry factory creation object.
	 */
	private final GeometryFactory geoFactory = new GeometryFactory();

	/**
	 * Initialises a new instance of the {@link NodeAssigner} class.
	 * 
	 * @throws UnknownHostException
	 * 
	 */
	public NodeAssigner(DBCollection nodes) throws UnknownHostException {
		this.nodes = nodes;
		loadNodes();
	}
	
	/**
	 * Load the network nodes from the datasource.
	 * 
	 * @throws UnknownHostException
	 */
	private void loadNodes() throws UnknownHostException {
		//nodes = DBWrapper.connectToCollection(new DBConnection("ib-dsimmie.ib.ic.ac.uk", "spatialnetwork", "nodes"));
		DBCursor cursor = nodes.find();
		nodePolygons = new HashMap<String, Polygon>();
		nodeCentroids = new HashMap<String, GeoCoords>();
		nodeLocalities = new HashMap<String, String>();
		nodeCountries = new HashMap<String, String>();
		
		while (cursor.hasNext()) {
			DBObject node = cursor.next();
			BasicDBList features = (BasicDBList) node.get("features");
			DBObject firstFeature = (DBObject) features.get(0);

			Polygon nodePolygon = createPolygonFromFeature(firstFeature);
			String id = node.get("id").toString();
			nodePolygons.put(id , nodePolygon);
			
			nodeLocalities.put(id, node.get("locality").toString());
			nodeCountries.put(id, node.get("country").toString());
			nodeCentroids.put(id, new GeoCoords( (Double)((BasicDBList) node.get("centroid")).get(0),(Double) ((BasicDBList) node.get("centroid")).get(1)));
		}
		// Introduce the noise node
		nodeCentroids.put(NOISE_NODE, new GeoCoords(55,15));
		nodeLocalities.put(NOISE_NODE, "NOISE");
		nodeCountries.put(NOISE_NODE, "NOISE");
	}
	
	public Polygon createPolygonFromFeature(DBObject feature) {
		Polygon res = null;
		BasicDBList coords = null;
		DBObject geo = (DBObject) feature.get("geometry");
		String type = geo.get("type").toString();
		BasicDBList coordinates = (BasicDBList) geo.get("coordinates");
		int maxSize = 0;
		if (type.equals("MultiPolygon")) {
			for (int i = 0; i < coordinates.size(); i++) {
				BasicDBList theseCoords = (BasicDBList) ((BasicDBList) coordinates.get(i)).get(0);
					if (theseCoords.size() > maxSize) {
						coords = theseCoords;
						maxSize = coords.size();
					}
			}

		} else {
			coords = (BasicDBList) (coordinates).get(0);
		}
		
		try {
			res = createPolygon(coords);
		} catch (Exception e) {
			System.out.println(feature);
		}
		
		
		return res;
	}
	
	public String getPolygonContainingPoint(Map<String, Polygon> polygons, GeoCoords point ) {
		Point testPoint = geoFactory.createPoint(new Coordinate(point.getLongitude(), point.getLatitude()));
		for (Entry<String, Polygon> poly : polygons.entrySet()) {
			if (poly.getValue().contains(testPoint)) {
				return poly.getKey();
			}
		}
		
		return "";
	}
	
	public String getClosestPolygonToPoint(Map<String, Polygon> polygons, GeoCoords point ) {
		Point testPoint = geoFactory.createPoint(new Coordinate(point.getLongitude(), point.getLatitude()));
		double minDistance = Double.MAX_VALUE;
		double distance;
		String resultNode = "";
		for (Entry<String, Polygon> poly : polygons.entrySet()) {
			distance = poly.getValue().distance(testPoint);
			if (distance < minDistance) {
				minDistance = distance;
				resultNode = poly.getKey();
			}
		}
		return resultNode;
	}

	/**
	 * Assign an individual <i>tweet</i> to a node in the network. If no node match is found then the <i>tweet</i> is
	 * added to cluster 0 also known as the 'the noise node'.
	 * 
	 * @param tweet
	 * @param processedTweet
	 */
	public void assignTweetToNode(DBObject rawTweet, DBObject processedTweet) {
		DBObject coords = (DBObject) rawTweet.get("coordinates");
		
		String poly = getPolygonContainingPoint(nodePolygons, DBQueries.createCoordsFromGeoJson(coords));
		if (poly.equals("")) {
			poly = NOISE_NODE;
		}
		
		processedTweet.put("node", Integer.valueOf(poly));
	}
	
	public String getClosestNode(GeoCoords coords) {
		return getClosestPolygonToPoint(nodePolygons, coords);
	}
	
	public Double getDistanceFromNode(GeoCoords point, String node) {
		Point testPoint = geoFactory.createPoint(new Coordinate(point.getLongitude(), point.getLatitude()));
		return nodePolygons.get(node).distance(testPoint);
	}
	
	public Map<ImmutableSet<String>, Map<String,Integer> > getEdges() {
		HashMap<ImmutableSet<String>, Map<String,Integer> > edges = new HashMap<ImmutableSet<String>, Map<String,Integer> >();
		ArrayList<String> nodeList = new ArrayList<String>(nodeCentroids.keySet());

		Collections.sort(nodeList);
		for (int i=0; i < nodeList.size(); i++) {
			for (int j=i+1; j < nodeList.size(); j++) {
				HashSet<String> pair = new HashSet<String>();
				pair.add(nodeList.get(i));
				pair.add(nodeList.get(j));
				ImmutableSet<String> key = ImmutableSet.copyOf(pair);
				edges.put(key, new HashMap<String,Integer>());
			}
		}
		return edges;
	}
	
	public Map<String, List<String>> assignNodesToPolygons(Map<String, Polygon> polys) {
		Map<String, List<String>> res = new HashMap<String, List<String>>();
		for (Entry<String, GeoCoords> centroid : nodeCentroids.entrySet()) {
			String polygonKey = getPolygonContainingPoint(polys, centroid.getValue());
			if (!polygonKey.equals("")) {
				if (res.get(polygonKey) == null) {
					res.put(polygonKey, new ArrayList<String>());
				}
				res.get(polygonKey).add(centroid.getKey());
			}
		}
		
		return res;
	}

	/**
	 * Create a closed ring polygon from the list of coordinates.
	 * 
	 * @param coords
	 * @return
	 */
	private Polygon createPolygon(BasicDBList coords) {
		final ArrayList<Coordinate> points = new ArrayList<Coordinate>();

		try {
			Iterator<Object> coordsIter = coords.iterator();
			while (coordsIter.hasNext()) {
				BasicDBList lonlat = (BasicDBList) coordsIter.next();
				float lon = Float.parseFloat(lonlat.get(0).toString());
				float lat = Float.parseFloat(lonlat.get(1).toString());
				points.add(new Coordinate(lon, lat));
			}
		} catch (Exception e) {
			System.out.println(coords.toString());
			throw e;
		}


		final Polygon polygon = geoFactory.createPolygon(
				new LinearRing(new CoordinateArraySequence(points.toArray(new Coordinate[points.size()])), geoFactory),
				null);
		return polygon;
	}
	
	public List<String> getNodeNames() {
		ArrayList<String> names = new ArrayList<String>();
		for (String nodeId: nodeCentroids.keySet()) {
			names.add(nodeId);
		}
		
		return names;
	}
	
	public String getNodeLocality(String id) {
		return nodeLocalities.get(id);
	}
	
	public String getNodeCountry(String id) {
		return nodeCountries.get(id);
	}
	
	public GeoCoords getNodeCentroid(String nodeName) {
		return nodeCentroids.get(nodeName);
	}

}
