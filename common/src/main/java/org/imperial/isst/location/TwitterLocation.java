/**
 * TwitterLocation.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.location;

import java.util.Date;

/**
 * Represents the location of either a </i>tweet</i> or a user on <i>Twitter</i>.
 */
public class TwitterLocation extends Location {

	/**
	 * The ID of the tweet.
	 */
	private final long tweetId;

	/**
	 * The ID of the user.
	 */
	private final long userId;
	
	/**
	 * The date of the tweet.
	 */
	private final Date created_date;

	/**
	 * Initialises a new instance of the {@link TwitterLocation} class.
	 * 


	 * @param country
	 * @param locality
	 * @param latitude
	 * @param longitude
	 * @param confidence
	 * @param tweetId
	 * @param userId
	 * @param date
	 */
	public TwitterLocation(String locality, String country, GeoCoords coords, Source source, long tweetId,
			long userId, Date date) {
		super(locality, "", country, coords, source,1);
		this.tweetId = tweetId;
		this.userId = userId;
		this.created_date = date;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TwitterLocation other = (TwitterLocation) obj;
		if (created_date == null) {
			if (other.created_date != null)
				return false;
		} else if (!created_date.equals(other.created_date))
			return false;
		if (tweetId != other.tweetId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}

	/**
	 * @return the tweetId
	 */
	public long getTweetId() {
		return tweetId;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	public Date getCreated_date() {
		return created_date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((created_date == null) ? 0 : created_date.hashCode());
		result = prime * result + (int) (tweetId ^ (tweetId >>> 32));
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "TwitterLocation [tweetId=" + tweetId + ", userId=" + userId
				+ ", created_date=" + created_date + "]";
	}

}
