package org.imperial.isst.location;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.imperial.isst.utils.Utils;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Abstract base class for all health data loading classes.
 * 
 */
public abstract class BaseHealthDataLoader {

	/**
	 * The ID of the noise node - not associated with any specific geographic area.
	 */
	private static final String NOISE_NODE = "0";

	/**
	 * The spatial features from the input health data.
	 */
	private Map<String, Polygon> healthFeatures;

	/**
	 * The node assigner component.
	 */
	private final NodeAssigner assigner;

	/**
	 * Map of health data areas to nodes in our node network.
	 */
	private final Map<String, Map<String, Double>> areas;

	/**
	 * Initialises a new instance of the {@link BaseHealthDataLoader} class.
	 * 
	 * @throws UnknownHostException
	 * 
	 */
	protected BaseHealthDataLoader(DBObject features, NodeAssigner assigner) throws UnknownHostException {
		this.assigner = assigner;
		areas = new HashMap<String, Map<String, Double>>();
		healthFeatures = new HashMap<String, Polygon>();

		getFeatures(healthFeatures, features);
		calculateAreas();
	}

	/**
	 * Initialises a new instance of the {@link BaseHealthDataLoader} class.
	 * 
	 * @throws UnknownHostException
	 * 
	 */
	protected BaseHealthDataLoader(String featuresGeoJsonFile, NodeAssigner assigner) throws UnknownHostException {
		this.assigner = assigner;
		areas = new HashMap<String, Map<String, Double>>();

		getFeatures(healthFeatures, loadFeaturesGeoJsonFile(featuresGeoJsonFile));
		calculateAreas();
	}

	/**
	 * Calculate the size of the intersection of areas for each node/health area pair.
	 * <p>
	 * A node in our network may intersect with multiple polygons in the health data. Hence we have a collection of of
	 * intersection sizes for each of the node/health area combinations.
	 * <p>
	 * The are
	 */
	private void calculateAreas() {
		for (String feature : healthFeatures.keySet()) {
			Map<String, Double> intersections = new HashMap<String, Double>();
			for (String node : assigner.getNodeNames()) {
				/*
				 * For all nodes except the noise node calculate the fraction of the total health region area that
				 * intersects with that node. For example if there health area has no intersection with the node this
				 * figure is 0.0 and if the health area is completely contained within the node then the figure is
				 * 100.0.
				 */
				if (!node.equals(NOISE_NODE)) {
					Geometry geo = healthFeatures.get(feature).intersection(assigner.nodePolygons.get(node));
					intersections.put(node, geo.getArea() / healthFeatures.get(feature).getArea());
				}
			}
			areas.put(feature, intersections);
		}
	}

	/**
	 * Get the feature identifier.
	 * 
	 * @param feature
	 * @return
	 */
	protected abstract String getFeatureId(DBObject feature);

	/**
	 * Extract the spatial features from the input json document.
	 * 
	 * @param featureMap
	 * @param json
	 */
	private void getFeatures(Map<String, Polygon> featureMap, DBObject json) {
		BasicDBList features = (BasicDBList) json.get("features");
		for (int i = 0; i < features.size(); i++) {
			DBObject feature = (DBObject) features.get(i);
			if (feature.containsField("properties")) {
				featureMap.put(getFeatureId(feature), assigner.createPolygonFromFeature(feature));
			} else {
				getFeatures(featureMap, feature);
			}
		}
	}

	/**
	 * Get a real valued quantification of the intersection between two shapes.
	 * 
	 * @param healthDataName
	 * @param nodeName
	 * @return
	 */
	public Double getIntersection(String healthDataName, String nodeName) {
		if (match(healthDataName) == null)
			return 0.0;

		Map<String, Double> nodeArea = areas.get(match(healthDataName));
		return nodeArea == null ? 0.0 : nodeArea.get(nodeName);
	}

	/**
	 * Load GeoJSON shape features from a file.
	 * 
	 * @param filePath
	 * @return
	 */
	private DBObject loadFeaturesGeoJsonFile(String filePath) {
		String healthFile = Utils.readFileAsStream(filePath);
		healthFeatures = new HashMap<String, Polygon>();
		return (DBObject) JSON.parse(healthFile);
	}

	/**
	 * Check if the name/code matches between datasets.
	 * 
	 * @param toMatch
	 * @return the matched identifier or null if no match found.
	 */
	public String match(String toMatch) {
		return toMatch;
	}
}
