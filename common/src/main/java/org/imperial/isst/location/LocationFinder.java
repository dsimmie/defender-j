/**
 * LocationFinder.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.location;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 
 * @author Donal Simmie
 * 
 */
public interface LocationFinder {

	/**
	 * 
	 * @param text
	 * @return
	 * @throws IOException
	 */
	List<Location> find(String text) throws IOException;

	/**
	 * 
	 * @param text
	 * @param title
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<Location> find(String text, String title) throws IOException;
}
