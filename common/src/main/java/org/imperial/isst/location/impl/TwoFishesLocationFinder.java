package org.imperial.isst.location.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.imperial.isst.location.GeoCoords;
import org.imperial.isst.location.Location;
import org.imperial.isst.location.LocationFinder;
import org.imperial.isst.location.Source;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;


/**
 * Will only return one location
 * 
 * @author Nick Thapen
 * 
 */
public class TwoFishesLocationFinder implements LocationFinder {

	/**
	 * The class logger.
	 */
	private final Logger logger;

	private String twoFishesURI;
	
	static final Map<Integer, String> woeTypes = ImmutableMap.<Integer, String>builder()
		    .put(0, "UNKNOWN")
			.put(7, "TOWN")
		    .put(8, "ADMIN1")
		    .put(9, "ADMIN2")
		    .put(10, "ADMIN3")
		    
		    .put(11, "POSTAL_CODE")
			.put(12, "COUNTRY")
		    .put(13, "ISLAND")
		    .put(14, "AIRPORT")
		    .put(15, "DRAINAGE")
		    
		    .put(16, "PARK")
			.put(20, "POI")
		    .put(22, "SUBURB")
		    .put(23, "SPORT")
		    .put(24, "COLLOQUIAL")
		    
		    .put(25, "ZONE")
			.put(26, "HISTORICAL_STATE")
		    .put(27, "HISTORICAL_COUNTY")
		    .put(29, "CONTINENT")
		    .put(31, "TIMEZONE")
		    .put(35, "HISTORICAL_TOWN")
		    .put(100, "STREET")
		    .build();



	
	/**
	 * The HTTP client.
	 */
	private final HttpClient client;
	
	public TwoFishesLocationFinder(String twoFishesURI) {
		this.twoFishesURI = twoFishesURI;
		client = new DefaultHttpClient();
		logger = LoggerFactory.getLogger(getClass());
		
	}
	
	public List<Location> find(String text) throws IOException {
		return find(text, null);
	}
	
	public List<Location> find(String text, String title) throws IOException {
		List<Location> results = new ArrayList<Location>();
		try {
			Location loc = getLocationFromTwoFishes(text);
			if (loc != null ) {
				results.add(loc);
			}
			
		} catch (InterruptedException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}
	
	
	public Location getLocationFromTwoFishes(String inputText) throws IOException,
	InterruptedException, JSONException {

		String request = twoFishesURI + URLEncoder.encode(inputText, "UTF-8");
		HttpGet post = new HttpGet(request);

		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		logger.debug("TwoFishes service HTTP status code: " + response.getStatusLine().getStatusCode());
		String line = "";
		String text = "";
		while ((line = rd.readLine()) != null) {
			text += line + "\n";
		}

		JSONObject jsonObj = new JSONObject(text);

		JSONArray interpretations = jsonObj.getJSONArray("interpretations");
		List<Location> parsedInterps = new ArrayList<Location>();
		
		for (int i = 0; i < interpretations.length(); i++) {
			Location loc = parseInterpretation(interpretations.getJSONObject(i));
			parsedInterps.add(loc);
		}
		
		// Return the first one that matches GB
		for (Location loc: parsedInterps) {
			if (loc.getCountry().equals("GB")) {
				return loc;
			}
		}
		// Otherwise return the first interpretation
		if (parsedInterps.size() > 0) {
			return parsedInterps.get(0);
		} else {
			return null;
		}
		
	}
	
	private Location parseInterpretation(JSONObject interp) throws JSONException {
		
		String name = interp.getJSONObject("feature").getString("name");
		String country = interp.getJSONObject("feature").getString("cc");
		String type = null;
		try {
			type = woeTypes.get(interp.getJSONObject("feature").getInt("woeType"));
		} catch (Exception e) {
			type = "UNKNOWN";
		}
		
		Double lat = interp.getJSONObject("feature").getJSONObject("geometry").getJSONObject("center").getDouble("lat");
		Double lng = interp.getJSONObject("feature").getJSONObject("geometry").getJSONObject("center").getDouble("lng");
		GeoCoords coords = new GeoCoords(lat, lng);
		Location result = new Location(name, type, country, coords, Source.TWOFISHES,1);
		return result;
	}
}
