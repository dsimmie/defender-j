package org.imperial.isst.location;

/**
 * The confidence in the location value.
 */
public enum Source {
	TWEET_GEO_CODED, TWEET_BIO_LOOKUP, PLACEMAKER, TWOFISHES
}