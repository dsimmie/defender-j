/**
 * SymptomProcessor.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.freebase;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.imperial.isst.database.DBConnection;
import org.imperial.isst.database.DBWrapper;
import org.imperial.isst.utils.SolrUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * 
 * @author Donal Simmie
 * 
 */
/**
 * @author Administrator
 * 
 */
public final class SymptomProcessor {

	/**
	 * The limit of symptoms to search for.
	 */
	private static final int MAX_SYMPTOMS = 2000;

	/**
	 * The minimum frequency required for symptom term to be maintained.
	 */
	// private static final int MIN_FREQ = 10;

	/**
	 * Freebase API Key for up to 100,000 requests per day.
	 */
	private static final String API_KEY = "AIzaSyCZ6NqcqQJxj-B5xf6u2vgTXqd_DwJIdvA";

	/**
	 * Cursor based query placeholder.
	 */
	private static final String CURSOR_PLACEHOLDER = "https://www.googleapis.com/freebase/v1/mqlread?cursor=$CURSOR$&query=$QUERY$&key="
			+ API_KEY;

	/**
	 * Non-cursor based query placeholder.
	 */
	private static final String QUERY_PLACEHOLDER = "https://www.googleapis.com/freebase/v1/mqlread?query=$QUERY$&key="
			+ API_KEY;

	/**
	 * The amount of results to return.
	 */
	private static final int limit = 200;

	/**
	 * The all symptoms query parameters.
	 */
	private static final String SYMPTOMS_QUERY_PARAM = "[{\n  \"id\": null,\n  \"name\": null,\n  \"type\": \"/medicine/symptom\"\n,\"limit\":"
			+ limit + "}]";

	/**
	 * The alias query json param.
	 */
	private final static String ALIAS_PARAM = "{\n  \"/common/topic/alias\": [],\n  \"id\": \"$ALIAS$\"\n}";

	public static void main(String[] args) {
		try {
			new SymptomProcessor().processSymptoms();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * The retrieved symptoms and aliases map.
	 */
	private final Map<String, List<String>> symptoms;

	private final Map<String, Integer> symptomFrequencies;

	/**
	 * Initialises a new instance of the {@link SymptomProcessor} class.
	 * 
	 */
	public SymptomProcessor() {
		symptoms = new HashMap<String, List<String>>();
		symptomFrequencies = new HashMap<String, Integer>();
	}

	/**
	 * 
	 * @param cursor
	 * @param query
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String createCursorQuery(String cursor, String query) throws UnsupportedEncodingException {
		return CURSOR_PLACEHOLDER.replace("$CURSOR$", cursor).replace("$QUERY$", URLEncoder.encode(query, "UTF-8"));
	}

	/**
	 * 
	 * @param start
	 * @param max
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	private void getAllSymptoms(int start, int max) throws JsonProcessingException, IOException {
		int numCalls = (int) Math.ceil(max / (double) limit);
		String cursor = "";
		ObjectMapper mapper = new ObjectMapper();
		for (int i = 0; i < numCalls; i++) {
			URL allSymptomsQuery = new URL(createCursorQuery(cursor, SYMPTOMS_QUERY_PARAM));

			System.out.println(allSymptomsQuery);
			JsonNode rootNode = null;
			try {
				rootNode = mapper.readValue(allSymptomsQuery, JsonNode.class);
			} catch (IOException e) {
				rootNode = mapper.readValue(allSymptomsQuery, JsonNode.class);
			}
			cursor = rootNode.get("cursor").getTextValue();
			JsonNode result = rootNode.get("result");
			int resultSize = result.size();
			for (int j = 0; j < resultSize; j++) {
				JsonNode symptom = result.get(j);
				String name = symptom.get("name").getTextValue();
				if (name == null) {
					continue;
				}
				String id = symptom.get("id").getTextValue();
				String aliasQueryString = QUERY_PLACEHOLDER.replace("$QUERY$",
						URLEncoder.encode(ALIAS_PARAM.replace("$ALIAS$", id), "UTF-8"));
				URL aliasQuery = new URL(aliasQueryString);
				JsonNode rootAliasNode = null;
				try {
					rootAliasNode = mapper.readValue(aliasQuery, JsonNode.class);
				} catch (IOException e) {
					rootAliasNode = mapper.readValue(aliasQuery, JsonNode.class);
				}
				JsonNode aliasNodes = rootAliasNode.get("result").get("/common/topic/alias");
				List<String> aliases = new LinkedList<String>();
				for (int k = 0; k < aliasNodes.size(); k++) {
					String alias = aliasNodes.get(k).getTextValue();
					aliases.add(alias.toLowerCase());
				}
				symptoms.put(name.toLowerCase(), aliases);
			}
		}
	}

	/**
	 * 
	 * @param /** Get the symptom and aliases names.
	 * 
	 * @param symptom
	 * @param aliases
	 * @return
	 */
	private String getSymptomNames(String symptom, List<String> aliases) {
		String name = symptom + ",";
		for (String alias : aliases) {
			name += alias + ",";
		}
		return name;
	}

	/**
	 * Get the frequency of occurence in our tweets dataList of the Freebase symptom terms.
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * 
	 */
	private void getSymptomTermCounts() throws JsonProcessingException, IOException {
		Iterator<Entry<String, List<String>>> symptomEntries = symptoms.entrySet().iterator();
		while (symptomEntries.hasNext()) {
			Entry<String, List<String>> symptomEntry = symptomEntries.next();
			String symptom = symptomEntry.getKey();
			List<String> aliases = symptomEntry.getValue();
			int symptomCount = SolrUtils.getNumDocs(symptom);
			for (String alias : aliases) {
				symptomCount += SolrUtils.getNumDocs(alias);
			}
			symptomFrequencies.put(getSymptomNames(symptom, aliases), symptomCount);
		}
	}

	/**
	 * Download all symptoms from Freebase.
	 * 
	 * @throws URISyntaxException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public void processSymptoms() throws URISyntaxException, JsonProcessingException, IOException {
		getAllSymptoms(0, MAX_SYMPTOMS);
		// storeSymptoms();
		getSymptomTermCounts();

		// Map<String, Integer> sortedFrequencies = Utils.sortByValues(symptomFrequencies);

		// Iterator<Entry<String, Integer>> frequencies = sortedFrequencies.entrySet().iterator();
		// while (frequencies.hasNext()) {
		// Entry<String, Integer> frequency = frequencies.next();
		// }
	}

	/**
	 * Persist the symptoms in MongoDB
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * 
	 */
	private void storeSymptoms() throws JsonProcessingException, IOException {
		List<DBObject> allSymptoms = new LinkedList<DBObject>();
		Iterator<Entry<String, List<String>>> symptomEntries = symptoms.entrySet().iterator();
		while (symptomEntries.hasNext()) {
			Entry<String, List<String>> symptomEntry = symptomEntries.next();
			String symptom = symptomEntry.getKey();
			List<String> aliases = symptomEntry.getValue();
			BasicDBObject symptomDoc = new BasicDBObject();

			symptomDoc.put("name", symptom);
			symptomDoc.put("aliases", aliases);
			allSymptoms.add(symptomDoc);
		}
		DBWrapper.connectInsert(new DBConnection("localhost", "symptoms", "allsymptoms"), allSymptoms);
	}
}
