/**
 * SolrUtils.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.utils;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class SolrUtils {

	/**
	 * A simple SOLR query with no documents returned just to get the count data.
	 */
	private static final String NUM_FOUND_QUERY = "http://localhost:8983/solr/collection1/select?q=\"$QUERY$\"&start=0&rows=0&wt=json";

	/**
	 * Get the number of documents a search term appears in.
	 * 
	 * @param term
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public static int getNumDocs(String term) throws JsonProcessingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readValue(
				new URL(NUM_FOUND_QUERY.replace("$QUERY$", URLEncoder.encode(term, "UTF-8"))), JsonNode.class);
		int numFound = rootNode.get("response").get("numFound").getIntValue();
		return numFound;
	}
}
