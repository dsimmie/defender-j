package org.imperial.isst.utils;

import java.util.regex.Pattern;

import org.jsoup.Jsoup;

public class StringUtils {
	
	private static Pattern regex = Pattern.compile("[^\\x20-\\x7E£]");
	
	// Examples of tweets:
	// What a cold, wet &amp; grey day in London today. Heading back home to sunny Wales ;-)
	// @Silvester24 Can't wait for you to visit. Ps it's so cold I had to wear a thermal vest #granny
	// @RachelClimo_xo I wish tooo :( you need to get rest &amp; get better though!!!! Miss you 💗❤️
	// It's soooo cold outside! Pleased to be at this fab hotel in the warm with a hot pot of tea!! http://t.co/IPHsd5bxWR
	// 5000 words to do for Friday. 1000 done, 4000 to go.. 😲 #dissertation #brain hurts
	
	public static String stripNonAscii(String text) {
		return regex.matcher(text).replaceAll("");
	}
	
	public static String parseHTMLTags(String text) {
		return Jsoup.parse(text).text();
	}
	
	public static boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}

}
