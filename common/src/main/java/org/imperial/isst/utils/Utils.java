package org.imperial.isst.utils;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Utilities class.
 * 
 * @author Donal Simmie
 * 
 */
public final class Utils {

	private static String convertStreamToString(java.io.InputStream is) {
		checkNotNull(is);
		@SuppressWarnings("resource")
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		try {
			return s.hasNext() ? s.next() : "";
		} finally {
			s.close();
		}
	}

	/**
	 * Read the contents of a file into a string.
	 * 
	 * @return
	 * @throws IOException
	 */
	public static String readFile(String filePath) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(filePath));
		return new String(encoded, StandardCharsets.UTF_8);
	}

	/**
	 * Read a file resource on the classpath into a string.
	 * 
	 * @param resource
	 * @return
	 */
	public static String readFileAsStream(String resource) {
		InputStream i = Utils.class.getResourceAsStream(resource);
		String res = convertStreamToString(i);
		return res;
	}

	/**
	 * Sort a map by value in descending order.
	 * 
	 * @param map
	 * @return
	 */
	public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
		Comparator<K> valueComparator = new Comparator<K>() {
			@Override
			public int compare(K k1, K k2) {
				int compare = map.get(k2).compareTo(map.get(k1));
				if (compare == 0)
					return 1;
				else
					return compare;
			}
		};
		Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
		sortedByValues.putAll(map);
		return sortedByValues;
	}
}
