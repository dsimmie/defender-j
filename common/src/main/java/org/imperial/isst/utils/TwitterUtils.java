package org.imperial.isst.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TwitterUtils {
	
	private static Pattern linkFinder;
	private static Pattern hashTagFinder;
	private static Pattern mentionFinder;
	private static String LATIN_ACCENTS_CHARS = "\\u00c0-\\u00d6\\u00d8-\\u00f6\\u00f8-\\u00ff" + // Latin-1
            "\\u0100-\\u024f" + // Latin Extended A and B
            "\\u0253\\u0254\\u0256\\u0257\\u0259\\u025b\\u0263\\u0268\\u026f\\u0272\\u0289\\u028b" + // IPA Extensions
            "\\u02bb" + // Hawaiian
            "\\u0300-\\u036f" + // Combining diacritics
            "\\u1e00-\\u1eff"; // Latin Extended Additional (mostly for Vietnamese)
	 private static final String URL_VALID_PRECEEDING_CHARS = "(?:[^A-Z0-9@＠$#＃\u202A-\u202E]|^)";

	  private static final String URL_VALID_CHARS = "[\\p{Alnum}" + LATIN_ACCENTS_CHARS + "]";
	  private static final String URL_VALID_SUBDOMAIN = "(?:(?:" + URL_VALID_CHARS + "[" + URL_VALID_CHARS + "\\-_]*)?" + URL_VALID_CHARS + "\\.)";
	  private static final String URL_VALID_DOMAIN_NAME = "(?:(?:" + URL_VALID_CHARS + "[" + URL_VALID_CHARS + "\\-]*)?" + URL_VALID_CHARS + "\\.)";
	  /* Any non-space, non-punctuation characters. \p{Z} = any kind of whitespace or invisible separator. */
	  private static final String URL_VALID_UNICODE_CHARS = "[.[^\\p{Punct}\\s\\p{Z}\\p{InGeneralPunctuation}]]";

	  private static final String URL_VALID_GTLD =
	      "(?:(?:" +
	      "academy|accountants|actor|aero|agency|airforce|archi|arpa|asia|associates|axa|bar|bargains|bayern|berlin|best|" +
	      "bid|bike|biz|black|blackfriday|blue|boutique|build|builders|buzz|cab|camera|camp|capital|cards|care|career|" +
	      "careers|cash|cat|catering|center|ceo|cheap|christmas|citic|claims|cleaning|clinic|clothing|club|codes|coffee|" +
	      "college|cologne|com|community|company|computer|construction|contractors|cooking|cool|coop|country|credit|" +
	      "creditcard|cruises|dance|dating|democrat|dental|desi|diamonds|digital|directory|discount|domains|edu|" +
	      "education|email|engineering|enterprises|equipment|estate|eus|events|exchange|expert|exposed|fail|farm|" +
	      "feedback|finance|financial|fish|fishing|fitness|flights|florist|foo|foundation|frogans|fund|furniture|futbol|" +
	      "gal|gallery|gift|glass|globo|gmo|gop|gov|graphics|gratis|gripe|guitars|guru|haus|holdings|holiday|horse|house|" +
	      "immobilien|industries|info|institute|insure|int|international|investments|jetzt|jobs|kaufen|kim|kitchen|kiwi|" +
	      "koeln|kred|land|lease|lighting|limited|limo|link|london|luxury|management|mango|marketing|media|meet|menu|" +
	      "miami|mil|mobi|moda|moe|monash|moscow|museum|nagoya|name|net|neustar|ninja|nyc|okinawa|onl|org|paris|partners|" +
	      "parts|photo|photography|photos|pics|pictures|pink|plumbing|post|pro|productions|properties|pub|qpon|quebec|" +
	      "recipes|red|reisen|ren|rentals|repair|report|rest|reviews|rich|rocks|rodeo|ruhr|ryukyu|saarland|schule|" +
	      "services|sexy|shiksha|shoes|singles|social|sohu|solar|solutions|soy|supplies|supply|support|surgery|systems|" +
	      "tattoo|tax|technology|tel|tienda|tips|today|tokyo|tools|town|toys|trade|training|travel|university|uno|" +
	      "vacations|vegas|ventures|viajes|villas|vision|vodka|vote|voting|voto|voyage|wang|watch|webcam|wed|wien|wiki|" +
	      "works|wtc|wtf|xxx|xyz|yokohama|zone|дети|москва|онлайн|орг|сайт|بازار|شبكة|संगठन|みんな|世界|中信|中文网|公司|公益|商城|在线|" +
	      "我爱你|政务|机构|游戏|移动|组织机构|网址|网络|集团|삼성" +
	      ")(?=[^\\p{Alnum}@]|$))";
	  private static final String URL_VALID_CCTLD =
	      "(?:(?:" +
	      "ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bl|bm|bn|bo|bq|br|bs|bt|bv|" +
	      "bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cw|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|" +
	      "fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|" +
	      "io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mf|" +
	      "mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|" +
	      "pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|" +
	      "sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|" +
	      "za|zm|zw|мкд|мон|рф|срб|укр|қаз|الاردن|الجزائر|السعودية|المغرب|امارات|ایران|بھارت|تونس|سودان|سورية|عمان|" +
	      "فلسطين|قطر|مصر|مليسيا|پاکستان|भारत|বাংলা|ভারত|ਭਾਰਤ|ભારત|இந்தியா|இலங்கை|சிங்கப்பூர்|భారత్|ලංකා|ไทย|გე|中国|中國|台湾|" +
	      "台灣|新加坡|香港|한국" +
	      ")(?=[^\\p{Alnum}@]|$))";
	  private static final String URL_PUNYCODE = "(?:xn--[0-9a-z]+)";

	  private static final String URL_VALID_DOMAIN =
	    "(?:" +                                                   // subdomains + domain + TLD
	        URL_VALID_SUBDOMAIN + "+" + URL_VALID_DOMAIN_NAME +   // e.g. www.twitter.com, foo.co.jp, bar.co.uk
	        "(?:" + URL_VALID_GTLD + "|" + URL_VALID_CCTLD + "|" + URL_PUNYCODE + ")" +
	      ")" +
	    "|(?:" +                                                  // domain + gTLD
	      URL_VALID_DOMAIN_NAME +                                 // e.g. twitter.com
	      "(?:" + URL_VALID_GTLD + "|" + URL_PUNYCODE + ")" +
	    ")" +
	    "|(?:" + "(?<=https?://)" +
	      "(?:" +
	        "(?:" + URL_VALID_DOMAIN_NAME + URL_VALID_CCTLD + ")" +  // protocol + domain + ccTLD
	        "|(?:" +
	          URL_VALID_UNICODE_CHARS + "+\\." +                     // protocol + unicode domain + TLD
	          "(?:" + URL_VALID_GTLD + "|" + URL_VALID_CCTLD + ")" +
	        ")" +
	      ")" +
	    ")" +
	    "|(?:" +                                                  // domain + ccTLD + '/'
	      URL_VALID_DOMAIN_NAME + URL_VALID_CCTLD + "(?=/)" +     // e.g. t.co/
	    ")";

	  private static final String URL_VALID_PORT_NUMBER = "[0-9]++";

	  private static final String URL_VALID_GENERAL_PATH_CHARS = "[a-z0-9!\\*';:=\\+,.\\$/%#\\[\\]\\-_~\\|&@" + LATIN_ACCENTS_CHARS + "]";
	  /** Allow URL paths to contain up to two nested levels of balanced parens
	   *  1. Used in Wikipedia URLs like /Primer_(film)
	   *  2. Used in IIS sessions like /S(dfd346)/
	   *  3. Used in Rdio URLs like /track/We_Up_(Album_Version_(Edited))/
	  **/
	  private static final String URL_BALANCED_PARENS = "\\(" +
	    "(?:" +
	      URL_VALID_GENERAL_PATH_CHARS + "+" +
	      "|" +
	      // allow one nested level of balanced parentheses
	      "(?:" +
	        URL_VALID_GENERAL_PATH_CHARS + "*" +
	        "\\(" +
	          URL_VALID_GENERAL_PATH_CHARS + "+" +
	        "\\)" +
	        URL_VALID_GENERAL_PATH_CHARS + "*" +
	      ")" +
	    ")" +
	  "\\)";

	  /** Valid end-of-path characters (so /foo. does not gobble the period).
	   *   2. Allow =&# for empty URL parameters and other URL-join artifacts
	  **/
	  private static final String URL_VALID_PATH_ENDING_CHARS = "[a-z0-9=_#/\\-\\+" + LATIN_ACCENTS_CHARS + "]|(?:" + URL_BALANCED_PARENS +")";

	  private static final String URL_VALID_PATH = "(?:" +
	    "(?:" +
	      URL_VALID_GENERAL_PATH_CHARS + "*" +
	      "(?:" + URL_BALANCED_PARENS + URL_VALID_GENERAL_PATH_CHARS + "*)*" +
	      URL_VALID_PATH_ENDING_CHARS +
	    ")|(?:@" + URL_VALID_GENERAL_PATH_CHARS + "+/)" +
	  ")";

	  private static final String URL_VALID_URL_QUERY_CHARS = "[a-z0-9!?\\*'\\(\\);:&=\\+\\$/%#\\[\\]\\-_\\.,~\\|@]";
	  private static final String URL_VALID_URL_QUERY_ENDING_CHARS = "[a-z0-9_&=#/]";
	  private static final String VALID_URL_PATTERN_STRING =
	  "(" +                                                            //  $1 total match
	    "(" + URL_VALID_PRECEEDING_CHARS + ")" +                       //  $2 Preceeding chracter
	    "(" +                                                          //  $3 URL
	      "(https?://)?" +                                             //  $4 Protocol (optional)
	      "(" + URL_VALID_DOMAIN + ")" +                               //  $5 Domain(s)
	      "(?::(" + URL_VALID_PORT_NUMBER +"))?" +                     //  $6 Port number (optional)
	      "(/" +
	        URL_VALID_PATH + "*+" +
	      ")?" +                                                       //  $7 URL Path and anchor
	      "(\\?" + URL_VALID_URL_QUERY_CHARS + "*" +                   //  $8 Query String
	              URL_VALID_URL_QUERY_ENDING_CHARS + ")?" +
	    ")" +
	  ")";
	
	static {
	//	linkFinder = Pattern.compile("http(\\S+)");
		linkFinder = Pattern.compile(VALID_URL_PATTERN_STRING, Pattern.CASE_INSENSITIVE);
		hashTagFinder = Pattern.compile("#(\\S+)");
		mentionFinder = Pattern.compile("@\\w{1,15}");
	}
	
	public static Date getTwitterDate(String date) throws ParseException
	{
	    final String TWITTER = "EEE MMM dd HH:mm:ss ZZZZZ yyyy";
	    SimpleDateFormat sf = new SimpleDateFormat(TWITTER, Locale.ENGLISH);
	    sf.setLenient(true);
	    return sf.parse(date);
	}
	
	public static String removeLinks(String text, ArrayList<String> links) {
		return find(linkFinder, text, links, true);
	}
	
	public static String removeMentions(String text, ArrayList<String> mentions) {
		return find(mentionFinder, text, mentions, true);
	}
	
	public static String findHashTags(String text, ArrayList<String> tags) {
		return find(hashTagFinder, text, tags, false);
	}
	
	private static String find(Pattern pattern, String text, ArrayList<String> matched, boolean remove) {
        Matcher m = pattern.matcher(text);

        while (m.find()) {
        	matched.add(m.group(0).trim());
        	if (remove) {
            	text=  text.replace(m.group(0),"").trim();       		
        	}

        }
		return text;
	}

}
