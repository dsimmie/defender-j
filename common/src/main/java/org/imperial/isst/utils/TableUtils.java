package org.imperial.isst.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.imperial.isst.date.DateFormat;

import au.com.bytecode.opencsv.CSVWriter;

import com.google.common.collect.ArrayTable;
import com.google.common.collect.ObjectArrays;

public class TableUtils {

	/**
	 * Shortened date format for presentation.
	 */
	private static final SimpleDateFormat PRETTY = new SimpleDateFormat("EEE, d MMM, yyyy");
	
	public static void incrementTable(ArrayTable<Date, String, Double> table, Date date, String key) {
		double count;
		if (table.get(date, key) != null) {
			count = table.get(date, key);
		} else {
			count = 0;
		}
		table.put(date, key, count+1);
	}
	
	public static void incrementTableByValue(ArrayTable<Date, String, Double> table, Date date, String key, Double value) {
		double count;
		if (table.get(date, key) != null) {
			count = table.get(date, key);
		} else {
			count = 0;
		}
		table.put(date, key, count+value);
	}
	
	
	public static void zeroTable(ArrayTable<Date, String, Double> table) {
		for (Date row: table.rowKeyList()) {
			for (String key: table.columnKeyList()) {
				table.put(row, key, (double)0 );
			}
		}
	}
	
	
	public static void outputToCSV(ArrayTable<Date, String, Double> table, String name, DateFormat dateFormat) throws IOException {
		File outputFile = new File(name);
		outputFile.getParentFile().mkdirs();
		CSVWriter writer = new CSVWriter(new FileWriter(outputFile));
		String[] columnHeaders = table.columnKeySet().toArray(new String[0]);
		String[] both = ObjectArrays.concat(new String[]{"Date"}, columnHeaders, String.class);		
				
		writer.writeNext(both);
		for (Date date : table.rowKeySet()) {
			Map<String, Double> values = table.row(date);
			String [] stringValues = new String[table.columnKeySet().size()+1];
			stringValues[0] = dateFormat == DateFormat.PRETTY ? PRETTY.format(date) : date.toString();
			int count = 1;
			for (Double value: values.values()) {
				stringValues[count] = String.valueOf(value);
				count++;
			}

			writer.writeNext(stringValues);
		}	
		writer.close();
	}
	
	public static void outputToCSV(ArrayTable<Date, String, String> table, String name) throws IOException {
		File outputFile = new File(name);
		outputFile.getParentFile().mkdirs();
		CSVWriter writer = new CSVWriter(new FileWriter(outputFile));
		String[] columnHeaders = table.columnKeySet().toArray(new String[0]);
		String[] both = ObjectArrays.concat(new String[]{"Date"}, columnHeaders, String.class);		
				
		writer.writeNext(both);
		for (Date date : table.rowKeySet()) {
			Map<String, String> values = table.row(date);
			String [] stringValues = new String[table.columnKeySet().size()+1];
			stringValues[0] =  date.toString();
			int count = 1;
			for (String value: values.values()) {
				stringValues[count] = value;
				count++;
			}

			writer.writeNext(stringValues);
		}	
		writer.close();
	}
}
