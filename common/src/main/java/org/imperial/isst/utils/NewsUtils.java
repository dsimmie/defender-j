package org.imperial.isst.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.imperial.isst.news.Config;
import org.imperial.isst.news.Feed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewsUtils {

	
	private static Logger logger;
	
	static {
		logger = LoggerFactory.getLogger(NewsUtils.class);
	}
	/**
	 * Load the feed details from file.
	 */
	public static Set<Feed> loadFeeds(String feedFileLocation) {
		HashSet<Feed> feeds = new HashSet<Feed>();
		InputStream fileStream = NewsUtils.class.getResourceAsStream(feedFileLocation);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
		String line = "";

		try {
			while ((line = reader.readLine()) != null) {
				if (line.charAt(0) == Config.COMMENT_CHAR)
					continue;
				StringTokenizer feedTokenizer = new StringTokenizer(line, ",");
				String name = null;
				boolean doesDescriptionContainHTML = false;
				URL feedURL = null;

				while (feedTokenizer.hasMoreTokens()) {
					name = feedTokenizer.nextToken().trim();
					doesDescriptionContainHTML = Boolean.parseBoolean(feedTokenizer.nextToken().trim());
					feedURL = new URL(feedTokenizer.nextToken().trim());
				}
				Feed feed = new Feed(name, feedURL, doesDescriptionContainHTML);
				feeds.add(feed);
				logger.debug("Loaded feed details: " + feed);
			}
			fileStream.close();
		} catch (IOException e) {
			logger.error("Fatal error. Cannot load feeds from feed file: " + feedFileLocation, e);
			System.exit(0);
		}
		
		return feeds;
	}
}
