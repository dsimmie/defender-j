package org.imperial.isst.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVReader;

import com.google.common.collect.ArrayTable;
import com.mongodb.BasicDBList;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class KeywordUtils {

	private static Logger logger = LoggerFactory.getLogger(KeywordUtils.class);

	public static Pattern buildRegex(List<String> keywords) {
		return buildRegex(keywords, true);
	}

	public static Pattern buildRegex(List<String> keywords, boolean useLowerCase) {
		StringBuilder builder = new StringBuilder();
		builder.append("\\b(");
		int index = 0;
		for (String word : keywords) {
			String searchWord;
			if (useLowerCase) {
				searchWord = word.toLowerCase();
			} else {
				searchWord = word;
			}
			builder.append(Pattern.quote(searchWord));
			if (index < keywords.size() - 1) {
				builder.append('|');
			}
			index++;
		}
		;
		builder.append(")\\b");
		Pattern regex = Pattern.compile(builder.toString());
		return regex;
	}

	public static Map<String, ArrayTable<Date, String, Double>> createCategoryTables(
			HashMap<String, List<String>> keywords, ArrayTable<Date, String, Double> resultTable) {
		Map<String, ArrayTable<Date, String, Double>> res = new HashMap<String, ArrayTable<Date, String, Double>>();
		for (String category : keywords.keySet()) {
			ArrayTable<Date, String, Double> categoryTable = ArrayTable.create(resultTable.rowKeyList(),
					keywords.get(category));
			TableUtils.zeroTable(categoryTable);
			for (String keyword : keywords.get(category)) {
				Map<Date, Double> map = resultTable.column(keyword);
				for (Date date : map.keySet()) {
					categoryTable.put(date, keyword, map.get(date));
				}
			}
			res.put(category, categoryTable);
		}

		return res;
	}

	public static List<String> getAllKeywords(Map<String, List<String>> keywords) {
		Set<String> allKeywords = new HashSet<String>();
		List<String> allKeywordsNoDups = new ArrayList<String>();
		for (List<String> value : keywords.values()) {
			allKeywords.addAll(value);
		}
		allKeywordsNoDups.addAll(allKeywords);
		return allKeywordsNoDups;
	}

	/**
	 * Get all words in a CSV file.
	 * 
	 * @param keywordFile
	 * @return
	 * @throws IOException
	 */
	public static List<String> getAllKeywords(String keywordFile) throws IOException {
		List<String> keywords = new ArrayList<String>();
		InputStream inputStream = KeywordUtils.class.getClassLoader().getResourceAsStream(keywordFile);

		CSVReader reader = new CSVReader(new InputStreamReader(inputStream));
		String[] row = null;
		while ((row = reader.readNext()) != null) {
			for (String word : row) {
				keywords.add(word);
			}
		}
		reader.close();
		return keywords;
	}

	/**
	 * Get the number of occurrences of each word in the phrase
	 * 
	 * @param words
	 * @param phrase
	 */
	public static Map<String, Integer> getOccurrences(List<String> words, String phrase) {
		Map<String, Integer> res = new HashMap<String, Integer>();
		Pattern regex = KeywordUtils.buildRegex(words);
		List<String> matches = KeywordUtils.returnMatches(regex, phrase);
		for (String word : words) {
			int occurrences = Collections.frequency(matches, word.toLowerCase());
			res.put(word, occurrences);
		}

		return res;

	}

	public static Map<String, Set<String>> getReverseMap(Map<String, List<String>> keywords) {
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();

		for (Map.Entry<String, List<String>> entry : keywords.entrySet()) {
			String key = entry.getKey();
			List<String> value = entry.getValue();
			for (String alias : value) {
				if (!result.containsKey(alias)) {
					result.put(alias, new HashSet<String>());
				}
				result.get(alias).add(key);

			}

		}
		return result;
	}

	/**
	 * Checks whether the keywords occur in the phrase.
	 * 
	 * @param keyword
	 * @param phrase
	 */
	public static boolean keywordMatch(List<String> keywords, String phrase) {
		Pattern regex = buildRegex(keywords);
		return keywordMatch(regex, phrase);
	}

	/**
	 * Checks whether the pattern matches the phrase.
	 * 
	 * @param regex
	 * @param phrase
	 */
	public static boolean keywordMatch(Pattern regex, String phrase) {
		String lowered = phrase.toLowerCase();
		Matcher regexMatcher = regex.matcher(lowered);
		return regexMatcher.find();
	}

	public static HashMap<String, List<String>> loadInclusionsAndExclusions(DBCollection collection) {
		HashMap<String, List<String>> outputMap = new HashMap<String, List<String>>();

		DBCursor keywordCursor = collection.find();

		while (keywordCursor.hasNext()) {

			DBObject obj = keywordCursor.next();

			String key = obj.get("type").toString();
			List<String> outputAliases = new ArrayList<String>();

			BasicDBList aliases = (BasicDBList) obj.get("words");
			for (Object alias : aliases) {
				outputAliases.add(alias.toString());
			}
			outputAliases.add(key);
			outputMap.put(key, outputAliases);
		}
		return outputMap;
	}

	public static HashMap<String, List<String>> loadKeywords(DBCollection collection) {
		HashMap<String, List<String>> outputMap = new HashMap<String, List<String>>();

		DBCursor keywordCursor = collection.find();

		while (keywordCursor.hasNext()) {

			DBObject obj = keywordCursor.next();

			String key = obj.get("symptom").toString();
			List<String> outputAliases = new ArrayList<String>();

			BasicDBList aliases = (BasicDBList) obj.get("aliases");
			for (Object alias : aliases) {
				outputAliases.add(alias.toString());
			}
			outputAliases.add(key);
			outputMap.put(key, outputAliases);
		}
		return outputMap;
	}

	public static HashMap<String, List<String>> loadKeywords(String keywordFile) {
		HashMap<String, List<String>> outputMap = new HashMap<String, List<String>>();
		String[] currentLine;
		InputStream inputStream = KeywordUtils.class.getClassLoader().getResourceAsStream(keywordFile);

		CSVReader reader = new CSVReader(new InputStreamReader(inputStream));
		try {
			currentLine = reader.readNext();
			while ((currentLine = reader.readNext()) != null) {
				List<String> items = Arrays.asList(currentLine[1].toLowerCase().split("\\s*,\\s*"));
				outputMap.put(currentLine[0], items);

			}
			logger.info(outputMap.toString());
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return outputMap;
	}

	public static String removeWhiteSpace(String phrase) {
		return phrase.replace(" ", "");
	}

	public static List<String> returnMatches(Pattern regex, String phrase) {
		String lowered = phrase.toLowerCase();
		Matcher regexMatcher = regex.matcher(lowered);
		ArrayList<String> allMatches = new ArrayList<String>();
		while (regexMatcher.find()) {
			allMatches.add(regexMatcher.group());
		}
		return allMatches;
	}

}