package org.imperial.isst.apps;

import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;

import org.imperial.isst.database.DBConnection;
import org.imperial.isst.database.DBWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

/**
 * Base class for common Defender application functionlity.
 * 
 * @author Donal Simmie
 * 
 */
public abstract class DefenderApplication {

	/**
	 * Configuration properties.
	 */
	protected Properties configProperties;

	/**
	 * The class logger.
	 */
	protected final Logger logger;

	/**
	 * Initialises a new instance of the {@link DefenderApplication} class.
	 * 
	 * @param configFile
	 */
	public DefenderApplication(String configFile) {
		logger = LoggerFactory.getLogger(this.getClass().getName());
		loadConfig(configFile);

	}

	/**
	 * Connect to the mongoDB database
	 * 
	 * @param databaseName
	 *            - the name used to identify the database in the config.properties file
	 * @return DBCollection
	 * @throws UnknownHostException
	 */
	public DBCollection connectToDB(String databaseName) throws UnknownHostException {
		logger.info("Connecting to MongoDB.");
		DBConnection connection = new DBConnection(configProperties, databaseName);

		MongoClient mongoClient;
		mongoClient = DBWrapper.getClient(connection);
		DB db = mongoClient.getDB(connection.getDb());

		return getCollection(db, connection.getCollection());
	}

	/**
	 * Get a collection from the specified database. Can be overridden to ensure the collection has certain properties
	 * 
	 * @param db
	 * @param collectionName
	 * @return DBCollection
	 */
	protected DBCollection getCollection(DB db, String collectionName) {
		// Can override this method to make sure the collection has certain properties on creation
		return db.getCollection(collectionName);
	}

	/**
	 * Loads the configuration file into the properties object.
	 * 
	 * @param configFile
	 */
	private void loadConfig(String configFile) {
		logger.info("Loading configuration.");
		InputStream i = this.getClass().getResourceAsStream(configFile);
		configProperties = new Properties();

		try {
			configProperties.load(i);
			logger.info(configProperties.toString());
		} catch (Exception e) {
			logger.info("Unable to locate configuration file. Exiting.",e);
			System.exit(0);
		}

	}
}
