package org.imperial.isst.graph;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.rest.graphdb.RestGraphDatabase;

public class Neo4JFactory {

	public static RestGraphDatabase createNeo4JService(String path) {
		//GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( path ).
		//	    setConfig( GraphDatabaseSettings.node_keys_indexable, "name" ).
		//	    setConfig( GraphDatabaseSettings.node_auto_indexing, "true" ).
		//	    newGraphDatabase();
		RestGraphDatabase graphDb = new RestGraphDatabase(path);
		//registerShutdownHook(graphDb);
		return graphDb;
	}
	
	public static GraphDatabaseService createNeo4JEmbeddedService(String path) {
		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( path ).
			    setConfig( GraphDatabaseSettings.node_keys_indexable, "name" ).
			    setConfig( GraphDatabaseSettings.node_auto_indexing, "true" ).
			    newGraphDatabase();
		registerShutdownHook(graphDb);
		return graphDb;
	}
	
	private static void registerShutdownHook( final GraphDatabaseService graphDb )
	{
	    // Registers a shutdown hook for the Neo4j instance so that it
	    // shuts down nicely when the VM exits (even if you "Ctrl-C" the
	    // running application).
	    Runtime.getRuntime().addShutdownHook( new Thread()
	    {
	        @Override
	        public void run()
	        {
	            graphDb.shutdown();
	        }
	    } );
	}
}
