package org.imperial.isst.graph;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.index.lucene.QueryContext;

public class Neo4JUtils {
	
	public final static String DATA = "DATA";
	public final static String FORECAST = "FORECAST";
	public final static String REFERENCE = "REFERENCE";
	
	public final static String FORECAST_REL = "FORECASTREL";

	// Needs to be called within a transaction for an embedded db
	public static Map<String, Node> getNodesForDate(Index<Node> timestamps, Date date) {

		IndexHits<Node> lastNodeIndex = timestamps.query(QueryContext
				.numericRange("timestamp", date.getTime(),
						date.getTime()));
		HashMap<String, Node> nodesForDate = new HashMap<String, Node>();
		try {
			for (Node last : lastNodeIndex) {
				nodesForDate.put((String) last.getProperty("name"), last);
			}
		} finally {
			lastNodeIndex.close();
		}
		return nodesForDate;
	}

}
