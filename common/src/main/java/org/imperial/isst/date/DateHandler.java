package org.imperial.isst.date;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateHandler {
	public static SimpleDateFormat sdf;
	static {
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	public static Date addDaysTo(Date date, int number) {
		return addDaysTo(date, number, false);
	}

	public static Date addDaysTo(Date date, int number, boolean ensureMidnight) {
		Calendar cal = getCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, number);
		if (ensureMidnight) {
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0);
		}
		return cal.getTime();
	}

	public static List<Date> datesBetween(Date startDate, Date endDate) {
		List<Date> ret = new ArrayList<Date>();
		Calendar start = getCalendar();
		start.setTime(startDate);
		Calendar end = getCalendar();
		end.setTime(endDate);
		for (Date date = start.getTime(); !start.after(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
			ret.add(date);
		}
		return ret;
	}

	public static Calendar getCalendar() {
		Calendar result = Calendar.getInstance();
		result.setTimeZone(TimeZone.getTimeZone("GMT"));
		return result;
	}

	public static boolean isWeekend(Date date) {
		Calendar cal = DateHandler.getCalendar();
		cal.setTime(date);
		boolean res = false;
		if ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
			res = true;
		} else {
			res = false;
		}
		return res;
	}

}
