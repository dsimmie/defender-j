/**
 * DateFormat.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.date;

/**
 *
 * @author Donal Simmie
 *
 */
public enum DateFormat {
	RAW, PRETTY
}
