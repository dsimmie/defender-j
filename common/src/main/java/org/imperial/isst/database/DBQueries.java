package org.imperial.isst.database;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.imperial.isst.date.DateHandler;
import org.imperial.isst.location.GeoCoords;
import org.imperial.isst.objects.Entity;
import org.imperial.isst.utils.KeywordUtils;
import org.imperial.isst.utils.TwitterUtils;
import org.slf4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class DBQueries {
	
	/**
	 * The class logger.
	 */
	private static Logger logger;

	/**
	 * The default Mongo client options.
	 */
//	private static final MongoClientOptions DEFAULT_OPTIONS = new MongoClientOptions.Builder().autoConnectRetry(true)
//			.socketKeepAlive(true).build();


	// Look for times between midnight starting on the current day and midnight starting
	// on the subsequent day
	public static DBObject buildQueryForDay(Date date, String fieldName) {
		BasicDBObject query = new BasicDBObject();
		Calendar nextCal = DateHandler.getCalendar();
		nextCal.setTime(date);
		nextCal.add(Calendar.DATE, 1);
		Date nextDay = nextCal.getTime();
		query.put(fieldName, BasicDBObjectBuilder.start("$gte", date).add("$lte", nextDay).get());
		return query;
	}
	
	// Note that this is inclusive of the start date but not the end date
	public static DBObject buildSymptomQuery(Date start, Date end, String symptom, String node) {
		BasicDBObject query = new BasicDBObject();
		query.put("created_date", BasicDBObjectBuilder.start("$gte", start).add("$lte", end).get());
		query.put("node", Integer.valueOf(node));
		query.put("symptoms", symptom);
		query.put("health", "Health");
		return query;
	}
	
	// Note that this is inclusive of the start date but not the end date
	public static DBObject buildTagQuery(Date start, Date end, BasicDBList tags, String node) {
		BasicDBObject query = new BasicDBObject();
		query.put("created_date", BasicDBObjectBuilder.start("$gte", start).add("$lte", end).get());
		query.put("node", Integer.valueOf(node));
		query.put("tags", BasicDBObjectBuilder.start("$in", tags).get() );
		return query;
	}
	
	// Note that this is inclusive of the start date but not the end date
	public static DBObject buildGeoQuery(Date start, Date end, DBObject point) {
		BasicDBObject query = new BasicDBObject();
		query.put("created_date", BasicDBObjectBuilder.start("$gte", start).add("$lte", end).get());
		query.put("geo", BasicDBObjectBuilder.start("$near", point).add("$maxDistance", 1000).get() );
		return query;
	}
	
	// Get a full tweet object from a location object
	public static DBObject getFullTweet(DBObject location, DBCollection tweetTextCollection) {
		BasicDBObject query = new BasicDBObject("id", location.get("tweetId"));
		DBObject fullTweet = tweetTextCollection.findOne(query);
		fullTweet.put("location", location);
		try {
			fullTweet.put("created_date", TwitterUtils.getTwitterDate(fullTweet.get("created_at").toString()));
		} catch (ParseException e) {
			logger.info("Could not parse date from: " + fullTweet.toString());
		}
		return fullTweet;
	}
	
	public static DBObject buildNewsSymptomQuery(DBObject dateQuery, String symptom) {
		BasicDBObject query = new BasicDBObject();
		query.put("pub_date", dateQuery);
		query.put("symptoms", symptom);
		return query;
	}
	
	public static DBObject buildNewsRegexQuery(DBObject dateQuery, String fieldToSearch, 
												List<String> words, boolean useLowerCase) {
		BasicDBObject query = new BasicDBObject();
		query.put("pub_date", dateQuery);
		query.put("classification", "health");
		query.put(fieldToSearch, KeywordUtils.buildRegex(words, useLowerCase));
		return query;
	}
	
	public static DBObject getDateQueryObject(Date start, Date end, int extraInterval) {
		Calendar intervalCal = DateHandler.getCalendar();
		intervalCal.setTime(start);
		intervalCal.add(Calendar.DATE, extraInterval*-1);
		Date revisedStart = intervalCal.getTime();
		
		intervalCal.setTime(end);
		intervalCal.add(Calendar.DATE, extraInterval);
		Date revisedEnd = intervalCal.getTime();	
		return BasicDBObjectBuilder.start("$gte", revisedStart).add("$lte", revisedEnd).get();
	}
	
	public static GeoCoords createCoordsFromGeoJson(DBObject geo) {
		BasicDBList coordsList = (BasicDBList) geo.get("coordinates");
		float lon = Float.parseFloat(coordsList.get(0).toString());
		float lat = Float.parseFloat(coordsList.get(1).toString());
		return new GeoCoords(lat, lon);
	}
	
	/**
	 * Convert the named entities into database format.
	 * <p>
	 * Stores the entities as phrase/entity type pairs. 
	 * 
	 * @param entities
	 */
	public static DBObject convertEntities(Map<Entity, HashMap<String, Integer>> entities) {
		DBObject entityDocs = new BasicDBObject();
		
		for (Entity ent: Entity.values()) {
			BasicDBList entList = new BasicDBList();
			if (entities.containsKey(ent)) {
				HashMap<String, Integer> values = entities.get(ent);
				for (Entry<String, Integer> entry : values.entrySet()) {
					DBObject instance = new BasicDBObject();
					instance.put("term", entry.getKey());
					instance.put("count", entry.getValue());
					entList.add(instance);
				}
			}
			entityDocs.put(ent.toString(), entList);
		}
		return entityDocs;
	}
	
	public static Map<Entity, HashMap<String, Integer>> convertEntities(DBObject entities) {
		Map<Entity, HashMap<String, Integer>> res = new HashMap<Entity, HashMap<String,Integer>>();
		for (Entity ent: Entity.values()) {
			HashMap<String, Integer> entMap = new HashMap<String, Integer>();
			res.put(ent, entMap);
			BasicDBList entList = (BasicDBList) entities.get(ent.toString());
			for (Object obj: entList) {
				entMap.put(((DBObject) obj).get("term").toString(), (Integer)((DBObject) obj).get("count"));
			}
		}
		
		return res;
	}

}
