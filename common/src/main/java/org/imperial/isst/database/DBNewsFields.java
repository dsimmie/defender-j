/**
 * DBFields.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.database;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class DBNewsFields {
	/**
	 * 
	 */
	public static final String ID = "id";

	/**
	 * 
	 */
	public static final String TITLE = "title";

	/**
	 * 
	 */
	public static final String LINK = "link";

	/**
	 * 
	 */
	public static final String DESCRIPTION = "description";

	/**
	 * 
	 */
	public static final String PUB_DATE = "pub_date";

	/**
	 * 
	 */
	public static final String ARTICLE = "article";

	/**
	 * 
	 */
	public static final String URL = "url";

	/**
	 * 
	 */
	public static final String NAME = "name";

	/**
	 * 
	 */
	public static final String FEED = "feed";

	/**
	 * The date the feed as entered into the system.
	 */
	public static final String ENTRY_TIME = "entry_time";

	/**
	 * The version string of the current iteration of the downloading code.
	 */
	public static final String CODE_VERSION = "code_version";

	/**
	 * The class that downloaded the news data. Note: when combined with code version allows replication of method.
	 */
	public static final String DOWNLOADER_CLASS = "downloader_class";

	/**
	 * The data describing a feed item.
	 */
	public static final String METADATA = "metadata";

	/**
	 * The parser used to get the article text.
	 */
	public static final String ARTICLE_PARSER = "article_parser";

	/**
	 * THe class that performed the parsing of the feed.
	 */
	public static final String PARSER_CLASS = "parser_class";

	/**
	 * The lowest level place name, e.g. town, city.
	 */
	public static final String LOCALITY = "locality";
	
	/**
	 * The lowest level place name, e.g. town, city.
	 */
	public static final String LOCALITY_TYPE = "localityType";
	
	/**
	 * A measure of the confidence of the place name.
	 */
	public static final String CONFIDENCE = "confidence";

	/**
	 * The country name.
	 */
	public static final String COUNTRY = "country";

	/**
	 * The sourc eof the location reading.
	 */
	public static final String SOURCE = "source";

	/**
	 * Latitude field.
	 */
	public static final String LAT = "lat";

	/**
	 * Longitude field.
	 */
	public static final String LONG = "long";

	/**
	 * The geographical coordinates field.
	 */
	public static final String GEO = "geo";

	/**
	 * The keyword field.
	 */
	public static final String KEYWORDS = "keywords";
	
	/**
	 * The symptoms field.
	 */
	public static final String SYMPTOMS = "symptoms";

	/**
	 * The feed access date.
	 */
	public static final String ACCESS_DATE = "access_date";

	/**
	 * The feed item summaries;
	 */
	public static final String ITEM_SUMMARIES = "item_summaries";

	/**
	 * A feed item summary field.
	 */
	public static final String ITEM_SUMMARY = "item_summary";

	public static final String LOCATIONS = "locations";

	/**
	 * The named entity word(s)
	 */
	public static final String TERM = "term";

	/**
	 * The named entity type.
	 */
	public static final String ENTITY = "entity";

	/**
	 * The named entity term and type pair.
	 */
	public static final String ENTITY_PAIR = "entity_pair";

	/**
	 * The named entities array.
	 */
	public static final String ENTITIES = "entities";
	
	/**
	 * The health classification of the article.
	 */
	public static final String CLASSIFICATION = "classification";
}
