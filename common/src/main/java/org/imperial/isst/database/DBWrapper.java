package org.imperial.isst.database;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.CommandFailureException;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.WriteResult;

/**
 * Convenience wrapper for common MongoDB operations.
 */
public final class DBWrapper {

	/**
	 * The class logger.
	 */
	private static Logger logger;

	/**
	 * The default Mongo client options.
	 */
	private static final MongoClientOptions DEFAULT_OPTIONS = new MongoClientOptions.Builder().autoConnectRetry(true)
			.socketKeepAlive(true).build();

	/**
	 * Class wide initialisation.
	 * 
	 */
	static {
		logger = LoggerFactory.getLogger(DBWrapper.class);
	}

	/**
	 * Connect to the given database collection and insert data.
	 * 
	 * @param docs
	 * @throws UnknownHostException
	 */
	public static void connectInsert(DBConnection connection, List<DBObject> docs) throws UnknownHostException {
		insert(connectToCollection(connection), docs);
	}

	/**
	 * Connect to the DB Collection using default client options.
	 * 
	 * @param connectionDetails
	 *            the host, DB and collection names.
	 * @return the collection object.
	 * @throws UnknownHostException
	 */
	public static DBCollection connectToCollection(DBConnection connectionDetails) throws UnknownHostException {
		return connectToCollection(connectionDetails, DEFAULT_OPTIONS);
	}

	/**
	 * Connect to the DB Collection using specific client options.
	 * 
	 * @param connectionDetails
	 *            the host, DB and collection names.
	 * @return the collection object.
	 * @throws UnknownHostException
	 */
	public static DBCollection connectToCollection(DBConnection connectionDetails, MongoClientOptions options)
			throws UnknownHostException {
		checkNotNull(connectionDetails);
		checkNotNull(options);

		MongoClient mongoClient = getClient(connectionDetails, options);
		DB db = mongoClient.getDB(connectionDetails.getDb());
		DBCollection collection = db.getCollection(connectionDetails.getCollection());
		logger.debug("Connected to collection " + connectionDetails.getCollection() + " on db "
				+ connectionDetails.getDb() + ".");
		return collection;
	}

	/**
	 * Get the correctly configured {@link MongoClient} object.
	 * <p>
	 * This method handles authentication if the {@link DBConnection} object has a set user field.
	 * 
	 * @param connectionDetails
	 * @return
	 * @throws UnknownHostException
	 */
	public static MongoClient getClient(DBConnection connectionDetails) throws UnknownHostException {
		checkNotNull(connectionDetails);
		return getClient(connectionDetails, DEFAULT_OPTIONS);
	}

	/**
	 * Get the correctly configured {@link MongoClient} object.
	 * <p>
	 * This method handles authentication if the {@link DBConnection} object has a set user field.
	 * 
	 * @param connectionDetails
	 * @param options
	 * @return
	 * @throws UnknownHostException
	 */
	public static MongoClient getClient(DBConnection connectionDetails, MongoClientOptions options)
			throws UnknownHostException {
		checkNotNull(connectionDetails);
		checkNotNull(options);

		MongoClient mongoClient;
		if (isAuthEnabled(connectionDetails.getHost()) && connectionDetails.hasUserDetails()) {
			MongoCredential credential = MongoCredential.createMongoCRCredential(connectionDetails.getUser(),
					connectionDetails.getDb(), connectionDetails.getPassword().toCharArray());
			mongoClient = new MongoClient(new ServerAddress(connectionDetails.getHost()), Arrays.asList(credential),
					options);
		} else {
			mongoClient = new MongoClient(connectionDetails.getHost(), options);
		}
		return mongoClient;
	}

	/**
	 * Connect to the given database collection and insert a document.
	 * 
	 * @param docs
	 * @throws UnknownHostException
	 */
	public static void insert(DBCollection collection, DBObject doc) throws UnknownHostException {
		insert(collection, doc, true);
	}

	/**
	 * Connect to the given database collection and insert a document.
	 * 
	 * @param docs
	 * @throws UnknownHostException
	 */
	public static void insert(DBCollection collection, DBObject doc, boolean isLogEnabled) throws UnknownHostException {
		checkNotNull(collection);
		checkNotNull(doc);
		WriteResult result = collection.insert(doc);
		logResult(collection, 0, result);
	}

	/**
	 * Connect to the given database collection and insert a list of documents.
	 * 
	 * @param docs
	 * @throws UnknownHostException
	 */
	public static void insert(DBCollection collection, List<DBObject> docs) throws UnknownHostException {
		insert(collection, docs, true);
	}

	/**
	 * Connect to the given database collection and insert a list of documents.
	 * 
	 * @param docs
	 * @throws UnknownHostException
	 */
	public static void insert(DBCollection collection, List<DBObject> docs, boolean isLogEnabled)
			throws UnknownHostException {
		checkNotNull(collection);
		checkNotNull(docs);
		checkArgument(!docs.isEmpty(), "Cannot insert empty documents into the database");
		WriteResult result = collection.insert(docs);
		if (isLogEnabled)
			logResult(collection, docs.size(), result);
	}

	/**
	 * Check if the database has authentication enabled.
	 * 
	 * @param host
	 *            the name or IP address of the host.
	 * @return true if the database has authentication enabled; false otherwise.
	 * @throws UnknownHostException
	 *             if the host cannot be found.
	 */
	public static boolean isAuthEnabled(String host) throws UnknownHostException {
		boolean isAuthEnabled = false;
		try {
			new MongoClient(host).getDatabaseNames();
		} catch (CommandFailureException cfe) {
			if (cfe.getMessage().contains("\"errmsg\" : \"unauthorized\"")) {
				isAuthEnabled = true;
			}
		}
		return isAuthEnabled;
	}

	/**
	 * Log the result of the insert operation.
	 * 
	 * @param collection
	 * @param docs
	 * @param result
	 */
	private static void logResult(DBCollection collection, int docSize, WriteResult result) {
		if (result == null) {
			logger.error("Error inserting documents: no result object returned");
		} else if (result.getError() != null) {
			logger.error("Error inserting documents: " + result.getError());
		} else {
			String errMsg = docSize > 0 ? "Inserted " + docSize + " documents into the " + collection.getName()
					+ " collection" : "Inserted a document into the " + collection.getName() + " collection";
			logger.debug(errMsg);
		}
	}
}
