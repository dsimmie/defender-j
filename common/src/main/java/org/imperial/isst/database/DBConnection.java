/**
 * DBConnection.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.database;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Properties;

/**
 * Connection details entity.
 * 
 */
public class DBConnection {

	/**
	 * The host name.
	 */
	private final String host;

	/**
	 * The database name.
	 */
	private final String db;

	/**
	 * The collection name.
	 */
	private final String collection;

	/**
	 * The user name.
	 */
	private final String user;

	/**
	 * The db password for this user.
	 */
	private final transient String password;

	/**
	 * Initialises a new instance of the {@link DBConnection} class.
	 * 
	 * @param config
	 * @param connectionName
	 */
	public DBConnection(Properties config, String connectionName) {
		super();
		this.host = config.getProperty(connectionName + "host");
		this.db = config.getProperty(connectionName + "db");
		this.collection = config.getProperty(connectionName + "collection");
		this.user = config.getProperty(connectionName + "user");
		this.password = config.getProperty(connectionName + "password");
	}

	/**
	 * Initialises a new instance of the {@link DBConnection} class.
	 * 
	 * @param host
	 * @param db
	 * @param collection
	 */
	public DBConnection(String host, String db, String collection) {
		this(host, db, collection, null, null);
	}

	/**
	 * Initialises a new instance of the {@link DBConnection} class.
	 * 
	 * @param host
	 * @param db
	 * @param collection
	 * @param user
	 *            - optional
	 * @param password
	 *            - optional
	 */
	public DBConnection(String host, String db, String collection, String user, String password) {
		checkArgument(host != null && host != "");
		checkArgument(db != null && db != "");
		checkArgument(collection != null && collection != "");
		this.host = host;
		this.db = db;
		this.collection = collection;
		this.user = user;
		this.password = password;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DBConnection other = (DBConnection) obj;
		if (collection == null) {
			if (other.collection != null)
				return false;
		} else if (!collection.equals(other.collection))
			return false;
		if (db == null) {
			if (other.db != null)
				return false;
		} else if (!db.equals(other.db))
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	/**
	 * @return the collection
	 */
	public String getCollection() {
		return collection;
	}

	/**
	 * @return the db
	 */
	public String getDb() {
		return db;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	public String getPassword() {
		return password;
	}

	public String getUser() {
		return user;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((collection == null) ? 0 : collection.hashCode());
		result = prime * result + ((db == null) ? 0 : db.hashCode());
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	/**
	 * Check if this connection includes user information.
	 * <p>
	 * This is used for authentication.
	 * 
	 * @return true if the user info is available; false otherwise.
	 */
	public boolean hasUserDetails() {
		return user != null && password != null;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DBConnection [host=");
		builder.append(host);
		builder.append(", db=");
		builder.append(db);
		builder.append(", collection=");
		builder.append(collection);
		builder.append(", user=");
		builder.append(user);
		builder.append("]");
		return builder.toString();
	}

}
