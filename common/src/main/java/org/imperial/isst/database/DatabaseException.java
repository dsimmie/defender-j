/**
 * DatabaseException.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.database;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class DatabaseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1689128792942928760L;

	/**
	 * Initialises a new instance of the {@link DatabaseException} class.
	 * 
	 */
	public DatabaseException() {
	}

	/**
	 * Initialises a new instance of the {@link DatabaseException} class.
	 * 
	 * @param message
	 */
	public DatabaseException(String message) {
		super(message);
	}

	/**
	 * Initialises a new instance of the {@link DatabaseException} class.
	 * 
	 * @param message
	 * @param cause
	 */
	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Initialises a new instance of the {@link DatabaseException} class.
	 * 
	 * @param cause
	 */
	public DatabaseException(Throwable cause) {
		super(cause);
	}

}
