package org.imperial.isst.database;

public class DBGistFields {

	public static final String MULTI_TAGS = "multiUserTags";
	public static final String ALL_TAGS = "allHashTags";
	public static final String HASH_TAGS = "hashtags";
	public static final String TOPIC_MATCHES = "semanticMatches";
	public static final String LOCATION_MATCH = "locationMatch";
	public static final String CLUSTERS = "clusters";
	public static final String LINGO = "Lingo";
	public static final String STC = "STC";
}
