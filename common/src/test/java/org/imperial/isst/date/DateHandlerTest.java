package org.imperial.isst.date;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.imperial.isst.database.DBQueries;
import org.junit.Test;

import com.mongodb.DBObject;

public class DateHandlerTest {
	
	@Test
	public void test() throws ParseException {

		Date start = DateHandler.sdf.parse("2014-03-29");
		Date end = DateHandler.sdf.parse("2014-04-01");
		
		List<Date> range = DateHandler.datesBetween(start, end);
		System.out.println(range);
		for (Date date: range) {
			DBObject query = DBQueries.buildQueryForDay(date, "created_date");
			System.out.println(query.toString());
		}
	}

}
