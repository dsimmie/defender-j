package org.imperial.isst.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Test;

public class KeywordUtilsTest {

	private final String test1 = "having a coughing fit.";
	private final String test2 = "what a cough!";
	private final String test3 = "who knew i'd have such a bad cough today?";
	private final String test4 = "cough fever";
	private final String test5 = "what a high fever!";
	private final String test6 = "nasty #cough";
	private final String test7 = "cough cough fever cough.";
	private final String test8 = "brother had scarlet fever yesterday";
	private final String test9 = "fever?";
	
	@Test
	public void testLoadKeywords() {
		HashMap<String, List<String>> keywordMap = KeywordUtils.loadKeywords("symptom-keywords.csv");
		assertEquals("coughing", keywordMap.get("cough").get(0) );
	}
	
	@Test
	public void testkeywordMatch() {
		boolean match = false;

		
		List<String> keywords = new ArrayList<String>();
		keywords.add("cough");
		keywords.add("fever");
		
		match = KeywordUtils.keywordMatch(keywords, test1);
		assertTrue(!match);
		
		match = KeywordUtils.keywordMatch(keywords, test2);
		assertTrue(match);
		
		match = KeywordUtils.keywordMatch(keywords, test3);
		assertTrue(match);		
		
		match = KeywordUtils.keywordMatch(keywords, test4);
		assertTrue(match);	
		
		match = KeywordUtils.keywordMatch(keywords, test5);
		assertTrue(match);	
		
		match = KeywordUtils.keywordMatch(keywords, test6);
		assertTrue(match);	
		
		match = KeywordUtils.keywordMatch(keywords, test9);
		assertTrue(match);	
	}
	
	@Test
	public void testRemoveWhiteSpace() {
		String test = KeywordUtils.removeWhiteSpace("sore throat");
		assertEquals("sorethroat", test);
	}
	
	@Test
	public void testreturnMatches() {
		List<String> matches;
		List<String> keywords = new ArrayList<String>();
		Map<String, List<String>> keyMap = new HashMap<String, List<String>>();
		keywords.add("cough");	
		keywords.add("fever");
		keywords.add("#cough");
		keywords.add("scarlet fever");
		List<String> otherKeys = new ArrayList<String>();
		otherKeys.add("cough");
		otherKeys.add("jumbo");
		keyMap.put("scarlet fever", keywords);
		keyMap.put("fever", otherKeys);

		Pattern regex = KeywordUtils.buildRegex(keywords);
		matches = KeywordUtils.returnMatches(regex, test1);
		assertTrue(matches.isEmpty());
		
		matches = KeywordUtils.returnMatches(regex, test4);
		System.out.println(matches.toString());
		assertTrue(matches.contains("cough"));
		assertTrue(matches.contains("fever"));
		
		
		matches = KeywordUtils.returnMatches(regex, test6);
		System.out.println(matches.toString());	
		
		matches = KeywordUtils.returnMatches(regex, test7);
		assertTrue(matches.size() == 4);
		System.out.println(matches.toString());
		
		matches = KeywordUtils.returnMatches(regex, test8);
		System.out.println(matches.toString());
		assertTrue(keyMap.get("scarlet fever").contains("scarlet fever"));
		
		System.out.println(KeywordUtils.getReverseMap(keyMap));
		
		matches = KeywordUtils.returnMatches(regex, test9);
		System.out.println(matches.toString());
	}
	
	@Test
	public void testOccurrences() {
		String test = "this is a Test of the test occurrences in test's testing testing.";
		List<String> words = new ArrayList<String>();
		words.add("Test");
		words.add("in");
		Map<String, Integer> occs = KeywordUtils.getOccurrences(words, test);
		System.out.println(occs);
		assertTrue(occs.get("in") == 1);
	}

}
