package org.imperial.isst.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StringUtilsTest {

	@Test
	public void test() {
		String testString1 = "the quick brown fox jumped over the lazy dog.THEQUICKBROWNFOXJUMPEDOVERTHELAZYDOG";
		String testString2 = " 123!£$%^&*()_+{}[]:@#<>,.?/|~'";
		String testString3 = "&amp; what about this? http://this.com";
		
		String output1 = StringUtils.stripNonAscii(testString1);
		System.out.println(output1);
		assertEquals(testString1, output1);
		
		String output2 = StringUtils.stripNonAscii(testString2);
		System.out.println(output2);
		assertEquals(testString2, output2);		
		
		String output3 = StringUtils.parseHTMLTags(testString3);
		assertEquals("& what about this? http://this.com", output3);

		
	}

}
