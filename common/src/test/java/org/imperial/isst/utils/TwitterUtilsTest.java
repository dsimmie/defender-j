package org.imperial.isst.utils;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.imperial.isst.utils.TwitterUtils;
import org.junit.Test;

public class TwitterUtilsTest {

	@Test
	public void testDate() {

		String test1 = "Mon Jan 20 16:38:17 +0000 2014";
		String test2 = "Fri Feb 28 09:38:17 +0000 2014";
		String test3 = "This is not a time";
		boolean exception = false;
		Calendar cal = Calendar.getInstance();
		Date testdate;
		try {
			testdate = TwitterUtils.getTwitterDate(test1);
			
			cal.setTime(testdate); // someDate is a Date
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertEquals(Calendar.MONDAY, cal.get(Calendar.DAY_OF_WEEK));
		assertEquals(Calendar.JANUARY, cal.get(Calendar.MONTH));
		assertEquals(20, cal.get(Calendar.DAY_OF_MONTH));
		assertEquals(16, cal.get(Calendar.HOUR_OF_DAY));
		assertEquals(2014, cal.get(Calendar.YEAR));	
		
		try {
			testdate = TwitterUtils.getTwitterDate(test2);
			cal.setTime(testdate); // someDate is a Date
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		assertEquals(Calendar.FRIDAY, cal.get(Calendar.DAY_OF_WEEK));
		assertEquals(Calendar.FEBRUARY, cal.get(Calendar.MONTH));
		assertEquals(28, cal.get(Calendar.DAY_OF_MONTH));
		assertEquals(9, cal.get(Calendar.HOUR_OF_DAY));
		
		try {
			testdate = TwitterUtils.getTwitterDate(test3);
		} catch (ParseException e) {
			exception = true;
		}
		assertTrue(exception);	
		
	}
	
	@Test
	public void testLinks() {
		String testString = "this is a test http://t.co/aAA90WqqXG http://bit.ly/";
		String test2 = "no links here.";
		String test3 = "(this is a link http://t.co/a8)";
		String test4 = "(http://t.co/uxAB7d9mwX";
		ArrayList<String> links = new ArrayList<String>();
		
		String out = TwitterUtils.removeLinks(testString, links);
		assertEquals("this is a test", out);
		assertEquals(links.get(0), "http://t.co/aAA90WqqXG");
		assertEquals(2, links.size());
		
		links.clear();
		
		out = TwitterUtils.removeLinks(test2, links);
		assertEquals(test2, out);
		assertTrue(links.size() ==0);	
		
		links.clear();
		
		out = TwitterUtils.removeLinks(test3, links);
		System.out.println(out);
		
		out = TwitterUtils.removeLinks(test4, links);
	}
	
	@Test
	public void testMentions() {
		String testString = "@dave this @stephen @AP_McCoy a test http://t.co/aAA90WqqXG";
		String test2 = "no mentions here.";
		String test3 = "A coffee then think about home... (at @BreadandMilkUk) http://t.co/emMziLq7eK";
		String test4 = "@IainFer @AP_McCoy @ayrracecourse tips posted http://t.co/65xQSta4EP";
		ArrayList<String> links = new ArrayList<String>();
		
		String out = TwitterUtils.removeMentions(testString, links);
		assertEquals("this   a test http://t.co/aAA90WqqXG", out);
		assertEquals("@dave",links.get(0));
		assertEquals("@stephen",links.get(1));
		System.out.println(out);
		links.clear();
		
		out = TwitterUtils.removeLinks(test2, links);
		System.out.println(out);
		assertEquals(test2, out);
		assertTrue(links.size() ==0);	
		
		out = TwitterUtils.removeMentions(test3, links);
		System.out.println(out);
		out = TwitterUtils.removeMentions(test4, links);
		System.out.println(out);
	}
	
	@Test
	public void testTags() {
		String testString = "this @dave is a test http://t.co/aAA90WqqXG #rad #awesome ( #weird8)";
		String test2 = "no links here.";
		ArrayList<String> links = new ArrayList<String>();
		
		String out = TwitterUtils.findHashTags(testString, links);
		assertEquals(out, out);
		assertEquals("#rad", links.get(0));
		assertEquals("#awesome", links.get(1));
		links.clear();
		
		out = TwitterUtils.removeLinks(test2, links);
		assertEquals(test2, out);
		assertTrue(links.size() ==0);	
	}

}
