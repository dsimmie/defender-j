/**
 * PlacemakerLocationFinderTest.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.location.impl;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Collection;

import org.imperial.isst.location.Location;
import org.imperial.isst.location.Source;
import org.junit.Test;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class PlacemakerLocationFinderTest {

	private final String testAppId = "A1b3nzDV34FQHUkW7BbXT0MbJG8jN.8r4HDEVeAO3X8Ux.5pNEwMRwfVWYrqP6rJBcux";

	private final PlacemakerLocationFinder target = new PlacemakerLocationFinder(testAppId);

	private final String sampleText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<contentlocation xmlns:yahoo=\"http://www.yahooapis.com/v1/base.rng\" xmlns:xml=\"http://www.w3.org/XML/1998/namespace\" xmlns=\"http://wherein.yahooapis.com/v1/schema\" xml:lang=\"en\">\n<processingTime>0.003536</processingTime>\n<version>2.0.6.7 build 131228</version>\n<documentLength>44</documentLength>\n<document>\n<administrativeScope>\n<woeId>23424781</woeId>\n<type>Country</type>\n<name><![CDATA[China]]></name>\n<centroid>\n<latitude>36.8944</latitude>\n<longitude>104.166</longitude>\n</centroid>\n</administrativeScope>\n<geographicScope>\n<woeId>23424781</woeId>\n<type>Country</type>\n<name><![CDATA[China]]></name>\n<centroid>\n<latitude>36.8944</latitude>\n<longitude>104.166</longitude>\n</centroid>\n</geographicScope>\n<localScopes>\n<localScope>\n<woeId>23424781</woeId>\n<type>Country</type>\n<name><![CDATA[China (Country)]]></name>\n<centroid>\n<latitude>36.8944</latitude>\n<longitude>104.166</longitude>\n</centroid>\n<southWest>\n<latitude>15.7754</latitude>\n<longitude>73.5577</longitude>\n</southWest>\n<northEast>\n<latitude>53.5606</latitude>\n<longitude>134.774</longitude>\n</northEast>\n<ancestors>\n</ancestors>\n</localScope>\n</localScopes>\n<extents>\n<center>\n<latitude>36.8944</latitude>\n<longitude>104.166</longitude>\n</center>\n<southWest>\n<latitude>15.7754</latitude>\n<longitude>73.5577</longitude>\n</southWest>\n<northEast>\n<latitude>53.5606</latitude>\n<longitude>134.774</longitude>\n</northEast>\n</extents>\n<placeDetails>\n<placeId>1</placeId>\n<place>\n<woeId>23424781</woeId>\n<type>Country</type>\n<name><![CDATA[China]]></name>\n<centroid>\n<latitude>36.8944</latitude>\n<longitude>104.166</longitude>\n</centroid>\n</place>\n<placeReferenceIds>4 5 6 7</placeReferenceIds>\n<matchType>0</matchType>\n<weight>4</weight>\n<confidence>8</confidence>\n</placeDetails>\n<referenceList>\n<reference>\n<woeIds>23424781</woeIds>\n<placeReferenceId>4</placeReferenceId>\n<placeIds>1</placeIds>\n<start>-1</start>\n<end>-1</end>\n<isPlaintextMarker>1</isPlaintextMarker>\n<text><![CDATA[China]]></text>\n<type>invalid</type>\n<xpath><![CDATA[]]></xpath>\n</reference>\n<reference>\n<woeIds>23424781</woeIds>\n<placeReferenceId>5</placeReferenceId>\n<placeIds>1</placeIds>\n<start>-1</start>\n<end>-1</end>\n<isPlaintextMarker>1</isPlaintextMarker>\n<text><![CDATA[China]]></text>\n<type>invalid</type>\n<xpath><![CDATA[]]></xpath>\n</reference>\n<reference>\n<woeIds>23424781</woeIds>\n<placeReferenceId>6</placeReferenceId>\n<placeIds>1</placeIds>\n<start>-1</start>\n<end>-1</end>\n<isPlaintextMarker>1</isPlaintextMarker>\n<text><![CDATA[Chinese]]></text>\n<type>invalid</type>\n<xpath><![CDATA[]]></xpath>\n</reference>\n<reference>\n<woeIds>23424781</woeIds>\n<placeReferenceId>7</placeReferenceId>\n<placeIds>1</placeIds>\n<start>30</start>\n<end>35</end>\n<isPlaintextMarker>1</isPlaintextMarker>\n<text><![CDATA[China]]></text>\n<type>plaintext</type>\n<xpath><![CDATA[]]></xpath>\n</reference>\n</referenceList>\n</document>\n</contentlocation>\n";

	private final String sample2 = "London. Birmingham.";

	@Test
	public void test() throws IOException {
		Collection<? extends Location> locations = target.find(sampleText);

		for (Location location : locations) {
			System.out.println(location);
		}

		assertEquals(1, locations.size());

		Location location = locations.iterator().next();
		assertEquals("China", location.getCountry());
		assertEquals(Source.PLACEMAKER, location.getSource());

		locations = target.find(sample2);
		System.out.println("Birmingham?");
		System.out.println(locations.toString());

		locations = target.find("United Kingdom");
		// locations = target.find(sample3);
		// System.out.println(locations.toString());
	}
}
