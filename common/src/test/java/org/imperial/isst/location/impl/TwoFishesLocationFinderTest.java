package org.imperial.isst.location.impl;

import java.io.IOException;

import org.imperial.isst.location.Location;
import org.junit.Test;

public class TwoFishesLocationFinderTest {
	private final String twoFishesURI = "http://ib-dsimmie.ib.ic.ac.uk:8081/?debug=0&maxInterpretations=2&query=";
	private final TwoFishesLocationFinder target = new TwoFishesLocationFinder(twoFishesURI);
	
	@Test
	public void test() {
		Location loc;
		try {
			loc = target.find("Essex").get(0);
			System.out.println(loc.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
