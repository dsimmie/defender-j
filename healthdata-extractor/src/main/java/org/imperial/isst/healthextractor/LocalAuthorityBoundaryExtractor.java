/**
 * LocalAuthorityBoundaryExtractor.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.healthextractor;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.imperial.isst.database.DBConnection;
import org.imperial.isst.database.DBWrapper;
import org.imperial.isst.date.DateFormat;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.location.LocalAuthorityHealthDataLoader;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.utils.TableUtils;
import org.imperial.isst.utils.Utils;

import com.google.common.collect.ArrayTable;
import com.google.common.collect.ImmutableList;
import com.mongodb.BasicDBList;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

/**
 * The {@link LocalAuthorityBoundaryExtractor} class filters and stores the local authority boundary data for the UK
 * from the <b>HPA GP In-Hours</b> data (<a href="http://bit.ly/1jndP32">here</a>) and the <b>Ordnance Survey Boundary
 * Line</b> data (<a href="https://www.ordnancesurvey.co.uk/opendatadownload/products.html">here</a>)
 * 
 * @author Donal Simmie
 * 
 */
public class LocalAuthorityBoundaryExtractor extends LocalAuthorityHealthExtractor {

	/**
	 * The noise node - not associated with any specific geographic area.
	 */
	private static final String NOISE_NODE = "0";

	/**
	 * The path to the local authorities geojson file.
	 */
	private static final String ALL_LOCAL_AUTHORITIES_PATH = System.getProperty("user.home")
			+ "/Dropbox/Defender/data/all-local-authorities.geojson";

	/**
	 * The population column label from the GP in-hours data.
	 */
	private final static String POP_COLUMN = "Denominator Population";

	/**
	 * The local authority code column label from the GP in-hours data.
	 */
	private final static String CODE_COLUMN = "ONS Upper Tier LA Code";

	/**
	 * The main application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LocalAuthorityBoundaryExtractor extractor = null;
		try {
			extractor = new LocalAuthorityBoundaryExtractor("/config.properties");
			extractor.createTimeSeriesData();
		} catch (IOException e) {
			extractor.logger.error("Exception creating the filtered local authority GEOJSON files.", e);
		} catch (ParseException e) {
			extractor.logger.error("Exception parsing .", e);
		}
	}

	/**
	 * The node assigner component.
	 */
	protected NodeAssigner nodeAssigner;

	/**
	 * The populations of the nodes in our node network.
	 */
	private final Map<String, Double> nodePopulations;

	/**
	 * The populations of the local authority region as reported by the most recent report.
	 */
	private final Map<String, Double> laPopulations;

	/**
	 * The data loader.
	 */
	private LocalAuthorityHealthDataLoader dataLoader;

	/**
	 * The weekly count data stored in tabular format. <Date, String, Double> = <Day, LaCode, CaseCount>
	 */
	private ArrayTable<Date, String, Double> iliResults;
	private ArrayTable<Date, String, Double> vomResults;
	private ArrayTable<Date, String, Double> diaResults;
	private ArrayTable<Date, String, Double> gasResults;

	/**
	 * The interpolated daily count data stored in tabular format. <Date, String, Double> = <Day, LaCode, CaseCount>
	 */
	private ArrayTable<Date, String, Double> dailyIliResults;
	private ArrayTable<Date, String, Double> dailyVomResults;
	private ArrayTable<Date, String, Double> dailyDiaResults;
	private ArrayTable<Date, String, Double> dailyGasResults;


	/**
	 * The base local authority output path.
	 */
	private final String LA_PATH = System.getProperty("user.home") + "/hpa-data/la/";

	/**
	 * Initialises a new instance of the {@link LocalAuthorityBoundaryExtractor} class.
	 * 
	 * @param configFile
	 * @throws UnknownHostException
	 */
	public LocalAuthorityBoundaryExtractor(String configFile) throws UnknownHostException {
		super(configFile);
		nodePopulations = new HashMap<String, Double>();
		laPopulations = new HashMap<String, Double>();
		nodeAssigner = new NodeAssigner(connectToDB("node"));
		extractData();
		getPopulationData();
	}

	/**
	 * Map the data from the local authority areas to nodes.
	 * 
	 * @param date
	 */
	private void addDataToResults(Date date) {
		System.out.println(date);
		checkNotNull(date);
		for (String node : nodeAssigner.getNodeNames()) {
			if (!node.equals(NOISE_NODE)) {
				double iliCasesForNode = 0.0;
				double vomCasesForNode = 0.0;
				double diaCasesForNode = 0.0;
				double gasCasesForNode = 0.0;
				double populationForNode = 0;
				for (String laCode : laPopulations.keySet()) {
					Double intersection = dataLoader.getIntersection(laCode, node);
					logger.debug("LA: " + laCode + " Node: " + node + " intersection: " + intersection.toString()
							+ " population: " + laPopulations.get(laCode));
					iliCasesForNode = getCasesForNode(iliCounts, date, iliCasesForNode, laCode, intersection);
					vomCasesForNode = getCasesForNode(vomCounts, date, vomCasesForNode, laCode, intersection);
					diaCasesForNode = getCasesForNode(diaCounts, date, diaCasesForNode, laCode, intersection);
					gasCasesForNode = getCasesForNode(gasCounts, date, gasCasesForNode, laCode, intersection);

					if (laPopulations.get(laCode) >= 0) {
						populationForNode = populationForNode + (laPopulations.get(laCode) * intersection);
					}
				}
				double iliRes = 0.0;
				double vomRes = 0.0;
				double diaRes = 0.0;
				double gasRes = 0.0;
				if (populationForNode != 0) {
					iliRes = iliCasesForNode;
					vomRes = vomCasesForNode;
					diaRes = diaCasesForNode;
					gasRes = gasCasesForNode;
				}
				if (!nodePopulations.containsKey(node)) {
					nodePopulations.put(node, populationForNode);
				}

				try {
					iliResults.put(date, node, iliRes);
					vomResults.put(date, node, vomRes);
					diaResults.put(date, node, diaRes);
					gasResults.put(date, node, gasRes);
				} catch (NullPointerException ne) {
					ne.printStackTrace();
				}
			} else {
				iliResults.put(date, node, 0.0);
				vomResults.put(date, node, 0.0);
				diaResults.put(date, node, 0.0);
				gasResults.put(date, node, 0.0);
			}
		}
	}

	/**
	 * 
	 * @see org.imperial.isst.healthextractor.BaseHealthDataExtractor#createCountTables()
	 */
	@Override
	protected void createCountTables() {
		List<String> laCodes = getLACodes();
		// Create tables of Date/Node/Count to update the graph
		iliCounts = ArrayTable.create(results.get(0).rowKeyList(), laCodes);
		vomCounts = ArrayTable.create(results.get(0).rowKeyList(), laCodes);
		diaCounts = ArrayTable.create(results.get(0).rowKeyList(), laCodes);
		gasCounts = ArrayTable.create(results.get(0).rowKeyList(), laCodes);
		TableUtils.zeroTable(iliCounts);
		TableUtils.zeroTable(vomCounts);
		TableUtils.zeroTable(diaCounts);
		TableUtils.zeroTable(gasCounts);
	}

	/**
	 * Create the local authority time-series data for cases.
	 * 
	 * @throws ParseException
	 * @throws IOException
	 */
	private void createTimeSeriesData() throws ParseException, IOException {
		loadFilteredGeoJsonBoundaries();

		List<Date> dates = getDateRange(resultFiles);

		// The first date value is the week end hence we have to subtract six days to get the first day of count data.
		Date start = dates.get(0);
		Date end = dates.get(dates.size() - 1);

		List<Date> dayDates = DateHandler.datesBetween(start, end);

		iliResults = ArrayTable.create(dates, nodeAssigner.getNodeNames());
		vomResults = ArrayTable.create(dates, nodeAssigner.getNodeNames());
		diaResults = ArrayTable.create(dates, nodeAssigner.getNodeNames());
		gasResults = ArrayTable.create(dates, nodeAssigner.getNodeNames());
		dailyIliResults = ArrayTable.create(dayDates, nodeAssigner.getNodeNames());
		dailyVomResults = ArrayTable.create(dayDates, nodeAssigner.getNodeNames());
		dailyDiaResults = ArrayTable.create(dayDates, nodeAssigner.getNodeNames());
		dailyGasResults = ArrayTable.create(dayDates, nodeAssigner.getNodeNames());

		for (int i = 0; i < resultFiles.size(); i++) {
			addDataToResults(dates.get(i));
		}

		interpolateResults();
		// graphBuilder.insertDataIntoGraph(dailyResults);

		try {
			TableUtils.outputToCSV(dailyIliResults, LA_PATH + "extractedDailyILI.csv", DateFormat.RAW);
			TableUtils.outputToCSV(dailyVomResults, LA_PATH + "extractedDailyVom.csv", DateFormat.RAW);
			TableUtils.outputToCSV(dailyDiaResults, LA_PATH + "extractedDailyDia.csv", DateFormat.RAW);
			TableUtils.outputToCSV(dailyGasResults, LA_PATH + "extractedDailyGas.csv", DateFormat.RAW);

			TableUtils.outputToCSV(iliResults, LA_PATH + "ili.csv", DateFormat.RAW);
			TableUtils.outputToCSV(vomResults, LA_PATH + "vom.csv", DateFormat.RAW);
			TableUtils.outputToCSV(diaResults, LA_PATH + "dia.csv", DateFormat.RAW);
			TableUtils.outputToCSV(gasResults, LA_PATH + "gas.csv", DateFormat.RAW);
		} catch (IOException e) {
			logger.error("Exception outputting the results to file.", e);
		}
	}

	/**
	 * Get the case count for this date.
	 * 
	 * @param date
	 * @param casesForNode
	 * @param laCode
	 * @param intersection
	 * @return
	 */
	private double getCasesForNode(ArrayTable<Date, String, Double> counts, Date date, double casesForNode,
			String laCode, Double intersection) {
		Double cases = 0.0;
		if (counts.get(date, laCode) != null) {
			cases = counts.get(date, laCode);
		}

		casesForNode = casesForNode + (cases * intersection);
		return casesForNode;
	}

	/**
	 * Extract the population data for each Local Authority Area.
	 */
	private void getPopulationData() {
		for (int i = 0; i < getNumberOfAreas(); i++) {
			ArrayTable<Date, String, String> areaResults = results.get(i);
			Date latestDate = areaResults.rowKeyList().get(areaResults.rowKeyList().size() - 1);
			String populationText = areaResults.get(latestDate, POP_COLUMN);
			double population = populationText.equals("-") ? -1 : Double.parseDouble(populationText);
			String code = areaResults.get(latestDate, CODE_COLUMN);
			laPopulations.put(code, population);
		}
	}

	/**
	 * Interpolate all the weekly figures to daily values.
	 */
	private void interpolateResults() {
		interpolateWeeklyCounts(iliResults, dailyIliResults);
		interpolateWeeklyCounts(vomResults, dailyVomResults);
		interpolateWeeklyCounts(diaResults, dailyDiaResults);
		interpolateWeeklyCounts(gasResults, dailyGasResults);
	}

	/**
	 * Interpolate the data from weekly period to daily values.
	 */
	private void interpolateWeeklyCounts(ArrayTable<Date, String, Double> weeklyResults,
			ArrayTable<Date, String, Double> dailyResults) {

		ImmutableList<Date> dates = weeklyResults.rowKeyList();

		for (int i = 0; i < dates.size() - 1; i++) {
			for (String node : nodeAssigner.getNodeNames()) {
				double thisVal = weeklyResults.get(dates.get(i), node);
				double nextVal = weeklyResults.get(dates.get(i + 1), node);
				for (int j = 0; j < 7; j++) {
					Date currentDay = DateHandler.addDaysTo(dates.get(i), j, true);
					double ratio = j / 7.0;
					double val = thisVal + (nextVal - thisVal) * ratio;

					dailyResults.put(currentDay, node, val);
				}
				// Add the last day value.
				if (i == dates.size() - 2) {
					dailyResults.put(dates.get(i + 1), node, nextVal);
				}
			}
		}
	}

	/**
	 * Load all of the boundary data, filter to the local authorities in the HPA GP in hours data and store that data in
	 * the database for future use.
	 * 
	 * @throws IOException
	 * 
	 */
	private void loadFilteredGeoJsonBoundaries() throws IOException {
		DBObject laBoundaries = loadLocalAuthorityBoundaries();
		dataLoader = new LocalAuthorityHealthDataLoader(laBoundaries, nodeAssigner);
		storeLocalAuthorityBoundaries(laBoundaries);
	}

	/**
	 * Load the polygon data from the local authorities file.
	 * 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private DBObject loadLocalAuthorityBoundaries() throws IOException {
		String laBoundariesFile = Utils.readFile(ALL_LOCAL_AUTHORITIES_PATH);
		DBObject laBoundaries = (DBObject) JSON.parse(laBoundariesFile);
		BasicDBList features = (BasicDBList) laBoundaries.get("features");
		int len = features.size();

		List<String> laCodes = getLACodes();

		for (int i = len - 1; i >= 0; i--) {
			String code = ((DBObject) ((DBObject) features.get(i)).get("properties")).get("CODE").toString();

			if (!laCodes.contains(code)) {
				features.remove(i);
			}
		}

		logger.debug("Count of local authority codes from HPA data: " + laCodes.size());
		logger.debug("Initially had " + len + " features from Ordnance Survey Boundary Line data. There are "
				+ features.size() + " features remaining after filtering.");

		return laBoundaries;
	}

	/**
	 * Store the filtered local authority polygon data in the database.
	 * 
	 * @param laBoundaries
	 * @throws UnknownHostException
	 */
	private void storeLocalAuthorityBoundaries(DBObject laBoundaries) throws UnknownHostException {
		DBCollection laCollection = DBWrapper.connectToCollection(new DBConnection(configProperties
				.getProperty("phecentreshost"), configProperties.getProperty("phecentresdb"), configProperties
				.getProperty("lascollection")));
		BasicDBList features = (BasicDBList) laBoundaries.get("features");
		for (int i = 0; i < features.size(); i++) {
			DBObject feature = (DBObject) features.get(i);
			DBWrapper.insert(laCollection, feature, false);
		}
	}
}
