package org.imperial.isst.healthextractor;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.imperial.isst.location.GeoCoords;
import org.imperial.isst.utils.TableUtils;
import org.neo4j.graphdb.Node;

import au.com.bytecode.opencsv.CSVReader;

import com.google.common.collect.ArrayTable;

public class LocalAuthorityHealthExtractor extends BaseHealthDataExtractor {

	public static void main(String[] args) {
		BaseHealthDataExtractor extractor;

		extractor = new LocalAuthorityHealthExtractor("/config.properties");
		extractor.extractData();
		extractor.outputData();
	}

	private final Map<String, String> gpNodes;

	private final HashMap<String, GeoCoords> locations;

	public LocalAuthorityHealthExtractor(String configFile) {
		super(configFile);

		gpNodes = new HashMap<String, String>();
		locations = loadLocations("lats.csv");

		for (String loc : locations.keySet()) {
			gpNodes.put(loc, nodeAssigner.getClosestNode(locations.get(loc)));
		}
	}

	@Override
	protected int getFirstIllnessColumn() {
		return 7;
	}

	@Override
	protected int getNumberOfAreas() {
		// Number of local health authorities
		return 149;
	}

	@Override
	protected int getWorkSheet() {
		return 2;
	}

	private HashMap<String, GeoCoords> loadLocations(String locationFile) {
		HashMap<String, GeoCoords> outputMap = new HashMap<String, GeoCoords>();
		String[] currentLine;
		InputStream inputStream = BaseHealthDataExtractor.class.getClassLoader().getResourceAsStream(locationFile);

		CSVReader reader = new CSVReader(new InputStreamReader(inputStream));
		try {
			currentLine = reader.readNext();
			while ((currentLine = reader.readNext()) != null) {
				GeoCoords geo = new GeoCoords(Double.valueOf(currentLine[1]), Double.valueOf(currentLine[0]));
				outputMap.put(currentLine[2], geo);

			}
			reader.close();
		} catch (IOException e) {
			logger.error("Exception loading locations from file", e);
		}

		return outputMap;
	}

	@Override
	protected double normaliseCount(Node node, double count) {
		int tweetCount = (int) node.getProperty("count");
		if (count == 0) {
			count = 1;
		}
		return count / tweetCount;
	}

	@Override
	protected void updateCountTables() {
		// For each GP practice, update the correct node with values for all dates that we have
		for (ArrayTable<Date, String, String> res : results) {
			// String node = gpNodes.get(res.at(0, 1));
			String code = res.at(0, 0);
			for (Date date : res.rowKeyList()) {
				Double ili = getRowValue(res, date, "Influenza-like illness Observed number of cases");
				Double vom = getRowValue(res, date, "Vomiting Observed number of cases");
				Double dia = getRowValue(res, date, "Diarrhoea Observed number of cases");
				Double gas = getRowValue(res, date, "Gastroenteritis Observed number of cases");

				TableUtils.incrementTableByValue(iliCounts, date, code, ili);
				TableUtils.incrementTableByValue(vomCounts, date, code, vom);
				TableUtils.incrementTableByValue(diaCounts, date, code, dia);
				TableUtils.incrementTableByValue(gasCounts, date, code, gas);
			}
		}
	}
}
