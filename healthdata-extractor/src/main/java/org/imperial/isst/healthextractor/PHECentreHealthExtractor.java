package org.imperial.isst.healthextractor;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.imperial.isst.utils.TableUtils;

import com.google.common.collect.ArrayTable;
import com.mongodb.BasicDBList;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.vividsolutions.jts.geom.Polygon;


public class PHECentreHealthExtractor extends BaseHealthDataExtractor {

	private DBCollection pheCentres;
	private Map<String, List<String>> nodesPerCentre;
	
	public PHECentreHealthExtractor(String configFile) {
		super(configFile);
		try {
			pheCentres = connectToDB("phecentres");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		nodesPerCentre = loadConfigData();
	}
	
	private Map<String, List<String>> loadConfigData() {
		Map<String, Polygon> centrePolygons = new HashMap<String, Polygon>();
		DBObject centres = pheCentres.find().next();
		BasicDBList features = (BasicDBList) centres.get("features");
		for (Object feature: features) {
			Polygon centrePolygon = nodeAssigner.createPolygonFromFeature((DBObject)feature);
			String code = ((DBObject)((DBObject)feature).get("properties")).get("code").toString();
			centrePolygons.put(code, centrePolygon);			
		}
		
		return nodeAssigner.assignNodesToPolygons(centrePolygons);
	}
	
	protected int getFirstIllnessColumn() {
		return 5;
	}
	
	protected int getWorkSheet() {
		return 1;
	}

	@Override
	protected int getNumberOfAreas() {

		return 15;
	}

	@Override
	protected void updateCountTables() {
		// For each area - update all the nodes in the area with the counts
		for (ArrayTable<Date, String, String> res: results) {
			List<String> nodes = nodesPerCentre.get( res.at(0, 1).toLowerCase() );
			if (nodes != null) {
				for (String node: nodes) {
					for (Date date: res.rowKeyList()) {
						Double ili = getRowValue(res, date, "Influenza-like illness Rate per 100,000");
						Double vom = getRowValue(res, date, "Vomiting Rate per 100,000");
						Double dia = getRowValue(res, date, "Diarrhoea Rate per 100,000");
						Double gas = getRowValue(res, date, "Gastroenteritis Rate per 100,000");
						
						TableUtils.incrementTableByValue(iliCounts, date, node, ili);
						TableUtils.incrementTableByValue(vomCounts, date, node, vom);
						TableUtils.incrementTableByValue(diaCounts, date, node, dia);
						TableUtils.incrementTableByValue(gasCounts, date, node, gas);
					}	
				}
			}
		}
	}

	
	public static void main(String[] args) {
		BaseHealthDataExtractor extractor;

		extractor = new PHECentreHealthExtractor(
				"/config.properties");
		extractor.extractData();
		extractor.outputData();

		
	}

}
