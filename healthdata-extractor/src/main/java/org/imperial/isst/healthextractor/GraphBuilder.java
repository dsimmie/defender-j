package org.imperial.isst.healthextractor;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.graph.GraphRelationship.RelTypes;
import org.imperial.isst.graph.Neo4JFactory;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.index.lucene.QueryContext;

public class GraphBuilder extends DefenderApplication {

	private Element dynamicNodeAttributes;
	private Element staticNodeAttributes;
	private Element dynamicEdgeAttributes;
	private Element graph;
	private Element nodes;
	private Element edges;
	private Document doc;
	private HashMap<String, Element> nodeMap;
	private HashMap<String, Element> edgeMap;
	
	private GraphDatabaseService graphDb;
	
	private String[] staticProps = {"name", "locality", "latitude", "longitude", "count", "timestamp", "date"};
	
	private Set<String> staticPropsSet;
	
	public GraphBuilder(String configFile) {
		super(configFile);
		Element root = new Element("gexf");
		graph = new Element("graph");
		graph.addAttribute(new Attribute("mode", "dynamic"));
		graph.addAttribute(new Attribute("defaultededgetype", "directed"));
		graph.addAttribute(new Attribute("timeformat", "date"));
		root.appendChild(graph);
		
		dynamicNodeAttributes = new Element("attributes");
		dynamicNodeAttributes.addAttribute(new Attribute("class", "node"));
		dynamicNodeAttributes.addAttribute(new Attribute("mode", "dynamic"));
		graph.appendChild(dynamicNodeAttributes);
		
		staticNodeAttributes = new Element("attributes");
		staticNodeAttributes.addAttribute(new Attribute("class", "node"));
		staticNodeAttributes.addAttribute(new Attribute("mode", "static"));
		graph.appendChild(staticNodeAttributes);
		
		dynamicEdgeAttributes = new Element("attributes");
		dynamicEdgeAttributes.addAttribute(new Attribute("class", "edge"));
		dynamicEdgeAttributes.addAttribute(new Attribute("mode", "dynamic"));
		graph.appendChild(dynamicEdgeAttributes);
		
		nodes = new Element("nodes");
		graph.appendChild(nodes);
		
		edges = new Element("edges");
		graph.appendChild(edges);
		
		doc = new Document(root);
		
		nodeMap = new HashMap<String, Element>();
		edgeMap = new HashMap<String, Element>();
		
		graphDb = Neo4JFactory.createNeo4JService("http://localhost:7474/db/data");

		staticPropsSet = new HashSet<String>(Arrays.asList(staticProps));
	}
	
	public void buildDynamicGraph(Date start, Date end) {
		// Connect to the DB and 
		List<Date> dateRange = org.imperial.isst.date.DateHandler.datesBetween(start, end);
		System.out.println(dateRange.toString());
		boolean nodesCreated = false;
		boolean propertiesAdded = false;

		addStaticNodeAttribute("0", "latitude", "float");
		addStaticNodeAttribute("1", "longitude", "float");
		addStaticNodeAttribute("2", "count", "float");
		addStaticNodeAttribute("3", "name", "string");
		addStaticNodeAttribute("4", "locality", "string");
		
		addDynamicEdgeAttribute("0", "weight", "float");
		

		// Fetch the nodes for this date from Neo4J and update them
		try ( Transaction tx = graphDb.beginTx() )
		{
			IndexManager index = graphDb.index();
			Index<Node> timestamps = index.forNodes( "timestamps" );
			for (Date date: dateRange) {
				Calendar cal = DateHandler.getCalendar();
				cal.setTime(date);
				cal.add(Calendar.DATE, 1);
				Date endDate = cal.getTime();
				String dateStr = DateHandler.sdf.format(date);
				String endDateStr = DateHandler.sdf.format(endDate);
				// Build a list of nodes at the timestamp

				IndexHits<Node> nodeIndex = timestamps.query( QueryContext.numericRange("timestamp", date.getTime(), date.getTime()));
				//IndexHits<Node> nodeIndex = timestamps.get("timestamp", date.getTime());
				
				try {
					for (Node node: nodeIndex) {
						String name = node.getProperty("name").toString();
						Iterable<Relationship> rels =  node.getRelationships(Direction.OUTGOING, RelTypes.SPATIAL);
						if (!nodesCreated) {
							addNode(name, name);
							addValueToNode(name, "0", String.valueOf(node.getProperty("latitude")) );
							addValueToNode(name, "1", String.valueOf(node.getProperty("longitude")) );
							addValueToNode(name, "2", node.getProperty("count").toString());
							addValueToNode(name, "3", name);
							String locality = "";
							try {
								locality = node.getProperty("locality").toString();
							} catch (NotFoundException e) {
								locality = "Noise";
							}
							addValueToNode(name, "4", locality);
							
							for (Relationship rel: rels) {
								String endName = rel.getEndNode().getProperty("name").toString();
								addEdge(name, endName);
							}
						}
						int count = 0;
						for (String property: node.getPropertyKeys()) {
							if (!staticPropsSet.contains(property)) {
								if (!propertiesAdded) {
									addDynamicNodeAttribute(String.valueOf(5+count), property, "float");
								}
								int nodeCount = (Integer)node.getProperty("count");
								if (nodeCount == 0) {
									nodeCount = 1;
								}
								addValueToNode(name, String.valueOf(5+count), String.valueOf(((Double)node.getProperty(property)/nodeCount)*100000), dateStr, endDateStr);
								count++;
							}
						}
						propertiesAdded = true;

						for (Relationship rel: rels) {
							String endName = rel.getEndNode().getProperty("name").toString();
							Integer numberTravelling = (Integer) rel.getProperty("all");
							addValueToEdge(getEdgeId(name, endName), "0", numberTravelling.toString(), dateStr, endDateStr);
						}
//						try {
//							Double gpILI = (Double) node.getProperty("GPILICount");
//							Double gpVom = (Double) node.getProperty("GPVomCount");
//							Double gpDia = (Double) node.getProperty("GPDiaCount");
//							Double gpGas = (Double) node.getProperty("GPGasCount");
//							addValueToNode(name, "0", String.valueOf(gpILI), dateStr, endDateStr);
//							addValueToNode(name, "1", String.valueOf(gpVom), dateStr, endDateStr);
//							addValueToNode(name, "2", String.valueOf(gpDia), dateStr, endDateStr);
//							addValueToNode(name, "3", String.valueOf(gpGas), dateStr, endDateStr);
//						} catch (Exception e) {
//							
//						}



					}
				} finally {
					nodeIndex.close();
				}
				nodesCreated = true;
			}
			tx.success();
		}

		printToFile("gpdata.gexf");
		System.out.println("Gexf generated");
	}
	
	public void addNode(String id, String label) {
		Element node = new Element("node");
		node.addAttribute(new Attribute("id", id));
		node.addAttribute(new Attribute("label", label));
		Element attvalues = new Element("attvalues");
		node.appendChild(attvalues);
		
		nodeMap.put(id, node);
		nodes.appendChild(node);
	}
	
	public void addEdge(String source, String target) {
		Element edge = new Element("edge");
		String id = getEdgeId(source, target);
		edge.addAttribute(new Attribute("id", id));
		edge.addAttribute(new Attribute("source", source));
		edge.addAttribute(new Attribute("target", target));
		Element attvalues = new Element("attvalues");
		edge.appendChild(attvalues);
		
		edgeMap.put(id, edge);
		edges.appendChild(edge);
	}
	
	private String getEdgeId(String source, String target) {
		return source+"-"+target;
	}
	
	public void addValueToNode(String nodeKey, String attributeID, String value, String start, String end) {
		Element nodeAtt = nodeMap.get(nodeKey).getFirstChildElement("attvalues");
		Element attValue = new Element("attvalue");
		attValue.addAttribute(new Attribute("for", attributeID));
		attValue.addAttribute(new Attribute("value", value));
		attValue.addAttribute(new Attribute("start", start));
		attValue.addAttribute(new Attribute("end", end));
		nodeAtt.appendChild(attValue);
	}
	
	public void addValueToEdge(String edgeKey, String attributeID, String value, String start, String end) {
		Element edgeAtt = edgeMap.get(edgeKey).getFirstChildElement("attvalues");
		Element attValue = new Element("attvalue");
		attValue.addAttribute(new Attribute("for", attributeID));
		attValue.addAttribute(new Attribute("value", value));
		attValue.addAttribute(new Attribute("start", start));
		attValue.addAttribute(new Attribute("end", end));
		edgeAtt.appendChild(attValue);
	}
	
	public void addValueToNode(String nodeKey, String attributeID, String value) {
		Element nodeAtt = nodeMap.get(nodeKey).getFirstChildElement("attvalues");
		Element attValue = new Element("attvalue");
		attValue.addAttribute(new Attribute("for", attributeID));
		attValue.addAttribute(new Attribute("value", value));
		nodeAtt.appendChild(attValue);
	}
	
	public void addDynamicNodeAttribute(String id, String title, String type) {
		addAttribute(id, title, type, dynamicNodeAttributes);
	}
	
	public void addDynamicEdgeAttribute(String id, String title, String type) {
		addAttribute(id, title, type, dynamicEdgeAttributes);
	}
	
	public void addStaticNodeAttribute(String id, String title, String type) {
		addAttribute(id, title, type, staticNodeAttributes);
	}
	
	private void addAttribute(String id, String title, String type, Element element) {
		Element att = new Element("attribute");
		att.addAttribute(new Attribute("id",id));
		att.addAttribute(new Attribute("title",title));
		att.addAttribute(new Attribute("type",type));
		element.appendChild(att);
	}
	
	public void printToFile(String file) {
		try {
			PrintWriter out = new PrintWriter(file);
			out.println(doc.toXML());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String toString() {
		return doc.toXML();
	}
	
	public static void main(String[] args) throws ParseException {
		GraphBuilder builder = new GraphBuilder("/config.properties");
		Date start = DateHandler.sdf.parse("2014-02-11");

		Date end = DateHandler.sdf.parse("2014-04-24");
		builder.buildDynamicGraph(start, end);
		
	}
}
