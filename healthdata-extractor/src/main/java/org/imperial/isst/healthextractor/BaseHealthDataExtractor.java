package org.imperial.isst.healthextractor;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jxl.Cell;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.graph.Neo4JFactory;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.utils.StringUtils;
import org.imperial.isst.utils.TableUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.graphdb.index.IndexManager;
import org.neo4j.index.lucene.QueryContext;

import com.google.common.collect.ArrayTable;

public abstract class BaseHealthDataExtractor extends DefenderApplication {

	protected static final String BASE_OUTPUT_DIR = System.getProperty("user.home") + "/gp-data/"
			+ DateHandler.sdf.format(new Date()) + "/";

	protected NodeAssigner nodeAssigner;
	private final GraphDatabaseService graphDb;

	protected ArrayTable<Date, String, Double> iliCounts;
	protected ArrayTable<Date, String, Double> vomCounts;
	protected ArrayTable<Date, String, Double> diaCounts;
	protected ArrayTable<Date, String, Double> gasCounts;

	public List<ArrayTable<Date, String, String>> results;

	protected final int COLUMNS_PER_ILLNESS = 5;
	protected final int NUMBER_OF_ILLNESSES = 4;
	protected final int ILLNESS_ROW = 3;
	protected final int HEADER_ROW = 4;
	protected final int FIRST_AREA_ROW = 5;

	protected List<File> resultFiles;

	public BaseHealthDataExtractor(String configFile) {
		super(configFile);

		try {
			nodeAssigner = new NodeAssigner(connectToDB("node"));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		results = new ArrayList<ArrayTable<Date, String, String>>();
		graphDb = Neo4JFactory.createNeo4JService(configProperties.getProperty("graphdb"));
	}

	protected void createCountTables() {
		// Create tables of Date/Node/Count to update the graph
		iliCounts = ArrayTable.create(results.get(0).rowKeyList(), nodeAssigner.getNodeNames());
		vomCounts = ArrayTable.create(results.get(0).rowKeyList(), nodeAssigner.getNodeNames());
		diaCounts = ArrayTable.create(results.get(0).rowKeyList(), nodeAssigner.getNodeNames());
		gasCounts = ArrayTable.create(results.get(0).rowKeyList(), nodeAssigner.getNodeNames());
		TableUtils.zeroTable(iliCounts);
		TableUtils.zeroTable(vomCounts);
		TableUtils.zeroTable(diaCounts);
		TableUtils.zeroTable(gasCounts);
	}

	public List<ArrayTable<Date, String, String>> createTables(List<File> files) {
		List<Date> dateRange = getDateRange(files);
		List<String> columns = null;
		try {
			columns = getColumns(files.get(0));
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < getNumberOfAreas(); i++) {
			ArrayTable<Date, String, String> table = ArrayTable.create(dateRange, columns);
			results.add(table);
		}

		return results;
	}

	public void extractData() {
		processFiles();
		createCountTables();
		updateCountTables();
	}

	private Date findClosestDate(Date dailyDate, List<Date> weeklyDates) {
		for (int i = 0; i < weeklyDates.size(); i++) {
			Date current = weeklyDates.get(i);
			if (dailyDate.equals(current)) {
				return current;
			}
			if (dailyDate.after(current) && dailyDate.before(weeklyDates.get(i + 1))) {
				return current;
			}
		}
		return weeklyDates.get(weeklyDates.size() - 1);
	}

	protected List<String> getColumns(File file) throws BiffException, IOException {
		List<String> res = new ArrayList<String>();
		Workbook workbook = Workbook.getWorkbook(file);
		Sheet sheet = workbook.getSheet(getWorkSheet());
		Cell[] illnesses = sheet.getRow(ILLNESS_ROW);
		Cell[] headers = sheet.getRow(HEADER_ROW);
		for (Cell cell : headers) {
			res.add(cell.getContents());
		}
		int currentColumn = getFirstIllnessColumn();
		// Make sure columns are unique
		for (int i = 0; i < NUMBER_OF_ILLNESSES; i++) {
			setupColumnsWithIllness(res, illnesses, currentColumn);
			currentColumn = currentColumn + COLUMNS_PER_ILLNESS;
		}

		return res;
	}

	public Date getDateForFile(File file) {
		Workbook workbook = null;
		try {
			workbook = Workbook.getWorkbook(file);
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sheet sheet = workbook.getSheet(0);
		DateCell dateCell = (DateCell) sheet.getCell(1, 7);
		return dateCell.getDate();
	}

	public List<Date> getDateRange(List<File> files) {
		List<Date> res = new ArrayList<Date>();
		for (File file : files) {
			res.add(getDateForFile(file));
		}
		return res;
	}

	protected abstract int getFirstIllnessColumn();

	/**
	 * Get the local authority codes from the individual tables.
	 * 
	 * @return
	 */
	protected List<String> getLACodes() {
		ArrayList<String> laCodes = new ArrayList<String>();
		for (ArrayTable<Date, String, String> res : results) {
			laCodes.add(res.at(0, 0));
		}
		return laCodes;
	}

	/**
	 * The number of locations.
	 * 
	 * @return
	 */
	protected abstract int getNumberOfAreas();

	protected Double getRowValue(ArrayTable<Date, String, String> gpTable, Date date, String column) {
		String result = gpTable.get(date, column);
		if (!StringUtils.isNumeric(result)) {
			result = "0";
		}
		return Double.valueOf(result);
	}

	/**
	 * The data worksheet number.
	 * 
	 * @return
	 */
	protected abstract int getWorkSheet();

	/**
	 * Get a list of the files in a directory referred to by the given path.
	 * 
	 * @param path
	 * @return
	 */
	public List<File> listFilesForFolder(String path) {
		List<File> result = new ArrayList<File>();
		File folder = new File(path);
		for (File fileEntry : folder.listFiles()) {
			if (!fileEntry.isHidden())
				result.add(fileEntry);
		}
		return result;
	}

	/**
	 * Normalise the count by ..
	 * 
	 * @param node
	 * @param count
	 * @return
	 */
	protected double normaliseCount(Node node, double count) {
		return count;
	}

	/**
	 * Output the tabular data as CSV files.
	 */
	public void outputData() {
		outputTablesToCSV();
		logger.info("Completed.");
	}

	/**
	 * Output the data to CSV files.
	 */
	protected void outputTablesToCSV() {
		for (ArrayTable<Date, String, String> res : results) {
			try {
				TableUtils.outputToCSV(res, BASE_OUTPUT_DIR + "/" + res.at(0, 0) + ".csv");
			} catch (IOException e) {
				logger.error("Exception outputting the CSV files", e);
			}
		}
	}

	protected void processFile(File file) {
		Date date = getDateForFile(file);
		List<String> columns;
		try {
			columns = getColumns(file);

			Workbook workbook;
			workbook = Workbook.getWorkbook(file);

			Sheet sheet = workbook.getSheet(getWorkSheet());
			for (int i = 0; i < getNumberOfAreas(); i++) {
				Cell[] row = sheet.getRow(i + FIRST_AREA_ROW);
				for (int j = 0; j < row.length; j++) {
					String text = row[j].getContents();

					results.get(i).put(date, columns.get(j), text);
				}
			}
		} catch (BiffException e) {
			logger.error("Exception processing Excel worksheet data", e);
		} catch (IOException e) {
			logger.error("Exception processing Excel worksheet data", e);
		}
	}

	private void processFiles() {
		resultFiles = listFilesForFolder("data/gp-in/");
		results = createTables(resultFiles);
		for (File file : resultFiles) {
			processFile(file);
		}
	}

	protected void setupColumnsWithIllness(List<String> results, Cell[] illnesses, int index) {
		for (int i = 0; i < COLUMNS_PER_ILLNESS; i++) {
			results.set(index + i, illnesses[index].getContents() + " " + results.get(index + i));
		}
	}

	protected abstract void updateCountTables();

	protected void updateGraphDB() {
		// Fetch the nodes for this date from Neo4J and update them
		try (Transaction tx = graphDb.beginTx()) {
			IndexManager index = graphDb.index();
			Index<Node> timestamps = index.forNodes("timestamps");
			Date start = results.get(0).rowKeyList().get(0);
			Date end = results.get(0).rowKeyList().get(results.get(0).rowKeyList().size() - 1);
			List<Date> dateRange = DateHandler.datesBetween(start, end);
			for (Date date : dateRange) {
				// Build a list of nodes at the previous timestamp
				Date closestDate = findClosestDate(date, results.get(0).rowKeyList());
				IndexHits<Node> nodeIndex = timestamps.query(QueryContext.numericRange("timestamp", date.getTime(),
						date.getTime()));
				try {
					for (Node node : nodeIndex) {
						// Update the nodes

						node.setProperty("GPILICount",
								normaliseCount(node, iliCounts.get(closestDate, node.getProperty("name"))));
						node.setProperty("GPVomCount",
								normaliseCount(node, vomCounts.get(closestDate, node.getProperty("name"))));
						node.setProperty("GPDiaCount",
								normaliseCount(node, diaCounts.get(closestDate, node.getProperty("name"))));
						node.setProperty("GPGasCount",
								normaliseCount(node, gasCounts.get(closestDate, node.getProperty("name"))));
					}
				} finally {
					nodeIndex.close();
				}
			}
			tx.success();
		}
	}

}
