/**
 * LaBoundaryGraphBuilder.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.healthextractor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.imperial.isst.date.DateHandler;
import org.imperial.isst.graph.Neo4JFactory;
import org.imperial.isst.graph.Neo4JUtils;
import org.imperial.isst.location.GeoCoords;
import org.imperial.isst.location.NodeAssigner;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayTable;

/**
 * The graph builder for the local authority boundary data.
 * 
 * @author Donal Simmie
 * 
 */
public class LaBoundaryGraphBuilder {

	/**
	 * The class logger.
	 */
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	/**
	 * The REST interface to the graph database for manipulating resources.
	 */
	private final RestGraphDatabase graphDb;

	/**
	 * The Neo4J query engine.
	 */
	private final RestCypherQueryEngine engine;

	/**
	 * The to-node assignment component.
	 */
	private final NodeAssigner nodeAssigner;

	/**
	 * The results of assembling the time-series on a daily basis.
	 */
	private final ArrayTable<Date, String, Double> dailyResults;

	/**
	 * The populations at each node.
	 */
	private final Map<String, Double> nodePopulations;

	/**
	 * Initialises a new instance of the {@link LaBoundaryGraphBuilder} class.
	 * 
	 * @param nodeAssigner
	 * @param dailyResults
	 * @param graphDB
	 */
	public LaBoundaryGraphBuilder(NodeAssigner nodeAssigner, ArrayTable<Date, String, Double> dailyResults,
			Map<String, Double> nodePopulations, String graphDB) {
		graphDb = Neo4JFactory.createNeo4JService(graphDB);
		engine = new RestCypherQueryEngine(graphDb.getRestAPI());
		this.nodeAssigner = nodeAssigner;
		this.dailyResults = dailyResults;
		this.nodePopulations = nodePopulations;
	}

	/**
	 * Create a node in the graph.
	 * 
	 * @param node
	 * @param date
	 */
	private void createNode(String node, Date date) {
		Map<String, Object> nodeProps = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();

		Double userCount = nodePopulations.get(node);
		if (userCount == null) {
			userCount = 0.0;
		}

		nodeProps.put("name", node);
		nodeProps.put("userCount", userCount);
		GeoCoords geo = nodeAssigner.getNodeCentroid(node);

		nodeProps.put("latitude", geo.getLatitude());
		nodeProps.put("longitude", geo.getLongitude());
		nodeProps.put("timestamp", date.getTime());
		nodeProps.put("date", DateHandler.sdf.format(date));
		nodeProps.put("locality", nodeAssigner.getNodeLocality(node));

		nodeProps.put("historicalflu", dailyResults.get(date, node));
		nodeProps.put("hpaExtract", true);

		params.put("nodeProps", nodeProps);
		String queryStr = "CREATE (a:" + Neo4JUtils.DATA + " {nodeProps}) return a";
		engine.query(queryStr, params);

	}

	/**
	 * Create a temporal edge or relationship between two nodes.
	 * 
	 * @param node
	 * @param date
	 */
	private void createTemporalRelationship(String node, Date date) {
		Map<String, Object> firstProps = new HashMap<String, Object>();
		Map<String, Object> secondProps = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		firstProps.put("name", node);
		secondProps.put("name", node);
		firstProps.put("date", DateHandler.sdf.format(date));
		secondProps.put("date", DateHandler.sdf.format(DateHandler.addDaysTo(date, -1)));
		params.put("firstProps", firstProps);
		params.put("secondProps", secondProps);
		String queryStr = "match (a:" + Neo4JUtils.DATA + " {name:\"" + node + "\" ,date:\""
				+ DateHandler.sdf.format(date) + "\"}), " + "(b:" + Neo4JUtils.DATA + " {name:\"" + node
				+ "\" ,date:\"" + DateHandler.sdf.format(DateHandler.addDaysTo(date, -1))
				+ "\" }) with a,b create (a)-[r:TEMPORAL]->(b)";

		engine.query(queryStr, new HashMap<String, Object>());
	}

	/**
	 * Insert data into the graph database.
	 * 
	 * @param dailyResults
	 * @param nodeNames
	 */
	void insertDataIntoGraph(ArrayTable<Date, String, Double> dailyResults) {
		for (int i = 0; i < dailyResults.rowKeyList().size() - 1; i++) {
			Date date = dailyResults.rowKeyList().get(i);
			for (String node : nodeAssigner.getNodeNames()) {
				logger.debug("Creating node: " + node + " for date: " + date.toString());
				createNode(node, date);
				createTemporalRelationship(node, date);
			}
		}
	}
}
