package org.imperial.isst.analyzer;


import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.imperial.isst.analyzer.social.SocialAnalyzer;
import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.date.DateFormat;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.utils.KeywordUtils;
import org.imperial.isst.utils.TableUtils;

import com.google.common.collect.ArrayTable;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public abstract class BaseAnalyzer extends DefenderApplication {

	/**
	 * The output directory for results files.
	 */
	protected static final String BASE_OUTPUT_DIR = System.getProperty("user.home") + "/defender-stats/"
			+ DateHandler.sdf.format(new Date()) + "/";
	

	/**
	 * The <i>tweet</i> collection.
	 */
	protected DBCollection collection;
	protected DBCollection symptomCollection;
	/**
	 * The keywords divided by different categories.
	 */
	protected HashMap<String, List<String>> keywords;
	protected List<String> allkeys;
	
	/**
	 * The totals we will save.
	 */
	protected List<String> totals;

	
	ArrayTable<Date, String, Double> distinctKeywordResults;
	
	ArrayTable<Date, String, Double> totalKeywordResults;
	
	ArrayTable<Date, String, Double> categoryResults;
	
	ArrayTable<Date, String, Double> totalResults;
	
	
	/**
	 * Initialises a new instance of the {@link SocialAnalyzer} class.
	 * 
	 * @param configFile
	 */
	public BaseAnalyzer(String configFile) {
		super(configFile);
		//keywords = KeywordUtils.loadKeywords(configProperties.getProperty("keywordfile"));
		//allkeys = KeywordUtils.getAllKeywords(keywords);
		
		totals = new ArrayList<String>();
		totals.add("total");
	}
	
	public void connectCollections() throws UnknownHostException {
		symptomCollection = connectToDB("symptom");
		
		keywords = KeywordUtils.loadKeywords(symptomCollection);
		allkeys = KeywordUtils.getAllKeywords(keywords);
		
		logger.info(keywords.toString());
	}
	
	protected abstract String getOutputName();

	protected abstract String getDateField();
	/**
	 * Increment the keyword group counts.
	 * 
	 * @param keywordResults
	 * @param keywords
	 * @param matches
	 * @param date
	 */
	private void incrementCategoryResults(ArrayTable<Date, String, Double> keywordResults,
			Set<String> matches, Date date) {
		Set<String> foundCategories = new HashSet<String>();

		for (String match : matches) {
			Set<String> categories = AnalyzerUtils.getCategories(keywords, match);
			if (categories.isEmpty()) {
				logger.error("Error keyword: " + match + " not found in any category");
			}
			foundCategories.addAll(categories);
		}

		for (String foundCategory : foundCategories) {
			TableUtils.incrementTable(keywordResults, date, foundCategory);
		}
	}

	/**
	 * Increment the keyword counts.
	 * 
	 * @param keywordResults
	 * @param matches
	 * @param date
	 */
	protected void incrementKeywordResults(ArrayTable<Date, String, Double> keywordResults, Iterable<String> matches, Date date) {
		for (String match : matches) {
			TableUtils.incrementTable(keywordResults, date, match);
		}
	}
	
	/**
	 * Get the keyword matches from the stored database object
	 * 
	 * @param DBObject The stored MongoDB database object. For social this is a tweet, for news
	 * 					this is our own key store
	 */
	protected abstract List<String> getMatchesFromStore(DBObject object);

	protected void createResultTables(List<Date> dateRange) {
		distinctKeywordResults = ArrayTable.create(dateRange, allkeys);
		TableUtils.zeroTable(distinctKeywordResults);
		
		totalKeywordResults = ArrayTable.create(dateRange, allkeys);
		TableUtils.zeroTable(totalKeywordResults);
		
		categoryResults = ArrayTable.create(dateRange, keywords.keySet());
		TableUtils.zeroTable(categoryResults);
		
		totalResults = ArrayTable.create(dateRange, totals);
		TableUtils.zeroTable(totalResults);
	}
	
	protected void outputResultsToCSV() throws IOException {
		TableUtils.outputToCSV(distinctKeywordResults, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "distinctKeywordResults.csv", DateFormat.PRETTY);
		TableUtils.outputToCSV(totalKeywordResults, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "totalKeywordResults.csv", DateFormat.PRETTY);
		TableUtils.outputToCSV(categoryResults, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "categoryResults.csv", DateFormat.PRETTY);
		TableUtils.outputToCSV(totalResults, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "totalResults.csv", DateFormat.PRETTY);
	}
	
	protected void processObject(DBObject obj, Date date) {
		Set<String> distinctMatches;
		List<String> allMatches;
		allMatches = getMatchesFromStore(obj);
		if (allMatches == null) {
			return;
		}
		distinctMatches = new HashSet<String>(allMatches);	
		
		if (!distinctMatches.isEmpty()) {
			incrementCategoryResults(categoryResults, distinctMatches, date);
			incrementKeywordResults(distinctKeywordResults, distinctMatches, date);
			incrementKeywordResults(totalKeywordResults, allMatches, date);
		}
	}
	
	protected void produceAnalysisForDate(Date date) {
		logger.info("Producing analysis for: " + date.toString());

		
		DBObject query = DBQueries.buildQueryForDay(date, getDateField());
		
		DBCursor cursor = collection.find(query);
		//cursor.batchSize(100);
		logger.info("Cursor has: " +  cursor.count());
		totalResults.put(date, "total", (double) cursor.count());
		long count = 0;
		while (cursor.hasNext()) {
			if (count % 10000 == 0) {
				logger.info("Processed 10000 docs. Count now: " + count);
			}
			count++;
			//cursor.next();
			DBObject obj = cursor.next();
		
			processObject(obj, date);
						
		}

	}
	
	
	/**
	 * Perform the keyword and keyword group analysis and persist the output to file(s).
	 * 
	 * @param startDate
	 * @param endDate
	 */
	protected void produceAnalysis(Date startDate, Date endDate) {
		List<Date> dateRange = DateHandler.datesBetween(startDate, endDate);

		long startTime = System.currentTimeMillis();

		createResultTables(dateRange);

		for (Date date : dateRange) {
			produceAnalysisForDate(date);
		}

		logger.info("Completed in " + (System.currentTimeMillis() - startTime) + " ms.");
		try {
			outputResultsToCSV();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

