/**
 * Analyzer.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.analyzer.news;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.imperial.isst.analyzer.AnalyzerUtils;
import org.imperial.isst.analyzer.BaseAnalyzer;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.date.DateFormat;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.news.Feed;
import org.imperial.isst.utils.NewsUtils;
import org.imperial.isst.utils.TableUtils;

import com.google.common.collect.ArrayTable;
import com.mongodb.BasicDBList;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class LiveAnalyzer extends BaseAnalyzer {

	/**
	 * The date field to query.
	 */
	private static final String DATE_FIELD = "pub_date";

	private static final String OUTPUT_NAME = "LiveNews";
	
	private ArrayTable<Date, String, Double> feedTotalResults;
	private ArrayTable<Date, String, Double> feedTotalKeywordResults;
	private List<String> feedNames;
	/**
	 * The launcher application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		LiveAnalyzer app = new LiveAnalyzer("/config.properties");
		Date start;
		try {
			start = DateHandler.sdf.parse("2014-02-20");

			Date end = DateHandler.sdf.parse("2014-03-13");
			try {
				app.connectCollections();
				app.produceAnalysis(start, end);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

	}



	/**
	 * Initialises a new instance of the {@link LiveAnalyzer} class.
	 * 
	 * @param configFile
	 */
	public LiveAnalyzer(String configFile) {
		super(configFile);
		feedNames = new ArrayList<String>();
		Set<Feed> feeds = NewsUtils.loadFeeds("/feeds.txt");
		for (Feed feed: feeds) {
			feedNames.add(feed.getName());
		}
	}
	
	/**
	 * 
	 * @throws UnknownHostException
	 */
	public void connectCollections() throws UnknownHostException {
		super.connectCollections();
		collection = connectToDB("live");
	}
	
	protected List<String> getMatchesFromStore(DBObject object) {
		return AnalyzerUtils.getKeywordMatchesFromStore(object);
	}
	
	protected void createResultTables(List<Date> dateRange) {
		super.createResultTables(dateRange);
		
		feedTotalResults = ArrayTable.create(dateRange, feedNames);
		TableUtils.zeroTable(feedTotalResults);
		
		feedTotalKeywordResults = ArrayTable.create(dateRange, feedNames);
		TableUtils.zeroTable(feedTotalKeywordResults);
	}
	
	protected void produceAnalysisForDate(Date date) {
		super.produceAnalysisForDate(date);
		
		DBObject query = DBQueries.buildQueryForDay(date, getDateField());
		DBCursor cursor = collection.find(query);
		
		while (cursor.hasNext()) {
			DBObject obj = cursor.next();

			if (obj.containsField("feed")) {
				TableUtils.incrementTable(feedTotalResults, date, obj.get("feed").toString());
				
				BasicDBList keys = (BasicDBList) obj.get("keywords");
				if (keys.size() > 0) {
					TableUtils.incrementTable(feedTotalKeywordResults, date, obj.get("feed").toString());				
				}
				
			}
		}
		

	}
	
	protected void outputResultsToCSV() throws IOException {
		super.outputResultsToCSV();
		TableUtils.outputToCSV(feedTotalResults, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "feedTotalResults.csv", DateFormat.PRETTY);
		TableUtils.outputToCSV(feedTotalKeywordResults, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "feedTotalKeywordResults.csv", DateFormat.PRETTY);
	}


	protected String getOutputName() {
		return OUTPUT_NAME;
	}
	
	protected String getDateField() {
		return DATE_FIELD;
	}
}
