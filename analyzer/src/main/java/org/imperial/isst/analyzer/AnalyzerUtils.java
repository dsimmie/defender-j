/**
 * AnalyzerUtils.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.analyzer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Common utilities class for the analyzer package.
 * 
 * @author Donal Simmie
 * 
 */
public final class AnalyzerUtils {

	/**
	 * Increment a count map based on key observation.
	 * 
	 * @param map
	 * @param key
	 */
	public static void incrementMap(Map<String, Long> map, String key) {
		long count = map.containsKey(key) ? map.get(key) : 0;
		map.put(key, count + 1);
	}
	
	/**
	 * Perform a reverse lookup to get the category from the word value.
	 * 
	 * @param categoryKeywords
	 * @param word
	 * @return
	 */
	public static Set<String> getCategories(HashMap<String, List<String>> categoryKeywords, String word) {
		Set<String> categories = new HashSet<String>();
		Iterator<Entry<String, List<String>>> catIter = categoryKeywords.entrySet().iterator();
		while (catIter.hasNext()) {
			Entry<String, List<String>> catEntry = catIter.next();
			if (catEntry.getValue().contains(word)) {
				categories.add(catEntry.getKey());
			}
		}
		return categories;
	}
	
	public static List<String> getKeywordMatchesFromStore(DBObject object) {
		BasicDBList keywordsDocObj = (BasicDBList) object.get("keywords");
		if (keywordsDocObj == null) {
			return null;
		}
		ArrayList<String> matches = new ArrayList<String>();
		for (Object word: keywordsDocObj) {
			matches.add((String)word);
		}

		return matches;
	}

	/**
	 * Write the date/keyword count results to file.
	 * 
	 * @param keywords
	 * @param dateMap
	 * @param outFileName
	 * @throws IOException
	 */
	public static void outputToCSV(HashMap<String, List<String>> keywords,
			LinkedHashMap<Date, HashMap<String, Long>> dateMap, String outFileName) throws IOException {
		CSVWriter writer = new CSVWriter(new FileWriter(outFileName));
		String[] line = new String[keywords.size() + 1];
		line[0] = "Date";
		int cat = 1;
		for (String category : keywords.keySet()) {
			line[cat] = category;
			cat++;
		}
		writer.writeNext(line);
		for (Date date : dateMap.keySet()) {
			int categoryCount = 0;
			line[0] = date.toString();
			for (String category : keywords.keySet()) {
				long count;
				HashMap<String, Long> map = dateMap.get(date);
				if (map.containsKey(category)) {
					count = map.get(category);
				} else {
					count = 0;
				}

				line[categoryCount + 1] = String.valueOf(count);

				categoryCount++;
			}
			writer.writeNext(line);
		}
		writer.close();
	}

	/**
	 * Exists to defeat instantiation.
	 */
	private AnalyzerUtils() {
		throw new AssertionError();
	}
}
