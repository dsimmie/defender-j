package org.imperial.isst.analyzer.social;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.imperial.isst.analyzer.AnalyzerUtils;
import org.imperial.isst.analyzer.BaseAnalyzer;
import org.imperial.isst.date.DateFormat;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.utils.TableUtils;

import com.google.common.collect.ArrayTable;
import com.mongodb.DBObject;

public class SocialAnalyzer extends BaseAnalyzer {



	
	/**
	 * The date field to query.
	 */
	private static final String DATE_FIELD = "created_date";
	
	private static final String OUTPUT_NAME = "Social";
	
	
	private ArrayTable<Date, String, Double> tweetUserResults;
	private HashMap<String, Set<String>> keywordsPerUser;
	
	//private ArrayTable<Date, String, Double> dailyNodeCounts = null;
	//private ArrayTable<Date, String, Double> dailyEdgeCounts = null;
	/**
	 * The application launcher.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		SocialAnalyzer app = new SocialAnalyzer("/config.properties");
		Date start;
		try {
			start = DateHandler.sdf.parse("2014-02-01");

			Date end = DateHandler.sdf.parse("2014-04-23");
			try {
				app.connectCollections();
				app.produceAnalysis(start, end);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	protected String getOutputName() {
		return OUTPUT_NAME;
	}
	
	protected String getDateField() {
		return DATE_FIELD;
	}

	/**
	 * Initialises a new instance of the {@link SocialAnalyzer} class.
	 * 
	 * @param configFile
	 */
	public SocialAnalyzer(String configFile) {
		super(configFile);
		keywordsPerUser = new HashMap<String, Set<String>>();

	}
	
	protected void produceAnalysis(Date startDate, Date endDate) {
		super.produceAnalysis(startDate, endDate);
	}
	

	/**
	 * 
	 * @throws UnknownHostException
	 */
	public void connectCollections() throws UnknownHostException {
		super.connectCollections();
		collection = connectToDB("tweet");
	}

	protected List<String> getMatchesFromStore(DBObject object) {
		return AnalyzerUtils.getKeywordMatchesFromStore(object);	
	}
	
	protected void createResultTables(List<Date> dateRange) {
		super.createResultTables(dateRange);

		//edges = nodeAssigner.getEdges();
		tweetUserResults = ArrayTable.create(dateRange, allkeys);
		TableUtils.zeroTable(tweetUserResults);
		
		
//		dailyNodeCounts = ArrayTable.create(tweetUserResults.rowKeyList(),  nodeAssigner.getNodeNames());
//		TableUtils.zeroTable(dailyNodeCounts);
//			
//		dailyEdgeCounts = ArrayTable.create(tweetUserResults.rowKeyList(), getEdgeRepresentations(edges));
//		TableUtils.zeroTable(dailyEdgeCounts);
	}
	
/*	private Set<String> getEdgeRepresentations(Map<ImmutableSet<String>, Integer > edges) {
		Set<String> res = new HashSet<String>();
		for (ImmutableSet<String> key: edges.keySet()) {
			res.add(key.toString());
		}

		return res;
	}*/
	
	
	protected void outputResultsToCSV() throws IOException {
		super.outputResultsToCSV();
		TableUtils.outputToCSV(tweetUserResults, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "tweetUserKeywordResults.csv", DateFormat.PRETTY);
		//TableUtils.outputToCSV(dailyNodeCounts, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "dailyNodeCounts.csv", DateFormat.RAW);
		//TableUtils.outputToCSV(dailyEdgeCounts, BASE_OUTPUT_DIR+getOutputName() +"/"+getOutputName()+ "dailyEdgeCounts.csv", DateFormat.RAW);
	}
	
/*	private DBObject getGeoForLocation(AggregationOutput output, String location) {
		for (DBObject res: output.results()) {
			if (res.get("_id").toString().equals(location)) {
				return (DBObject) res.get("geos");
			}
		}
		return null;
	}*/
	

	
	protected void processObject(DBObject obj, Date date) {
		super.processObject(obj, date);
		Set<String> distinctMatches;
		List<String> allMatches;
		allMatches = getMatchesFromStore(obj);
		if (allMatches == null) {
			return;
		}
		distinctMatches = new HashSet<String>(allMatches);	
		
		for (String match: distinctMatches) {
			if (!keywordsPerUser.containsKey(match)) {
				keywordsPerUser.put(match, new HashSet<String>());
			}
			keywordsPerUser.get(match).add(  (obj.get("userId")).toString() );
		}
	}
	
	protected void produceAnalysisForDate(Date date) {
		keywordsPerUser.clear();
		super.produceAnalysisForDate(date);
		
		logger.info("Producing social-specific analysis for: " + date.toString());
		
		for (String key: keywordsPerUser.keySet()) {
			tweetUserResults.put(date, key, (double) keywordsPerUser.get(key).size());
		}

	}



}
