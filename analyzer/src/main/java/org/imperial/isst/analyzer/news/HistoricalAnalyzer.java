/**
 * Analyzer.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.analyzer.news;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.imperial.isst.analyzer.AnalyzerUtils;
import org.imperial.isst.analyzer.BaseAnalyzer;
import org.imperial.isst.date.DateHandler;

import com.mongodb.DBObject;

/**
 * 
 * @author Donal Simmie
 * 
 */


public class HistoricalAnalyzer extends BaseAnalyzer {

	/**
	 * The date field to query.
	 */
	private static final String DATE_FIELD = "pub_date";

	private static final String OUTPUT_NAME = "historical";
	/**
	 * The launcher application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		HistoricalAnalyzer app = new HistoricalAnalyzer("/config.properties");
		Date start;
		try {
			start = DateHandler.sdf.parse("2014-01-20");

			Date end = DateHandler.sdf.parse("2014-03-10");
			try {
				app.connectCollections();
				app.produceAnalysis(start, end);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

	}



	/**
	 * Initialises a new instance of the {@link HistoricalAnalyzer} class.
	 * 
	 * @param configFile
	 */
	public HistoricalAnalyzer(String configFile) {
		super(configFile);
	}
	
	/**
	 * 
	 * @throws UnknownHostException
	 */
	public void connectCollections() throws UnknownHostException {
		super.connectCollections();
		collection = connectToDB("historical");
	}
	
	protected List<String> getMatchesFromStore(DBObject object) {
		return AnalyzerUtils.getKeywordMatchesFromStore(object);
	}


	protected String getOutputName() {
		return OUTPUT_NAME;
	}
	
	protected String getDateField() {
		return DATE_FIELD;
	}
}


