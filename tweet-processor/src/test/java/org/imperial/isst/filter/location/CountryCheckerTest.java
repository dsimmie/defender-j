package org.imperial.isst.filter.location;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.imperial.isst.location.Location;
import org.imperial.isst.location.Source;
import org.imperial.isst.processor.location.CountryChecker;
import org.junit.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class CountryCheckerTest extends FilterTest {


	private final BasicDBObject tweet;

	public CountryCheckerTest() {
		tweet = new BasicDBObject("id", 123l);
		tweet.put("user", new BasicDBObject("id", 456));
		tweet.put("created_at", "Fri Jan 17 13:45:25 +0000 2014");
	}

//	@Test
//	public void testGettingLocationFromBio() {
//		CountryChecker checker = new CountryChecker(testURL);
//
//		String nongeotweet = readFile("/nongeotweet.data");
//		DBObject nongeoObj = (DBObject) JSON.parse(nongeotweet);
//		assertEquals("UK", checker.getLocationFromBio(nongeoObj).get().getCountry());
//
//		DBObject nonGeoUser = (DBObject) nongeoObj.get("user");
//		nonGeoUser.put("utc_offset", -3600);
//		assertEquals(Optional.absent(), checker.getLocationFromBio(nongeoObj));
//	}

	@Test
	public void testGettingLocationFromPlace() {
		CountryChecker checker = new CountryChecker(testURL);
		String geotweet = readFile("/geotweet.data");
		DBObject geoTweetObj = (DBObject) JSON.parse(geotweet);
		Location loc = checker.getLocationFromPlaceInfo(geoTweetObj);
		assertEquals("UK", loc.getCountry());
		assertEquals("Trafford", loc.getLocality());
		assertEquals(53.387592249999997307, loc.getCoords().getLatitude(), DELTA);
		assertEquals(Source.TWEET_GEO_CODED, loc.getSource());
	}

//	@Test
//	public void testGettingTweetLocation() {
//		CountryChecker checker = new CountryChecker(testURL);
//
//		assertEquals(Optional.absent(), checker.getLocation("Mcdonald's Ballpit", tweet));
//		assertEquals(Optional.absent(), checker.getLocation("{Harry' & Niall's Hearts♡}", tweet));
//		assertEquals(Optional.absent(), checker.getLocation("", tweet));
//		assertEquals("UK", checker.getLocation("London", tweet).get().getCountry());
//		// 03/03/14 This has stopped working in datasciencetoolkit
//	//	Location southEnd = checker.getLocation("Southend", tweet).get();
//	//	assertEquals("UK", southEnd.getCountry());
//	//	assertEquals(Source.TWEET_BIO_LOOKUP, southEnd.getSource());
//
////		String nongeotweet = readFile("/nongeotweet.data");
////		DBObject nongeoObj = (DBObject) JSON.parse(nongeotweet);
////
////		assertEquals("UK", checker.getLocationFromBio(nongeoObj).get().getCountry());
//	}

	@Test
	public void testIfTweetHasPlace() {
		CountryChecker checker = new CountryChecker(testURL);
		// Need a saved test tweet for this
		String nongeotweet = readFile("/nongeotweet.data");
		System.out.println(nongeotweet);
		assertFalse(checker.tweetHasPlaceInfo((DBObject) JSON.parse(nongeotweet)));

		String geotweet = readFile("/geotweet.data");
		assertTrue(checker.tweetHasPlaceInfo((DBObject) JSON.parse(geotweet)));
	}
}
