/**
 * NodeAssignerTest.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.filter.location;

import static org.junit.Assert.*;

import java.net.UnknownHostException;

import org.imperial.isst.database.DBConnection;
import org.imperial.isst.database.DBWrapper;
import org.imperial.isst.location.NodeAssigner;
import org.junit.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;


/**
 * 
 * @author Donal Simmie
 * 
 */
public class NodeAssignerTest {

	private static final String TWEET_PLACEHOLDER = "{\"coordinates\":\n{\n    \"coordinates\":\n    [\nLONG,\nLAT\n    ],\n    \"type\":\"Point\"\n}}";

	private NodeAssigner target;

	/**
	 * Test method for
	 * {@link org.imperial.isst.location.NodeAssigner#assignTweetToNode(com.mongodb.DBObject, com.mongodb.DBObject)}
	 * .
	 * 
	 * @throws UnknownHostException
	 */
	@Test
	public void testAssignTweetToNode() throws UnknownHostException {
		target = new NodeAssigner(DBWrapper.connectToCollection(new DBConnection("ib-dsimmie.ib.ic.ac.uk", "spatialnetwork", "nodes")));
		DBObject londonTweet1 = (DBObject) JSON.parse(TWEET_PLACEHOLDER.replace("LONG", "-0.65643310546875").replace(
				"LAT", "51.57536261053028"));
		DBObject londonTweet2 = (DBObject) JSON.parse(TWEET_PLACEHOLDER.replace("LONG", "-0.4010009765625").replace(
				"LAT", "51.316880504045876"));
		DBObject gloucester = (DBObject) JSON.parse(TWEET_PLACEHOLDER.replace("LONG", "-2.3682403564453125").replace(
				"LAT", "51.912508594508694"));
		DBObject edinburgh = (DBObject) JSON.parse(TWEET_PLACEHOLDER.replace("LONG", "-3.188095092773437").replace(
				"LAT", "55.94881534959578"));
		DBObject noise = (DBObject) JSON.parse(TWEET_PLACEHOLDER.replace("LONG", "-2.3023223876953125").replace("LAT",
				"52.004328577925094"));

		BasicDBObject processedTweet = new BasicDBObject();

		target.assignTweetToNode(londonTweet1, processedTweet);
		assertEquals(2, ((Integer) processedTweet.get("node")).intValue());
		target.assignTweetToNode(londonTweet2, processedTweet);
		assertEquals(2, ((Integer) processedTweet.get("node")).intValue());
		target.assignTweetToNode(gloucester, processedTweet);
		assertEquals(1, ((Integer) processedTweet.get("node")).intValue());
		target.assignTweetToNode(edinburgh, processedTweet);
		assertEquals(42, ((Integer) processedTweet.get("node")).intValue());
		target.assignTweetToNode(noise, processedTweet);
		assertEquals(0, ((Integer) processedTweet.get("node")).intValue());
	}
}
