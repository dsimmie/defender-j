package org.imperial.isst.filter.location;

import java.io.InputStream;

public class FilterTest {
	public static final double DELTA = 1e-5;

	public static final String testURL = "http://www.datasciencetoolkit.org/maps/api/geocode/json";

	static String convertStreamToString(java.io.InputStream is) {
		@SuppressWarnings("resource")
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		try {
			return s.hasNext() ? s.next() : "";
		} finally {
			s.close();
		}
	}
	
	public String readFile(String resource) {
		InputStream i = this.getClass().getResourceAsStream(resource);
		String res = convertStreamToString(i);
		return res;
	}
}
