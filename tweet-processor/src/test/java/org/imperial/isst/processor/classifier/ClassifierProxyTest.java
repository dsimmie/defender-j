/**
 * ClassifierDetailsTest.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.processor.classifier;

import org.junit.Test;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class ClassifierProxyTest {

	/**
	 * Test method for
	 * {@link org.imperial.isst.processor.classifier.ClassifierDetails#ClassifierDetails(java.lang.String, java.lang.String, int, int)}
	 * .
	 */
	@Test
	public void sanityCheckZeroMQ() {
		new ClassifierProxy(new ClassifierDetails("test", "127.0.0.1", 5555, 5556), null);
	}

}
