/**
 * ClassifierDetails.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.processor.classifier;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class ClassifierDetails {

	/**
	 * The numeric representation of the IP4 address.
	 */
	private final String host;

	/**
	 * The port to send messages on.
	 */
	private final int sendPort;

	/**
	 * The port to receive messages on.
	 */
	private final int receivePort;

	/**
	 * The name of this classifer.
	 */
	private final String name;

	/**
	 * Initialises a new instance of the {@link ClassifierDetails} class.
	 * 
	 * @param host
	 * @param sendPort
	 * @param receivePort
	 */
	public ClassifierDetails(String name, String host, int sendPort, int receivePort) {
		super();
		this.name = name;
		this.host = host;
		this.sendPort = sendPort;
		this.receivePort = receivePort;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassifierDetails other = (ClassifierDetails) obj;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (receivePort != other.receivePort)
			return false;
		if (sendPort != other.sendPort)
			return false;
		return true;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the receivePort
	 */
	public int getReceivePort() {
		return receivePort;
	}

	/**
	 * @return the sendPort
	 */
	public int getSendPort() {
		return sendPort;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + receivePort;
		result = prime * result + sendPort;
		return result;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClassifierDetails [host=");
		builder.append(host);
		builder.append(", sendPort=");
		builder.append(sendPort);
		builder.append(", receivePort=");
		builder.append(receivePort);
		builder.append(", name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}
}
