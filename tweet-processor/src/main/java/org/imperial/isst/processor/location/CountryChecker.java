package org.imperial.isst.processor.location;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.JsonParseException;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.location.Location;
import org.imperial.isst.location.Source;
import org.imperial.isst.location.TwitterLocation;
import org.imperial.isst.utils.TwitterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DBObject;

public class CountryChecker {

	static class AddressComponent {
		public ArrayList<String> types;
		public String long_name;
		public String short_name;
	}

	// pattern to remove all high-ASCII characters
	//private long callsToWebService = 0;
	private final Logger logger;
	//private final HashMap<String, TwitterLocation> UKLocations;

	//private final Set<String> nonUKLocations;
	//private final WebTarget webTarget;

	//private final JsonFactory jsonFactory;

	public CountryChecker(String url) {
		//ClientConfig clientConfig = new ClientConfig();
		//jsonFactory = new MappingJsonFactory();
		logger = LoggerFactory.getLogger("CountryChecker");
		//UKLocations = new HashMap<String, TwitterLocation>();
		//nonUKLocations = new HashSet<String>();

		//Client client = ClientBuilder.newClient(clientConfig);

	//	webTarget = client.target(url);
	}

	/**
	 * Get a {@link Location} object from a textual representation of a location.
	 * 
	 * @param locationText
	 * @return an optional location object with a value provided the location has been found and is in the UK.
	 */
//	public Optional<TwitterLocation> getLocation(String locationText, DBObject tweet) {
//		Optional<TwitterLocation> result = Optional.absent();
//		/*
//		 * If the location is blank or non UK then we don't need to save it.
//		 */
//		if (locationText.equals("") || nonUKLocations.contains(locationText.toLowerCase())) {
//			result = Optional.absent();
//		}
//		/*
//		 * If we already know the address then use the cached version.
//		 */
//		else if (UKLocations.containsKey(locationText.toLowerCase())) {
//			logger.info(locationText + " already known.");
//			result = Optional.fromNullable(UKLocations.get(locationText.toLowerCase()));
//		} else {
//			try {
//				locationText = java.net.URLEncoder.encode(locationText, "UTF-8");
//			} catch (UnsupportedEncodingException ue) {
//				logger.error("Error URL encoding location text", ue);
//			}
//
//			WebTarget webTargetWithQueryParam = webTarget.queryParam("address", locationText);
//
//			Invocation.Builder invocationBuilder = webTargetWithQueryParam.request(MediaType.TEXT_PLAIN_TYPE);
//
//			logger.info("Making call to web-service. Calls so far: " + callsToWebService);
//			Response response = invocationBuilder.get();
//			callsToWebService++;
//			if (response.getStatus() != 200) {
//				result = Optional.absent();
//			}
//
//			String responseString = response.readEntity(String.class);
//			logger.debug(responseString);
//
//			// Now parse the JSON response
//			try {
//				result = parseResponse(responseString, tweet);
//
//				if (result.get().getSource() == Source.TWEET_BIO_LOOKUP) {
//					if (locationInUK(result.get())) {
//						UKLocations.put(locationText.toLowerCase(), result.get());
//						logger.debug("UK Locations: " + UKLocations.keySet().toString());
//					} else {
//						nonUKLocations.add(locationText.toLowerCase());
//						logger.debug("Non UK Locations: " + nonUKLocations.toString());
//					}
//				}
//			} catch (Exception e) {
//				logger.debug("Could not parse response.");
//			}
//		}
//		return result;
//	}

	/**
	 * Get the location from the Bio text.
	 * 
	 * @param tweet
	 * @return
	 */
//	public Optional<TwitterLocation> getLocationFromBio(DBObject tweet) {
//		DBObject user = (DBObject) tweet.get("user");
//		String bioLocation = user.get("location").toString();
//
//		if (user.get("utc_offset") != null) {
//			int utc_offset = (Integer) user.get("utc_offset");
//			if (utc_offset != 0 && utc_offset != 3600) {
//				return Optional.absent();
//			}
//		}
//		return getLocation(bioLocation, tweet);
//	}

	/**
	 * Gets a location from geo-coded place information.
	 * 
	 * @param tweet
	 * @return
	 */
	public TwitterLocation getLocationFromPlaceInfo(DBObject tweet) {
		DBObject place = (DBObject) tweet.get("place");
		String country = standardiseUKName(place.get("country_code").toString());
		String locality = place.get("name").toString();
		DBObject coords = (DBObject) tweet.get("coordinates");

		Source source = Source.TWEET_GEO_CODED;
		Date date;
		try {
			date = TwitterUtils.getTwitterDate(tweet.get("created_at").toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.error("Could not parse date", e);
			date = new Date();
		}

		long tweetId = (Long) tweet.get("id");
		Object userIdObj = (((DBObject) tweet.get("user")).get("id"));
		long userId = userIdObj instanceof Long ? (Long) userIdObj : new Long((Integer) userIdObj);

		TwitterLocation result = new TwitterLocation(locality, country, DBQueries.createCoordsFromGeoJson(coords), source,
				tweetId, userId, date);

		return result;
	}

	/**
	 * Check is this location within the UK.
	 * 
	 * @param loc
	 * @return
	 */
	public boolean locationInUK(Location loc) {
		return ("UK".equals(loc.getCountry()));
	}

	/**
	 * Parse the response form web service to create a TwitterLocation object.
	 * 
	 * @param response
	 * @return an optional location
	 * @throws JsonParseException
	 * @throws IOException
	 */
//	private Optional<TwitterLocation> parseResponse(String response, DBObject tweet) throws JsonParseException, IOException {
//		JsonParser jsonParser = jsonFactory.createJsonParser(response);
//		double latitude = 0;
//		double longitude = 0;
//		String country = null;
//		String locality = null;
//		Source source = null;
//
//		while (jsonParser.nextToken() != null) {
//			String token = jsonParser.getCurrentName();
//
//			if ("location".equals(token)) {
//				jsonParser.nextToken();
//				JsonNode node = jsonParser.readValueAsTree();
//				latitude = (node.findValue("lat").getDoubleValue());
//				longitude = (node.findValue("lng").getDoubleValue());
//			}
//
//			if ("address_components".equals(token)) {
//				source = Source.TWEET_BIO_LOOKUP;
//
//				// move to the start of the array
//				jsonParser.nextToken();
//				AddressComponent[] addresses = jsonParser.readValueAs(AddressComponent[].class);
//				for (AddressComponent address : addresses) {
//					if (address.types.contains("country")) {
//						country = standardiseUKName(address.short_name);
//					} else if (address.types.contains("locality")) {
//						locality = address.short_name;
//					}
//
//				}
//			}
//
//			if ("status".equals(token)) {
//				jsonParser.nextToken();
//				if (!jsonParser.getText().equals("OK")) {
//					logger.warn("Error parsing JSON string. Status: " + jsonParser.getText());
//					return Optional.absent();
//				}
//			}
//		}
//
//		long tweetId = (Long) tweet.get("id");
//		Object userIdObj = (((DBObject) tweet.get("user")).get("id"));
//		long userId = userIdObj instanceof Long ? (Long) userIdObj : new Long((Integer) userIdObj);
//		Date date;
//		try {
//			date = TwitterUtils.getTwitterDate(tweet.get("created_at").toString());
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			logger.error("Could not parse date", e);
//			date = new Date();
//		}
//		TwitterLocation location = new TwitterLocation(locality, country, new GeoCoords(latitude, longitude), source, tweetId,
//				userId, date);
//		return Optional.fromNullable(location);
//	}

	private String standardiseUKName(String name) {
		if (name.equals("GB")) {
			return "UK";
		}
		return name;
	}

	public boolean tweetHasPlaceInfo(DBObject tweet) {
		return (tweet.get("place") != null) && (tweet.get("geo") != null);
	}

}
