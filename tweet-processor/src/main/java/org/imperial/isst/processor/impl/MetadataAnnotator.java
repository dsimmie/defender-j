package org.imperial.isst.processor.impl;

import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.imperial.isst.database.DBQueries;
import org.imperial.isst.entities.NamedEntityRecogniser;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.processor.classifier.ClassifierDetails;
import org.imperial.isst.processor.classifier.ClassifierProxy;
import org.imperial.isst.utils.KeywordUtils;
import org.imperial.isst.utils.StringUtils;
import org.imperial.isst.utils.TwitterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Takes a geo-coded tweet and outputs a version which has been processed ready for analysis.
 * 
 */
public class MetadataAnnotator {

	/**
	 * The keyword regular expression.
	 */
	private final Pattern keywordRegex;

	private final Map<String, Set<String>> reverseKeywords;

	/**
	 * Assign the tweet to a specific node in our network.
	 */
	private final NodeAssigner nodeAssigner;

	/**
	 * The binary health or not classifier.
	 */
	private ClassifierProxy healthClassifier;

	/**
	 * Whether to attempt to classify tweets
	 */
	private final boolean useClassifier;

	/**
	 * The named entity recognition component.
	 */
	private final NamedEntityRecogniser entityRecogniser;

	/**
	 * The class logger.
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass().getName());

	/**
	 * Initialises a new instance of the {@link MetadataAnnotator} class.
	 * 
	 * @param keywords
	 * @param healthClassifierConnection
	 * @throws UnknownHostException
	 */
	public MetadataAnnotator(Map<String, List<String>> keywords, ClassifierDetails healthClassifierConnection,
			boolean useClassifier, NamedEntityRecogniser entityRecogniser, Map<String, List<String>> incsandExcs,
			NodeAssigner nodeAssigner) throws UnknownHostException {

		reverseKeywords = KeywordUtils.getReverseMap(keywords);
		keywordRegex = KeywordUtils.buildRegex(KeywordUtils.getAllKeywords(keywords));
		this.nodeAssigner = nodeAssigner;
		if (useClassifier) {
			healthClassifier = new ClassifierProxy(healthClassifierConnection, incsandExcs);
		}

		this.useClassifier = useClassifier;
		this.entityRecogniser = entityRecogniser;
	}

	/**
	 * Add basic <i>tweet</i> metadata.
	 * 
	 * @param rawTweet
	 * @param processedTweet
	 */
	private void addBasicMetadata(DBObject rawTweet, DBObject processedTweet) {
		processedTweet.put("tweetId", rawTweet.get("id"));
		processedTweet.put("userId", ((DBObject) rawTweet.get("user")).get("id"));
		try {
			processedTweet.put("created_date", TwitterUtils.getTwitterDate(rawTweet.get("created_at").toString()));
		} catch (ParseException e) {
			processedTweet.put("created_date", new Date());
		}

		processedTweet.put("geo", rawTweet.get("geo"));
		processedTweet.put("locality", ((DBObject) rawTweet.get("place")).get("name").toString());
	}

	/**
	 * Add text-based metadata to the input <i>tweet</i> entity.
	 * <p>
	 * Metadata added:
	 * <ul>
	 * <li>URLs</li>
	 * <li>Mentions</li>
	 * <li>Hashtags</li>
	 * <li>Keywords</li>
	 * </ul>
	 * 
	 * @param rawTweet
	 * @param processedTweet
	 */
	private void addTextMetadata(DBObject rawTweet, DBObject processedTweet) {
		String rawText = rawTweet.get("text").toString();

		String noAscii = StringUtils.stripNonAscii(rawText);

		String parsedHTML = StringUtils.parseHTMLTags(noAscii);

		ArrayList<String> links = new ArrayList<String>();
		String noLinks = TwitterUtils.removeLinks(parsedHTML, links);

		processedTweet.put("links", links);

		// NER needs the uppercase information
		processedTweet.put("entities", DBQueries.convertEntities(entityRecogniser.find(noLinks)));

		ArrayList<String> mentions = new ArrayList<String>();
		String noMentions = TwitterUtils.removeMentions(noLinks, mentions).toLowerCase();

		processedTweet.put("mentions", mentions);

		ArrayList<String> tags = new ArrayList<String>();
		TwitterUtils.findHashTags(noMentions, tags);

		processedTweet.put("tags", tags);

		List<String> keyMatches = KeywordUtils.returnMatches(keywordRegex, noMentions);
		processedTweet.put("keywords", keyMatches);
		Set<String> symptomMatches = new HashSet<String>();
		for (String match : keyMatches) {
			symptomMatches.addAll(reverseKeywords.get(match));
		}
		processedTweet.put("symptoms", new ArrayList<String>(symptomMatches));

		processedTweet.put("processedText", noMentions);
	}

	/**
	 * Annotate a batch of <i>tweets</i> with relevant metadata.
	 * 
	 * Metadata:
	 * <ul>
	 * <li>URLs</li>
	 * <li>Mentions</li>
	 * <li>Hashtags</li>
	 * <li>Keywords</li>
	 * <li>Node assignment</li>
	 * <li>Health classification</li>
	 * </ul>
	 * 
	 * @param filteredTweets
	 * @return
	 */
	public List<DBObject> annotateBatch(List<DBObject> filteredTweets) {
		logger.info("Annotating batch of " + filteredTweets.size() + " filtered tweets.");
		List<DBObject> processedTweets = new ArrayList<DBObject>(filteredTweets.size());
		for (DBObject filteredTweet : filteredTweets) {
			processedTweets.add(processTweet(filteredTweet));
		}
		if (useClassifier) {
			healthClassifier.classify(processedTweets);
		}

		return processedTweets;
	}

	/**
	 * Process an individual <i>tweet</i> by adding relevant metadata to that post.
	 * 
	 * @param rawTweet
	 * @return the processed <i>tweet</i>
	 */
	public DBObject processTweet(DBObject rawTweet) {
		DBObject output = new BasicDBObject();
		addBasicMetadata(rawTweet, output);
		addTextMetadata(rawTweet, output);
		nodeAssigner.assignTweetToNode(rawTweet, output);
		return output;
	}
}
