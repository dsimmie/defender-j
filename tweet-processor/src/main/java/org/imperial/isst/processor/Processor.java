package org.imperial.isst.processor;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.docstreamer.DocStreamer;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * Post-process ingested <i>tweets</i>. *
 */
public abstract class Processor extends DefenderApplication {

	/**
	 * The processed <i>tweets</i> collection.
	 */
	protected DBCollection outputCollection;

	/**
	 * The input collection.
	 */
	protected DBCollection inputCollection;
	
	protected DBCollection classifierWordsCollection;

	/**
	 * Flag to indicate if this thread has stopped running.
	 */
	public static boolean stopped = false;

	/**
	 * The DB streaming component.
	 */
	public DocStreamer streamer;

	/**
	 * The size of the batch to process.
	 */
	private final int BATCH_SIZE = 200;

	/**
	 * Initialises a new instance of the {@link Processor} class.
	 * 
	 * @param configFile
	 */
	public Processor(String configFile) {
		super(configFile);
	}

	/**
	 * Add a shutdown hook.
	 */
	public void addShutdownBehaviour() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				logger.info("Stopping processor.");
				stopped = true;
			}
		});
	}

	/**
	 * Connect to the database collections.
	 * 
	 * @throws UnknownHostException
	 */
	public void connectCollections() throws UnknownHostException {
		outputCollection = connectToDB("output");
		outputCollection.ensureIndex(new BasicDBObject("tweetId", 1), null, true);
		DBObject index2d = BasicDBObjectBuilder.start("geo", "2dsphere").get();
		outputCollection.ensureIndex(index2d);
		outputCollection.ensureIndex("created_date");

		inputCollection = connectToDB("symptom");
		
		classifierWordsCollection = connectToDB("classwords");
	}

	/**
	 * Perform post-processing to a list of <i>tweets</i>.
	 * 
	 * @param tweet
	 */
	protected abstract void processTweetBatch(List<DBObject> tweet);

	/**
	 * Main location processing loop.
	 * 
	 * @param documents
	 */
	public void startProcessing(LinkedBlockingQueue<DBObject> documents) {
		logger.info("Starting Tweet Metadata Processing");
		boolean streamerStopped = false;

		while (true) {
			try {
				// shutdown behaviour
				if (stopped) {
					if (!streamerStopped) {
						logger.info("Stop signal received. Processor stopping gracefully.");
						streamer.stopStreaming();
						streamerStopped = true;
					}
					// If we've finished processing we can exit
					if (documents.size() == 0) {
						logger.info("Finished processing documents. Stopping.");
						break;
					}
				}

				logger.info("Documents in queue: " + documents.size());
				List<DBObject> tweetBatch = new LinkedList<DBObject>();

				for (int i = 0; i < BATCH_SIZE; i++) {
					DBObject tweet = documents.take();
					tweetBatch.add(tweet);
				}

				processTweetBatch(tweetBatch);
			} catch (Exception e) {
				logger.error("Exception detected in main processing loop: " + e.toString(), e);
			}
		}
	}
}
