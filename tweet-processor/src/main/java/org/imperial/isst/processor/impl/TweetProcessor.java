package org.imperial.isst.processor.impl;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.imperial.isst.entities.impl.StanfordNER;
import org.imperial.isst.location.Location;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.location.TwitterLocation;
import org.imperial.isst.processor.Processor;
import org.imperial.isst.processor.classifier.ClassifierDetails;
import org.imperial.isst.processor.location.CountryChecker;
import org.imperial.isst.utils.KeywordUtils;

import com.google.common.base.Optional;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

/**
 * Process incoming <i>tweets</i> by filtering for <i>tweets<i> with location set to UK and by adding metadata based on
 * performing textual analysis.
 */
public class TweetProcessor extends Processor {

	/**
	 * Check the country of <i>tweet</i>.
	 */
	private final CountryChecker checker;

	/**
	 * The list of keywords for tagging.
	 */
	private HashMap<String, List<String>> keywords;

	/**
	 * The <i>tweet</i> metadata annotation component.
	 */
	private MetadataAnnotator metadataAnnotator;
	

	/**
	 * Initialises a new instance of the {@link TweetProcessor} class.
	 * 
	 * @param configFile
	 */
	public TweetProcessor(String configFile) {
		super(configFile);
		checker = new CountryChecker(configProperties.getProperty("georesource"));
		
	}

	/**
	 * Find the location of a <i>tweet</i>.
	 * 
	 * @param tweet
	 * @return
	 */
	public Optional<TwitterLocation> findLocationOf(DBObject tweet) {
		Optional<TwitterLocation> result = Optional.absent();
		if (tweet.get("id") != null) {
			logger.debug("Finding location for tweet: " + tweet.get("id").toString());
		} else {
			logger.warn("Anomalous input detected: " + tweet.toString());
		}

		if (checker.tweetHasPlaceInfo(tweet)) {
			result = Optional.fromNullable(checker.getLocationFromPlaceInfo(tweet));
		} else {
			result = Optional.absent();
		}

		return result;
	}

	/**
	 * Get the classifier connection details.
	 * 
	 * @param name
	 * @return
	 */
	private ClassifierDetails getConnection(String name) {
		return new ClassifierDetails(name, configProperties.getProperty(name + ".classifier.host"),
				Integer.parseInt(configProperties.getProperty(name + ".classifier.port.send")),
				Integer.parseInt(configProperties.getProperty(name + ".classifier.port.receive")));
	}

	/**
	 * Get the JSON encoding of a location object.
	 * 
	 * @param loc
	 * @return
	 */
	public String getJSONFromLocation(Location loc) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
		try {
			return mapper.writeValueAsString(loc);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * Process a batch of <i>tweets</i>.
	 * 
	 * @param batch
	 * @see org.imperial.isst.processor.Processor#processTweetBatch(java.util.List)
	 */
	@Override
	protected void processTweetBatch(List<DBObject> batch) {
		logger.info("Processing batch of " + batch.size() + " tweets.");
		List<DBObject> filteredTweets = new LinkedList<DBObject>();
		for (DBObject tweet : batch) {
			Optional<TwitterLocation> loc = findLocationOf(tweet);
			if (loc.isPresent() && checker.locationInUK(loc.get())) {
				logger.debug("UK tweet found. Archiving.");
				filteredTweets.add(tweet);
			} else {
				logger.debug("Tweet not in UK. " + tweet.toString());
			}
		}

		logger.info("Filtered tweets size: " + filteredTweets.size());
		if (!filteredTweets.isEmpty()) {
			List<DBObject> processedTweets = metadataAnnotator.annotateBatch(filteredTweets);
			try {
				for (DBObject obj: processedTweets) {
					try {
						outputCollection.insert(obj);
					} catch (MongoException.DuplicateKey e) {
						logger.debug("Duplicate key found. Ignoring.");
						
					}
					
				}				
			} catch (Exception e) {
				logger.info("Exception writing to database. Exception: " + e.toString() + " Procssed batch size: "
						+ processedTweets.size());
			}
		}
	}

	/**
	 * Start processing the collection of <i>tweet</i> documents.
	 * 
	 * @param documents
	 * @see org.imperial.isst.processor.Processor#startProcessing(java.util.concurrent.LinkedBlockingQueue)
	 */
	@Override
	public void startProcessing(LinkedBlockingQueue<DBObject> documents) {
		logger.info("Start processing.");
		keywords = KeywordUtils.loadKeywords(inputCollection);
		Map<String, List<String>> incsAndExcs = KeywordUtils.loadInclusionsAndExclusions(classifierWordsCollection);
		boolean useClassifier = Boolean.valueOf(configProperties.getProperty("useClassifier"));
		try {
			NodeAssigner assigner = new NodeAssigner(connectToDB("node"));
			metadataAnnotator = new MetadataAnnotator(keywords, getConnection("health"), useClassifier, new StanfordNER(), incsAndExcs, assigner);
		} catch (UnknownHostException e) {
			logger.info("Error loading tweet text metadataAnnotator: " + e.getMessage());
		}
		super.startProcessing(documents);
	}
}
