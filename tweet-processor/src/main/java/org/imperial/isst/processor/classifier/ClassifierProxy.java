/**
 * ClassifierProxy.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.processor.classifier;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.imperial.isst.utils.KeywordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

import com.mongodb.DBObject;

/**
 * Proxy for Python-based classifier.
 * 
 * <p>
 * The communication layer is handled by the ZeroMQ messaging library.
 * 
 * @author Donal Simmie
 */
public class ClassifierProxy {

	/**
	 * The amount of time for the socket to wait until the message buffer has been emptied. if set to -1 the time is
	 * infinite and the socket will block indefinitely or until the buffer empty.
	 * <p>
	 * This value is set in milliseconds.
	 */
	private static final int LINGER_TIME = 1000;

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		ClassifierProxy proxy = new ClassifierProxy(new ClassifierDetails("", "127.0.0.1", 5555, 5556), null);
	}

	/**
	 * The ZeroMQ messaging context.
	 */
	private final ZMQ.Context context;

	/**
	 * The ZeroMQ socket to <b>send</b> classification messages to.
	 */
	private final ZMQ.Socket sender;

	/**
	 * The ZeroMQ socket to <b>receive</b> classification messages from.
	 */
	private final ZMQ.Socket receiver;

	/**
	 * The classifier communication details.
	 */
	private final ClassifierDetails details;

	/**
	 * The class logger.
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass().getName());
	
	private Pattern exclusions;
	private Pattern inclusions;

	/**
	 * Initialises a new instance of the {@link ClassifierProxy} class.
	 * 
	 */
	public ClassifierProxy(ClassifierDetails details, Map<String, List<String>> incsandExcs) {
		this.details = details;

		if (incsandExcs != null) {
			exclusions = KeywordUtils.buildRegex(incsandExcs.get("exclusion"));
			inclusions = KeywordUtils.buildRegex(incsandExcs.get("inclusion"));		
		}

		
		context = ZMQ.context(1);
		String baseUrl = "tcp://" + details.getHost() + ":";

		sender = context.socket(ZMQ.PUSH);
		sender.bind(baseUrl + String.valueOf(details.getSendPort()));
		sender.setLinger(LINGER_TIME);

		receiver = context.socket(ZMQ.PULL);
		receiver.bind(baseUrl + String.valueOf(details.getReceivePort()));
		receiver.setLinger(LINGER_TIME);

		logger.info("Starting classifier proxy: " + details.getName() + ". Host: " + details.getHost()
				+ " sender port: " + details.getSendPort() + " receiver port: " + details.getReceivePort());
		closeZmqSocketsOnShutdown();
	}

	/**
	 * Classifies a batch of processed <i>tweets</i>.
	 * <p>
	 * This method is a proxy for classification via communication with classifier service.
	 * 
	 * @param rawTweet
	 * @param output
	 * @return
	 */
	public void classify(List<DBObject> processedTweets) {
		logger.info("Classifying " + processedTweets.size() + " tweets");

		sender.send(processedTweets.toString().getBytes(), 0);

		String response = receiver.recvStr();

		logger.info("Received classification response: " + response);

		String[] labels = response.split(",");

		/*
		 * Add the retrieved classification labels to the processed tweets collection.
		 */
		for (int i = 0; i < processedTweets.size(); i++) {
			DBObject tweet = processedTweets.get(i);
			boolean exclude = KeywordUtils.keywordMatch(exclusions, tweet.get("processedText").toString());
			if (exclude) {
				tweet.put(details.getName(), "Not");
			} else {
				boolean include = KeywordUtils.keywordMatch(inclusions, tweet.get("processedText").toString());
				if (include) {
					tweet.put(details.getName(), "Health");
				} else {
					tweet.put(details.getName(), labels[i]);
				}
			}
		}
	}

	/**
	 * Add a shutdown hook to close socket connections.
	 */
	public void closeZmqSocketsOnShutdown() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				sender.close();
				receiver.close();
				context.term();
			}
		});
	}
}