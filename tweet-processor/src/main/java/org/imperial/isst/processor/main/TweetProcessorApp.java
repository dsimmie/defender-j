package org.imperial.isst.processor.main;

import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;

import org.imperial.isst.docstreamer.DocStreamer;
import org.imperial.isst.processor.Processor;
import org.imperial.isst.processor.impl.TweetProcessor;

import com.mongodb.DBObject;

public class TweetProcessorApp {

	
	public static void main(String[] args) {
		Processor processor;
		String name;
		try {
			processor = new TweetProcessor("/config.properties");
			processor.connectCollections();

			DocStreamer streamer;
			try {
				if (args.length > 0) {
					name = args[0];
				} else {
					name = processor.getClass().getName();
				}
				streamer = new DocStreamer("/config.properties", "id", name );
				processor.streamer = streamer;
				
				processor.addShutdownBehaviour();

				LinkedBlockingQueue<DBObject> documents = streamer.beginStreaming();
				processor.startProcessing(documents);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
