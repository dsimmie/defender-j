package org.imperial.isst.forecaster;

import java.text.ParseException;

import org.junit.Test;

public class ForecasterTest {

	
	
	// So...
	// 	For each of these symptoms
	//		Do a forecast per week using the previous 4 weeks
	// We're interested in... 
	// 		Which nodes it gets right, which wrong
	//			MAE/node, per date, totals
	
	// How does changing window size from 4 weeks or forecasting size from 7 days affect this?
	// Just look at last days error?
	// Use MAPE - divide absolute error by actual value. If 0 divide by average of past 7 days instead.
	// Also consider MASE
	// Divide out the results by 0 nodes and other nodes.
	
	@Test
	public void testFullForecast() throws ParseException {
//		Date date = DateHandler.sdf.parse("2014-05-15");
//		Forecaster caster = new Forecaster("/config.properties");
//		caster.forecast("sorethroat", date);
	}
//	
//	@Test
//	public void testJRI() throws REXPMismatchException, REngineException {
//		RConnection c = new RConnection("ib-dsimmie.ib.ic.ac.uk");
//
//		REXP x = c.eval("R.version.string");
//		System.out.println(x.asString());
//		
//		double darr[] = c.eval("rnorm(10)").asDoubles();
//		for (double d: darr ) {
//			System.out.println(String.valueOf(d));
//		}
//		c.assign("y", darr);
//		System.out.println(Arrays.toString(c.eval("y").asDoubles()));
//		c.close();
//	}
	
//	@Test
//	public void testRLMForecasts() {
//		Forecaster caster = new Forecaster("/config.properties");
//		List<Double> series = new ArrayList<Double>();
//		series.add(1.0E-7);
//		series.add(2.0);
//		series.add(4.0);
//		List<Double> symps = new ArrayList<Double>();
//		symps.add(0.0);
//		symps.add(1.0);
//		symps.add(2.0);
//		List<Double> nextSymps = new ArrayList<Double>();
//		nextSymps.add(3.0);
//		List<Double> fores = caster.getRLMForecast(series, symps, nextSymps, 1);
//		System.out.println(fores.toString());
//	}
	
//	@Test
//	public void lagTests() throws ParseException, IOException {
//		Date date = DateHandler.sdf.parse("2014-05-02");
//		File outputFile = new File(System.getProperty("user.home") + "/forecasts/" + "lagtests" + DateHandler.sdf.format(date)+ ".csv");
//		outputFile.getParentFile().mkdirs();
//		CSVWriter writer = new CSVWriter(new FileWriter(outputFile));
//		
//		ArrayList<String> symptoms = new ArrayList<String>(Arrays.asList("sorethroat", "tonsillitis", "commoncold", "flu", "cough"));
//		String[] firstLine = {"Lag","0","1","2","3","4","5","6"};
//		writer.writeNext(firstLine);
//		
//		
//		for (String symptom: symptoms) {
//			System.out.println("Test for symptom: " + symptom);
//			String[] line = new String[8];
//			line[0] = symptom;
//			for (int i = 0; i < 7; i++) {
//				Forecaster caster = new Forecaster("/config.properties");
//				double error = caster.forecast(symptom, date,i);
//				line[i+1] = String.valueOf(error);
//				System.out.println(error);
//			}
//			writer.writeNext(line);
//			
//		}
//		writer.close();	
//	}
	
//	@Test
//	public void testWekaForecast() throws Exception {
//		Forecaster caster = new Forecaster("/config.properties");
//		List<Double> vals = new ArrayList<Double>();
//		vals.add(0.0);
//		vals.add(0.0);
//		vals.add(0.0);
//		List<Double> res = caster.getWekaForecast(vals, 1);
//		System.out.println(res.toString());
//	}
//	
//	@Test
//	public void testBasicForecast() throws Exception {
//		try {
//			Forecaster caster = new Forecaster("/config.properties");
//			List<Double> vals = caster.getTimeSeries("2", DateHandler.sdf.parse("2014-04-02"), "sorethroat");
//			List<Double> pred = caster.getBasicForecast(vals, 7);
//			List<Double> wekaPred = caster.getWekaForecast(vals, 7);
//			List<Double> actual = caster.getFutureTimeSeries("2", DateHandler.sdf.parse("2014-04-02"), "sorethroat", 7);
//			
//			File outputFile = new File("output.csv");
//
//			CSVWriter writer = new CSVWriter(new FileWriter(outputFile));
//		
//			for (double d: vals) {
//				String[] line = new String[3];
//				line[0] = String.valueOf(d);
//				writer.writeNext(line);
//			}	
//			for (int i = 0; i < pred.size(); i++) {
//				String[] line = new String[4];
//				line[1] = String.valueOf(pred.get(i));
//				if (i < wekaPred.size()) {
//					line[2] = String.valueOf(wekaPred.get(i));			
//				}
//				if (i < actual.size()) {
//					line[3] = String.valueOf(actual.get(i));			
//				}
//
//				writer.writeNext(line);
//			}
//			writer.close();
//		} catch (Exception e) {
//			
//		}
//
//	}
}
