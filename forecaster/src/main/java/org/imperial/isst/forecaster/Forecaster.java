package org.imperial.isst.forecaster;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.date.DateFormat;
import org.imperial.isst.date.DateHandler;
import org.imperial.isst.graph.Neo4JFactory;
import org.imperial.isst.graph.Neo4JUtils;
import org.imperial.isst.location.NodeAssigner;
import org.imperial.isst.utils.TableUtils;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import com.google.common.collect.ArrayTable;

public class Forecaster extends DefenderApplication {

	// Arguments - symptom then date in yyyy-mm-dd
	public static void main(String[] args) throws ParseException {
		Forecaster caster = new Forecaster("/config.properties");
		if (args.length == 2) {
			String symptom = args[0];
			String date = args[1];
			caster.forecast(symptom, DateHandler.sdf.parse(date));
		} else {
			caster.multiForecast();
		}
	}

	private RestGraphDatabase graphDb;

	private RestCypherQueryEngine engine;

	private ExecutionEngine embeddedEngine;
	private final int TIMESERIES_LENGTH = 28;
	private final int FORECAST_LENGTH = 7;
	private final String ACTUAL = "Actual";
	private final String NAIVE = "Naive";
	private final String ADJUSTED = "Adjusted";
	private final String INCOMING = "Incoming";
	private final String ACTUAL_INCOMING = "ActualIncoming";
	// private final String[] symptomsToCheck = {"sorethroat", "tonsillitis", "commoncold", "flu", "cough" };
	private final String[] symptomsToCheck = { "cough" };
	private final String start = "2014-02-11";
	private final String end = "2014-06-11";

	// private final String end = "2014-04-11";
	private int forecastLockNumber;
	private NodeAssigner nodeAssigner;

	private List<List<Double>> errorSeries;
	private double grandTotalNaiveError = 0;
	private double grandTotalAdjustedError = 0;

	private int numberOfRuns = 0;
	// These start at t0-29
	// Gets the forecasts added at the end
	private Map<String, List<Double>> nodeSeries;

	// holds the whole series directly from the data
	private Map<String, List<Double>> originalNodeSeries;
	// These start at t1
	private Map<String, List<Double>> nodeForecasts;
	private Map<String, List<Double>> naiveNodeForecasts;
	// This starts at t0
	private Map<String, List<Double>> incomingSymptomCounts;

	private Map<String, List<Double>> incomingActualSymptomCounts;
	private ArrayTable<Date, String, Double> output;
	private ArrayTable<Date, String, Double> errors;

	private List<Date> dateRange;

	public Forecaster(String configFile) {
		super(configFile);
		setupVariables();
		setupErrorTable();
	}

	private void addIncomingSymptomCounts(Date date) {
		for (String node : incomingSymptomCounts.keySet()) {
			incomingSymptomCounts.get(node).add(getIncomingSymptoms(node, date));
		}
	}

	private double calculateError(Date date) {
		double totalNaiveError = 0;
		double totalAdjustedError = 0;

		int numberOfNodes = 0;
		for (String node : nodeAssigner.getNodeNames()) {
			// Put last element of actual series for comparisons
			double lastValue = originalNodeSeries.get(node).get(originalNodeSeries.get(node).size() - 1);

			System.out.println("Last value for node " + node + ": " + lastValue);
			double naiveError = 0;
			double adjustedError = 0;

			if (lastValue > 0) {
				numberOfNodes++;
				int seriesLength = originalNodeSeries.get(node).size();
				int forecastLength = nodeForecasts.get(node).size();
				double actualFigure = originalNodeSeries.get(node).get(seriesLength - 1);
				naiveError = Math.abs(actualFigure - naiveNodeForecasts.get(node).get(forecastLength - 1));
				adjustedError = Math.abs(actualFigure - nodeForecasts.get(node).get(forecastLength - 1));

				totalNaiveError += naiveError;
				totalAdjustedError += adjustedError;

				if (errors.rowKeyList().contains(date)) {
					errors.put(date, getLabel(node, ACTUAL), lastValue);
					errors.put(date, getLabel(node, NAIVE), naiveError);
					errors.put(date, getLabel(node, ADJUSTED), adjustedError);
				}
			}

		}

		totalNaiveError = totalNaiveError / numberOfNodes;
		totalAdjustedError = totalAdjustedError / numberOfNodes;
		System.out.println("Mean naive error: " + totalNaiveError);
		System.out.println("Mean adjusted error: " + totalAdjustedError);

		grandTotalAdjustedError += totalAdjustedError;
		grandTotalNaiveError += totalNaiveError;
		numberOfRuns++;

		return (totalNaiveError - totalAdjustedError) / totalNaiveError;
	}

	public void clearDatabase() {
		ExecutionResult result;
		result = embeddedEngine.execute("MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r");
		result.iterator().close();
	}

	public void clearForecastNodes() {
		String query = "MATCH (n:" + Neo4JUtils.FORECAST + " {lockId: " + forecastLockNumber
				+ "}) OPTIONAL MATCH (n)-[r]-() DELETE n,r";
		engine.query(query, new HashMap<String, Object>());
	}

	private void createAdjustedForecasts(Map<String, Double> forecasts) {
		for (String node : forecasts.keySet()) {
			Double newForecast = Math.max(0, forecasts.get(node));
			nodeForecasts.get(node).add(newForecast);
			nodeSeries.get(node).add(newForecast);
		}
	}

	public void createForecastNodes(String symptom) {
		// Use data from the first date we're interested in
		Date date = dateRange.get(0);
		String dateStr = "\"" + DateHandler.sdf.format(date) + "\"";

		// First copy the data from the data nodes into the forecast nodes
		engine.query("match (a:" + Neo4JUtils.DATA + " {date:" + dateStr + "}) create" + " (b: " + Neo4JUtils.FORECAST
				+ " {lockId: " + forecastLockNumber + ", name:a.name, userCount:a.userCount, locality:a.locality, "
				+ "symptom:a." + symptom + "})", new HashMap<String, Object>());

		// Then copy the relationships from the reference nodes
		String statement = String.format("match (a:%s)-[r]->(b:%s), "
				+ "(c:%s {lockId: %d, name:str(a.nodeId)}),(d:%s {lockId: %d, name:str(b.nodeId)}) "
				+ "merge (c)-[r2:%s {weekDay:r.weekDay, weekEnd:r.weekEnd}]->(d)", Neo4JUtils.REFERENCE,
				Neo4JUtils.REFERENCE, Neo4JUtils.FORECAST, forecastLockNumber, Neo4JUtils.FORECAST, forecastLockNumber,
				Neo4JUtils.FORECAST_REL);

		engine.query(statement, new HashMap<String, Object>());

		updateForecastRelationships(date, symptom);
	}

	public void forecast(String symptom, Date date) {
		// The order of these is important
		forecastLockNumber = getForecastLockNumber();
		try {
			setupForecasting(date);
			logger.info("Fetching time series");
			getNodeTimeSeries(symptom, date);
			setupOutputTable(date);
			logger.info("Get naive forecasts.");
			getNaiveForecasts();
			createForecastNodes(symptom);
			logger.info("Build initial symptom counts.");
			getInitialSymptomCounts(symptom);
			// t0 is now set up - can start iterating
			Date currentTimeStep = date;
			logger.info("Starting forecasting iterations");
			// Forecast for t+1 and store the results in a temporary place
			// Set up the forecast graph with the t+1 figures just worked out
			// Work out the incoming symptoms for t+1
			// Adjust the forecast and save it
			// Add the forecast to the time series
			for (int i = 0; i < FORECAST_LENGTH; i++) {
				logger.info("Forecast iteration: " + i);
				Calendar cal = DateHandler.getCalendar();
				cal.setTime(currentTimeStep);
				cal.add(Calendar.DATE, 1);
				currentTimeStep = cal.getTime();
				Map<String, Double> tomorrowsForecasts = getNextForecasts();
				updateForecastNodes(tomorrowsForecasts);
				addIncomingSymptomCounts(currentTimeStep);
				createAdjustedForecasts(tomorrowsForecasts);
			}
		} finally {
			clearForecastNodes();
		}

		outputForecastsToCSV(symptom, date);
		calculateError(date);

	}

	private double getActualIncomingSymptoms(String node, Date date) {
		return 0.0;
		// Double res = 0.0;
		// String statement = String
		// .format("match (n:%s { lockId: %d, name:\"%s\" } ) with n match (n)-[r]-() with r "
		// + "return sum(r.actualsymptom) as res"
		// , Neo4JUtils.FORECAST, forecastLockNumber, node);
		//
		// long startTime = System.currentTimeMillis();
		// QueryResult<Map<String, Object>> result = engine.query(statement,
		// new HashMap<String, Object>());
		// logger.debug("Fetched actual symptoms for node: " + node + ". Completed in "
		// + (System.currentTimeMillis() - startTime) + " ms.");
		// for (Map<String, Object> row : result) {
		// Object returnedVal = row.get("res");
		// if (returnedVal instanceof Double) {
		// res = (Double) returnedVal;
		// } else {
		// res = ((Integer) returnedVal).doubleValue();
		// }
		//
		// }
		// return res;
	}

	public List<Double> getARIMAForecast(List<Double> timeSeries, int numberOfDays) {
		List<Double> res = new ArrayList<Double>();
		RConnection c = null;
		try {
			c = new RConnection(configProperties.getProperty("rhost"));
		} catch (RserveException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		double[] times = new double[timeSeries.size()];
		for (int i = 0; i < timeSeries.size(); i++) {
			times[i] = timeSeries.get(i);
		}

		try {
			c.voidEval("library(forecast)");
			c.assign("timeSeries", times);
			c.voidEval("fit1 <- auto.arima(ts(timeSeries))");
			c.voidEval(String.format("f <- forecast(fit1, %d)", numberOfDays));
			double[] forecasts = c.eval("f$mean").asDoubles();
			for (double d : forecasts) {
				res.add(d);
			}
		} catch (REngineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorSeries.add(new ArrayList<Double>(timeSeries));
			res.add(0.0);
		} catch (REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorSeries.add(new ArrayList<Double>(timeSeries));
			res.add(0.0);
		}

		c.close();

		return res;
	}

	private int getForecastLockNumber() {
		Node lockNode = null;
		int res;

		String queryString = "MERGE (n:LOCK) RETURN n";
		QueryResult<Map<String, Object>> result = engine.query(queryString, new HashMap<String, Object>());

		for (Map<String, Object> row : result) {
			lockNode = (Node) row.get("n");
		}

		try (Transaction tx = graphDb.beginTx()) {
			// tx.acquireWriteLock( lockNode );
			Integer lockId = (Integer) lockNode.getProperty("lockId", null);
			if (lockId == null) {
				res = 1;
			} else {
				res = lockId;
			}
			lockNode.setProperty("lockId", res + 1);
			tx.success();
		}
		return res;

	}

	private List<Date> getForecastRange() {
		List<Date> res = new ArrayList<Date>();
		Date startDate;
		try {
			startDate = DateHandler.sdf.parse(start);
			Date endDate = DateHandler.sdf.parse(end);
			Calendar start = DateHandler.getCalendar();
			start.setTime(startDate);
			start.add(Calendar.DATE, TIMESERIES_LENGTH);

			Calendar end = DateHandler.getCalendar();
			end.setTime(endDate);
			for (Date date = start.getTime(); !start.after(end); start.add(Calendar.DATE, TIMESERIES_LENGTH), date = start
					.getTime()) {
				res.add(date);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return res;

	}

	public List<Double> getFutureTimeSeries(String node, Date date, String symptom, int numberOfDays) {
		List<Double> res = new ArrayList<Double>();
		String dateStr = DateHandler.sdf.format(date);
		String queryStr = String.format(
				"match (a:%s {name:\"%s\", date:\"%s\"}) with a match (a)<-[:TEMPORAL*1..%d]-(b) return b",
				Neo4JUtils.DATA, node, dateStr, numberOfDays);
		QueryResult<Map<String, Object>> result = engine.query(queryStr, new HashMap<String, Object>());

		// logger.info(result.toString());
		for (Map<String, Object> row : result) {
			Node n = (Node) row.get("b");
			res.add((Double) n.getProperty(symptom));
		}
		// logger.info(res.toString());

		return res;

	}

	public double getIncomingSymptoms(String node, Date date) {
		String edgeToUse;
		// if (DateHandler.isWeekend(date)) {
		// edgeToUse = "weekEnd";
		// } else {
		edgeToUse = "weekDay";
		// }
		Double res = 0.0;
		String statement = String.format(
				"match (n:%s { lockId: %d, name:\"%s\" } ) with n match (n)-[r]-(b) where b.userCount > 0 with r,b "
						+ "return sum(b.symptom*r." + edgeToUse + "/b.userCount) as res", Neo4JUtils.FORECAST,
				forecastLockNumber, node);

		long startTime = System.currentTimeMillis();
		QueryResult<Map<String, Object>> result = engine.query(statement, new HashMap<String, Object>());
		logger.debug("Fetched symptoms for node: " + node + ". Completed in "
				+ (System.currentTimeMillis() - startTime) + " ms.");
		for (Map<String, Object> row : result) {
			Object returnedVal = row.get("res");
			if (returnedVal instanceof Double) {
				res = (Double) returnedVal;
			} else {
				res = ((Integer) returnedVal).doubleValue();
			}

		}
		return res;
	}

	private void getInitialSymptomCounts(String symptom) {
		// Do it an extra time to include the present day - don't want to update
		// the nodes for this day though
		for (int i = 0; i < TIMESERIES_LENGTH + 1; i++) {
			Date date = dateRange.get(i);
			logger.debug("Getting symptom counts for: " + date.toString());
			long startTime = System.currentTimeMillis();
			Map<String, Double> nextValues = new HashMap<String, Double>();
			for (String node : nodeAssigner.getNodeNames()) {
				incomingSymptomCounts.get(node).add(getIncomingSymptoms(node, date));
				incomingActualSymptomCounts.get(node).add(getActualIncomingSymptoms(node, date));
				if (i != TIMESERIES_LENGTH) {
					nextValues.put(node, nodeSeries.get(node).get(i + 1));
				}

			}
			logger.debug("Completed in " + (System.currentTimeMillis() - startTime) + " ms.");

			if (i != TIMESERIES_LENGTH) {
				logger.debug("Updating forecast nodes with new values.");
				updateForecastNodes(nextValues);
				updateForecastRelationships(date, symptom);
			}

		}

	}

	private String getLabel(String node, String labelType) {
		return labelType + ":" + node;
	}

	private void getNaiveForecasts() {
		naiveNodeForecasts = new HashMap<String, List<Double>>();
		for (String node : nodeAssigner.getNodeNames()) {
			try {
				naiveNodeForecasts.put(node, getARIMAForecast(nodeSeries.get(node), FORECAST_LENGTH));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private Map<String, Double> getNextForecasts() {
		Map<String, Double> res = new HashMap<String, Double>();
		for (String node : nodeAssigner.getNodeNames()) {
			try {
				// List<Double> forecast = getBasicForecast(nodeSeries.get(node),
				// 1);
				int symptomCount = incomingSymptomCounts.get(node).size();
				List<Double> forecast = getRLMForecast(nodeSeries.get(node).subList(1, symptomCount),
						incomingSymptomCounts.get(node).subList(0, symptomCount - 1), incomingSymptomCounts.get(node)
								.subList(symptomCount - 1, symptomCount), 1);
				res.put(node, forecast.get(0));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return res;
	}

	private void getNodeTimeSeries(String symptom, Date date) {
		nodeSeries = new HashMap<String, List<Double>>();
		originalNodeSeries = new HashMap<String, List<Double>>();
		for (String node : nodeAssigner.getNodeNames()) {
			// long startTime = System.currentTimeMillis();
			List<Double> timeSeries = getTimeSeries(node, date, symptom);
			nodeSeries.put(node, timeSeries);

			List<Double> wholeSeries = new ArrayList<Double>();
			wholeSeries.addAll(timeSeries);

			// Get the whole actual series
			List<Double> futureSeries = getFutureTimeSeries(node, date, symptom, FORECAST_LENGTH);
			wholeSeries.addAll(futureSeries);
			originalNodeSeries.put(node, wholeSeries);

		}

	}

	public List<Double> getRLMForecast(List<Double> timeSeries, List<Double> incomingSymptoms,
			List<Double> nextSymptoms, int numberOfDays) {

		List<Double> res = new ArrayList<Double>();
		RConnection c = null;
		try {
			c = new RConnection(configProperties.getProperty("rhost"));
		} catch (RserveException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		double[] times = new double[timeSeries.size()];
		double[] symps = new double[incomingSymptoms.size()];
		double[] nextSymps = new double[nextSymptoms.size()];
		for (int i = 0; i < timeSeries.size(); i++) {
			times[i] = timeSeries.get(i);
			symps[i] = incomingSymptoms.get(i);
		}

		for (int i = 0; i < nextSymptoms.size(); i++) {
			nextSymps[i] = nextSymptoms.get(i);
		}
		try {
			c.voidEval("library(forecast)");
			c.assign("timeSeries", times);
			c.assign("symptoms", symps);
			c.assign("nextSymps", nextSymps);

			// c.voidEval("combined.symptoms <- cbind(symptoms[1:28],c(NA,NA,NA,NA,NA,NA,NA,symptoms[1:21]),"
			// + "c(NA,NA,NA,NA,NA,NA,NA,actualincoming[1:21]))" );
			// c.voidEval("combined.nextsymps <- cbind(nextSymps, symptoms[length(symptoms)-6], "
			// + "actualincoming[length(actualincoming)- 6 + (length(symptoms)-length(actualincoming))] ) ");
			// System.out.println(c.eval("combined.symptoms").toDebugString());
			// System.out.println(c.eval("combined.nextsymps").toDebugString());
			c.voidEval("fit1 <- auto.arima(ts(timeSeries), xreg=ts(symptoms))");
			c.voidEval(String.format("f <- forecast(fit1, %d, xreg=nextSymps  )", numberOfDays));
			double[] forecasts = c.eval("f$mean").asDoubles();
			for (double d : forecasts) {
				res.add(d);
			}
		} catch (REngineException e) {
			try {
				System.out.println(c.parseAndEval("geterrmessage()").toDebugString());
			} catch (REngineException | REXPMismatchException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorSeries.add(new ArrayList<Double>(timeSeries));
			res.add(0.0);
		} catch (REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorSeries.add(new ArrayList<Double>(timeSeries));
			res.add(0.0);
		}

		c.close();

		return res;
	}

	public List<Double> getTimeSeries(String node, Date date, String symptom) {
		List<Double> res = new ArrayList<Double>();
		String dateStr = DateHandler.sdf.format(date);
		String queryStr = String.format(
				"match (a:%s {name:\"%s\", date:\"%s\"}) with a match (a)-[:TEMPORAL*0..%d]->(b) return b",
				Neo4JUtils.DATA, node, dateStr, TIMESERIES_LENGTH);
		QueryResult<Map<String, Object>> result = engine.query(queryStr, new HashMap<String, Object>());

		// logger.info(result.toString());
		for (Map<String, Object> row : result) {
			Node n = (Node) row.get("b");
			res.add((Double) n.getProperty(symptom));
		}
		// logger.info(res.toString());
		Collections.reverse(res);

		return res;

	}

	public void multiForecast() {
		// Set up the date range for forecasting
		// Create the errors output table
		// This has date rows, columns are two per node - naive and adjusted forecasts

		for (String symptom : symptomsToCheck) {
			for (Date date : getForecastRange()) {
				setupVariables();
				forecast(symptom, date);
			}
		}
		outputErrorsToCSV();
		grandTotalAdjustedError = grandTotalAdjustedError / numberOfRuns;
		grandTotalNaiveError = grandTotalNaiveError / numberOfRuns;
		logger.info("Grand total naive error: " + grandTotalNaiveError);
		logger.info("Grand total adjusted error: " + grandTotalAdjustedError);

	}

	private void outputErrorsToCSV() {
		try {
			TableUtils.outputToCSV(errors, System.getProperty("user.home") + "/forecasts/allErrors.csv",
					DateFormat.PRETTY);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void outputForecastsToCSV(String symptom, Date forecastDate) {
		System.out.println("Series in error: " + errorSeries.size());
		for (int i = 0; i < errorSeries.size(); i++) {
			System.out.println(errorSeries.get(i).toString());
		}
		int index = 0;
		int forecastIndex = 0;
		for (Date date : output.rowKeyList()) {
			for (String node : originalNodeSeries.keySet()) {
				output.put(date, getLabel(node, ACTUAL), originalNodeSeries.get(node).get(index));
				output.put(date, getLabel(node, INCOMING), incomingSymptomCounts.get(node).get(index));
				if (date.before(forecastDate)) {
					output.put(date, getLabel(node, ACTUAL_INCOMING), incomingActualSymptomCounts.get(node).get(index));
				}

				if (date.after(forecastDate)) {
					output.put(date, getLabel(node, NAIVE), naiveNodeForecasts.get(node).get(forecastIndex));
					output.put(date, getLabel(node, ADJUSTED), nodeForecasts.get(node).get(forecastIndex));

				}
			}
			index++;
			if (date.after(forecastDate)) {
				forecastIndex++;
			}
		}
		try {
			TableUtils.outputToCSV(output,
					System.getProperty("user.home") + "/forecasts/" + symptom + DateHandler.sdf.format(forecastDate)
							+ ".csv", DateFormat.PRETTY);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setupErrorTable() {
		List<String> errorLabels = new ArrayList<String>();

		for (String node : nodeAssigner.getNodeNames()) {
			errorLabels.add(getLabel(node, ACTUAL));
			errorLabels.add(getLabel(node, NAIVE));
			errorLabels.add(getLabel(node, ADJUSTED));
		}

		errors = ArrayTable.create(getForecastRange(), errorLabels);
	}

	private void setupForecasting(Date date) {
		nodeForecasts = new HashMap<String, List<Double>>();
		for (String node : nodeAssigner.getNodeNames()) {
			nodeForecasts.put(node, new ArrayList<Double>());
		}

	}

	private void setupOutputTable(Date date) {
		Calendar cal = DateHandler.getCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, (nodeSeries.values().iterator().next().size() - 1) * -1);
		Date start = cal.getTime();

		cal.setTime(date);
		cal.add(Calendar.DATE, FORECAST_LENGTH);
		Date end = cal.getTime();

		List<String> outputLabels = new ArrayList<String>();
		for (String node : nodeAssigner.getNodeNames()) {
			outputLabels.add(getLabel(node, ACTUAL));
			outputLabels.add(getLabel(node, NAIVE));
			outputLabels.add(getLabel(node, ADJUSTED));
			outputLabels.add(getLabel(node, INCOMING));
			outputLabels.add(getLabel(node, ACTUAL_INCOMING));
		}

		dateRange = DateHandler.datesBetween(start, end);
		output = ArrayTable.create(dateRange, outputLabels);
		TableUtils.zeroTable(output);
	}

	private void setupVariables() {
		graphDb = Neo4JFactory.createNeo4JService(configProperties.getProperty("graphdb"));
		engine = new RestCypherQueryEngine(graphDb.getRestAPI());
		System.setProperty("org.neo4j.rest.batch_transaction", "true");
		try {
			nodeAssigner = new NodeAssigner(connectToDB("node"));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		incomingSymptomCounts = new HashMap<String, List<Double>>();
		incomingActualSymptomCounts = new HashMap<String, List<Double>>();
		for (String node : nodeAssigner.getNodeNames()) {
			List<Double> counts = new ArrayList<Double>();
			incomingSymptomCounts.put(node, counts);
			incomingActualSymptomCounts.put(node, new ArrayList<Double>());
		}
		errorSeries = new ArrayList<List<Double>>();
	}

	private void updateForecastNodes(Map<String, Double> newValues) {
		try (Transaction tx = graphDb.beginTx()) {
			for (String node : newValues.keySet()) {
				String statement = String.format(
						"match (a:%s {lockId:%d}) where a.name = \"%s\" set a.symptom = %.2f ", Neo4JUtils.FORECAST,
						forecastLockNumber, node, newValues.get(node));
				engine.query(statement, new HashMap<String, Object>());
			}
			tx.success();
		}

	}

	private void updateForecastRelationships(Date date, String symptom) {
		String dateStr = "\"" + DateHandler.sdf.format(date) + "\"";
		String statement = "match (a:DATA {date:" + dateStr + "})-[r:SPATIAL]->(b:DATA) with a,r,b "
				+ "match (c:FORECAST {lockId:" + forecastLockNumber + "})-[rf]->(d:FORECAST {lockId:"
				+ forecastLockNumber + "}) " + "where c.name=a.name and d.name=b.name set rf.actualsymptom = r."
				+ symptom;
		engine.query(statement, new HashMap<String, Object>());
	}

}