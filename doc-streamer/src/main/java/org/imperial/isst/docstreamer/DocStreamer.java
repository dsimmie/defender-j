package org.imperial.isst.docstreamer;

import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;

import org.imperial.isst.apps.DefenderApplication;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;


/**
 * The {@link DocStreamer} is the entry point for connecting clients to the streaming solution. This class configures
 * the connection to the database collection and begins execution of the streaming thread.
 * <p>
 * The database collection must be a <b>capped collection</b> for this approach to work as it relies on <i>tailable</i>
 * cursors.
 * <p>
 * More information on the general approach can bee seen in the following DZone article.
 * 
 * @see <a href="http://java.dzone.com/articles/event-streaming-mongodb">MongoDB Event Streaming DZone</a>
 */
public class DocStreamer extends DefenderApplication {

	/**
	 * The document reader component.
	 */
	private final DocReader reader;

	/**
	 * Initialises a new instance of the {@link DocStreamer} class.
	 * 
	 * @param configFile
	 *            the configuration file with details of both database connections.
	 * @param index
	 *            the id field to use as the index see
	 *            http://docs.mongodb.org/manual/reference/method/db.collection.ensureIndex/ for more info.
	 * @param clientID
	 *            the class name of the calling class (used to identify the last read id for that class).
	 * @param clients
	 *            the client that will consume documents from the streamer.
	 * @throws UnknownHostException
	 */
	public DocStreamer(String configFile, String index, String clientID) throws UnknownHostException {
		super(configFile);
		
		DBCollection streamCollection = connectToDB("stream");
		streamCollection.ensureIndex(index);

		DBCollection idCollection = connectToDB("config");
		reader = new DocReader(streamCollection, idCollection, index, clientID);
	}

	/**
	 * Start streaming documents from the host database collection.
	 * <p>
	 * Any provided client implementation will begin to receive documents as they are inserted.
	 * 
	 * @return a reference to the document queue.
	 */
	public LinkedBlockingQueue<DBObject> beginStreaming() {
		return reader.beginReading();
	}


	/**
	 * Stop streaming documents from the host database collection.
	 */
	public void stopStreaming() {
		logger.info("Received stop message");
		reader.stopped = true;
	}
}
