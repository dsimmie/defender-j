/**
 * SampleClient.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.docstreamer.demo;

import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;

import org.imperial.isst.docstreamer.DocStreamer;

import com.mongodb.DBObject;

/**
 * Simple demo application which just prints received documents.
 * 
 * @author Donal Simmie
 * 
 */
public class SampleClient {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			DocStreamer streamer = new DocStreamer("/config.properties", "tweet_id", SampleClient.class.getName());
			LinkedBlockingQueue<DBObject> documents = streamer.beginStreaming();

			DBObject first = documents.take();

			System.out.println(first);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
