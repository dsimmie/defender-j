/**
 * DocReader.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.docstreamer;

import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.Bytes;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * The {@link DocReader} class executes on it's own thread and sends data on to all interested stream observers.
 * 
 * @author Donal Simmie
 * 
 */
public class DocReader extends Thread {

	/**
	 * The last read ID field key stored in the database.
	 */
	private static final String LAST_READ_ID = "last_read_id";

	/**
	 * The client id field that uniquely identifies the streaming client.
	 */
	private static final String CLIENT_ID = "client_id";

	/**
	 * The class logger.
	 */
	private final Logger logger;

	/**
	 * The database collection.
	 */
	private final DBCollection docCollection;

	/**
	 * The ID of the calling client.
	 */
	private final String clientID;

	/**
	 * The indexed field.
	 */
	private final String index;

	/**
	 * The ID database collection.
	 */
	private final DBCollection idCollection;

	/**
	 * The work queue of documents to be handled by any client application.
	 */
	private final LinkedBlockingQueue<DBObject> documentQueue;

	public boolean stopped = false;

	/**
	 * Initialises a new instance of the {@link DocReader} class.
	 * 
	 * @param cursor
	 * @param clients
	 */
	public DocReader(DBCollection docCollection, DBCollection idCollection, String index, String clientID) {
		this.docCollection = docCollection;
		this.idCollection = idCollection;
		this.index = index;
		this.clientID = clientID;
		documentQueue = new LinkedBlockingQueue<DBObject>(5000);
		logger = LoggerFactory.getLogger(getClass().getName());
	}

	/**
	 * Start the current thread and return the document queue reference to the caller.
	 * 
	 * @return
	 */
	public LinkedBlockingQueue<DBObject> beginReading() {
		start();
		return documentQueue;
	}

	/**
	 * Load the last read id from the config database.
	 * <p>
	 * There should be an entry per client as each will have it's own last ID read.
	 * 
	 * @param clientID
	 *            the unique ID of the client.
	 * @return
	 * @throws UnknownHostException
	 */
	private Optional<Long> loadLastReadId(String clientID) throws UnknownHostException {

		DBObject query = BasicDBObjectBuilder.start(CLIENT_ID, clientID).get();
		DBCursor cursor = idCollection.find(query);

		Optional<Long> lastReadId;
		if (cursor.hasNext()) {
			DBObject lastRead = cursor.next();
			lastReadId = Optional.of((Long) lastRead.get(LAST_READ_ID));
			logger.info("Loaded last read ID " + lastReadId.get());
		} else {
			lastReadId = Optional.absent();
			logger.info("No last read ID stored for class " + clientID);
		}
		return lastReadId;
	}

	/**
	 * Save the last read ID to the database.
	 * <p>
	 * Persist the new last read ID to the database. This should overwrite any existing value or insert a new one if the
	 * client ID does not exist in the database.
	 * <p>
	 * <i>Note: If this write is causing performance problems consider performing on different thread.<i>
	 * 
	 * @param lastReadId
	 */
	public void persistLastReadId(Optional<Long> lastReadId) {
		BasicDBObject lastReadDoc = new BasicDBObject();
		lastReadDoc.put(DocReader.CLIENT_ID, clientID);
		lastReadDoc.put(DocReader.LAST_READ_ID, lastReadId.get());
		idCollection.update(BasicDBObjectBuilder.start(DocReader.CLIENT_ID, clientID).get(), lastReadDoc, true, false);
		logger.debug("Updated last read ID: " + lastReadId.get());
	}

	/**
	 * The run method performs the handling of data from the <i>tailable</i> cursor. It listens for data received from
	 * the MongoDB database. The approach handle the cursor becoming inactive (dead) and will continue to run until
	 * terminated manually. The data is read from the cursor in natural order, i.e. the order they are inserted.
	 * <p>
	 * This code is an adapted version of the C++ example available on the MongoDB Web site.
	 * 
	 * @see <a href="http://docs.mongodb.org/manual/tutorial/create-tailable-cursor/">MongoDB Tailable Cursor</a>
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		Optional<Long> lastReadId = Optional.absent();
		try {
			lastReadId = loadLastReadId(clientID);
		} catch (UnknownHostException e) {
			logger.error("Unable to read last known value from due to exception: " + e.getMessage());
		}

		while (true) {
			try {
				/*
				 * Look for IDs greater than or equal to the last read IDs. This requires that the index IDs are in
				 * increasing sequential order.
				 */
				boolean spaceInQueue = true;
				DBObject query = lastReadId.isPresent() ? BasicDBObjectBuilder.start(index,
						BasicDBObjectBuilder.start("$gte", lastReadId.get()).get()).get() : null;
				logger.debug("Querying database using: " + query);

				DBObject sortBy = BasicDBObjectBuilder.start("$natural", 1).get();
				DBCursor cursor = docCollection.find(query).sort(sortBy).addOption(Bytes.QUERYOPTION_TAILABLE)
						.addOption(Bytes.QUERYOPTION_AWAITDATA);

				while (true) {
					if (!cursor.hasNext()) {
						// Check if the cursor is dead if it is we need re-issue the query.
						if (cursor.getCursorId() == 0) {
							break;
						}
						continue;
					}
					DBObject document = cursor.next();

					do {
						try {
							spaceInQueue = documentQueue.offer(document, 30, TimeUnit.SECONDS);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} while (!spaceInQueue);

					/*
					 * Update the last read ID so that it is always current. This will be used in subsequent queries if
					 * the cursor becomes inactive.
					 */
					try {
						lastReadId = Optional.of(((Long) document.get(index)));
						persistLastReadId(lastReadId);
					} catch (Exception e) {
						logger.error("Could not persist id", e);
					}

					if (stopped) {
						return;
					}
				}
			} catch (Exception e) {
				logger.error("Streamer stopped with exception: ", e);
				throw e;
			}
		}
	}
}
