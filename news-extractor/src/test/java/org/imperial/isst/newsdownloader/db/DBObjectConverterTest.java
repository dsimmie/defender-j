/**
 * DBObjectConverterTest.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.db;

import org.junit.Test;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class DBObjectConverterTest {


	/**
	 * Test method for
	 * {@link org.imperial.isst.newsdownloader.db.DBObjectConverter#convertEntities(org.imperial.isst.newsdownloader.domain.NewsSummary, com.mongodb.DBObject)}
	 * .
	 */
	
	@Test
	public void testConvertEntities() {
//		Set<Location> locations = new HashSet<Location>();
//		List<String> keywords = new ArrayList<String>();
//		Set<String> symptoms = new HashSet<String>();
//		List<Entry<String, Entity>> entities = new ArrayList<Entry<String, Entity>>();
//		entities.add(new SimpleImmutableEntry<String, Entity>("Imperial", Entity.ORGANIZATION));
//		entities.add(new SimpleImmutableEntry<String, Entity>("College", Entity.ORGANIZATION));
//		entities.add(new SimpleImmutableEntry<String, Entity>("London", Entity.ORGANIZATION));
//		entities.add(new SimpleImmutableEntry<String, Entity>("50%", Entity.PERCENT));
//		entities.add(new SimpleImmutableEntry<String, Entity>("100%", Entity.PERCENT));
//		entities.add(new SimpleImmutableEntry<String, Entity>("Donal", Entity.PERSON));
//		entities.add(new SimpleImmutableEntry<String, Entity>("Simmie", Entity.PERSON));
//
//		DBObject doc = new BasicDBObject();
//		target.convertEntities(new NewsSummary(123, "A story", "http://123.com", new Date(), "ProMed", locations,
//				keywords, symptoms, entities), doc);
//		assertEquals(7, ((List<DBObject>) doc.get("entities")).size());
//
//		entities = new ArrayList<Entry<String, Entity>>();
//		entities.add(new SimpleImmutableEntry<String, Entity>("Imperial", Entity.ORGANIZATION));
//		entities.add(new SimpleImmutableEntry<String, Entity>("College", Entity.ORGANIZATION));
//		entities.add(new SimpleImmutableEntry<String, Entity>("London", Entity.ORGANIZATION));
//		entities.add(new SimpleImmutableEntry<String, Entity>("50%", Entity.PERCENT));
//		entities.add(new SimpleImmutableEntry<String, Entity>("Donal", Entity.PERSON));
//		entities.add(new SimpleImmutableEntry<String, Entity>("100%", Entity.PERCENT));
//		entities.add(new SimpleImmutableEntry<String, Entity>("Simmie", Entity.PERSON));
//
//		doc = new BasicDBObject();
//		target.convertEntities(new NewsSummary(123, "A story", "http://123.com", new Date(), "ProMed", locations,
//				keywords, symptoms, entities), doc);
//		assertEquals(7, ((List<DBObject>) doc.get("entities")).size());
	}
}
