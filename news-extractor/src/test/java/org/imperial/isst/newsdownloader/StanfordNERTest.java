package org.imperial.isst.newsdownloader;

import java.util.HashMap;

import org.imperial.isst.entities.impl.StanfordNER;
import org.imperial.isst.objects.Entity;
import org.junit.Test;

public class StanfordNERTest {

	private String test1 = "Jocelyn Parkes is a pharmacist and director for the Royal "
			+ "Pharmaceutical Society in Wales\nTaking too many medications when ill can be risky\n"
			+ "You wake up feeling awful - stuffy nose, headache, aching limbs - and seek refuge in your"
			+ " local pharmacy. Shelves of coloured boxes and bottles blur in front of your eyes."
			+ " How do you decide which one to buy? Well, the answer is you don't need to -"
			+ " it's safer to ask for help and advice.\nWhen you feel unwell, it's tempting to "
			+ "take more than one medicine to feel better but mixing medicines can be dangerous."
			+ " You may have taken a particular cold and flu remedy before but the active ingredients"
			+ " and dosage can change. It's easier than you think to take two products that"
			+ " contain Paracetamol. Overdosing on Paracetamol can cause liver damage"
			+ " and death.\nOver-the-counter (OTC) remedies can also interact"
			+ " adversely with prescribed medication. For example,"
			+ " both Aspirin and bloodthinners like Warfarin protect"
			+ " against heart attack by helping to prevent blood clots from forming."
			+ " If you take these medications together you may suffer from excessive"
			+ " bleeding.\nIn the UK, around 6% of admissions to hospital are for adverse"
			+ " reactions to medicines. Around 70% of these are avoidable. Pharmacists"
			+ " understand how drugs affect the body and how different drugs"
			+ " interact.\nThey can advise you on what medicines you can"
			+ " take at the same time, preventing you overdosing on the"
			+ " same active ingredient.\nTell your pharmacist or GP what"
			+ " medicines you are taking, and, if you take a lot of medicines,"
			+ " consider keeping a list. Use one pharmacy and get to know your"
			+ " local pharmacist.\nTalk about the medicines on your prescription"
			+ " with your pharmacist. Ask them about side effects or potential"
			+ " problems. Before you take any medicine - prescribed or OTC"
			+ " - ensure you read the patient information leaflet carefully."
			+ " Report any side effects to the Yellow Card Scheme.\nThe Yellow"
			+ " Card Scheme is run by the Medicines and Healthcare Regulatory"
			+ " Agency and the Commission on Human Medicines. It's used to collect"
			+ " information on suspected side effects to medicines and vaccines."
			+ " Complete a Yellow card at www.mhra.yellowcard.gov.uk or call the"
			+ " Yellow Card freephone on 0808 100 3352 (available weekdays"
			+ " 10am and 2pm).\n \nSee more stories you'll love\nYou've"
			+ " turned off story recommendations. Turn them on and we'll"
			+ " update the list below with stories we think you'll love"
			+ " ( how we do this ).\nRecommended in News Why?\n";
	
	private String test2 = "Coventry Telegraph. This article is about Coventry. How many mentions of Coventry?"
			+ "How about Coventry hospitals? Some people in Coventry are worried about this. Many hospitals"
			+ "in Coventry are bad.";
	
	@Test
	public void test() {
		StanfordNER ner = new StanfordNER();
		
		HashMap<Entity, HashMap<String, Integer>> ents = ner.find(test1);
		System.out.println(ents.toString());
		
		ents = ner.find(test2);
		System.out.println(ents.toString());
		
		ents = ner.find("shout out to  going on her Sahara trek today!");
		System.out.println(ents.toString());

	}

}
