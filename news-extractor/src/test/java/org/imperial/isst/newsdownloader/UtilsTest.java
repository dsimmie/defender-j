package org.imperial.isst.newsdownloader;

import java.io.InputStream;

import org.imperial.isst.newsdownloader.article.ArticleParser;
import org.imperial.isst.newsdownloader.article.ParserException;
import org.imperial.isst.newsdownloader.article.impl.BoilerpipeBodyTextParser;
import org.imperial.isst.newsdownloader.util.Utils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

public class UtilsTest {

	public String readFile(String resource) {
		InputStream i = this.getClass().getResourceAsStream(resource);
		String res = convertStreamToString(i);
		return res;
	}
	
	static String convertStreamToString(java.io.InputStream is) {
		@SuppressWarnings("resource")
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		try {
			return s.hasNext() ? s.next() : "";
		} finally {
			s.close();
		}
	}
	
	@Test
	public void test() throws ParserException {
		
		
		String exampleDescription = readFile("/html_description.txt");
		Document doc = Jsoup.parse(exampleDescription);
		String out = Utils.getHTMLText(doc);
		System.out.println(out);
		ArticleParser parser = new BoilerpipeBodyTextParser();
		String parsedText = parser.parseHTMLArticle(exampleDescription);
		System.out.println(parsedText);
	}

}
