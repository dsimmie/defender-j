/**
 * DataExtracter.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.imperial.isst.database.DBNewsFields;
import org.imperial.isst.entities.NamedEntityRecogniser;
import org.imperial.isst.location.Location;
import org.imperial.isst.location.LocationFinder;
import org.imperial.isst.newsdownloader.classifier.NewsClassifier;
import org.imperial.isst.newsdownloader.domain.NewsSummary;
import org.imperial.isst.objects.Entity;
import org.imperial.isst.utils.KeywordUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DBObject;

/**
 * The {@link DataExtractor} component extracts summaries of the data by searching for semantic entities within the
 * text, title and description.
 * <p>
 * This class can be extended by adding more meaning extracting components. TODO: make a common interface and a pipeline
 * type approach with hooks for additional components.
 * 
 * @author Donal Simmie
 * 
 */
public class DataExtractor {

	/**
	 * The location finding component used to scan the whole of the text.
	 */
	private final LocationFinder wholeTextFinder;
	
	/**
	 * The location finding component used to scan each location detected by NER.
	 */
	private final LocationFinder specificFinder;
	
	private final NewsClassifier newsClassifier;

	/**
	 * The keywords.
	 */
	private final Pattern keywordRegex;
	private Map<String, Set<String>> reverseKeywords;

	/**
	 * The named entity finding component.
	 */
	private final NamedEntityRecogniser entityRecogniser;
	
	private final Logger logger;

	/**
	 * Initialises a new instance of the {@link DataExtractor} class.
	 * 
	 * @param locationFinder
	 * @param keywordFinder
	 * @param entityFinder
	 */
	public DataExtractor(LocationFinder wholeTextFinder, LocationFinder specificFinder, Map<String, List<String>> keywords,
			NamedEntityRecogniser entityRecogniser, NewsClassifier nc) {
		this.wholeTextFinder = wholeTextFinder;
		this.specificFinder = specificFinder;
		this.newsClassifier = nc;
		reverseKeywords = KeywordUtils.getReverseMap(keywords);
		keywordRegex = KeywordUtils.buildRegex(KeywordUtils.getAllKeywords(keywords));
		this.entityRecogniser = entityRecogniser;
		logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * Extract the news summary data from the feed items.
	 * 
	 * @param docs
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public Set<NewsSummary> getSummaries(List<DBObject> docs) throws IOException, JSONException, InterruptedException {
		Set<NewsSummary> summaries = new HashSet<NewsSummary>();
		for (DBObject doc : docs) {
			int id = (Integer) doc.get(DBNewsFields.ID);
			String title = doc.get(DBNewsFields.TITLE).toString();
			String link = doc.get(DBNewsFields.LINK).toString();
			String description = doc.get(DBNewsFields.DESCRIPTION).toString();
			Date pubDate = (Date) doc.get(DBNewsFields.PUB_DATE);
			String feed = ((DBObject)doc.get(DBNewsFields.FEED)).get("name").toString();
			List<Location> locations = new ArrayList<Location>();
			String text = null;
			if (doc.containsField(DBNewsFields.ARTICLE)) {
				text = doc.get(DBNewsFields.ARTICLE).toString();
			} else {
				text = description;
			}

			try {
				List<Location> generalLocations = wholeTextFinder.find(text,title);

				Set<String> matchedSymptoms = new HashSet<String>();
				List<String> matchedKeywords =  KeywordUtils.returnMatches(keywordRegex, text);
				for (String match: matchedKeywords) {
					matchedSymptoms.addAll(reverseKeywords.get(match));
				}
				HashMap<Entity, HashMap<String, Integer>> entities = entityRecogniser.find(text);
				
				// Put every location found by NER through PlaceMaker separately in case it has missed them
				
				
				List<Location> specificLocations = new ArrayList<Location>();
				if (entities.containsKey(Entity.LOCATION)) {
					HashMap<String, Integer> nerLocations = entities.get(Entity.LOCATION);
					for (String ent: nerLocations.keySet()) {
						specificLocations.addAll(specificFinder.find(ent));
					}
				}


				locations.addAll(generalLocations);
				locations.addAll(specificLocations);
				
				List<String> names = new ArrayList<String>();
				for (Location loc: locations) {
					if (loc.getLocality() != null) {
						names.add(loc.getLocality());
					}
				}
				
				Map<String, Integer> occs = KeywordUtils.getOccurrences(names, text);

				for (Location loc: locations) {
					if (loc.getLocality() != null) {
						loc.setConfidence(occs.get(loc.getLocality()));
					}
				}
				
				String classification = newsClassifier.classify(text);
				
				NewsSummary summary = new NewsSummary(id, title, text, description, link, pubDate, feed, locations, matchedKeywords, matchedSymptoms, entities, classification);
				summaries.add(summary);
			} catch (Exception e) {
				logger.error("Error summarising document.", e);
				logger.error("Title: " + title +" Text: " + text);
			}
		}
		return summaries;
	}
}
