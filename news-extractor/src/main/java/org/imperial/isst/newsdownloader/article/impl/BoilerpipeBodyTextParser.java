/**
 * BoilerpipeBodyTextParser.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.article.impl;

import org.imperial.isst.newsdownloader.article.ArticleParser;
import org.imperial.isst.newsdownloader.article.ParserException;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class BoilerpipeBodyTextParser implements ArticleParser {

	/**
	 * Parse the text of an HTML article.
	 * 
	 * @param htmlArticle
	 * @return
	 */
	@Override
	public String parseHTMLArticle(String htmlArticle) throws ParserException {
		try {
			return ArticleExtractor.INSTANCE.getText(htmlArticle);
		} catch (BoilerpipeProcessingException e) {
			throw new ParserException(e);
		}
	}
}
