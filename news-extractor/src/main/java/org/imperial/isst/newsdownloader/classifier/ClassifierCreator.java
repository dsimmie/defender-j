package org.imperial.isst.newsdownloader.classifier;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.news.Feed;
import org.imperial.isst.utils.NewsUtils;
import org.imperial.isst.utils.StringUtils;

import com.datumbox.opensource.classifiers.NaiveBayes;
import com.datumbox.opensource.dataobjects.NaiveBayesKnowledgeBase;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class ClassifierCreator extends DefenderApplication{

	private DBCollection extractedNews;
	private final double CHI_SQUARE_VALUE = 6.63;
	
	private Set<Feed> feeds;
	public ClassifierCreator(String configFile) {
		super(configFile);
	}
	
	public void createClassifier() {
		
		feeds = NewsUtils.loadFeeds("/feeds.txt");
		
		DBCursor cursor = extractedNews.find();
		Map<String, String[]> trainingExamples = new HashMap<String, String[]>();
		List<String> healthExamples = new ArrayList<String>();
		List<String> otherExamples = new ArrayList<String>();
		
		while (cursor.hasNext()) {
			DBObject obj = cursor.next();
			String feedname = obj.get("feed").toString();
			String article = StringUtils.stripNonAscii(obj.get("article").toString().replaceAll("\\d",""));
			Feed feed = getFeed(feedname);
			if (feed.getFeedPath().toString().toLowerCase().contains("health") || feedname.toLowerCase().contains("health")) {
				healthExamples.add(article);
			} else {
				otherExamples.add(article);
			}
		}
		String[] healtharr = healthExamples.toArray(new String[healthExamples.size()]);
		List<String> shortenedOther = otherExamples.subList(1, healthExamples.size());
		String[] otharr = shortenedOther.toArray(new String[shortenedOther.size()]);
		trainingExamples.put("health", healtharr);
		trainingExamples.put("other", otharr);
		
		NaiveBayes nb = new NaiveBayes();
        nb.setChisquareCriticalValue(CHI_SQUARE_VALUE); //0.01 pvalue is 6.63
        nb.train(trainingExamples);
        
        //get trained classifier knowledgeBase
        NaiveBayesKnowledgeBase knowledgeBase = nb.getKnowledgeBase();
        nb = null;
        trainingExamples = null;
        
        
        //Use classifier
        nb = new NaiveBayes(knowledgeBase);
        
       // System.out.println(knowledgeBase.logLikelihoods.toString());
       // System.out.println(knowledgeBase.logPriors.toString());
        
        String exampleEn = "elections are awful said mr cameron";
        String outputEn = nb.predict(exampleEn);
        System.out.format("The sentence \"%s\" was classified as \"%s\".%n", exampleEn, outputEn);
        
        exampleEn = "the health service in wales was rocked by scandal yesterday.";
        outputEn = nb.predict(exampleEn);
        System.out.format("The sentence \"%s\" was classified as \"%s\".%n", exampleEn, outputEn);
		
        FileOutputStream fout;
		try {
			fout = new FileOutputStream("knowledgebase.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(knowledgeBase);
			oos.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        
        classifyExtractedNews(nb);
	}
	
	private Feed getFeed(String name) {
		for (Feed feed: feeds) {
			if (feed.getName().equals(name)) {
				return feed;
			}
		}
		return null;
	}
	
	public void classifyExtractedNews(NaiveBayes classifier) {
		DBCursor cursor = extractedNews.find();

		
		while (cursor.hasNext()) {
			DBObject obj = cursor.next();
			String article = obj.get("article").toString();
			String classification = classifier.predict(article);
			DBObject update = new BasicDBObject("classification", classification);
			
			DBObject updateDoc = new BasicDBObject();
			updateDoc.put("$set", update);

			
			if (!obj.get("classification").equals(classification)) {
				System.out.println(obj.get("description"));
				System.out.println("New classification: " + classification);
			}
			
			BasicDBObject searchQuery = new BasicDBObject("id", obj.get("id"));
			extractedNews.update(searchQuery, updateDoc);
			
			
		}
	}

	public void doSetup() {
		try {
			extractedNews = connectToDB("extracted");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		ClassifierCreator creator = new ClassifierCreator("/config.properties");
		creator.doSetup();
		creator.createClassifier();
	}
	
}
