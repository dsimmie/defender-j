/**
 * ArticleParser.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.article;

/**
 * The different article parser types, persisted for provenance purposes.
 * 
 * @author Donal Simmie
 * 
 */
public enum ParserType {
	BOILERPIPE, JSOUP;
}
