package org.imperial.isst.newsdownloader.main;

import java.net.MalformedURLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.imperial.isst.database.DatabaseException;
import org.imperial.isst.newsdownloader.NewsDownloader;

/**
 * Application.java
 *
 * Copyright of Donal Simmie 2014.
 */

/**
 * Main launcher for the news downloading application.
 * 
 * @author Donal Simmie
 * 
 */
public class Application {
	/**
	 * Main class to start the news extraction process.
	 * <p>
	 * This job is run on a fixed rate schedule and periodically downloads new news articles. TODO: add checks for
	 * duplicate articles.
	 * 
	 * @param args
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) {
		NewsDownloader app;
		ScheduledExecutorService scheduler = null;
		try {
			app = new NewsDownloader("/config.properties");
			
			if ((args.length > 0) && (args[0].equals("rerun")) ) {
				app.rerunExtraction();
			} else {
				scheduler = Executors.newScheduledThreadPool(1);
				scheduler.scheduleAtFixedRate(app, 0, 1, TimeUnit.DAYS);
			}	

		} catch (DatabaseException e) {
		}
	}
}
