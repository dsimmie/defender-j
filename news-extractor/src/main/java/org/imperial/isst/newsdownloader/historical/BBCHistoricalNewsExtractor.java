/**
 * BBCHistoricalNewsExtractor.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.historical;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.database.DBConnection;
import org.imperial.isst.database.DBNewsFields;
import org.imperial.isst.database.DBWrapper;
import org.imperial.isst.location.Location;
import org.imperial.isst.location.LocationFinder;
import org.imperial.isst.news.Config;
import org.imperial.isst.newsdownloader.article.ArticleDownloader;
import org.imperial.isst.newsdownloader.article.ArticleParser;
import org.imperial.isst.newsdownloader.article.ParserException;
import org.imperial.isst.newsdownloader.util.Utils;
import org.imperial.isst.utils.KeywordUtils;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * Download and extract historical news data from the BBC Search service. TODO: this should be its own project not
 * building two jars. Fix this!
 * 
 * @author Donal Simmie
 * 
 */
public class BBCHistoricalNewsExtractor extends DefenderApplication implements HistoricalNewsExtractor {

	/**
	 * The location finding component.
	 */
	private final LocationFinder locationFinder;

	/**
	 * The keyword regular expression.
	 */
	private final Pattern keywordRegex;
	
	private Map<String, List<String>> keywords;
	private Map<String, Set<String>> reverseKeywords;
	
	/**
	 * The article downloader.
	 */
	private final ArticleDownloader downloader;

	/**
	 * The article parser.
	 */
	private final ArticleParser parser;

	/**
	 * The placeholder URL string for the BBC search service.
	 * <p>
	 * For more information see here :
	 */
	private final String BBC_TEXT_SEARCH_URL = "http://www.bbc.co.uk/search/news/?page=$PAGE&q=$SEARCH_TERM&start_day=$START_DAY&start_month=$START_MONTH&"
			+ "start_year=$START_YEAR&end_day=$END_DAY&end_month=$END_MONTH&end_year=$END_YEAR&text=on";

	/**
	 * The BBC date format.
	 */
	private static final SimpleDateFormat BBC_PUB_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");

	/**
	 * The class logger.
	 */
	private final Logger logger;

	/**
	 * The delay to sleep for between failed requests.
	 */
	private final int SLEEP_DELAY = 30000;

	/**
	 * The number of times to retry a request.
	 */
	private final int RETRY_ATTEMPTS = 100;

	/**
	 * The source collection.
	 */
	private final DBCollection sourceColl;

	/**
	 * The parsed collection.
	 */
	private final DBCollection parsedColl;

	/**
	 * The extracted collection.
	 */
	private final DBCollection extractedColl;
	
	/**
	 * The symptom collection.
	 */
	private final DBCollection symptomColl;

	/**
	 * Initialises a new instance of the {@link BBCHistoricalNewsExtractor} class.
	 * 
	 * @param downloader
	 * @throws UnknownHostException
	 */
	public BBCHistoricalNewsExtractor(ArticleDownloader downloader, ArticleParser parser,
			LocationFinder locationFinder) throws UnknownHostException {
		super("/config.properties");
		this.downloader = downloader;
		this.parser = parser;
		this.locationFinder = locationFinder;

		logger = LoggerFactory.getLogger(getClass());

		String dbUser = configProperties.getProperty(Config.DB_USER);
		String dbPass = configProperties.getProperty(Config.DB_PASS);
		sourceColl = DBWrapper.connectToCollection(new DBConnection("localhost", "historicalnews", "source", dbUser,
				dbPass));
		parsedColl = DBWrapper.connectToCollection(new DBConnection("localhost", "historicalnews", "parsed", dbUser,
				dbPass));
		extractedColl = DBWrapper.connectToCollection(new DBConnection("localhost", "historicalnews", "extracted",
				dbUser, dbPass));
		extractedColl.ensureIndex(DBNewsFields.PUB_DATE);
		
		symptomColl = DBWrapper.connectToCollection(new DBConnection("localhost", "symptom", "curated",
				dbUser, dbPass));
		this.keywords = KeywordUtils.loadKeywords(symptomColl);
		reverseKeywords = KeywordUtils.getReverseMap(keywords);
		keywordRegex = KeywordUtils.buildRegex(KeywordUtils.getAllKeywords(keywords));
	}

	/**
	 * Request the historic news data from the BBC Search service.
	 * 
	 * @param keyword
	 * @param startDay
	 * @param startMonth
	 * @param startYear
	 * @param endDay
	 * @param endMonth
	 * @param endYear
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	private Document attemptGetHistoricData(String keyword, int startDay, int startMonth, int startYear, int endDay,
			int endMonth, int endYear, int page) throws IOException, UnsupportedEncodingException {
		checkArgument(keyword != null && keyword != "", "No keyword provided for search");
		Document searchResults = null;
		try {
			String request = BBC_TEXT_SEARCH_URL.replace("$PAGE", String.valueOf(page))
					.replace("$SEARCH_TERM", URLEncoder.encode(keyword, "UTF-8"))
					.replace("$START_DAY", padDigit(startDay)).replace("$START_MONTH", padDigit(startMonth))
					.replace("$START_YEAR", String.valueOf(startYear)).replace("$END_DAY", padDigit(endDay))
					.replace("$END_MONTH", padDigit(endMonth)).replace("$END_YEAR", String.valueOf(endYear));

			logger.debug("Request URL: " + request);
			return Jsoup.connect(request).userAgent(Utils.SAMPLE_AGENT).get();
		} catch (SocketTimeoutException se) {
			logger.debug("Request for historic data timed out");
		}
		return searchResults;
	}

	/**
	 * Check if the response contains no results i.e, the no_results class.
	 * 
	 * @param keyword
	 * @param searchResults
	 */
	private boolean checkHasResults(String keyword, Document searchResults) {
		boolean hasResults = true;
		Elements elems = searchResults.getElementsByClass("cmWidget");
		Iterator<Element> elemIter = elems.iterator();
		while (elemIter.hasNext()) {
			Element elem = elemIter.next();
			if (elem.className().endsWith("no_result")) {
				logger.warn("No search results found for term: " + keyword + " in that date range");
				hasResults = false;
			}
		}
		return hasResults;
	}

	/**
	 * Create the source documents collection.
	 * 
	 * @param articles
	 * @return
	 */
	private List<DBObject> createSourceData(Set<HistoricalArticle> articles) {
		List<DBObject> sourceDocs = new LinkedList<DBObject>();
		for (HistoricalArticle article : articles) {
			DBObject sourceDoc = new BasicDBObject();
			sourceDoc.put(DBNewsFields.TITLE, article.getTitle());
			sourceDoc.put(DBNewsFields.LINK, article.getUrl());
			sourceDoc.put(DBNewsFields.ARTICLE, article.getArticle());
			sourceDoc.put(DBNewsFields.ENTRY_TIME, new Date());
			sourceDoc.put(DBNewsFields.PUB_DATE, article.getPubDate());
			sourceDocs.add(sourceDoc);
		}
		return sourceDocs;
	}

	/**
	 * Extract news data from the BBC Search service using a search term and date range.
	 * 
	 * @param keyword
	 * @param startDay
	 * @param startMonth
	 * @param startYear
	 * @param endDay
	 * @param endMonth
	 * @param endYear
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws JSONException
	 * @throws ParserException
	 * @see org.imperial.isst.newsdownloader.historical.HistoricalNewsExtractor#extract(java.lang.String, int, int, int,
	 *      int, int, int)
	 */
	@Override
	public void extract(String keyword, int startDay, int startMonth, int startYear, int endDay, int endMonth,
			int endYear) throws IOException, JSONException, InterruptedException, ParserException {
		checkArgument(keyword != null && keyword != "");
		logger.info("Getting historic data for keyword: " + keyword);

		int currentPage = 1;
		Document searchResults = getHistoricData(keyword, startDay, startMonth, startYear, endDay, endMonth, endYear,
				currentPage);

		/*
		 * Handle the case where there are no results to return. If there are results then traverse pages, if there are
		 * multiple, and store the resulting article set in: source, parsed and extracted form.
		 */
		Set<HistoricalArticle> articles = new HashSet<HistoricalArticle>();
		boolean hasResults = checkHasResults(keyword, searchResults);
		if (hasResults) {
			parseArticles(searchResults, articles);

			while (hasNextPage(searchResults)) {
				currentPage++;
				searchResults = getHistoricData(keyword, startDay, startMonth, startYear, endDay, endMonth, endYear,
						currentPage);
				parseArticles(searchResults, articles);
				logger.debug("Got data from paged response: " + currentPage);

				// To save on memory usage save the article set every fifth page returned.
				if (currentPage % 5 == 0) {
					persistNewsArticles(articles);
					articles = new HashSet<HistoricalArticle>();
				}
			}

			/*
			 * Store source data, parsed data and the semantic data that which has been extracted from the parsed data.
			 */
			if (!articles.isEmpty())
				persistNewsArticles(articles);
		}
	}

	/**
	 * Extract semantic data from the news articles.
	 * 
	 * @param docs
	 * @throws IOException
	 */
	private List<DBObject> extractData(List<DBObject> docs) throws IOException {
		List<DBObject> extractedDocs = new LinkedList<DBObject>();
		for (DBObject doc : docs) {
			DBObject extractedDoc = new BasicDBObject();
			extractedDoc.put(DBNewsFields.TITLE, doc.get(DBNewsFields.TITLE).toString());
			extractedDoc.put(DBNewsFields.LINK, doc.get(DBNewsFields.LINK).toString());
			extractedDoc.put(DBNewsFields.ENTRY_TIME, new Date());
			extractedDoc.put(DBNewsFields.PUB_DATE, doc.get(DBNewsFields.PUB_DATE));
			extractedDoc.put(DBNewsFields.FEED, "BBC Search");
			Object article = doc.get(DBNewsFields.ARTICLE);

			if (article != null) {
				String articleText = article.toString();
				Collection<? extends Location> locations = locationFinder.find(doc.get(DBNewsFields.TITLE).toString(),
						articleText);
				List<String> matchedKeywords =  KeywordUtils.returnMatches(keywordRegex, articleText);
				Set<String> symptomMatches = new HashSet<String>();
				for (String match: matchedKeywords) {
					symptomMatches.addAll(reverseKeywords.get(match));
				}
				List<DBObject> locationDocs = new LinkedList<DBObject>();
				for (Location location : locations) {
					DBObject locationDoc = new BasicDBObject();
					locationDoc.put(DBNewsFields.LOCALITY, location.getLocality());
					locationDoc.put(DBNewsFields.COUNTRY, location.getCountry());
					BasicDBObject coords = new BasicDBObject();
					coords.put(DBNewsFields.LAT, location.getCoords().getLatitude());
					coords.put(DBNewsFields.LONG, location.getCoords().getLongitude());
					locationDoc.put(DBNewsFields.GEO, coords);
					locationDoc.put(DBNewsFields.SOURCE, location.getSource().toString());
					locationDocs.add(locationDoc);
				}

				extractedDoc.put(DBNewsFields.LOCATIONS, locationDocs);
				extractedDoc.put(DBNewsFields.KEYWORDS, matchedKeywords);
				extractedDoc.put(DBNewsFields.SYMPTOMS, symptomMatches);
			}
			extractedDocs.add(extractedDoc);
		}
		return extractedDocs;
	}

	/**
	 * Get the historic data from the search service.
	 * <p>
	 * This method includes a configurable retry approach that allows call to be re-performed if they timeout. In
	 * preliminary testing socket timeouts were not uncommon, hence the addition of a retry method.
	 * 
	 * @param keyword
	 * @param startDay
	 * @param startMonth
	 * @param startYear
	 * @param endDay
	 * @param endMonth
	 * @param endYear
	 * @param currentPage
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * @throws InterruptedException
	 */
	private Document getHistoricData(String keyword, int startDay, int startMonth, int startYear, int endDay,
			int endMonth, int endYear, int currentPage) throws IOException, UnsupportedEncodingException,
			InterruptedException {
		checkArgument(keyword != null && keyword != "", "No keyword provided for search");

		Document searchResults = attemptGetHistoricData(keyword, startDay, startMonth, startYear, endDay, endMonth,
				endYear, currentPage);
		if (searchResults == null) {
			logger.warn("Request timed out. Sleeping then retrying " + RETRY_ATTEMPTS + " times");
			Thread.sleep(SLEEP_DELAY);
			for (int i = 0; i < RETRY_ATTEMPTS; i++) {
				searchResults = attemptGetHistoricData(keyword, startDay, startMonth, startYear, endDay, endMonth,
						endYear, currentPage);
				if (searchResults != null) {
					break;
				}
				Thread.sleep(SLEEP_DELAY);
			}
		}
		return searchResults;
	}

	/**
	 * Check if the results have another page of results.
	 * 
	 * @param searchResults
	 * @return
	 */
	private boolean hasNextPage(Document searchResults) {
		checkNotNull(searchResults);
		boolean hasNextPage = false;
		// Results may contain several pages of response data. A CSS class for pagination exists in a response with more
		// than one page.
		Elements pagination = searchResults.getElementsByClass("pagination");
		if (pagination.size() > 0) {
			logger.debug("Found pagination");
			Iterator<Element> pageElems = pagination.get(0).children().iterator();
			while (pageElems.hasNext()) {
				Element elem = pageElems.next();
				String id = elem.id();
				String tagName = elem.tagName();
				if ("next".equals(id) && "a".equals(tagName)) {
					hasNextPage = true;
				}
			}

		}
		return hasNextPage;
	}

	/**
	 * Pad with leading zeros, if number less than ten.
	 * <p>
	 * The BBC Search service requires this for correct filtering.
	 * 
	 * @param endDay
	 * @return
	 */
	private String padDigit(int value) {
		String valueText = String.valueOf(value);
		return value < 10 ? "0" + valueText : valueText;
	}

	/**
	 * Parse the news articles from the HTML data returned by the web service.
	 * 
	 * @param searchResults
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	private void parseArticles(Document searchResults, Set<HistoricalArticle> articles) throws IOException,
			MalformedURLException {
		checkNotNull(searchResults);
		checkNotNull(articles);
		Elements elems = searchResults.getElementsByClass("cmWidget");

		Iterator<Element> elemsIter = elems.iterator();
		while (elemsIter.hasNext()) {
			Element elem = elemsIter.next();
			if (elem.className().equals("cmWidget news")) {
				// linktrack-item thumbItem lead
				Elements results = elem.getElementsByClass("linktrack-item");
				Iterator<Element> resultIter = results.iterator();
				while (resultIter.hasNext()) {
					Element listItem = resultIter.next();
					Elements resultAnchors = listItem.select("a.title");
					Elements newsDateTimes = listItem.select("span.newsDateTime");
					if (resultAnchors.size() > 1) {
						logger.warn("More than 1 result anchor found, taking first only");
					}
					if (newsDateTimes.size() > 1) {
						logger.warn("More than 1 news time found, taking first only");
					}
					Element resultAnchor = resultAnchors.first();
					Element newsDateTime = newsDateTimes.first();

					String url = resultAnchor.attr("href");
					String title = resultAnchor.text();
					String articleText = null;
					try {
						articleText = downloader.download(new URL(url));
					} catch (MalformedURLException m) {
						logger.warn("Error in HTML structure. Skipping download of article");
						continue;
					}

					try {
						Date pubDate = BBC_PUB_DATE_FORMAT.parse(newsDateTime.text());
						HistoricalArticle article = new HistoricalArticle(title, url, articleText, pubDate);
						articles.add(article);
					} catch (ParseException e) {
						logger.warn("Failed to parse publication date for article");
					}
				}
			}
		}
	}

	/**
	 * Parse the article text.
	 * 
	 * @param docs
	 * @throws ParserException
	 */
	private void parseData(List<DBObject> docs) throws ParserException {
		checkNotNull(docs);
		for (DBObject doc : docs) {
			Object article = doc.get(DBNewsFields.ARTICLE);
			/*
			 * If the article download failed then it won't be in the document object.
			 */
			if (article != null) {
				String parsedArticle = parser.parseHTMLArticle(doc.get(DBNewsFields.ARTICLE).toString());
				doc.put(DBNewsFields.ARTICLE, parsedArticle);
			}
		}
	}

	/**
	 * Parse the news articles, extract meaning from them and store them in the database.
	 * 
	 * @param articles
	 * @throws ParserException
	 * @throws IOException
	 */
	private void persistNewsArticles(Set<HistoricalArticle> articles) throws ParserException, IOException {
		checkArgument(!articles.isEmpty(), "Cannot persist empty article set");

		List<DBObject> docs = createSourceData(articles);

		DBWrapper.insert(sourceColl, docs);

		parseData(docs);

		DBWrapper.insert(parsedColl, docs);

		List<DBObject> extractedDocs = extractData(docs);

		DBWrapper.insert(extractedColl, extractedDocs);
	}
}
