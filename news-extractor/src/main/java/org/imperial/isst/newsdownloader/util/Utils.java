/**
 * Utils.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;

/**
 * 
 * @author Donal Simmie
 * 
 */
public final class Utils {

	/**
	 * The class logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(Utils.class);


	/**
	 * Sample User Agent to include with HTTP GET request so as to minimise number of 403 errors when this is missing.
	 * <p>
	 */
	public static final String SAMPLE_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36";

	/**
	 * The <i>git</i> command to display the hash of the current HEAD commit. This is as close to a version number as we
	 * can get with <i>git</i>.
	 * 
	 * @see <a href="http://stackoverflow.com/questions/15798862/what-does-git-rev-parse-do">More info on rev-parse
	 *      command</a>
	 */
	private static final String GIT_REV_PARSE_HEAD = "/usr/bin/git rev-parse HEAD";

	/**
	 * System line separator.
	 */
	public static final String LINE_SEP = System.getProperty("line.separator");

	/**
	 * Get the hash of the current GIT HEAD commit.
	 * 
	 * @return the hash text.
	 * @param repoPath
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static String getGitHeadCommitHash(String repoPath) throws IOException, InterruptedException {
		return runOneLineCLProgram(GIT_REV_PARSE_HEAD, repoPath);
	}

	/**
	 * Gets the main text from HTML with formatting removed.
	 * <p>
	 * At the moment this uses a naive approach that gets the text attribute of each HTML element and concatenates them
	 * together using whitespace. TODO: check for video and other unsupported formats.
	 * 
	 * @param article
	 * @return
	 */
	public static String getHTMLText(Document article) {

		Elements elems = article.getAllElements();
		String text = "";
		for (Element elem : elems) {
			text += elem.text() + " ";
		}
		return text;
	}

	/**
	 * 
	 * @param inputText
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public static String getLocationsFromGeocode(String inputText) throws IOException, JSONException {
		String request = "http://www.datasciencetoolkit.org/maps/api/geocode/json?sensor=false&address="
				+ URLEncoder.encode(inputText, "UTF-8");

		HttpClient client = new DefaultHttpClient();
		HttpGet post = new HttpGet(request);

		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		logger.debug("Geocode service HTTP status code: " + response.getStatusLine().getStatusCode());
		if (response.getStatusLine().getStatusCode() != 200)
			return "Error determining location";

		String line = "";
		String text = "";
		while ((line = rd.readLine()) != null) {
			text += line + "\n";
		}

		if (JsonPath.read(text, ".status").toString().equalsIgnoreCase("zero_results"))
			return "No locations";

		return JsonPath.read(text, ".long_name").toString();
	}


	/**
	 * @throws JSONException
	 * 
	 * @param inputText
	 * @return
	 * @throws IOException
	 * @throws
	 */
	public static String getLocationsFromPlacemaker(String title, String inputText) throws IOException, JSONException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://wherein.yahooapis.com/v1/document");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("appId",
				"A1b3nzDV34FQHUkW7BbXT0MbJG8jN.8r4HDEVeAO3X8Ux.5pNEwMRwfVWYrqP6rJBcux"));
		nameValuePairs.add(new BasicNameValuePair("documentTitle", title));
		nameValuePairs.add(new BasicNameValuePair("documentType", "text/plain"));
		nameValuePairs.add(new BasicNameValuePair("documentContent", inputText));
		HttpEntity urlEcondedEntity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");

		post.setEntity(urlEcondedEntity);

		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		logger.debug("Placemaker service HTTP status code: " + response.getStatusLine().getStatusCode());
		String line = "";
		String text = "";
		while ((line = rd.readLine()) != null) {
			text += line + "\n";
		}

		System.out.println(text.toString());

		String types = JsonPath.read(XML.toJSONObject(text).toString(), ".type").toString();
		String names = JsonPath.read(XML.toJSONObject(text).toString(), ".name").toString();
		return types + names;
	}

	/**
	 * Gets the location details from unstructured text using the geodict program.
	 * 
	 * TODO: determine can we run more than one of these at once?
	 * 
	 * @param text
	 * @param outputFormat
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws JSONException
	 */
	public static synchronized final String getLocationsFromTwoFishes(String inputText) throws IOException,
			InterruptedException, JSONException {
		HttpClient client = new DefaultHttpClient();
		String request = "http://localhost:8081/?debug=0&maxInterpretations=3&query="
				+ URLEncoder.encode(inputText, "UTF-8");
		HttpGet post = new HttpGet(request);

		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		logger.debug("TwoFishes service HTTP status code: " + response.getStatusLine().getStatusCode());
		String line = "";
		String text = "";
		while ((line = rd.readLine()) != null) {
			text += line + "\n";
		}

		JSONObject jsonObj = new JSONObject(text);
		String result = null;
		JSONArray interpretations = jsonObj.getJSONArray("interpretations");
		if (interpretations.length() > 0) {
			String name = jsonObj.getJSONArray("interpretations").getJSONObject(0).getJSONObject("feature")
					.getString("name");
			String country = jsonObj.getJSONArray("interpretations").getJSONObject(0).getJSONObject("feature")
					.getString("cc");
			result = name + "," + country;
		} else {
			result = "No locations";
		}
		return result;
	}


	/**
	 * Run a basic command line program and return the concatenation of all the text received from the standard output
	 * stream.
	 * <p>
	 * This version does not handle the error stream.
	 * 
	 * @param command
	 *            the command to run.
	 * @param dirPath
	 *            the directory in which to the command.
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static String runCLProgram(String[] cmdArray) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec(cmdArray);

		BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		BufferedReader error = new BufferedReader(new InputStreamReader(pr.getErrorStream()));

		String line = "";
		String inText = "";
		while ((line = input.readLine()) != null) {
			inText += line + "\n";
		}

		while ((line = error.readLine()) != null) {
			logger.warn(line);
		}

		int exitVal = pr.waitFor();
		if (exitVal != 0)
			throw new IOException("The external process returned a non-zero exit code.");

		return inText;
	}

	/**
	 * Run a basic command line program and return the concatenation of all the text received from the standard output
	 * stream.
	 * <p>
	 * This version does not handle the error stream.
	 * 
	 * @param command
	 *            the command to run.
	 * @param dirPath
	 *            the directory in which to the command.
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static String runOneLineCLProgram(String command, String dirPath) throws IOException, InterruptedException {
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec(command, null, new File(dirPath));

		BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));

		String line = input.readLine();

		int exitVal = pr.waitFor();
		if (exitVal != 0)
			throw new IOException("The external process returned a non-zero exit code.");

		return line;
	}
}
