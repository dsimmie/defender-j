/**
 * HistoricalArticle.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.historical;

import java.util.Date;

/**
 * An historical news article.
 * 
 * @author Donal Simmie
 * 
 */
public class HistoricalArticle {

	/**
	 * The article title.
	 */
	private final String title;

	/**
	 * The article URL.
	 */
	private final String url;

	/**
	 * The source article text (as is).
	 */
	private final String article;

	/**
	 * The date the article was published.
	 */
	private final Date pubDate;

	/**
	 * Initialises a new instance of the {@link HistoricalArticle} class.
	 * 
	 * @param title
	 * @param url
	 * @param article
	 */
	public HistoricalArticle(String title, String url, String article, Date pubDate) {
		super();
		this.title = title;
		this.url = url;
		this.article = article;
		this.pubDate = pubDate;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricalArticle other = (HistoricalArticle) obj;
		if (article == null) {
			if (other.article != null)
				return false;
		} else if (!article.equals(other.article))
			return false;
		if (pubDate == null) {
			if (other.pubDate != null)
				return false;
		} else if (!pubDate.equals(other.pubDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	/**
	 * @return the article
	 */
	public String getArticle() {
		return article;
	}

	/**
	 * @return the pubDate
	 */
	public Date getPubDate() {
		return pubDate;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((article == null) ? 0 : article.hashCode());
		result = prime * result + ((pubDate == null) ? 0 : pubDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	/**
	 * 
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HistoricalArticle [title=");
		builder.append(title);
		builder.append(", url=");
		builder.append(url);
		builder.append(", article=");
		builder.append(article);
		builder.append(", pubDate=");
		builder.append(pubDate);
		builder.append("]");
		return builder.toString();
	}

}
