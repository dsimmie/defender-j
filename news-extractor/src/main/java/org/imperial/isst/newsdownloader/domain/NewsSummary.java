package org.imperial.isst.newsdownloader.domain;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.imperial.isst.location.Location;
import org.imperial.isst.objects.Entity;

/**
 * NewsSummary.java
 *
 * Copyright of Donal Simmie 2014.
 */

/**
 * 
 * @author Donal Simmie
 * 
 */
public class NewsSummary {

	/**
	 * The unique id of the news article
	 */
	private final int id;

	/**
	 * The title of the article. 
	 */
	private final String title;
	
	/**
	 * The text of the article. 
	 */
	private final String text;
	
	/**
	 * The description of the article. 
	 */
	private final String description;

	/**
	 * The link the article refers to.
	 */
	private final String link;

	/**
	 * The date the article was published.
	 */
	private final Date pubDate;
	
	/**
	 * The feed the article was taken from.
	 */
	private final String feed;

	/**
	 * The health classification of the article.
	 */
	private final String classification;
	
	/**
	 * The identified locations from the text.
	 */
	private final List<Location> locations;

	/**
	 * The named entities.
	 */
	private final HashMap<Entity, HashMap<String, Integer>> entities;

	/**
	 * The keywords found in the news article.
	 */
	private final List<String> keywords;
	
	/**
	 * The symptoms found in the news article.
	 */
	private final Set<String> symptoms;

	/**
	 * Initialises a new instance of the {@link NewsSummary} class.
	 * 
	 */
	public NewsSummary(int id, String title, String text, String description, String link, Date pubDate, String feed, List<Location> locations,
			List<String> keywords, Set<String> symptoms, HashMap<Entity, HashMap<String, Integer>> entities, String classification) {
		checkArgument(title != null);
		checkArgument(text != null);
		checkArgument(link != null);
		checkArgument(locations != null);
		checkArgument(keywords != null);
		checkArgument(symptoms != null);
		checkArgument(entities != null);

		this.id = id;
		this.title = title;
		this.text = text;
		this.description = description;
		this.link = link;
		this.pubDate = pubDate;
		this.locations = locations;
		this.keywords = keywords;
		this.symptoms = symptoms;
		this.entities = entities;
		this.feed = feed;
		this.classification = classification;
	}

	public String getClassification() {
		return classification;
	}

	public String getText() {
		return text;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsSummary other = (NewsSummary) obj;
		if (classification == null) {
			if (other.classification != null)
				return false;
		} else if (!classification.equals(other.classification))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (entities == null) {
			if (other.entities != null)
				return false;
		} else if (!entities.equals(other.entities))
			return false;
		if (feed == null) {
			if (other.feed != null)
				return false;
		} else if (!feed.equals(other.feed))
			return false;
		if (id != other.id)
			return false;
		if (keywords == null) {
			if (other.keywords != null)
				return false;
		} else if (!keywords.equals(other.keywords))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		if (locations == null) {
			if (other.locations != null)
				return false;
		} else if (!locations.equals(other.locations))
			return false;
		if (pubDate == null) {
			if (other.pubDate != null)
				return false;
		} else if (!pubDate.equals(other.pubDate))
			return false;
		if (symptoms == null) {
			if (other.symptoms != null)
				return false;
		} else if (!symptoms.equals(other.symptoms))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public String getDescription() {
		return description;
	}

	public String getFeed() {
		return feed;
	}

	/**
	 * @return the entities
	 */
	public HashMap<Entity, HashMap<String, Integer>> getEntities() {
		return entities;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the keywords
	 */
	public List<String> getKeywords() {
		return keywords;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @return the locations
	 */
	public Collection<? extends Location> getLocations() {
		return locations;
	}

	/**
	 * @return the pubDate
	 */
	public Date getPubDate() {
		return pubDate;
	}

	public Set<String> getSymptoms() {
		return symptoms;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((classification == null) ? 0 : classification.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((entities == null) ? 0 : entities.hashCode());
		result = prime * result + ((feed == null) ? 0 : feed.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((keywords == null) ? 0 : keywords.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		result = prime * result
				+ ((locations == null) ? 0 : locations.hashCode());
		result = prime * result + ((pubDate == null) ? 0 : pubDate.hashCode());
		result = prime * result
				+ ((symptoms == null) ? 0 : symptoms.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "NewsSummary [id=" + id + ", title=" + title + ", text=" + text
				+ ", description=" + description + ", link=" + link
				+ ", pubDate=" + pubDate + ", feed=" + feed
				+ ", classification=" + classification + ", locations="
				+ locations + ", entities=" + entities + ", keywords="
				+ keywords + ", symptoms=" + symptoms + "]";
	}

}
