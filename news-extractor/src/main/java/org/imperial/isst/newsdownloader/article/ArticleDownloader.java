/**
 * ArticleDownloader.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.article;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.imperial.isst.newsdownloader.util.Utils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Download the original articles from the RSS feed links that refer to them.
 * <p>
 * For this application we are only interested in text so video and audio sources should be ignored.
 * 
 * @author Donal Simmie
 * 
 */
public class ArticleDownloader {

	/**
	 * The class logger.
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Download the original article that the link refers to.
	 * 
	 * @param articleURL
	 * @return the article text or null if it could not be downloaded.
	 * @throws IOException
	 */
	public String download(URL articleURL) throws IOException {
		String articlePath = articleURL.toString();
		String text = null;

		/*
		 * Attempt to download the article twice if there is a timeout. If not successful on the second attempt then log
		 * the issue and keep the article text as null.
		 */
		try {
			Document article = Jsoup.connect(articlePath).userAgent(Utils.SAMPLE_AGENT).get();
			text = article.toString();
		} catch (Exception ignore) {
			try {
				Thread.sleep(30000);
				Document article = Jsoup.connect(articlePath).get();
				text = article.toString();
			} catch (SocketTimeoutException e) {
				logger.warn("Failed to download article: " + articleURL + " on second attempt due to socket timeout");
			} catch (Exception e) {
				logger.warn("Failed to download article: " + articleURL + " on second attempt", e);
			}
		}

		return text;
	}
}
