/**
 * HistoricalNewsExtractor.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.historical;

import java.io.IOException;

import org.imperial.isst.newsdownloader.article.ParserException;
import org.json.JSONException;

/**
 * Download and extract meaningful information from historical news sources.
 * 
 * @author Donal Simmie
 * 
 */
public interface HistoricalNewsExtractor {

	/**
	 * Extract news data from a given source using a keyword and date range.
	 * 
	 * @param keyword
	 * @param startDay
	 * @param startMonth
	 * @param startYear
	 * @param endDay
	 * @param endMonth
	 * @param endYear
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws JSONException
	 * @throws ParserException
	 */
	void extract(String keyword, int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear)
			throws IOException, JSONException, InterruptedException, ParserException;
}
