/**
 * FeedReader.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader;

import java.io.IOException;
import java.util.Iterator;

import org.imperial.isst.news.Feed;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

/**
 * The {@link FeedReader} component read feed entries from a given feed location.
 * 
 * @author Donal Simmie
 * 
 */
public class FeedReader {

	/**
	 * Read feed entries from the given feed parameters.
	 * <p>
	 * Note: warning suppressed as cannot infer type arguments explicitly from dependency code.
	 * 
	 * @param feedDetails
	 * @return an iterator over the feed items
	 * @throws IOException
	 * @throws FeedException
	 */
	@SuppressWarnings("rawtypes")
	public Iterator read(Feed feedDetails) throws IOException, FeedException {
		XmlReader reader = null;
		try {
			reader = new XmlReader(feedDetails.getFeedPath());
			SyndFeed feed = new SyndFeedInput().build(reader);
			return feed.getEntries().iterator();
		} finally {
			if (reader != null)
				reader.close();
		}
	}
}
