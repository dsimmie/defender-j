/**
 * ArticleParser.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.article;


/**
 * 
 * @author Donal Simmie
 * 
 */
public interface ArticleParser {
	/**
	 * Parse the text of an HTML article.
	 * 
	 * @param htmlArticle
	 * @return
	 */
	String parseHTMLArticle(String htmlArticle) throws ParserException;
}
