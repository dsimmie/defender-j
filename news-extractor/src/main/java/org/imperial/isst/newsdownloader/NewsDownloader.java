/**
 * NewsDownloader.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.BasicBSONObject;
import org.imperial.isst.apps.DefenderApplication;
import org.imperial.isst.database.DBNewsFields;
import org.imperial.isst.database.DBWrapper;
import org.imperial.isst.database.DatabaseException;
import org.imperial.isst.entities.impl.StanfordNER;
import org.imperial.isst.location.impl.PlacemakerLocationFinder;
import org.imperial.isst.location.impl.TwoFishesLocationFinder;
import org.imperial.isst.news.Config;
import org.imperial.isst.news.Feed;
import org.imperial.isst.news.FeedItem;
import org.imperial.isst.news.FeedMetadata;
import org.imperial.isst.newsdownloader.article.ArticleDownloader;
import org.imperial.isst.newsdownloader.article.ArticleParser;
import org.imperial.isst.newsdownloader.article.ParserException;
import org.imperial.isst.newsdownloader.article.ParserType;
import org.imperial.isst.newsdownloader.article.impl.BoilerpipeBodyTextParser;
import org.imperial.isst.newsdownloader.classifier.NewsClassifier;
import org.imperial.isst.newsdownloader.db.DBObjectConverter;
import org.imperial.isst.newsdownloader.domain.NewsSummary;
import org.imperial.isst.newsdownloader.util.Utils;
import org.imperial.isst.utils.KeywordUtils;
import org.imperial.isst.utils.NewsUtils;
import org.jsoup.Jsoup;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.io.FeedException;

/**
 * The main news downloading application. TODO: finish doc comment
 * 
 * @author Donal Simmie
 * 
 */
public class NewsDownloader extends DefenderApplication implements Runnable {

	/**
	 * The feed details.
	 */
	private final Set<Feed> feeds;

	private final Map<String, Feed> feedsMap;
	/**
	 * The feed reading component.
	 */
	private final FeedReader reader;

	/**
	 * This component downloads the articles that the link refers to.
	 */
	private final ArticleDownloader downloader;

	/**
	 * The metadata describing the feed data.
	 */
	private FeedMetadata metadata;

	/**
	 * Converts to DBObject from domain format.
	 */
	private final DBObjectConverter converter;

	/**
	 * The body text article parser.
	 */
	private final ArticleParser parser;

	/**
	 * The source collection.
	 */
	private DBCollection sourceColl;

	/**
	 * The parsed collection.
	 */
	private DBCollection parsedColl;

	/**
	 * The extracted collection.
	 */
	private DBCollection extractedColl;

	/**
	 * The feeds collection.
	 */
	private DBCollection feedsColl;

	/**
	 * The symptoms collection.
	 */
	private DBCollection symptomColl;

	/**
	 * The health article classifier
	 */
	private NewsClassifier classifier;

	private final String twoFishesURI;

	private final String placemakerAppId;

	/**
	 * Initialises a new instance of the {@link NewsDownloader} class.
	 * 
	 * @param configFile
	 * @throws DatabaseException
	 */
	public NewsDownloader(String configFile) throws DatabaseException {
		super(configFile);
		twoFishesURI = configProperties.getProperty("twoFishesURI");
		placemakerAppId = configProperties.getProperty("placemaker.app.id");
		reader = new FeedReader();
		downloader = new ArticleDownloader();
		converter = new DBObjectConverter();
		parser = new BoilerpipeBodyTextParser();
		connectToCollections();
		feedsMap = new HashMap<String, Feed>();
		feeds = NewsUtils.loadFeeds(configProperties.getProperty(Config.FEEDS_FILE));
		for (Feed feed : feeds) {
			feedsMap.put(feed.getName(), feed);
		}
		try {
			classifier = new NewsClassifier(configProperties.getProperty(Config.KNOWLEDGE_FILE));
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			logger.info("Unable to load classifier", e);
		}
	}

	/**
	 * Connect to the database collections.
	 * 
	 * @throws DatabaseException
	 */
	private void connectToCollections() throws DatabaseException {

		try {
			sourceColl = connectToDB(Config.SOURCE_COLL);
			parsedColl = connectToDB(Config.PARSED_COLL);
			extractedColl = connectToDB(Config.EXTRACTED_COLL);
			extractedColl.ensureIndex(new BasicDBObject("id", 1),
					new BasicDBObject("unique", true).append("dropDups", true));
			feedsColl = connectToDB(Config.FEEDS_COLL);
			symptomColl = connectToDB(Config.SYMPTOM_COLL);
		} catch (UnknownHostException e) {
			logger.error("Fatal error, failed to connect to database collections. Exiting application.");
			logger.error("Reason:", e);
			throw new DatabaseException(e);
		}
	}

	/**
	 * Get the VCS string that identifies this version of the code.
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	/*
	 * private String getCodeVersion() throws IOException, InterruptedException { String repo =
	 * configProps.get(Config.VCS_REPO).toString().trim(); logger.debug("Getting commit info from repo: " + repo);
	 * return Utils.getGitHeadCommitHash(repo); }
	 */

	/**
	 * Parse the HTML body text and optionally the description text - if required.
	 * 
	 * @param feed
	 * @param docs
	 */
	private void parseData(List<DBObject> docs) {
		for (DBObject doc : docs) {
			String description = doc.get(DBNewsFields.DESCRIPTION).toString();
			Feed docFeed = feedsMap.get(((BasicBSONObject) doc.get(DBNewsFields.FEED)).get(DBNewsFields.NAME));
			if (docFeed.doesDescriptionContainsHTML()) {
				try {
					doc.put(DBNewsFields.DESCRIPTION, parser.parseHTMLArticle(description));
				} catch (ParserException e) {
					logger.error("Exception using BoilerPipe to get description text falling back to basic parse.");
					doc.put(DBNewsFields.DESCRIPTION, Utils.getHTMLText(Jsoup.parse(description)));
				}
			}

			Object article = doc.get(DBNewsFields.ARTICLE);
			if (article != null) {
				String parsedText;
				String parserType;
				String parserClass;

				String htmlArticle = article.toString();
				try {
					parsedText = parser.parseHTMLArticle(htmlArticle);
					parserType = ParserType.BOILERPIPE.toString();
					parserClass = parser.getClass().getName();
				} catch (ParserException e) {
					logger.error("Exception using BoilerPipe to get story text falling back to basic parse.");
					parsedText = Utils.getHTMLText(Jsoup.parse(htmlArticle));
					parserType = ParserType.JSOUP.toString();
					parserClass = getClass().getName();
				}

				doc.put(DBNewsFields.ARTICLE, parsedText);
				doc.put(DBNewsFields.ARTICLE_PARSER, parserType);
				doc.put(DBNewsFields.PARSER_CLASS, parserClass);
			}
		}
	}

	/**
	 * Read the feed items from the given feed into the feed object.
	 * <p>
	 * This method alters the state of the input object and hence is not pure and may produce side-effects.
	 * 
	 * @param feed
	 * @return
	 * @throws IOException
	 * @throws FeedException
	 * @throws MalformedURLException
	 */
	@SuppressWarnings("rawtypes")
	private void readItems(Feed feed) throws IOException, FeedException, MalformedURLException {
		Iterator feedEntries = reader.read(feed);
		Set<FeedItem> items = new HashSet<FeedItem>();
		while (feedEntries.hasNext()) {
			SyndEntry entry = (SyndEntry) feedEntries.next();
			URL articleURL = new URL(entry.getLink());
			String articleText = downloader.download(articleURL);
			FeedItem item = new FeedItem(entry.getTitle(), articleURL, entry.getDescription().getValue(),
					entry.getPublishedDate(), articleText, metadata, feed);
			items.add(item);
		}
		feed.setItems(items);
	}

	/**
	 * Rerun the extraction - loops through parsed articles and extracts data to the output collection
	 * 
	 */
	public void rerunExtraction() {
		logger.info("Rerunning parsing and extraction for whole period.");
		DataExtractor extractor = null;

		extractor = new DataExtractor(new PlacemakerLocationFinder(placemakerAppId), new TwoFishesLocationFinder(
				twoFishesURI), KeywordUtils.loadKeywords(symptomColl), new StanfordNER(), classifier);

		List<DBObject> docs = new ArrayList<DBObject>();

		Set<Integer> ids = new HashSet<Integer>();

		DBCursor cursor = sourceColl.find();
		while (cursor.hasNext()) {
			docs.add(cursor.next());
			Integer id = (Integer) docs.get(0).get(DBNewsFields.ID);
			if (!ids.contains(id)) {
				ids.add(id);
				try {
					parseData(docs);

					storeParsedData(docs);
					logger.info("Parsing complete. Extracting.");
					Set<NewsSummary> summaries = extractor.getSummaries(docs);
					logger.info("Data summarisation complete. Storing.");
					storeExtractedData(summaries);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				logger.debug("Skipping id: " + id);
			}

			docs.clear();
		}

		logger.info("Stored extracted data for all articles.");
	}

	/**
	 * Scheduled task to download news from a variety of sources specified in the feeds file.
	 * <p>
	 * The task is run according to the cron job specified in the Spring 3.0 annotation of this method. According to the
	 * Spring spec this method must have no arguments and contain no parameters.
	 * 
	 * @return
	 * @throws Exception
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public void run() {
		DataExtractor extractor = null;

		extractor = new DataExtractor(new PlacemakerLocationFinder(placemakerAppId), new TwoFishesLocationFinder(
				twoFishesURI), KeywordUtils.loadKeywords(symptomColl), new StanfordNER(), classifier);

		try {
			try {
				updateMetaData();
			} catch (InterruptedException ie) {
				logger.warn("Interrupted while retrieving feed metadata", ie);
			} catch (IOException ioe) {
				logger.warn("I/O exception while retrieving feed metadata", ioe);
			}

			/*
			 * Iterate through the available feeds, read them into memory and store them into 4 database collections: 1.
			 * The feeds collection: contains data about the feed such as the access data and the links it contained at
			 * that time. 2. The source collection: stores primary data and metadata about the downloaded feed, it is
			 * stored as with no parsing for provenance purposes. 3. The parsed collection: stores data once parsing
			 * (e.g. HTML) has been performed. 4. The extracted collection: stores semantically extracted data such as
			 * location and symptoms mentioned in the text.
			 */
			for (Feed feed : feeds) {
				logger.info("Commencing data acquisition for feed: " + feed.getName());

				try {

					readItems(feed);

					storeFeed(feed);

					if (feed.getItems().isEmpty()) {
						logger.info("Skipping feed with no articles: " + feed.getName());
						continue;
					}

					List<DBObject> docs = storeSourceData(feed);

					logger.debug("Stored source data for feed: " + feed.getName());

					parseData(docs);

					storeParsedData(docs);

					logger.debug("Stored parsed data for feed: " + feed.getName());

					Set<NewsSummary> summaries = extractor.getSummaries(docs);

					storeExtractedData(summaries);

					logger.info("Stored extracted data for feed: " + feed.getName());
				} catch (Exception e) {
					logger.info("Exception reading feed: " + feed.toString(), e);
				}
			}

		} catch (Exception ie) {
			logger.error("Error getting data", ie);
			logger.error(ie.getMessage());
			logger.error("Is authentication error: " + ie.getMessage().contains("\"errmsg\" : \"auth fails\""));
		}
	}

	/**
	 * Store the data after semantic extraction has been performed. This data has locations, entities, symptoms and
	 * diseases and other meaningful semantic data in a format which is easier to take advantage of in later analysis
	 * and learning activities.
	 * 
	 * @param summaries
	 * @throws UnknownHostException
	 */
	private void storeExtractedData(Set<NewsSummary> summaries) throws UnknownHostException {
		checkArgument(summaries != null && summaries.size() > 0);
		List<DBObject> extractedDocs = converter.convertToExtractedDBObjects(summaries);

		for (DBObject doc : extractedDocs) {
			try {
				DBWrapper.insert(extractedColl, doc);
			} catch (Exception ie) {
				logger.error("Error writing to database", ie);
				logger.error(ie.getMessage());
			}
		}

	}

	/**
	 * Store this feed in the feeds collection as provenance metadata of potential future benefit.
	 * 
	 * @param feed
	 * @throws UnknownHostException
	 */
	private void storeFeed(Feed feed) throws UnknownHostException {
		checkNotNull(feed);
		checkNotNull(feed.getFeedPath());
		checkNotNull(feed.getAccessDate());
		checkNotNull(feed.getItems());

		DBObject feedDoc = new BasicDBObject();
		feedDoc.put(DBNewsFields.URL, feed.getFeedPath().toString());
		feedDoc.put(DBNewsFields.ACCESS_DATE, feed.getAccessDate());

		List<DBObject> feedItemSummaries = new LinkedList<DBObject>();
		for (FeedItem item : feed.getItems()) {
			DBObject itemSummaryContents = new BasicDBObject(DBNewsFields.TITLE, item.getTitle());
			itemSummaryContents.put(DBNewsFields.LINK, item.getLink().toString());
			DBObject feedItemSummary = new BasicDBObject(DBNewsFields.ITEM_SUMMARY, itemSummaryContents);
			feedItemSummaries.add(feedItemSummary);
		}
		feedDoc.put(DBNewsFields.ITEM_SUMMARIES, feedItemSummaries);
		DBWrapper.insert(feedsColl, feedDoc);
	}

	/**
	 * Store the data that has been parsed, hence we can see if the parsing created any errors when analysing extracted
	 * data at a future point.
	 * 
	 * @param items
	 * @throws UnknownHostException
	 */
	private void storeParsedData(List<DBObject> parsedDocs) throws UnknownHostException {
		checkArgument(parsedDocs != null && !parsedDocs.isEmpty(), "Feed must contain some items");
		DBWrapper.insert(parsedColl, parsedDocs);
	}

	/**
	 * Store the source data in the database.
	 * 
	 * @param items
	 * @throws UnknownHostException
	 */
	private List<DBObject> storeSourceData(Feed feed) throws UnknownHostException {
		checkNotNull(feed);
		Set<FeedItem> items = feed.getItems();
		checkArgument(feed.getItems() != null && !feed.getItems().isEmpty(), "Feed must contain some items");

		List<DBObject> sourceDocs = converter.convertToSourceDBObjects(items, metadata);
		DBWrapper.insert(sourceColl, sourceDocs);
		return sourceDocs;
	}

	/**
	 * Update the feed metadata for the current execution run.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private void updateMetaData() throws IOException, InterruptedException {
		// TODO: implement version storage.
		metadata = new FeedMetadata(getClass(), "BETA", new Date());
	}
}
