/**
 * ParserException.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.article;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class ParserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1545063022939850919L;

	/**
	 * Initialises a new instance of the {@link ParserException} class.
	 * 
	 */
	public ParserException() {
	}

	/**
	 * Initialises a new instance of the {@link ParserException} class.
	 * 
	 * @param message
	 */
	public ParserException(String message) {
		super(message);
	}

	/**
	 * Initialises a new instance of the {@link ParserException} class.
	 * 
	 * @param message
	 * @param cause
	 */
	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Initialises a new instance of the {@link ParserException} class.
	 * 
	 * @param cause
	 */
	public ParserException(Throwable cause) {
		super(cause);
	}

}
