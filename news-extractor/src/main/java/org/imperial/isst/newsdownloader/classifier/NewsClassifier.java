package org.imperial.isst.newsdownloader.classifier;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Map;

import com.datumbox.opensource.classifiers.NaiveBayes;
import com.datumbox.opensource.dataobjects.NaiveBayesKnowledgeBase;

public class NewsClassifier {

	private final double CHI_SQUARE_VALUE = 1;
	private NaiveBayes nb;
	
	public NewsClassifier(Map<String, String[]> trainingExamples) {
		
        nb = new NaiveBayes();
        nb.setChisquareCriticalValue(CHI_SQUARE_VALUE); //0.01 pvalue is 6.63
        nb.train(trainingExamples);
        
        //get trained classifier knowledgeBase
        NaiveBayesKnowledgeBase knowledgeBase = nb.getKnowledgeBase();
        
        nb = null;
        trainingExamples = null;
        
        
        //Use classifier
        nb = new NaiveBayes(knowledgeBase);
	}
	
	public NewsClassifier(String serializedKnowledge) throws IOException, ClassNotFoundException  {
		
		InputStream streamIn = NewsClassifier.class.getResourceAsStream(serializedKnowledge);
        ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
        NaiveBayesKnowledgeBase kb = (NaiveBayesKnowledgeBase) objectinputstream.readObject();
        nb = new NaiveBayes(kb);
	}
	
	public String classify(String example) {
		return nb.predict(example);
	}
	
}
