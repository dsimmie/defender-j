/**
 * HistoricalApplication.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.main;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.imperial.isst.location.impl.PlacemakerLocationFinder;
import org.imperial.isst.newsdownloader.article.ArticleDownloader;
import org.imperial.isst.newsdownloader.article.impl.BoilerpipeBodyTextParser;
import org.imperial.isst.newsdownloader.historical.BBCHistoricalNewsExtractor;
import org.imperial.isst.newsdownloader.historical.HistoricalNewsExtractor;
import org.imperial.isst.utils.KeywordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Donal Simmie
 * 
 */
public class HistoricalApplication {

	private static final String KEYWORDS_FILE = "keywordsetv2.csv";

	private static HistoricalNewsExtractor extractor;

	private static Logger logger;

	private static final String testAppId = "A1b3nzDV34FQHUkW7BbXT0MbJG8jN.8r4HDEVeAO3X8Ux.5pNEwMRwfVWYrqP6rJBcux";

	/**
	 * Initialises a new instance of the {@link HistoricalApplication} class.
	 * 
	 */
	static {
		try {
			logger = LoggerFactory.getLogger(HistoricalApplication.class);
			extractor = new BBCHistoricalNewsExtractor(new ArticleDownloader(), new BoilerpipeBodyTextParser(),
					new PlacemakerLocationFinder(testAppId));

		} catch (IOException ie) {
			logger.error("I/O error creating historical news extractor", ie);
		}
	}

	static String test = "<li class=\"linktrack-item lead\">\n <div class=\"itemText\">\n  <div>\n   <a class=\"title linktrack-title\" href=\"http://www.bbc.co.uk/news/uk-scotland-tayside-central-21523077\">Angus hospital closes ward due to suspected norovirus outbreak</a>\n  </div>\n  <div class=\"details\">\n   <span class=\"newsSection\">Tayside and Central Scotland / </span>\n   <span class=\"newsDateTime 2013-02-20T14:52:03Z\">20 February 2013</span>\n   <p>The health board said they believed the outbreak was as a result of <span class=\"highlight\">norovirus</span>, also known as the winter vomiting bug. A suspected <span class=\"highlight\">norovirus</span> outbreak\u2026</p>\n  </div>\n </div></li>";

	/**
	 * The main function; the application entry point.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 6) {
			logger.error("Must provide time range for historical news collection.");
			logger.error("Arguments: startDay (dd), startMonth (mm), startYear (yyyy), endDay (dd), endMonth (mm), endYear (yyyy)");
		} else {
			int startDay = Integer.parseInt(args[0]);
			int startMonth = Integer.parseInt(args[1]);
			int startYear = Integer.parseInt(args[2]);
			int endDay = Integer.parseInt(args[3]);
			int endMonth = Integer.parseInt(args[4]);
			int endYear = Integer.parseInt(args[5]);
			try {
				Set<String> keywords = new HashSet<String>(KeywordUtils.getAllKeywords(KeywordUtils
						.loadKeywords(KEYWORDS_FILE)));

				for (String keyword : keywords) {
					extractor.extract(keyword, startDay, startMonth, startYear, endDay, endMonth, endYear);
				}
			} catch (Exception e) {
				logger.error("Exception running historical news extraction", e);
			}
		}
	}
}
