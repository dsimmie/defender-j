/**
 * DBObjectConverter.java
 *
 * Copyright of Donal Simmie 2014.
 */
package org.imperial.isst.newsdownloader.db;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.imperial.isst.database.DBNewsFields;
import org.imperial.isst.database.DBQueries;
import org.imperial.isst.location.Location;
import org.imperial.isst.news.Feed;
import org.imperial.isst.news.FeedItem;
import org.imperial.isst.news.FeedMetadata;
import org.imperial.isst.newsdownloader.domain.NewsSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Convert from domain objects to database format objects.
 * 
 * @author Donal Simmie
 * 
 */
public class DBObjectConverter {

	/**
	 * The class logger.
	 */
	private final Logger logger;

	/**
	 * Initialises a new instance of the {@link DBObjectConverter} class.
	 * 
	 */
	public DBObjectConverter() {
		logger = LoggerFactory.getLogger(getClass());
	}



	/**
	 * Convert from the input set of news summaries to database objects.
	 * 
	 * @param summaries
	 * @return database objects ready for insertion.
	 */
	public List<DBObject> convertToExtractedDBObjects(Set<NewsSummary> summaries) {
		List<DBObject> docs = new LinkedList<DBObject>();
		for (NewsSummary summary : summaries) {
			DBObject doc = new BasicDBObject();
			doc.put(DBNewsFields.ID, summary.getId());
			doc.put(DBNewsFields.TITLE, summary.getTitle());
			doc.put(DBNewsFields.DESCRIPTION, summary.getDescription());
			doc.put(DBNewsFields.ARTICLE, summary.getText());
			doc.put(DBNewsFields.LINK, summary.getLink());
			doc.put(DBNewsFields.ENTRY_TIME, new Date());
			doc.put(DBNewsFields.PUB_DATE, summary.getPubDate());
			doc.put(DBNewsFields.FEED, summary.getFeed());
			

			List<DBObject> locationDocs = new LinkedList<DBObject>();
			for (Location location : summary.getLocations()) {
				locationDocs.add(location.toDBObject());
			}

			doc.put(DBNewsFields.LOCATIONS, locationDocs);
			doc.put(DBNewsFields.KEYWORDS, summary.getKeywords());
			doc.put(DBNewsFields.SYMPTOMS, summary.getSymptoms());
			doc.put(DBNewsFields.ENTITIES, DBQueries.convertEntities(summary.getEntities()));
			doc.put(DBNewsFields.CLASSIFICATION, summary.getClassification());

			docs.add(doc);
			logger.debug("Converted summary to extracted doc: " + doc);
		}
		return docs;
	}

	/**
	 * Convert from a collection of feed items to BSON objects for persistence in MongoDB.
	 * <p>
	 * The article text is an optional field which may not be included if there a problem downloading it.
	 * 
	 * @param items
	 * @return
	 */
	public List<DBObject> convertToSourceDBObjects(Set<FeedItem> items, FeedMetadata metadata) {
		List<DBObject> docs = new LinkedList<DBObject>();
		for (FeedItem item : items) {
			DBObject doc = new BasicDBObject();
			doc.put(DBNewsFields.ID, item.getID());
			doc.put(DBNewsFields.TITLE, item.getTitle());
			doc.put(DBNewsFields.LINK, item.getLink().toString());
			doc.put(DBNewsFields.DESCRIPTION, item.getDescription());
			doc.put(DBNewsFields.PUB_DATE, item.getPubDate());

			if (item.getArticleText() != null)
				doc.put(DBNewsFields.ARTICLE, item.getArticleText());

			Feed feed = item.getFeed();
			doc.put(DBNewsFields.FEED,
					new BasicDBObject(ImmutableMap.of(DBNewsFields.NAME, feed.getName(), DBNewsFields.URL, feed.getFeedPath()
							.toString())));

			DBObject metadataDocument = new BasicDBObject(DBNewsFields.DOWNLOADER_CLASS, metadata.getDownloader().getName());
			/* Code version field may not exist if there is a problem with the external process. */
			if (metadata.getCodeVersion() != null) {
				metadataDocument.put(DBNewsFields.CODE_VERSION, metadata.getCodeVersion());
			}
			metadataDocument.put(DBNewsFields.METADATA, metadata.getEntryTime());
			doc.put("metadata", metadataDocument);

			docs.add(doc);
			logger.debug("Converted feed item to source doc: " + doc);
		}
		return docs;
	}
}
